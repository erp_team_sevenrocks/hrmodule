package com.seekex.hrmodule.criticalevent;

import android.os.Bundle;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import com.seekex.hrmodule.Preferences;
import com.seekex.hrmodule.R;
import com.seekex.hrmodule.grievance.GrievanceMenuFragment;
import com.seekx.utils.DialogUtils;
import com.seekx.utils.ExtraUtils;
import com.seekx.webService.ApiImp;

public class EventActivity extends AppCompatActivity {
    public int uid;

    public Preferences pref;
    public DialogUtils dialogUtil;
    public ApiImp apiImp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.framelayout_container);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        init();
    }



    private void init(){
        pref=new Preferences(this);
        dialogUtil=new DialogUtils(this);
        apiImp=new ApiImp(this);
        ExtraUtils.changeFragment(getSupportFragmentManager(), EventMenuFragment.getInstance(),true);
    }

    @Override
    public void  onBackPressed(){
        //  FragmentManager supportFragmentManager=getSupportFragmentManager();
        //  Fragment f=supportFragmentManager.findFragmentById(R.id.framcontainer);

      /*  if(f instanceof InventoryFor)
            ExtraUtils.changeFragment(getSupportFragmentManager(), InventoryMenuFragment.getInstance(),true);
        else*/
//            ExtraUtils.handleFragment(getSupportFragmentManager(),this);
        finish();

    }


}