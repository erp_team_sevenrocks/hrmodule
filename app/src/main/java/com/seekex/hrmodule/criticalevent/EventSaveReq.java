package com.seekex.hrmodule.criticalevent;

import com.seekex.hrmodule.grievance.GreivanceSaveReq2;
import com.seekx.webService.models.RequestBase;

public class EventSaveReq extends RequestBase {

    private EventSaveReq2 data;

    public EventSaveReq2 getData() {
        return data;
    }

    public void setData(EventSaveReq2 data) {
        this.data = data;
    }



}
