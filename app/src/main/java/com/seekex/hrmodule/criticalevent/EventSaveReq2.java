package com.seekex.hrmodule.criticalevent;

import com.seekex.hrmodule.grievance.GreivanceSaveReq2;
import com.seekex.hrmodule.markrewarks.RewardUserSaveReq;
import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class EventSaveReq2 extends RequestBase {

    private String name;
    private String remarks;
    private String user_id;
    private String audio;
    private ArrayList<RewardUserSaveReq> parameters;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public ArrayList<RewardUserSaveReq> getParameters() {
        return parameters;
    }

    public void setParameters(ArrayList<RewardUserSaveReq> parameters) {
        this.parameters = parameters;
    }
}
