package com.sevenrocks.taskapp.appModules.grievance

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.os.*
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.ButterKnife
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException
import com.google.gson.Gson
import com.rizlee.rangeseekbar.RangeSeekBar
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.adapter.EventParamAdapter
import com.seekex.hrmodule.adapter.ParticipantAdapter
import com.seekex.hrmodule.adapter.PickedlistAdapter
import com.seekex.hrmodule.criticalevent.EventActivity
import com.seekex.hrmodule.criticalevent.EventSaveReq
import com.seekex.hrmodule.criticalevent.EventSaveReq2
import com.seekex.hrmodule.criticalevent.ParamlistResponse
import com.seekex.hrmodule.databinding.ImageItemBinding
import com.seekex.hrmodule.grievance.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.BaseResponse
import com.seekx.webService.models.RequestBase
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import com.sevenrocks.taskapp.appModules.markrewarks.AddRewards
import com.sevenrocks.taskapp.appModules.meetings.MarkerListAdapter
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.dialog_addmarker.*
import kotlinx.android.synthetic.main.grievance_reg.*
import kotlinx.android.synthetic.main.grievance_reg.bt_save
import kotlinx.android.synthetic.main.grievance_reg.iv_action
import kotlinx.android.synthetic.main.grievance_reg.ll_attach
import kotlinx.android.synthetic.main.grievance_reg.tvTimers
import kotlinx.android.synthetic.main.grievance_reg.view.*
import kotlinx.android.synthetic.main.grievance_reg.view.bt_reset
import kotlinx.android.synthetic.main.grievance_reg.view.iv_action
import kotlinx.android.synthetic.main.grievance_reg.view.submit
import kotlinx.android.synthetic.main.grievance_reg.view.txtheader
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.play_audio.*
import kotlinx.android.synthetic.main.saveevent.*
import kotlinx.android.synthetic.main.saveevent.view.*
import org.florescu.android.rangeseekbar.RangeSeekBar.OnRangeSeekBarChangeListener
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList


/**
 */
class SaveEvent : Fragment() {


    private var toduration: String = ""
    private var mediaRecordingId: String? = ""

    private var recordingPath: String? = ""
    public var eventActivity: EventActivity? = null
    private lateinit var rootView: View
    var markerList = ArrayList<MarkerDataDTO>()
    var player: MediaPlayer? = null

    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var paramlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var imagePathList: ArrayList<ImagesGRDTO> = java.util.ArrayList()
    private lateinit var adapterRewarduser: SpinnerAdapter_Users
    private var permissionCallBack: PermissionCallBack? = null
    var gtypelist: ArrayList<DropdownModel> = java.util.ArrayList()
    var userid: String = "0"
    var audioFilename: String = ""
    lateinit var apiImp: ApiImp
    var mediaRecorder: MediaRecorder? = null
    var countDownTimer: CountDownTimer? = null
    var isRecorded = 0
    private var count: Long? = null
    private var timer = ""
    private var file_name = ""

    private var call_Counter = 0
    var mediaPlayer = MediaPlayer()
    var rangeMediaPlayer = MediaPlayer()
    private lateinit var adapter: EventParamAdapter

    var wasPlaying = false
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.saveevent, BasicActi, false)
        ButterKnife.bind(this, rootView)
        eventActivity = activity as EventActivity?
        rootView.txtheader.text = "Add Event"
//        rootView.rangeseekbar.listenerRealTime = this
        setlisteners()
        setAdapters()
        getUserList()
        getparamlist()



        return rootView
    }

    private fun setlisteners() {

        rootView.edteventuser.setOnClickListener {
            DataUtils.openSearchDialogeuser(
                activity!!,
                userlist,
                "Select User",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edteventuser.setText(ss.name)
                        userid = ss.id


                    }

                })
        }



        rootView.btn_addparam.setOnClickListener {
          /*  Log.e("TAG", "setlisteners: "+paramlist.size)
            val dialog =
                Dialog(activity!!, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
            dialog.setContentView(R.layout.dialog) //layout for dialog
            dialog.setTitle("Add Param Type")
            dialog.setCancelable(false) //none-dismiss when touching outside Dialog
            // set the custom dialog components - texts and image
            val recyclerView = dialog.findViewById<View>(R.id.rec_participant) as RecyclerView
            val btnAdd = dialog.findViewById<View>(R.id.btn_ok)
            val btnCancel = dialog.findViewById<View>(R.id.btn_cancel)
            val edt_search = dialog.findViewById<EditText>(R.id.edt_search)
            //set spinner adapter
            adapter = EventParamAdapter(paramlist, activity)
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.adapter = adapter

            edt_search.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    // TODO Auto-generated method stub
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                    // TODO Auto-generated method stub
                }

                override fun afterTextChanged(s: Editable) {

                    // filter your list from your input
                    filterSearch(s.toString())
                    //you can use runnable postDelayed like 500 ms to delay search text
                }
            })

            btnAdd.setOnClickListener {
                Log.e("TAG", "setlisteners: "+paramlist.size )

                for (i in paramlist.indices) {
                    Log.e("TAG", "onClick: " + paramlist[i].checked)
                    if (paramlist[i].checked == true) {
                        if (!namestoShow.contains(paramlist[i])) {
                            namestoShow.add(paramlist[i])
                        }
                    } else {
                        if (namestoShow.contains(paramlist[i])) {
                            namestoShow.remove(paramlist[i])
                        }
                    }
                }
                Log.e("TAG", "onClick: " + namestoShow.size)
                showDatainPickList(namestoShow)
                dialog.dismiss()
            }
            btnCancel.setOnClickListener {
                for (i in paramlist.indices) {
                    Log.e("TAG", "onClick: " + paramlist[i].checked)
                    if (namestoShow.contains(paramlist[i])) {
                        paramlist[i].checked = true
                    } else {
                        paramlist[i].checked = false
                    }
                }
                Log.e("TAG", "onClick: " + namestoShow.size)
                showDatainPickList(namestoShow)
                dialog.dismiss()
            }
            dialog.show()*/
            DataUtils.openSearchDialogeMultiSelect(
                activity!!,
                mulUserlist,
                paramlist,
                "Select Parameter",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        val list: ArrayList<String> = ArrayList()
                       namestoShow.clear()
                        rootView.txtparamselectedevent.setText("")
                        var ss = any as ArrayList<DropdownModel>
                        Log.e("TAG", "onSelected: " + ss.size)

                        for (dd in ss) {
                            var aa = ParicipantModel("")
                            aa.id = dd.id
                            list.add(dd.name)
                          namestoShow.add(aa)
                        }
                        if (ss.size > 1) {
                            rootView.txtparamselectedevent.setText(TextUtils.join(", ", list))
                        } else {
                            rootView.txtparamselectedevent.setText(TextUtils.join(", ", list))

                        }

//                        userId = ss.id

                    }

                })

        }

        rootView.submit.setOnClickListener {
            call_Counter = 0
            if (isRecorded == 1) {
                bt_save.setVisibility(View.INVISIBLE)
                stopMedia()
                playBeep()
                isRecorded = 2
                countDownTimer!!.cancel()
                timer = tvTimers.getText().toString()
                iv_action.setImageResource(android.R.drawable.ic_media_play)
            }
            if (edt_eventname.text.isEmpty()) {
                Toast.makeText(activity, "Enter Name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edt_eventremark.text.isEmpty()) {
                Toast.makeText(activity, "Enter Remark", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if ( rootView.txtparamselectedevent.text.equals("")) {
                Toast.makeText(eventActivity, "Add param type !!", Toast.LENGTH_SHORT)
                    .show()
                return@setOnClickListener
            }
            if (isRecorded==2) {
                saveaudiofile()
            } else {
                saveGrievance()
            }

        }
        rootView.iv_action.setOnClickListener(View.OnClickListener {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    if (isRecorded == 0) {
                        isRecorded = 1
                        playBeep()
                        startRecording()
                        iv_action.setImageResource(android.R.drawable.ic_media_pause)
                    } else if (isRecorded == 1) {
                        bt_save.setVisibility(View.INVISIBLE)
                        stopMedia()
                        playBeep()
                        isRecorded = 2
                        countDownTimer!!.cancel()
                        timer = tvTimers.getText().toString()
                        iv_action.setImageResource(android.R.drawable.ic_media_play)

//                iv_action.setImageDrawable(activity!!.resources.getDrawable(R.drawable.aar_ic_play))
                    } else if (isRecorded == 2) {
//                FileUtils.openDocument(file_name, activity)
                    }
                }
            })


        })
        rootView.bt_reset.setOnClickListener(View.OnClickListener {
            try {
                isRecorded = 0
                stopMedia()
                playBeep()
                countDownTimer!!.cancel()
                tvTimers.setText("")
                bt_save.setVisibility(View.GONE)
                iv_action.setImageDrawable(activity!!.resources.getDrawable(R.drawable.audiorec))
            } catch (e: java.lang.Exception) {
            }
        })


    }

//    fun filterSearch(text: String?) {
//        val temp = ArrayList<ParicipantModel>()
//        for (d in paramlist) {
//
//            if (d.name.toLowerCase().contains(text!!.toLowerCase())) {
//                temp.add(d)
//            }
//        }
//        //update recyclerview
//        adapter.updateList(temp)
//    }
var mulUserlist: ArrayList<DropdownModel> = java.util.ArrayList()

    private fun showDatainPickList(list: ArrayList<ParicipantModel>) {
        val adapter = PickedlistAdapter(list, activity)
        rec_paramlist!!.layoutManager = LinearLayoutManager(activity)
        rec_paramlist!!.adapter = adapter
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    private fun saveaudiofile() {

        eventActivity!!.apiImp.saveAudio(file_name, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as uploadaudiores
                    audioFilename = ss.data.path + "" + ss.data.filename

                    saveGrievance()


                }

            }

        })


    }


    private fun stopMedia() {
        try {
            mediaRecorder!!.stop()
        } catch (e: java.lang.Exception) {
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getparamlist() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = eventActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.EVENTPARAMLIST


        eventActivity!!.apiImp.getparamlist(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as ParamlistResponse
//                    var aa = DropdownModel()
//                    aa.id = "0"
//                    aa.name = "Select User"
//                    userlist.add(aa)

                    var list=ArrayList<DropdownModel>()
                    list.addAll(ss.data)
                    for (i in list.indices) {
                        paramlist.add(
                            DropdownModel(
                                list.get(i).id,
                                list.get(i).name,
                                false
                            )
                        )
                    }
//                    adapterRewarduser.notifyDataSetChanged()

                }


            }

        })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUserList() {

//        val getMasterRequest = RequestBase()

        val getMasterRequest = GetSalaryReq()
        val get2 = GetSalaryReq2()
        getMasterRequest.conditions= get2

        getMasterRequest.token = eventActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GUSERS


        eventActivity!!.apiImp.getkrausers(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
//                    var aa = DropdownModel()
//                    aa.id = "0"
//                    aa.name = "Select User"
//                    userlist.add(aa)

                    userlist.addAll(ss.data)
//                    adapterRewarduser.notifyDataSetChanged()

                }


            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveGrievance() {
        Log.e("TAG", "saveGrievance: " + Gson().toJson(imagePathList))
        val greq = EventSaveReq()
        val greq2 = EventSaveReq2()

        var ss = ArrayList<RewardUserSaveReq>()
        for (i in namestoShow.indices) {
            var aa = RewardUserSaveReq()
            aa.id = namestoShow.get(i).id
            ss.add(aa)
        }

        greq.token = eventActivity?.pref?.get(AppConstants.token)
        greq.service_name = ApiUtils.save_critical_event

        greq2.name = edt_eventname.text.toString()
        greq2.remarks = edt_eventremark.text.toString()
        greq2.audio = audioFilename
        greq2.user_id = userid
        greq2.parameters =ss

        greq.data = greq2
        eventActivity!!.apiImp.saveeventdata(greq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    Toast.makeText(activity, "Record Saved", Toast.LENGTH_SHORT).show()
                    eventActivity!!.finish()
                }


            }

        })
    }

    private fun getTimeValues(minValue: Float): String {
        val day = TimeUnit.SECONDS.toDays(minValue.toLong())
        val hours: Long = TimeUnit.SECONDS.toHours(minValue.toLong()) - day * 24
        val minute: Long =
            TimeUnit.SECONDS.toMinutes(minValue.toLong()) - TimeUnit.SECONDS.toHours(minValue.toLong()) * 60
        val second: Long =
            TimeUnit.SECONDS.toSeconds(minValue.toLong()) - TimeUnit.SECONDS.toMinutes(minValue.toLong()) * 60
        var data = hours.toString() + ":" + minute + ":" + second
        Log.e("TAG", "getTimeValues: " + data)

        return data
    }


    private fun startRecording() {
        mediaRecorderReady()
        try {
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startCountdownTimer()
    }

    fun convertSecondToHHMMString(count: Long): String? {
        val df = SimpleDateFormat("mm:ss")
        df.timeZone = TimeZone.getTimeZone("UTC")
        return df.format(Date(count * 1000L))
    }

    private fun startCountdownTimer() {
        count = 0L
        countDownTimer = object : CountDownTimer(10800000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // Used for formatting digit to be in 2 digits only
                count = count!! + 1
                tvTimers.setText(convertSecondToHHMMString(count!!))
            }

            // When the task is over it will print 00:00:00 there
            override fun onFinish() {
                tvTimers.setText("00:00:00")
            }
        }.start()
    }

    private fun mediaRecorderReady() {
        val currentTime = System.currentTimeMillis()
        val extBaseDir = activity?.externalCacheDir
        val file = File(extBaseDir!!.absolutePath + "/SeekMeeting/" + "")
        if (!file.exists()) {
            if (!file.mkdirs()) {
            }
        }
        file_name = "$file/myRecording.mp3"
        Log.e("TAG", "mediaRecorderReady: $file_name")
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        mediaRecorder!!.setOutputFile(file_name)
    }

    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

    private fun setAdapters() {



    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {

            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }


    companion object {
        @JvmStatic
        val namestoShow = ArrayList<ParicipantModel>()

        @JvmStatic
        var alreadySelectedNames = ArrayList<ParicipantModel>()


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = SaveEvent()
    }////


}