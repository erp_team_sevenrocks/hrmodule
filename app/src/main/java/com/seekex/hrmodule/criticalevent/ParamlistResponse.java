
package com.seekex.hrmodule.criticalevent;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.DropdownModel;
import com.seekex.hrmodule.markrewarks.ParicipantModel;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class ParamlistResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<DropdownModel> data;

    public ArrayList<DropdownModel> getData() {
        return data;
    }

    public void setData(ArrayList<DropdownModel> data) {
        this.data = data;
    }


}

