
package com.seekex.hrmodule.jobposting;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.DropdownModel;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class FilterListDTO extends BaseResponse {

    @SerializedName("designation")
    private ArrayList<DropdownModel> designation;
    @SerializedName("hierarchy_level_list")
    private ArrayList<DropdownModel> hierarchy_level_list;
    @SerializedName("qualification_list")
    private ArrayList<DropdownModel> qualification_list;
    @SerializedName("department_list")
    private ArrayList<DropdownModel> department_list;
    @SerializedName("location_list")
    private ArrayList<DropdownModel> location_list;
    @SerializedName("work_from_home")
    private ArrayList<DropdownModel> work_from_home;

    public ArrayList<DropdownModel> getDesignation() {
        return designation;
    }

    public void setDesignation(ArrayList<DropdownModel> designation) {
        this.designation = designation;
    }

    public ArrayList<DropdownModel> getHierarchy_level_list() {
        return hierarchy_level_list;
    }

    public void setHierarchy_level_list(ArrayList<DropdownModel> hierarchy_level_list) {
        this.hierarchy_level_list = hierarchy_level_list;
    }

    public ArrayList<DropdownModel> getQualification_list() {
        return qualification_list;
    }

    public void setQualification_list(ArrayList<DropdownModel> qualification_list) {
        this.qualification_list = qualification_list;
    }

    public ArrayList<DropdownModel> getDepartment_list() {
        return department_list;
    }

    public void setDepartment_list(ArrayList<DropdownModel> department_list) {
        this.department_list = department_list;
    }

    public ArrayList<DropdownModel> getLocation_list() {
        return location_list;
    }

    public void setLocation_list(ArrayList<DropdownModel> location_list) {
        this.location_list = location_list;
    }

    public ArrayList<DropdownModel> getWork_from_home() {
        return work_from_home;
    }

    public void setWork_from_home(ArrayList<DropdownModel> work_from_home) {
        this.work_from_home = work_from_home;
    }
}
