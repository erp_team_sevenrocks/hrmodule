package com.seekex.hrmodule.jobposting;


import com.seekx.webService.models.RequestBase;

public class PostingListReq extends RequestBase {

    private ConditionDTO conditions  ;
    private String user_id  ;
    private Integer page  ;

    public ConditionDTO getConditions() {
        return conditions;
    }

    public void setConditions(ConditionDTO conditions) {
        this.conditions = conditions;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
