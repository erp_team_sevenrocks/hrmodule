package com.seekex.hrmodule.jobposting

import android.app.DatePickerDialog
import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekex.hrmodule.R
import com.seekex.hrmodule.adapter.ApplyJobAdapter
import com.seekex.hrmodule.markrewarks.DropdownModel
import com.seekex.hrmodule.markrewarks.SpinnerAdapter_Users

import com.seekx.utils.DialogUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import com.sevenrocks.taskapp.appModules.markrewarks.PostingListAdapter
import kotlinx.android.synthetic.main.apply_dialog.*
import kotlinx.android.synthetic.main.filter_view.*
import kotlinx.android.synthetic.main.home_adapter.*
import kotlinx.android.synthetic.main.jobposting.*
import kotlinx.android.synthetic.main.kra_assessment.view.*
import java.util.*


class JobPostingActivity : AppCompatActivity() {
    private var ss: String = ""
    lateinit var pref: Preferences
    lateinit var dialogUtils: DialogUtils
    var apiImp: ApiImp? = null

    private lateinit var myAdapter: PostingListAdapter
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<PostingDTO> = java.util.ArrayList()

    private lateinit var adapterdesignation: Filter_designation
    private lateinit var adapterhierarchy_level_list: Filter_hierarchy_level_list
    private lateinit var adaptequalification_list: Filter_qualification_list
    private lateinit var adaptedepartment_list: Filter_department_list
    private lateinit var adapterlocation_list: Filter_location_list
    private lateinit var adapterwork_from_home: Filter_work_from_home
    var designationid: String = ""
    var levelid: String = ""
    var departmentid: String = ""
    var qualificationid: String = ""
    var locationid: String = ""
    var workfromhomeid: String = ""
    var from_date: String = ""
    var todate: String = ""
    var designationlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var levelelist: ArrayList<DropdownModel> = java.util.ArrayList()
    var departmentlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var qualificationlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var locationlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var workfromhomelist: ArrayList<DropdownModel> = java.util.ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.jobposting)
        pref = Preferences(this)
        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)
        setAdapter()
        clickListeners()
        getList()
        adapterdesignation = Filter_designation(this, designationlist)
        adaptedepartment_list = Filter_department_list(this, departmentlist)
        adaptequalification_list = Filter_qualification_list(this, qualificationlist)
        adapterhierarchy_level_list = Filter_hierarchy_level_list(this, levelelist)
        adapterlocation_list = Filter_location_list(this, locationlist)
        adapterwork_from_home = Filter_work_from_home(this, workfromhomelist)

        getFilterDropdownList()

    }

    private val cal = Calendar.getInstance()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun initializePopUp() {


        var dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.filter_view)
        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.bt_reset.setOnClickListener {

            dialog.edt_fromdate.setText("")
            dialog.edt_todate.setText("")

            dialog.spin_wfh.setSelection(0)
            dialog.spin_Qualification.setSelection(0)
            dialog.spin_level.setSelection(0)
            dialog.spin_department.setSelection(0)
            dialog.spin_designation.setSelection(0)
            dialog.spin_location.setSelection(0)

            departmentid = ""
            designationid = ""
            workfromhomeid = ""
            qualificationid = ""
            levelid = ""
            from_date = ""
            todate = ""
            locationid = ""
            page = 1
            mydataList.clear()
            myAdapter.notifyDataSetChanged()
            getList()


        }
        dialog.bt_save.setOnClickListener {
            page = 1
            mydataList.clear()
            myAdapter.notifyDataSetChanged()
            getList()
            dialog.dismiss()
        }



        dialog.spin_designation.setAdapter(adapterdesignation)
        dialog.spin_department.setAdapter(adaptedepartment_list)
        dialog.spin_level.setAdapter(adapterhierarchy_level_list)
        dialog.spin_Qualification.setAdapter(adaptequalification_list)
        dialog.spin_location.setAdapter(adapterlocation_list)
        dialog.spin_wfh.setAdapter(adapterwork_from_home)


        dialog.spin_designation.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterdesignation.getItem(position)!!
                designationid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        dialog.spin_department.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adaptedepartment_list.getItem(position)!!
                departmentid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        dialog.spin_location.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterlocation_list.getItem(position)!!
                locationid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        dialog.spin_Qualification.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adaptequalification_list.getItem(position)!!
                qualificationid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        dialog.spin_wfh.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterwork_from_home.getItem(position)!!
                workfromhomeid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
        dialog.spin_level.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterhierarchy_level_list.getItem(position)!!
                levelid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        dialog.edt_fromdate.setOnClickListener {
            val dpd = DatePickerDialog(this, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                dialog.edt_fromdate.setText(dob)
                from_date = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))

//            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
        dialog.edt_todate.setOnClickListener {
            val dpd = DatePickerDialog(this, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                dialog.edt_todate.setText(dob)
                todate = startDate

            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))
//            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }

    }

    private fun clickListeners() {
        job_filters.setOnClickListener {
            initializePopUp()
        }

    }

    private fun setAdapter() {


        myAdapter = PostingListAdapter(this, object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                var dataModel = any1 as PostingDTO

                when (i) {
                    2 -> {

                        openDialog(dataModel)

//                        applyPost(dataModel)
                    }

                }
            }

        })
        rec_jobposting!!.setLayoutManager(LinearLayoutManager(this))
        rec_jobposting!!.adapter = myAdapter

        rec_jobposting!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.v("addOnScrollListener", "false")
                    getList()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }

    private fun openDialog(dataModel: PostingDTO) {
        val dialog =
            Dialog(this)
        dialog.setContentView(R.layout.apply_dialog) //layout for dialog
        dialog.setTitle("Apply Post")
        dialog.setCancelable(true) //none-dismiss when touching outside Dialog
        // set the custom dialog components - texts and image
        val applyreason = dialog.findViewById<EditText>(R.id.edt_applyreason)
        //set spinner adapter
        var adapter = ApplyJobAdapter(dataModel.kra, this)
        dialog.rec_kras.layoutManager = LinearLayoutManager(this)
        dialog.rec_kras.adapter = adapter

        dialog.reason.setText(dataModel.reason)
        dialog.designation.setText(dataModel.designation)
        dialog.workfromhome.setText(dataModel.workfromhome)
        dialog.location.setText(dataModel.location)
        dialog.assignedhr.setText(dataModel.assigned_hr)
        dialog.assets.setText(dataModel.assets)
        dialog.qualification.setText(dataModel.qualification)
        dialog.salarypackage.setText(dataModel.salary_package)
        dialog.level.setText(dataModel.hierarchy_level)
        dialog.postedon.setText(dataModel.posted_on)

        dialog.applypost.setOnClickListener {
            applyPost(dataModel, dialog, dialog.edt_applyreason.text.toString())
            dialog.dismiss()
        }
        dialog.show()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getList() {
        val reqData =
            PostingListReq()

        val condata =
            ConditionDTO()
        condata.dept_id = departmentid
        condata.designation_id = designationid
        condata.work_from_home = workfromhomeid
        condata.qualification = qualificationid
        condata.hierarchy_level = levelid
        condata.from_date = from_date
        condata.to_date = todate
        condata.location_id = locationid
        reqData.service_name = ApiUtils.GETJOBPOSTING
        reqData.token = pref?.get(AppConstants.token)
        reqData.user_id = pref?.get(AppConstants.uid)
        reqData.conditions = condata
        reqData.page = page;

        apiImp?.getPostingList(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as PostingListResponse
                        val datalist = ss.data

                        if (datalist.isEmpty()) {
                            donHit = true
                        } else {
                            mydataList.addAll(datalist)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()
                            page++
                        }
                    }
                }

            }
        )


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getFilterDropdownList() {
        val reqData =
            PostingListReq()

        reqData.service_name = ApiUtils.FILTERDATA
        reqData.token = pref?.get(AppConstants.token)


        apiImp?.getFilterData(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as FilterListResponse
                        val datalist = ss.data

                        departmentlist.addAll(datalist.department_list)
                        locationlist.addAll(datalist.location_list)
                        designationlist.addAll(datalist.designation)
                        qualificationlist.addAll(datalist.qualification_list)
                        locationlist.addAll(datalist.location_list)
                        workfromhomelist.addAll(datalist.work_from_home)

                        adaptedepartment_list.notifyDataSetChanged()
                        adaptequalification_list.notifyDataSetChanged()
                        adapterdesignation.notifyDataSetChanged()
                        adapterhierarchy_level_list.notifyDataSetChanged()
                        adapterlocation_list.notifyDataSetChanged()
                        adapterwork_from_home.notifyDataSetChanged()


                    }
                }

            }
        )


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun applyPost(model: PostingDTO, dialog: Dialog, toString: String) {

        val reqData =
            ApplyPostReq()


        reqData.service_name = ApiUtils.APPLYJOB
        reqData.token = pref?.get(AppConstants.token)
        reqData.user_id = pref?.get(AppConstants.uid)
        reqData.message = toString
        reqData.req_id = model.id

        apiImp?.applyPost(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        dialogUtils.showAlert("Thanks for applying")
                        mydataList.clear()
                        page = 1
                        getList()
                        myAdapter.notifyDataSetChanged()
                    }

                }

            }
        )
    }


    companion object {

    }

}