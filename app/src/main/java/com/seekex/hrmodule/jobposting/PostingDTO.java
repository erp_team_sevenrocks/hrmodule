
package com.seekex.hrmodule.jobposting;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;
import com.seekex.hrmodule.markrewarks.DropdownModel;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class PostingDTO extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("hires")
    private String hires;
    @SerializedName("reason")
    private String reason;
    @SerializedName("designation")
    private String designation;
    @SerializedName("workfromhome")
    private String workfromhome;
    @SerializedName("location")
    private String location;
    @SerializedName("assigned_hr")
    private String assigned_hr;
    @SerializedName("assets")
    private String assets;
    @SerializedName("applied")
    private String applied;
    @SerializedName("qualification")
    private String qualification;
    @SerializedName("salary_package")
    private String salary_package;
    @SerializedName("hierarchy_level")
    private String hierarchy_level;
    @SerializedName("posted_on")
    private String posted_on;
    @SerializedName("kra")
    private ArrayList<DropdownModel> kra;

    public String getPosted_on() {
        return "posted On: "+posted_on;
    }

    public void setPosted_on(String posted_on) {
        this.posted_on = posted_on;
    }

    public String getQualification() {
        return "Qualification: "+qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getSalary_package() {
        return "Package: "+salary_package;
    }

    public void setSalary_package(String salary_package) {
        this.salary_package = salary_package;
    }

    public String getHierarchy_level() {
        return "Hierarchy Level: "+hierarchy_level;
    }

    public void setHierarchy_level(String hierarchy_level) {
        this.hierarchy_level = hierarchy_level;
    }

    public ArrayList<DropdownModel> getKra() {
        return kra;
    }

    public void setKra(ArrayList<DropdownModel> kra) {
        this.kra = kra;
    }

    public Boolean showApplied() {
        if (applied.equals("0")) {
            return true;
        }
        return false;
    }


    public String getApplied() {
        return applied;
    }

    public void setApplied(String applied) {
        this.applied = applied;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHiresTxt() {
        return "Hires: " + hires;
    }

    public String getHires() {
        return hires;
    }

    public void setHires(String hires) {
        this.hires = hires;
    }

    public String getReason() {
        return "Reason: " + reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getDesignation() {
        return "Designation: " + designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getWorkfromhome() {
        return "Work from home: " + workfromhome;
    }

    public void setWorkfromhome(String workfromhome) {
        this.workfromhome = workfromhome;
    }

    public String getLocation() {
        return "Location: " + location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAssigned_hr() {
        return "Assigned HR: " + assigned_hr;
    }

    public void setAssigned_hr(String assigned_hr) {
        this.assigned_hr = assigned_hr;
    }

    public String getAssets() {
        return "Assets: " + assets;
    }

    public void setAssets(String assets) {
        this.assets = assets;
    }
}
