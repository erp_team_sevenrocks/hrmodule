package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.PostingAdapterBinding
import com.seekex.hrmodule.databinding.RewardAdapterBinding
import com.seekex.hrmodule.jobposting.JobPostingActivity
import com.seekex.hrmodule.jobposting.PostingDTO
import com.seekex.hrmodule.jobposting.PostingListResponse
import com.seekex.hrmodule.markrewarks.RewardListDTO
import com.seekex.hrmodule.markrewarks.RewardsActivity
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*

class PostingListAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<PostingListAdapter.ViewHolder>() {
    private val postingActivity: JobPostingActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<PostingDTO> = ArrayList()

    fun setDataValues(items: ArrayList<PostingDTO>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        postingActivity = context as JobPostingActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = PostingAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.applypost.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)
        }

        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: PostingAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PostingDTO) {
            binding.model = item
            binding.executePendingBindings()


        }

    }

}