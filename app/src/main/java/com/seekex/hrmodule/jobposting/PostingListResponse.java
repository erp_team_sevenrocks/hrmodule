
package com.seekex.hrmodule.jobposting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.RewardListDTO;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class PostingListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<PostingDTO> data;

    public ArrayList<PostingDTO> getData() {
        return data;
    }

    public void setData(ArrayList<PostingDTO> data) {
        this.data = data;
    }


}

