
package com.seekex.hrmodule.jobposting;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.DropdownModel;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class ConditionDTO  {

    @SerializedName("designation_id")
    private String designation_id;
    @SerializedName("dept_id")
    private String dept_id;
    @SerializedName("qualification")
    private String qualification;
    @SerializedName("work_from_home")
    private String work_from_home;
    @SerializedName("hierarchy_level")
    private String hierarchy_level;
    @SerializedName("from_date")
    private String from_date;
    @SerializedName("to_date")
    private String to_date;
    @SerializedName("location_id")
    private String location_id;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getDesignation_id() {
        return designation_id;
    }

    public void setDesignation_id(String designation_id) {
        this.designation_id = designation_id;
    }

    public String getDept_id() {
        return dept_id;
    }

    public void setDept_id(String dept_id) {
        this.dept_id = dept_id;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getWork_from_home() {
        return work_from_home;
    }

    public void setWork_from_home(String work_from_home) {
        this.work_from_home = work_from_home;
    }

    public String getHierarchy_level() {
        return hierarchy_level;
    }

    public void setHierarchy_level(String hierarchy_level) {
        this.hierarchy_level = hierarchy_level;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }
}
