package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.PostingAdapterBinding
import com.seekex.hrmodule.databinding.PostingListAdapterBinding
import com.seekex.hrmodule.databinding.RewardAdapterBinding
import com.seekex.hrmodule.jobposting.JobPostingActivity
import com.seekex.hrmodule.jobposting.PostingDTO
import com.seekex.hrmodule.jobposting.PostingListResponse
import com.seekex.hrmodule.markrewarks.DropdownModel
import com.seekex.hrmodule.markrewarks.RewardListDTO
import com.seekex.hrmodule.markrewarks.RewardsActivity
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*

class PostingListKraAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<PostingListKraAdapter.ViewHolder>() {
    private val postingActivity: JobPostingActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<DropdownModel> = ArrayList()

    fun setDataValues(items: ArrayList<DropdownModel>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        postingActivity = context as JobPostingActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = PostingListAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }


        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: PostingListAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: DropdownModel) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}