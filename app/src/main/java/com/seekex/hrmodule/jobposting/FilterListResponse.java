
package com.seekex.hrmodule.jobposting;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class FilterListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private FilterListDTO data;

    public FilterListDTO getData() {
        return data;
    }

    public void setData(FilterListDTO data) {
        this.data = data;
    }


}

