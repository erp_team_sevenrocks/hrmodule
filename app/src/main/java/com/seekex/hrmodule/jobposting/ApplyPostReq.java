package com.seekex.hrmodule.jobposting;


import com.seekx.webService.models.RequestBase;

public class ApplyPostReq extends RequestBase {

    private String req_id  ;
    private String user_id  ;
    private Integer page  ;
    private String message  ;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getReq_id() {
        return req_id;
    }

    public void setReq_id(String req_id) {
        this.req_id = req_id;
    }
}
