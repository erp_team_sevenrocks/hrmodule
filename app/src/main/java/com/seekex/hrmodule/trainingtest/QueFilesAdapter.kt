package com.seekex.trainingtestapp


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.media.MediaPlayer.OnPreparedListener
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.MediaController
import android.widget.SeekBar
import androidx.annotation.NonNull
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.FileAdapterBinding
import com.seekex.hrmodule.trainingtest.MacFileDTO
import com.seekx.interfaces.AdapterListener
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import kotlinx.android.synthetic.main.play_audio.btn_pause
import kotlinx.android.synthetic.main.play_audio.btn_play
import kotlinx.android.synthetic.main.play_audio.pb_audio
import kotlinx.android.synthetic.main.play_video.*
import java.io.File
import java.util.*

class QueFilesAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<QueFilesAdapter.ViewHolder>() {
    private val dashboardActivity: TestDashboardActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<MacFileDTO> = ArrayList()

    fun setDataValues(items: ArrayList<MacFileDTO>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        dashboardActivity = context as TestDashboardActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = FileAdapterBinding.inflate(inflater)

        var isPb = true
        var isPaused = false
        val handler = Handler()
        var mediaPlayer = MediaPlayer()

        mediaPlayer.setOnCompletionListener {
            isPaused = false
            binding.seekbar.progress = 100
            Log.v("isPlaying", "complete")
        }
        Handler().postDelayed(Runnable {
            binding.pbAudio.visibility = View.INVISIBLE
        }, 3000)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }

        binding.llAudio.setOnClickListener {
//            openAudioPlayer(ApiUtils.DOMAIN + binding?.model?.url!!)
        }
        binding.llVideo.setOnClickListener {
            Log.e("TAG", "onCreateViewHolder: "+ApiUtils.DOMAIN + binding?.model?.url!! )
            openVideoPLayer(ApiUtils.DOMAIN + binding?.model?.url!!)
        }
        binding.llFile.setOnClickListener {
            if (binding?.model?.type.equals(AppConstants.QUE_FILE)) {
                initializeDownloader(ApiUtils.DOMAIN + binding?.model?.url!!)

            } else {

            }

        }

        binding.btnPlay.setOnClickListener{
            if (isPb) {
                binding.pbAudio.visibility = View.VISIBLE
            }
            isPb = false
            player = mediaPlayer
            if (!isPaused) {
                count = 0
            }

            binding.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }


                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
                        mediaPlayer.seekTo(progress * 1000)
                    }
                }
            })


            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(ApiUtils.DOMAIN + binding?.model?.url!!) // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare()
                mediaPlayer.prepareAsync()
                // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start()
               binding.seekbar.max = mediaPlayer.duration / 1000

            } else {

                mediaPlayer.pause()
            }

            updateSeekbarNew(binding, handler, mediaPlayer)
        }
        binding.btnPause.setOnClickListener{
            isPaused = true
            mediaPlayer.pause()
        }
        return ViewHolder(binding)
    }

    fun openDocument(paths: String, context: Context): Boolean {
        Log.e("FileType", "pdf hai")
        try {
            val file = File(paths)
            if (!file.exists())
                return false
            Log.e("FileType", "pdf hai")
            val uri = Uri.fromFile(file)
            var type = "*/*"

            // Check what kind of file you are trying to open, by comparing the paths with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) masterType,
            // so Android knew what application to use to open the file
            if (paths.contains(".doc") || paths.contains(".docx")) {
                // Word document
                type = "application/msword"
            } else if (paths.contains(".pdf")) {
                // PDF file
                type = "application/pdf"
            } else if (paths.contains(".ppt") || paths.contains(".pptx")) {
                // Powerpoint file
                type = "application/vnd.ms-powerpoint"
            } else if (paths.contains(".xls") || paths.contains(".xlsx")) {
                // Excel file
                type = "application/vnd.ms-excel"
            } else if (paths.contains(".zip") || paths.contains(".rar")) {
                // WAV audio file
                type = "application/x-wav"
            } else if (paths.contains(".rtf")) {
                // RTF file
                type = "application/rtf"
            } else if (paths.contains(".wav") || paths.contains(".mp3")) {
                // WAV audio file
                type = "audio/x-wav"
            } else if (paths.contains(".gif")) {
                // GIF file
                type = "image/gif"
            } else if (paths.contains(".jpg") || paths.contains(".jpeg") || paths.contains(".png")) {
                // JPG file
                type = "image/jpeg"
            } else if (paths.contains(".txt")) {
                // Text file
                type = "text/plain"
            } else if (paths.contains(".3gp") || paths.contains(".mpg") || paths.contains(".mpeg") || paths.contains(
                    ".mpe"
                ) || paths.contains(".mp4") || paths.contains(".avi")
            ) {
                // Video files
                type = "video/*"
            }

            Log.e("FileType", type)
            Log.v("FileType", paths)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, type)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        } catch (e: Exception) {
            Log.v("openDocument", e.toString())
            return false
        }
        return true
    }


    fun initializeDownloader(resume: String) {

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
        val config = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .build()

        val extBaseDir = context?.externalCacheDir

        val file = File(extBaseDir!!.absolutePath + "/Rewaurdoc/" + "")
        PRDownloader.initialize(dashboardActivity, config)

        val apkName = getFileNameFromUrl(resume)

        val apkPath = file.absolutePath//AppConstants.downloadDir

        val returnString = "$apkPath/$apkName"
        Log.e("openDocument",returnString)
        if (openDocument(returnString, dashboardActivity!!)) {
            return
        }

        PRDownloader.download(resume, apkPath, apkName)
            .build()
            .setOnStartOrResumeListener {}
            .setOnPauseListener {}
            .setOnCancelListener {}
            .setOnProgressListener {}
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    openDocument(returnString, dashboardActivity)
                }

                override fun onError(error: com.downloader.Error?) {
                }

            })
    }

    fun getFileNameFromUrl(url: String): String {
        return url.substring(url.lastIndexOf("/") + 1)
    }

    private fun openAudioPlayer(toString: String) {
        var isPb = true
        var isPaused = false
        val handler = Handler()
        var mediaPlayer = MediaPlayer()
        val dialog = Dialog(dashboardActivity!!, R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_audio)

        dialog.setCancelable(true)
        dialog.setOnDismissListener {
            dialog.dismiss()
        }
        mediaPlayer.setOnBufferingUpdateListener { mp, percent ->

//            dialog.seeplay_audio.xmlkbar.secondaryProgress = percent
        }

        mediaPlayer.setOnCompletionListener {
            isPaused = false
            dialog.seekbar.progress = 100
            Log.v("isPlaying", "complete")
            dialog.dismiss()
        }
        Handler().postDelayed(Runnable {
            dialog.pb_audio.visibility = View.INVISIBLE
        }, 3000)

        dialog.btn_play.setOnClickListener {

            if (isPb) {
                dialog.pb_audio.visibility = View.VISIBLE
            }
            isPb = false
            player = mediaPlayer
            if (!isPaused) {
                count = 0
            }

            dialog.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }


                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
                        mediaPlayer.seekTo(progress * 1000)
                    }
                }
            })


            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(toString) // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare()
                mediaPlayer.prepareAsync()
                // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start()
                dialog.seekbar.max = mediaPlayer.duration / 1000

            } else {

                mediaPlayer.pause()
            }

            updateSeekbar(dialog, handler, mediaPlayer)
        }
        dialog.btn_pause.setOnClickListener {
            isPaused = true
            mediaPlayer.pause()
        }

        dialog.setOnDismissListener {
            count = 0
            if (mediaPlayer.isPlaying || player != null) {
                mediaPlayer.stop()
                player?.stop()
            }


        }
        dialog.show()


    }

    private fun openVideoPLayer(toString: String) {

        val dialog = Dialog(dashboardActivity!!, R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_video)

        dialog.setCancelable(true)

dialog.pb_video.visibility=View.VISIBLE
        val mc = MediaController(dashboardActivity!!)
        mc.setAnchorView(dialog.videoview)
        mc.setMediaPlayer(dialog.videoview)
        dialog.videoview.setMediaController(mc);
        val video =
            Uri.parse(toString)
        dialog.videoview.setVideoURI(video)
        dialog.videoview.setOnPreparedListener(OnPreparedListener { mp ->
            dialog.pb_video.visibility=View.GONE

            mp.isLooping = true
            dialog.videoview.start()
        })



        dialog.setOnDismissListener {
            dialog.dismiss()
        }


        dialog.btn_play.setOnClickListener {


        }
        dialog.btn_pause.setOnClickListener {

        }

        dialog.setOnDismissListener {
        }
        dialog.show()


    }
    private fun updateSeekbarNew(
        binding: FileAdapterBinding,
        handler: Handler,
        mediaPlayer: MediaPlayer
    ) {


        val notification = Runnable {
            val mCurrentPosition = mediaPlayer.currentPosition / 1000
            binding.seekbar.progress = mCurrentPosition

//            val total =
//                binding.seekbar.max.toString()
//            val played = TimeUtils.convertSecondToHHMMString(count)
//
//            Log.e("totalplayed", "updateSeekbar: "+total+" -- "+played )
//            val displayDuration = "$played / $total"
//
//            ++count
//
//            binding.durationtxt.text = displayDuration

//            if (total == played) {
//                return@Runnable
//            }

            updateSeekbarNew(binding, handler, mediaPlayer)
        }

        handler.postDelayed(notification, 1000)


    }
    private fun updateSeekbar(
        binding: Dialog,
        handler: Handler,
        mediaPlayer: MediaPlayer
    ) {


        val notification = Runnable {
            val mCurrentPosition = mediaPlayer.currentPosition / 1000
            binding.seekbar.progress = mCurrentPosition

//            val total =
//                binding.seekbar.max.toString()
//            val played = TimeUtils.convertSecondToHHMMString(count)
//
//            Log.e("totalplayed", "updateSeekbar: "+total+" -- "+played )
//            val displayDuration = "$played / $total"
//
//            ++count
//
//            binding.durationtxt.text = displayDuration

//            if (total == played) {
//                return@Runnable
//            }

            updateSeekbar(binding, handler, mediaPlayer)
        }

        handler.postDelayed(notification, 1000)


    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: FileAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: MacFileDTO) {
            binding.model = item
            binding.executePendingBindings()
//            Log.e("TAG", "bind: "+ items.get(position).grievanceMediaFile.get(0).url)

        }

    }

}