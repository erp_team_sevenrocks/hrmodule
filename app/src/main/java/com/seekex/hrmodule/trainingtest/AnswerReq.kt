package com.seekex.hrmodule.trainingtest

import com.seekx.webService.models.RequestBase

data class AnswerReq(
    var data: AnswerDTO?
):RequestBase(){

    constructor():this(null)
}
