package com.seekex.trainingtestapp

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.activities.DashboardActivity
import com.seekex.hrmodule.databinding.*
import com.seekex.hrmodule.grievance.ImagesGRDTO
import com.seekex.hrmodule.markrewarks.SelectedMasterCallback
import com.seekex.hrmodule.markrewarks.uploadaudiores
import com.seekex.hrmodule.trainingtest.*
import com.seekex.trainingtestapp.TestDashboardActivity.Companion.QUE_COUNT

import com.seekex.trainingtestapp.login.QuestionRes
import com.seekx.interfaces.AdapterListener
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.interfaces.PermissionCallBack

import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack


import com.seekx.webService.models.apiRequest.GetQuestionReq
import com.vincent.filepicker.Constant
import com.vincent.filepicker.activity.NormalFilePickActivity
import com.vincent.filepicker.filter.entity.NormalFile
import kotlinx.android.synthetic.main.add_loc_feedback.*
import kotlinx.android.synthetic.main.add_loc_feedback.view.*
import kotlinx.android.synthetic.main.apply_leave.*
import kotlinx.android.synthetic.main.apply_leave.view.*
import kotlinx.android.synthetic.main.fragment_card.*
import kotlinx.android.synthetic.main.fragment_card.ll_attach
import kotlinx.android.synthetic.main.fragment_card.view.*
import kotlinx.android.synthetic.main.fragment_card.view.ll_attach
import kotlinx.android.synthetic.main.image_view.*

import java.io.File
import java.io.IOException


@Suppress("DEPRECATION")
class MachineTestFragment : Fragment() {
    private var questionType: String? = ""
    private var quesTionId: String? = ""
    lateinit var rootview: View
    public var fileList = ArrayList<FileDTOTest>()
    public var fileListWeb = ArrayList<FileDTOTest>()
    private var audioFilename: String = ""
    private var rd_value: String = ""
    var mediaRecorder: MediaRecorder? = null
    var countDownTimer: CountDownTimer? = null
    var isRecorded = 0
    private var timer = ""
    private var rd1_val = ""
    private var rd2_val = ""
    private var rd3_val = ""
    private var rd4_val = ""
    public var imageList = ArrayList<ImageDTO>()
    public var imageListWeb = ArrayList<ImageDTO>()

    private var count: Long? = null
    private var counter: Int? = null
    public var dashboardActivity: TestDashboardActivity? = null
    private var excelPath: String = ""
    private var excelName: String = ""
    private var permissionCallBack: PermissionCallBack? = null
    var imagePathList: ArrayList<ImagesGRDTO> = java.util.ArrayList()
    private var call_Counter = 0
    private var xlFilename: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dashboardActivity = activity as TestDashboardActivity?
        if (arguments != null) {
            counter = requireArguments().getInt(ARG_COUNT)
        }
        Log.e("TAG", "onCreate: counter" + counter)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        rootview = inflater.inflate(R.layout.fragment_card, container, false)
        startTimer()
        counter = counter!! + 1

        rootview.tv_counter.text = "Question No $counter / ${TestDashboardActivity.QUE_COUNT}"

//        if (counter == TestDashboardActivity.QUE_COUNT) {
//            rootview.btn_submit.visibility = View.VISIBLE
//            rootview.btn_next.isEnabled =false
//
//        } else {
//            rootview.btn_submit.visibility = View.GONE
//            rootview.btn_next.isEnabled =true
//        }


        getQuestion(counter.toString())




        clickListeners()


        return rootview
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constant.REQUEST_CODE_PICK_FILE) {
                val list: ArrayList<NormalFile> =
                    intent!!.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE)!!

                for (i in list.indices) {
                    var isExists: Boolean = false
                    excelName = list.get(i).name
                    excelPath = list.get(i).path
//                excelUri = list.get(0).uri
                    if (fileList.size > 0) {
                        for (j in fileList.indices) {
                            if (fileList.get(j).path.toLowerCase()
                                    .equals(excelPath.toLowerCase())
                            ) {
                                isExists = true
                            }
                        }
                        if (!isExists) {
                            fileList.add(
                                FileDTOTest(
                                    excelName, excelPath, ""
                                )
                            )
                        }
                    } else {
                        fileList.add(
                            FileDTOTest(
                                excelName, excelPath, ""
                            )
                        )

                    }


                }
                showAttachmentView("me", ll_viewattach!!,
                    fileList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            fileList = any as ArrayList<FileDTOTest>
                        }
                    })
            } else if (requestCode == 100) {

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!

                val fileUri = intent?.data
                imageList.add(
                    ImageDTO(
                        Uri.parse("file://" + returnValue.get(0)).toString(), "", "me", ""
                    )
                )
                showDynamicImage("me", ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })
            }
        }
    }

    fun showDynamicImage(
        addedtype: String,
        llAtach: LinearLayout,
        imageList: ArrayList<ImageDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()

        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        if (addedtype.equals("me")) {
            for (model in imageList.indices) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = ImageItemTestBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = imageList[model]

                binding.model = imageDTO

                binding.ivImage.setOnClickListener {
                    showImageUri(imageDTO.getUri(), context)
                }

                binding.ivCut.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete Image",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {
                                    if (imageDTO.id.equals("")) {
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    } else {

                                        deleteMediaWeb(imageDTO.id)
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    }


                                }

                            }

                        }
                    )

                }
            }
        } else {
            for (model in imageList.indices) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = ImageItemWebBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = imageList[model]

                binding.model = imageDTO

                binding.ivCut.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete Image",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {

                                    if (imageDTO.id.equals("")) {
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    } else {
                                        deleteMediaWeb(imageDTO.id)
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    }

                                }

                            }

                        }
                    )

                }
            }
        }


    }

    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }


    fun showAttachmentView(
        addedtype: String,
        llAtach: LinearLayout,
        fileList: ArrayList<FileDTOTest>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)
        if (addedtype.equals("me")) {
            for (model in 0 until fileList.size) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = FileItemNewBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = fileList[model]

                binding.model = imageDTO



                binding.removefile.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete File",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {
                                    if (imageDTO.id.equals("")) {
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    } else {
                                        deleteMediaWeb(imageDTO.id)
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    }
                                }
                            }
                        })
                }
            }
        } else {
            for (model in 0 until fileList.size) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = FileItemNewWebBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = fileList[model]

                binding.model = imageDTO



                binding.removefile.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete File",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {
                                    if (imageDTO.id.equals("")) {
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    } else {
                                        deleteMediaWeb(imageDTO.id)
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    }
                                }
                            }
                        })
                }
            }
        }

    }

    private fun deleteMediaWeb(id: String) {
        dashboardActivity!!.apiImp?.showProgressBar()
        val answerReq = DeleteMediaReq()

        answerReq.record_id = id
        answerReq.token = dashboardActivity?.pref?.get(AppConstants.token)!!
        answerReq.service_name = ApiUtils.DELETEMEDIA


        dashboardActivity!!.apiImp?.deleteMedia(answerReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                dashboardActivity!!.apiImp?.cancelProgressBar()
                if (status) {
                }
            }


        })

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {

            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }

    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@MachineTestFragment, options)

    }

    private fun clickListeners() {
//        rootview.addimages.setOnClickListener {
//            pickImage()
//        }
        rootview.iv_action.setOnClickListener {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    if (isRecorded == 0) {
                        isRecorded = 1
                        playBeep()
                        startRecording()
                        iv_action!!.setImageResource(android.R.drawable.ic_media_pause)
                    } else if (isRecorded == 1) {
                        bt_save!!.visibility = View.INVISIBLE
                        stopMedia()
                        playBeep()
                        isRecorded = 2
                        countDownTimer!!.cancel()
                        timer = tvTimers!!.text.toString()
                        iv_action!!.setImageResource(android.R.drawable.ic_media_play)
                    } else if (isRecorded == 2) {
//                        FileUtils.openDocument(file_name, activity)
                    }

                }
            })


        }
        rootview.bt_reset.setOnClickListener {
            try {
                isRecorded = 0
                stopMedia()
                playBeep()
                countDownTimer!!.cancel()
                tvTimers!!.text = ""
                bt_save!!.visibility = View.GONE
                iv_action!!.setImageDrawable(requireActivity().resources.getDrawable(R.drawable.audiorec))
            } catch (e: Exception) {
            }
        }
        rootview.btn_submit.setOnClickListener {


        }

//        rootview.btn_addattchment.setOnClickListener {
//            val intent4 = Intent(activity, NormalFilePickActivity::class.java)
//            intent4.putExtra(Constant.MAX_NUMBER, 4)
//            intent4.putExtra(
//                NormalFilePickActivity.SUFFIX,
//                arrayOf("xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf")
//            )
//            startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE)
//        }
        rootview.btn_next.setOnClickListener {
            if (rootview.btn_next.text.equals("Submit")) {

                dashboardActivity!!.dialogUtils!!.showAlertWithCallBack(
                    "Do you want to submit your test ?",
                    "yes",
                    "No",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {

                            if (clickStatus) {
                                if (imageList.size > 0) {
                                    var ifcontain = false
                                    for (i in imageList.indices) {
                                        if (imageList.get(i).id.equals("")) {
                                            uploadImages(true)
                                            ifcontain = true
                                        }
                                    }
                                    if (!ifcontain) {
                                        if (bt_save.visibility == View.INVISIBLE) {
                                            saveaudiofile(true)
                                        } else if (fileList.size > 0) {
                                            var ifcontain = false
                                            for (i in fileList.indices) {

                                                if (fileList.get(i).id.equals("")) {
                                                    ifcontain = true
                                                    saveExcelFile(true)
                                                }
                                            }
                                            if (!ifcontain) {
                                                saveData(true)
                                            }

                                        } else {
                                            saveData(true)
                                        }
                                    }
                                } else if (bt_save.visibility == View.INVISIBLE) {
                                    saveaudiofile(true)
                                } else if (fileList.size > 0) {
                                    var ifcontain = false
                                    for (i in fileList.indices) {

                                        if (fileList.get(i).id.equals("")) {
                                            ifcontain = true
                                            saveExcelFile(true)
                                        }
                                    }
                                    if (!ifcontain) {
                                        if (bt_save.visibility == View.INVISIBLE) {
                                            saveaudiofile(true)
                                        } else {
                                            saveData(true)

                                        }

                                    }

                                } else {
                                    saveData(true)
                                }
                            }
                        }

                    })

            } else {
                if (imageList.size > 0) {
                    var ifcontain = false
                    for (i in imageList.indices) {
                        if (imageList.get(i).id.equals("")) {
                            uploadImages(true)
                            ifcontain = true
                        }
                    }
                    if (!ifcontain) {
                        if (bt_save.visibility == View.INVISIBLE) {
                            saveaudiofile(true)
                        } else if (fileList.size > 0) {
                            var ifcontain = false
                            for (i in fileList.indices) {

                                if (fileList.get(i).id.equals("")) {
                                    ifcontain = true
                                    saveExcelFile(true)
                                }
                            }
                            if (!ifcontain) {
                                saveData(true)
                            }

                        } else {
                            saveData(true)
                        }
                    }
                } else if (bt_save.visibility == View.INVISIBLE) {
                    saveaudiofile(true)
                } else if (fileList.size > 0) {
                    var ifcontain = false
                    for (i in fileList.indices) {

                        if (fileList.get(i).id.equals("")) {
                            ifcontain = true
                            saveExcelFile(true)
                        }
                    }
                    if (!ifcontain) {
                        if (bt_save.visibility == View.INVISIBLE) {
                            saveaudiofile(true)
                        } else {
                            saveData(true)


                        }

                    }

                } else {
                    saveData(true)
                }
            }


        }
        rootview.btn_prev.setOnClickListener {
            rootview.btn_next.setText("Next")
            if (counter!!.toInt() > 1) {
// save data

                if (imageList.size > 0) {
                    var ifcontain = false
                    for (i in imageList.indices) {
                        if (imageList.get(i).id.equals("")) {
                            uploadImages(false)
                            ifcontain = true
                        }
                    }
                    if (!ifcontain) {
                        if (bt_save.visibility == View.INVISIBLE) {
                            saveaudiofile(false)
                        } else if (fileList.size > 0) {
                            var ifcontain = false
                            for (i in fileList.indices) {

                                if (fileList.get(i).id.equals("")) {
                                    ifcontain = true
                                    saveExcelFile(false)
                                }
                            }
                            if (!ifcontain) {
                                saveData(false)
                            }

                        } else {
                            saveData(false)
                        }
                    }
                } else if (bt_save.visibility == View.INVISIBLE) {
                    saveaudiofile(false)
                } else if (fileList.size > 0) {
                    var ifcontain = false
                    for (i in fileList.indices) {

                        if (fileList.get(i).id.equals("")) {
                            ifcontain = true
                            saveExcelFile(false)
                        }
                    }
                    if (!ifcontain) {
                        if (bt_save.visibility == View.INVISIBLE) {
                            saveaudiofile(false)
                        } else {
                            saveData(false)

                        }

                    }

                } else {
                    saveData(false)
                }

                //d savedata


                // clear data and get question
                newInstance(counter)
                counter = counter!! - 1

                rootview.tv_counter.text =
                    "Question No $counter / ${TestDashboardActivity.QUE_COUNT}"

                rootview.edt_answer.setText("")
                imagePathList.clear()
                fileList.clear()
                fileListWeb.clear()

                imageList.clear()
                imageListWeb.clear()
                bt_save.visibility == View.GONE
                rootview.ll_viewattachwebmac.removeAllViews()
                rootview.ll_attach_webmac.removeAllViews()
//                rootview.ll_attach_web.removeAllViews()
                getQuestion(counter.toString())
                //clear data and get question

            }
        }
        rootview.rd1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                rootview.rd2.isChecked = false
                rootview.rd3.isChecked = false
                rootview.rd4.isChecked = false
                rd_value = rd1_val
            }
        }
        rootview.rd2.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                rootview.rd1.isChecked = false
                rootview.rd3.isChecked = false
                rootview.rd4.isChecked = false
                rd_value = rd2_val
            }
        }
        rootview.rd3.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                rootview.rd1.isChecked = false
                rootview.rd2.isChecked = false
                rootview.rd4.isChecked = false
                rd_value = rd3_val
            }
        }
        rootview.rd4.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                rootview.rd1.isChecked = false
                rootview.rd2.isChecked = false
                rootview.rd3.isChecked = false
                rd_value = rd4_val
            }
        }
    }

    private fun uploadImages(isnext: Boolean) {

        dashboardActivity!!.apiImp!!.showProgressBar()
        dashboardActivity?.apiImp?.hitApiTEst(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                dashboardActivity!!.apiImp!!.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)


                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = AppConstants.QUE_IMAGE
                    model.name = imageFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < imageList.size) {
                        if (imageList.get(call_Counter).id.equals("")) {
                            uploadImages(isnext)
                        }
                    } else {
                        call_Counter = 0
                        if (bt_save.visibility == View.INVISIBLE) {
                            saveaudiofile(isnext)
                        } else if (fileList.size > 0) {
                            var ifcontain = false
                            for (i in fileList.indices) {
                                if (fileList.get(i).id.equals("")) {
                                    saveExcelFile(isnext)
                                    ifcontain = true
                                }
                            }
                            if (!ifcontain) {

                                saveData(isnext)
                            }
                        } else {
                            saveData(isnext)
                        }
                    }
                }
            }
        })
    }

    private fun saveData(isnext: Boolean) {

        if (isRecorded == 1) {
            bt_save!!.visibility = View.INVISIBLE
            stopMedia()
            playBeep()
            isRecorded = 2
            countDownTimer!!.cancel()
            timer = tvTimers!!.text.toString()
            iv_action!!.setImageDrawable(requireActivity().resources.getDrawable(R.drawable.audiorec))
        }



        dashboardActivity!!.apiImp?.showProgressBar()
        val answerReq = AnswerReq()
        val answerReq2 = AnswerDTO()
//
        answerReq2.user_id = dashboardActivity?.pref?.get(AppConstants.cand_id)!!
        answerReq2.type = questionType!!
        answerReq2.question_id = quesTionId!!
        answerReq2.answer_mcq = rd_value
        answerReq2.answer_text = rootview.edt_answer.text.toString()

        answerReq2.answer_url = imagePathList
        answerReq.data = answerReq2
        answerReq.token = dashboardActivity?.pref?.get(AppConstants.token)!!
        answerReq.service_name = ApiUtils.COLLECTANSWER


        dashboardActivity!!.apiImp?.saveAnswer(answerReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                dashboardActivity!!.apiImp?.cancelProgressBar()
                if (status) {
                    if (isnext) {
                        clearData(true)
                    }

                }
            }


        })
    }

    private fun saveDataPrev() {
        dashboardActivity!!.apiImp?.showProgressBar()
        val answerReq = AnswerReq()
        val answerReq2 = AnswerDTO()
//
        answerReq2.user_id = dashboardActivity?.pref?.get(AppConstants.cand_id)!!
        answerReq2.type = questionType!!
        answerReq2.question_id = quesTionId!!
        answerReq2.answer_mcq = rd_value
        answerReq2.answer_text = rootview.edt_answer.text.toString()

        answerReq2.answer_url = imagePathList
        answerReq.data = answerReq2
        answerReq.token = dashboardActivity?.pref?.get(AppConstants.token)!!
        answerReq.service_name = ApiUtils.COLLECTANSWER


        dashboardActivity!!.apiImp?.saveAnswer(answerReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                dashboardActivity!!.apiImp?.cancelProgressBar()
                if (status) {
                }
            }


        })
    }

    private fun saveaudiofile(isnext: Boolean) {
        dashboardActivity!!.apiImp!!.showProgressBar()
        dashboardActivity!!.apiImp!!.saveAudio(audioFilename, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                dashboardActivity!!.apiImp!!.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    audioFilename = ss.data.path + "" + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = AppConstants.QUE_AUDIO
                    model.name = audioFilename
                    imagePathList.add(model)
                    isRecorded = 0
                    tvTimers!!.text = ""
                    bt_save!!.visibility = View.GONE
                    iv_action!!.setImageDrawable(requireActivity().resources.getDrawable(R.drawable.audiorec))
                    if (fileList.size > 0) {
                        var ifcontain = false
                        for (i in fileList.indices) {
                            if (fileList.get(i).id.equals("")) {
                                ifcontain = true
                                saveExcelFile(isnext)
                            }
                        }
                        if (!ifcontain) {
                            saveData(isnext)
                        }
                    } else {
                        saveData(isnext)
                    }
                }

            }

        })


    }

    private fun saveExcelFile(isnext: Boolean) {
        dashboardActivity!!.apiImp!!.showProgressBar()
        dashboardActivity!!.apiImp!!.saveExcelTest(call_Counter, fileList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                dashboardActivity!!.apiImp!!.cancelProgressBar()
                Log.e("TAG", "onSuccess: upload excel")

                if (status) {

                    var ss = any as uploadaudiores
                    xlFilename = ss.data.path + "" + ss.data.filename
                    Log.e("TAG", "onSuccess: upload excel" + xlFilename)

                    var model = ImagesGRDTO()
                    model.type = AppConstants.QUE_FILE
                    model.name = xlFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < fileList.size) {
                        if (fileList.get(call_Counter).id.equals("")) {
                            saveExcelFile(isnext)
                        } else {
                            call_Counter = 0
                            saveData(isnext)
                        }
                    } else {
                        call_Counter = 0
                        saveData(isnext)
                    }
                }

            }

        })


    }

    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

    private fun startTimer() {
        var test_timer = dashboardActivity?.pref?.get(AppConstants.timer)
        var timermillis = test_timer?.toLong()!! * 1000
        object : CountDownTimer(timermillis, 1000) {
            override fun onTick(milliseconds: Long) {
                rootview.txt_timer.setText(DataUtils.formatMilliSecondsToTime(milliseconds))
            }

            override fun onFinish() {
                try {
                    Toast.makeText(activity, "Timer Finished", Toast.LENGTH_SHORT).show()
                    rootview.txt_timer.setText("FINISH!!")
                } catch (e: Exception) {
                }


                val i = Intent(activity, DashboardActivity::class.java)
// set the new task and clear flags
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i)

            }
        }.start()
    }

    private fun getQuestion(s: String) {

        val reqData =
            GetQuestionReq()
        reqData.service_name = ApiUtils.GETQUESTION
        reqData.page = s
        reqData.token = dashboardActivity?.pref?.get(AppConstants.token)
        reqData.user_id = dashboardActivity?.pref?.get(AppConstants.cand_id)
        reqData.training_test_id = dashboardActivity?.pref?.get(AppConstants.test_id)

        dashboardActivity?.apiImp?.getQuestion(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {

                        var quedata = any as QuestionRes

                        var quesDTO: QuesDTO? = quedata.data?.get(0)
                        quesTionId = quesDTO?.TrainingQuestion?.id
                        questionType = quesDTO?.TrainingQuestion?.type
                        if (quesDTO?.TrainingQuestion?.type.equals(AppConstants.TYPE_MCQ)) {


                            rootview.ll_inputque.visibility = View.GONE
                            rootview.ll_mcq.visibility = View.VISIBLE
                            rootview.txt_questionmcq.setText(quesDTO?.TrainingQuestion?.name)
                            var optionCount = quesDTO?.TrainingQuestionMcqOption?.size

                            when (optionCount) {
                                1 -> {
                                    rootview.rd1.visibility = View.VISIBLE
                                    rootview.rd1.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            0
                                        )?.option
                                    )

                                    rd1_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        0
                                    )?.id.toString()
                                    rootview.rd2.visibility = View.GONE
                                    rootview.rd3.visibility = View.GONE
                                    rootview.rd4.visibility = View.GONE

                                    if (quesDTO?.TrainingQuestion?.is_edit.equals("1")) {
                                        var ss1 =
                                            MacMcqDTO(quesDTO!!.TrainingTestUserMcqAnswer?.get(0)?.answer)
                                        if (quesDTO.TrainingQuestionMcqOption?.contains(ss1)!!) {

                                            if (rd1_val.equals(
                                                    quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                        0
                                                    )?.answer
                                                )
                                            ) {
                                                rootview.rd1.isChecked = true
                                            }
                                        }
                                    }


                                }
                                2 -> {
                                    rootview.rd1.visibility = View.VISIBLE
                                    rootview.rd2.visibility = View.VISIBLE
                                    rootview.rd3.visibility = View.GONE
                                    rootview.rd4.visibility = View.GONE

                                    rootview.rd1.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            0
                                        )?.option
                                    )
                                    rootview.rd2.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            1
                                        )?.option
                                    )
                                    rd1_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        0
                                    )?.id.toString()
                                    rd2_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        1
                                    )?.id.toString()

                                    if (quesDTO?.TrainingQuestion?.is_edit.equals("1")) {
                                        var ss1 =
                                            MacMcqDTO(quesDTO!!.TrainingTestUserMcqAnswer?.get(0)?.answer)
                                        if (quesDTO.TrainingQuestionMcqOption?.contains(ss1)!!) {

                                            if (rd1_val.equals(
                                                    quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                        0
                                                    )?.answer
                                                )
                                            ) {
                                                rootview.rd1.isChecked = true
                                            }

                                            if (rd2_val.equals(
                                                    quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                        0
                                                    )?.answer
                                                )
                                            ) {
                                                rootview.rd2.isChecked = true
                                            }
                                        }
                                    }
                                }
                                3 -> {
                                    rootview.rd1.visibility = View.VISIBLE
                                    rootview.rd2.visibility = View.VISIBLE
                                    rootview.rd3.visibility = View.VISIBLE
                                    rootview.rd4.visibility = View.GONE

                                    rootview.rd1.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            0
                                        )?.option
                                    )
                                    rootview.rd2.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            1
                                        )?.option
                                    )
                                    rootview.rd3.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            2
                                        )?.option
                                    )
                                    rd1_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        0
                                    )?.id.toString()
                                    rd2_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        1
                                    )?.id.toString()
                                    rd3_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        2
                                    )?.id.toString()

                                    if (quesDTO?.TrainingQuestion?.is_edit.equals("1")) {
                                        var ss1 =
                                            MacMcqDTO(quesDTO!!.TrainingTestUserMcqAnswer?.get(0)?.answer)
                                        if (quesDTO.TrainingQuestionMcqOption?.contains(ss1)!!) {

                                            if (rd1_val.equals(
                                                    quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                        0
                                                    )?.answer
                                                )
                                            ) {
                                                rootview.rd1.isChecked = true
                                            }

                                            if (rd2_val.equals(
                                                    quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                        0
                                                    )?.answer
                                                )
                                            ) {
                                                rootview.rd2.isChecked = true
                                            }

                                            if (rd3_val.equals(
                                                    quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                        0
                                                    )?.answer
                                                )
                                            ) {
                                                rootview.rd3.isChecked = true
                                            }


                                        }
                                    }


                                }
                                4 -> {
                                    rootview.rd1.visibility = View.VISIBLE
                                    rootview.rd2.visibility = View.VISIBLE
                                    rootview.rd3.visibility = View.VISIBLE
                                    rootview.rd4.visibility = View.VISIBLE

                                    rootview.rd1.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            0
                                        )?.option
                                    )
                                    rootview.rd2.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            1
                                        )?.option
                                    )
                                    rootview.rd3.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            2
                                        )?.option
                                    )
                                    rootview.rd4.setText(
                                        quesDTO?.TrainingQuestionMcqOption?.get(
                                            3
                                        )?.option
                                    )
                                    rd1_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        0
                                    )?.id.toString()
                                    rd2_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        1
                                    )?.id.toString()
                                    rd3_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        2
                                    )?.id.toString()
                                    rd4_val = quesDTO?.TrainingQuestionMcqOption?.get(
                                        3
                                    )?.id.toString()

                                    if (quesDTO?.TrainingQuestion?.is_edit.equals("1")) {

//                                        var ss1 =
//                                            MacMcqDTO(quesDTO!!.MachineTestCandidateMcqAnswer?.get(0)?.answer)
//                                        Log.e("TAG", "onSuccess: "+ss1.id)
//
//                                        if (quesDTO.MachineTestQuestionMcqOption?.contains(ss1)!!) {
                                        Log.e("TAG", "incontains: ")
                                        if (rd1_val.equals(
                                                quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                    0
                                                )?.answer
                                            )
                                        ) {
                                            rootview.rd1.isChecked = true
                                        }

                                        if (rd2_val.equals(
                                                quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                    0
                                                )?.answer
                                            )
                                        ) {
                                            rootview.rd2.isChecked = true
                                        }

                                        if (rd3_val.equals(
                                                quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                    0
                                                )?.answer
                                            )
                                        ) {
                                            rootview.rd3.isChecked = true
                                        }
                                        if (rd4_val.equals(
                                                quesDTO!!.TrainingTestUserMcqAnswer?.get(
                                                    0
                                                )?.answer
                                            )
                                        ) {
                                            rootview.rd4.isChecked = true
                                        }

//                                        }
                                    }


                                }
                            }

                        } else {
                            var mydataList: ArrayList<MacFileDTO> = java.util.ArrayList()

                            rootview.ll_mcq.visibility = View.GONE
                            rootview.ll_inputque.visibility = View.VISIBLE
                            rootview.txt_queinput.setText(quesDTO?.TrainingQuestion?.name)
                            var myAdapter = QueFilesAdapter(activity, object : AdapterListener {
                                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                                override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                                    var dataModel = any1 as MacFileDTO
                                    when (i) {
                                        2 -> {
                                        }

                                    }
                                }

                            })
                            Log.e("TAG", "onSuccess check : "+quesDTO?.TrainingQuestionMediaFile!!.size)

                            rootview.rec_files!!.setLayoutManager(LinearLayoutManager(activity))
                            rootview.rec_files!!.adapter = myAdapter
                            mydataList.addAll(quesDTO?.TrainingQuestionMediaFile!!)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()

                            if (quesDTO?.TrainingQuestion?.is_edit.equals("1")) {
                                if (quesDTO?.TrainingTestUserMediaAnswer!!.size > 0) {
                                    for (i in quesDTO?.TrainingTestUserMediaAnswer!!.indices) {
                                        Log.e("TAG", "onSuccess check : "+quesDTO?.TrainingTestUserMediaAnswer!!.get(i).type )
                                        if (quesDTO?.TrainingTestUserMediaAnswer!!.get(i).type.equals(
                                                AppConstants.QUE_FILE
                                            ) || quesDTO?.TrainingTestUserMediaAnswer!!.get(i).type.equals(
                                                AppConstants.QUE_AUDIO
                                            )
                                        ) {
                                            fileListWeb.add(
                                                FileDTOTest(
                                                    quesDTO?.TrainingTestUserMediaAnswer!!.get(i).answer,
                                                    quesDTO?.TrainingTestUserMediaAnswer!!.get(i).answer,
                                                    quesDTO?.TrainingTestUserMediaAnswer!!.get(i).id
                                                )
                                            )


                                        } else if (quesDTO?.TrainingTestUserMediaAnswer!!.get(i).type.equals(
                                                AppConstants.QUE_IMAGE
                                            )
                                        ) {
                                            imageListWeb.add(
                                                ImageDTO(
                                                    "",
                                                    quesDTO?.TrainingTestUserMediaAnswer!!.get(i).id!!,
                                                    "web",
                                                    quesDTO?.TrainingTestUserMediaAnswer!!.get(i).answer!!
                                                )
                                            )
                                        }
                                    }
                                }

                                if (quesDTO?.TrainingTestUserTextAnswer!!.size > 0) {
                                    rootview.edt_answer.setText(
                                        quesDTO?.TrainingTestUserTextAnswer!!.get(
                                            0
                                        ).answer
                                    )
                                }

                            }
                            if (fileListWeb.size > 0) {
                                showAttachmentView("web",
                                   rootview. ll_viewattachwebmac!!,
                                    fileListWeb,
                                    object : SelectedMasterCallback {
                                        override fun onSelected(any: Any) {
                                            fileListWeb = any as ArrayList<FileDTOTest>
                                        }
                                    })
                            }
                            if (imageListWeb.size > 0) {
                                showDynamicImage("web", ll_attach_webmac!!,
                                    imageListWeb, object : SelectedMasterCallback {
                                        override fun onSelected(any: Any) {
                                            imageListWeb = any as ArrayList<ImageDTO>
                                        }
                                    })
                            }

                        }
                        if (counter == TestDashboardActivity.QUE_COUNT)
                            btn_next.setText("Submit")
                    }
                }

            }
        )
    }

    private fun clearData(b: Boolean) {
        if (b) {

            Log.e("TAG", "clearData: " + counter + " " + TestDashboardActivity.QUE_COUNT)
            if (counter!! == TestDashboardActivity.QUE_COUNT) {
                Toast.makeText(activity, "last question attempted", Toast.LENGTH_SHORT).show()

                val i = Intent(activity, DashboardActivity::class.java)
// set the new task and clear flags
                i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                startActivity(i)
                return
            }
            if (counter!! < TestDashboardActivity.QUE_COUNT) {

                rootview.edt_answer.setText("")
                imagePathList.clear()
                fileList.clear()
                fileListWeb.clear()
                imageList.clear()
                imageListWeb.clear()
                bt_save.visibility == View.GONE
//                rootview.ll_viewattach.removeAllViews()
                rootview.ll_attach.removeAllViews()
//                rootview.ll_attach_web.removeAllViews()
//                rootview.ll_viewattachweb.removeAllViews()
                MachineTestFragment.newInstance(counter)
                if (counter == TestDashboardActivity.QUE_COUNT) {
                    rootview.btn_next.setText("Submit")
                    rootview.btn_submit.visibility = View.GONE
                    rootview.btn_next.isClickable = false
                    rootview.btn_next.isEnabled = false

                } else {
                    rootview.btn_submit.visibility = View.GONE
                    rootview.btn_next.isClickable = true
                    rootview.btn_next.isEnabled = true
                    counter = counter!! + 1
                    if (counter == TestDashboardActivity.QUE_COUNT) {
                        rootview.btn_next.setText("Submit")
                    }
                    rootview.tv_counter.text =
                        "Question No $counter / ${TestDashboardActivity.QUE_COUNT}"
                    getQuestion(counter.toString())

                }
            } else {
                rootview.edt_answer.setText("")
                imagePathList.clear()
                fileList.clear()
                fileListWeb.clear()

                imageList.clear()
                imageListWeb.clear()
                bt_save.visibility == View.GONE
//                rootview.ll_viewattach.removeAllViews()
//                rootview.ll_viewattachweb.removeAllViews()
                rootview.ll_attach.removeAllViews()
//                rootview.ll_attach_web.removeAllViews()
                MachineTestFragment.newInstance(counter)
                counter = counter!! + 1

                rootview.tv_counter.text =
                    "Question No $counter / ${TestDashboardActivity.QUE_COUNT}"

            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


    }

    private fun stopMedia() {
        try {
            mediaRecorder!!.stop()
        } catch (e: Exception) {
        }
    }

    private fun startRecording() {
        mediaRecorderReady()
        try {
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startCountdownTimer()
    }


    private fun startCountdownTimer() {
        count = 0L
        countDownTimer = object : CountDownTimer(10800000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // Used for formatting digit to be in 2 digits only
                count = count!! + 1
                tvTimers!!.text = ExtraUtils.convertSecondToHHMMString(count!!)
            }

            // When the task is over it will print 00:00:00 there
            override fun onFinish() {
                tvTimers!!.text = "00:00:00"
            }
        }.start()
    }

    private fun mediaRecorderReady() {
        val currentTime = System.currentTimeMillis()
        val extBaseDir = activity!!.externalCacheDir
        val file = File(extBaseDir!!.absolutePath + "/SeekTest/" + "")
        if (!file.exists()) {
            if (!file.mkdirs()) {
            }
        }
        audioFilename = "$file/myRecording.mp3"
        Log.e("TAG", "mediaRecorderReady: $audioFilename")
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        mediaRecorder!!.setOutputFile(audioFilename)
    }

    companion object {
        private const val ARG_COUNT = "param1"
        fun newInstance(counter: Int?): MachineTestFragment {
            Log.e("TAG", "newInstance: " + counter)
            val fragment = MachineTestFragment()
            val args = Bundle()
            args.putInt(ARG_COUNT, counter!!)
            fragment.arguments = args
            return fragment
        }
    }
}