package com.seekex.hrmodule.trainingtest

import android.net.Uri

public class ImageDTO(
    var image: String, var id: String, var addedfrom: String, var path: String){

    fun getUri(): Uri {
        return  Uri.parse(image)
    }
}