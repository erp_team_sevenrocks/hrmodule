package com.seekex.hrmodule.trainingtest

import com.google.gson.annotations.SerializedName

class MacMcqDTO(answer: String?) {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("training_question_id")
    var training_question_id: String? = ""

    @SerializedName("option")
    var option: String? = null

    @SerializedName("status")
    var status: String? = null

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as MacMcqDTO

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id?.hashCode() ?: 0
    }

}