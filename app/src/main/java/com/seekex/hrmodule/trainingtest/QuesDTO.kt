package com.seekex.hrmodule.trainingtest

import com.google.gson.annotations.SerializedName

class QuesDTO  {
    @SerializedName("TrainingQuestion")
    var TrainingQuestion: MacTestDTO =MacTestDTO()
    @SerializedName("TrainingQuestionMcqOption")
    var TrainingQuestionMcqOption: ArrayList<MacMcqDTO> = ArrayList<MacMcqDTO>()

    @SerializedName("TrainingQuestionMediaFile")
    var TrainingQuestionMediaFile: ArrayList<MacFileDTO> =  ArrayList<MacFileDTO>()
    @SerializedName("TrainingTestUserMcqAnswer")
    var TrainingTestUserMcqAnswer: ArrayList<AnswerMcqDTO> = ArrayList<AnswerMcqDTO>()
    @SerializedName("TrainingTestUserMediaAnswer")
    var TrainingTestUserMediaAnswer: ArrayList<AnswerMcqDTO> =  ArrayList<AnswerMcqDTO>()
    @SerializedName("TrainingTestUserTextAnswer")
    var TrainingTestUserTextAnswer: ArrayList<AnswerMcqDTO> =  ArrayList<AnswerMcqDTO>()


}