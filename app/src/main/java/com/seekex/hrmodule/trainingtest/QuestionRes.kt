package com.seekex.trainingtestapp.login

import com.seekx.webService.models.BaseResponse
import com.google.gson.annotations.SerializedName
import com.seekex.hrmodule.trainingtest.QuesDTO

class QuestionRes : BaseResponse() {
    @SerializedName("data")
    var data: ArrayList<QuesDTO>? = null

    @SerializedName("total_pages")
    var total_pages: String? = null
    @SerializedName("current_page")
    var current_page: String? = null
    @SerializedName("limit")
    var limit: String? = null
}