package com.seekex.hrmodule.trainingtest

import com.google.gson.annotations.SerializedName
import com.seekex.hrmodule.AppConstants

 public class MacFileDTO {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("training_question_id")
    var machine_test_question_id: String? = ""

    @SerializedName("url")
    var url: String? = null

    @SerializedName("created")
    var created: String? = null

    @SerializedName("type")
    var type: String? = null

    fun showImage(): Boolean {
        if (type.equals(AppConstants.QUE_IMAGE)) {
            return true
        }
        return false
    }

    fun showVideo(): Boolean {
        if (type.equals(AppConstants.QUE_VIDEO)) {
            return true
        }
        return false
    }

    fun showFile(): Boolean {
        if (type.equals(AppConstants.QUE_FILE)) {
            return true
        }
        return false
    }

    fun showAudio(): Boolean {
        if (type.equals(AppConstants.QUE_AUDIO)) {
            return true
        }
        return false
    }

}