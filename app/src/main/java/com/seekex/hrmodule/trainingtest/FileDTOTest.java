package com.seekex.hrmodule.trainingtest;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.Objects;

public class FileDTOTest extends BaseResponse {

    @SerializedName("name")
    private String name;
    @SerializedName("path")
    private String path;
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileDTOTest fileDTO = (FileDTOTest) o;
        return Objects.equals(path, fileDTO.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }


    public FileDTOTest(String name, String path, String id) {
        this.name = name;
        this.path = path;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
