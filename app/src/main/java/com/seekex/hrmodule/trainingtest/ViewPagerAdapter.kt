package com.seekex.hrmodule.trainingtest

import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.seekex.trainingtestapp.MachineTestFragment

class ViewPagerAdapter(fragmentActivity: FragmentActivity, ss: String) :
    FragmentStateAdapter(fragmentActivity) {

    var ss = ss
    var pos: Int = 0
    override fun createFragment(position: Int): Fragment {
        pos = position
        return MachineTestFragment.newInstance(position)
    }

    fun getCurrentPos(): Int {
        return pos
    }

    override fun getItemCount(): Int {
        Log.e("TAG", "getItemCount: " + ss)
        return ss.toInt()
    }

    companion object {
//        private const val CARD_ITEM_SIZE = LoginActivity.Qu
    }
}