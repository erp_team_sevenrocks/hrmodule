package com.seekex.trainingtestapp

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekex.hrmodule.R
import com.seekex.hrmodule.trainingtest.PasscodeReq
import com.seekex.hrmodule.trainingtest.PasscodeReq2
import com.seekex.hrmodule.utils.ActivityUtils
import com.seekex.trainingtestapp.login.PasscodeResponse
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ValidationUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack

import kotlinx.android.synthetic.main.passcodeactivity.*

class PasscodeActivity : AppCompatActivity() {
    lateinit var pref: Preferences
    lateinit var apiImp: ApiImp
    lateinit var dialogUtils: DialogUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.passcodeactivity)

        init()
        setListener()

    }

    private fun init() {
        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)
        pref = Preferences(this)

    }


    private fun setListener() {

        login_button.setOnClickListener {

//            ActivityUtils.navigate(this@LoginActivity, DashboardActivity::class.java, false)


            apiImp?.showProgressBar()
            if (check())
                validateUser(
                    ExtraUtils.getVal(et_pass)
                )
            else
                apiImp?.cancelProgressBar()
        }


    }

    fun validateUser(pass: String) {
        val loginReq = PasscodeReq()
        val loginReq2 = PasscodeReq2()

        loginReq2.passcode = pass
        loginReq2.user_id = pref.get(AppConstants.uid)
        loginReq.data = loginReq2
        loginReq.service_name = ApiUtils.PASSCODE
        loginReq.token = pref?.get(AppConstants.token)


        apiImp.passcodeverification(loginReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("TAG", "onSuccess: ")

                if (status) {
                    val detail = any as PasscodeResponse
                    pref.setBoolean(AppConstants.isLogin, true)
                    pref.set(AppConstants.cand_id, detail.data!!.user_id!!)
                    pref.set(AppConstants.test_id, detail.data!!.training_test_id!!)
                    pref.set(AppConstants.que_count, detail.data!!.total_count!!)
                    pref.set(AppConstants.timer, detail.data!!.test_timer!!)
                    QUE_COUNT = detail.data!!.total_count!!

                    ActivityUtils.navigate(
                        this@PasscodeActivity,
                        TestDashboardActivity::class.java,
                        false
                    )


//                    ActivityUtils.navigate(this@PasscodeActivity, StartingActivity::class.java, false)
                }
            }

        })

    }

    override fun onResume() {
        super.onResume()
    }

    private fun check(): Boolean {
        return when {

            ValidationUtils.isEmpty(et_pass) -> {
                dialogUtils.showAlert(getString(R.string.err_password))
                false
            }
            else -> true
        }

    }

    companion object {

        @JvmStatic
        var QUE_COUNT: String = ""

    }
}