package com.seekex.trainingtestapp

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentPagerAdapter
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekex.hrmodule.R
import com.seekex.hrmodule.activities.DashboardActivity
import com.seekex.hrmodule.login.LoginActivity
import com.seekex.hrmodule.trainingtest.ViewPagerAdapter

import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.utils.DialogUtils
import com.seekx.webService.ApiImp
import kotlinx.android.synthetic.main.dashboardactivity.*
import kotlinx.android.synthetic.main.dashboardactivity_new.*


class TestDashboardActivity : AppCompatActivity() {
    private var ss: String = ""
    lateinit var pref: Preferences
    lateinit var dialogUtils: DialogUtils
    var adapterViewPager: FragmentPagerAdapter? = null
    var apiImp: ApiImp? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboardactivity_new)
        pref = Preferences(this)
        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)

        ss = pref.get(AppConstants.que_count)
        QUE_COUNT = ss.toInt()
        Log.e("TAG", "onCreate: "+ QUE_COUNT )
        viewPager2.setAdapter(createCardAdapter())
        viewPager2.isUserInputEnabled = false

    }

    private fun createCardAdapter(): ViewPagerAdapter? {
        return ViewPagerAdapter(this@TestDashboardActivity, ss)
    }


    override fun onBackPressed() {
//        super.onBackPressed()
        dialogUtils.showAlertWithCallBack("Do you want to Submit test ", "Ok", "Cancel", object :
            DialogeUtilsCallBack {
            override fun onDoneClick(clickStatus: Boolean) {
                if (clickStatus) {
                    goToLogin()
                }
            }

        })


    }

    fun goToLogin() {
        val i = Intent(this@TestDashboardActivity, DashboardActivity::class.java)
// set the new task and clear flags
        i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK


        startActivity(i)
    }

    companion object {

        @JvmStatic
        var QUE_COUNT: Int = 0
    }

}