package com.seekex.hrmodule.trainingtest

import com.seekex.hrmodule.grievance.ImagesGRDTO


data class AnswerDTO(
    var user_id: String,
    var type: String,
    var question_id: String,
    var answer_mcq: String,
    var answer_text: String,
    var answer_url: ArrayList<ImagesGRDTO>?
) {
    constructor() : this("","","","","",null)

}
