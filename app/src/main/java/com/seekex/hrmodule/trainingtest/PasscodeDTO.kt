package com.seekex.hrmodule.trainingtest

import com.google.gson.annotations.SerializedName
import com.seekx.webService.models.BaseResponse

class PasscodeDTO : BaseResponse() {
    @SerializedName("user_id")
    var user_id: String? =null
    @SerializedName("total_count")
    var total_count: String? =""

    @SerializedName("training_test_id")
    var training_test_id: String? = null

    @SerializedName("test_timer")
    var test_timer: String? = null
}