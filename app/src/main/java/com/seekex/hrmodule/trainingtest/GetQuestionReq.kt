package com.seekx.webService.models.apiRequest

import com.seekx.webService.models.RequestBase

data class GetQuestionReq(
    var training_test_id: String?,var user_id: String?,var page: String?
):RequestBase(){

    constructor():this("","","")
}
