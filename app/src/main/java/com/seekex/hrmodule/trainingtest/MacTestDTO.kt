package com.seekex.hrmodule.trainingtest

import com.google.gson.annotations.SerializedName

class MacTestDTO {
    @SerializedName("id")
    var id: String? = null

    @SerializedName("training_test_id")
    var training_test_id: String? = ""

    @SerializedName("name")
    var name: String? = null

    @SerializedName("score")
    var score: String? = null

    @SerializedName("type")
    var type: String? = null

    @SerializedName("created_by")
    var created_by: String? = null

    @SerializedName("created")
    var created: String? = null

    @SerializedName("is_edit")
    var is_edit: String? = null
}