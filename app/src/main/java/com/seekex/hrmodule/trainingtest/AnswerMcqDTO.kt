package com.seekex.hrmodule.trainingtest

import com.google.gson.annotations.SerializedName

class AnswerMcqDTO {
    @SerializedName("id")
    var id: String? = ""

//    @SerializedName("user_id")
//    var user_id: String? = ""
    @SerializedName("answer")
    var answer: String? = ""
    @SerializedName("created")
    var created: String? = ""
    @SerializedName("type")
    var type: String? = ""


}