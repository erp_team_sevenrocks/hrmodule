package com.seekex.trainingtestapp.login

import com.seekx.webService.models.BaseResponse
import com.google.gson.annotations.SerializedName
import com.seekex.hrmodule.trainingtest.PasscodeDTO

class PasscodeResponse : BaseResponse() {
    @SerializedName("data")
    var data: PasscodeDTO? = null

    @SerializedName("token")
    var token: String? = null
}