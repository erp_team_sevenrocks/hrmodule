package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.recyclerview.widget.RecyclerView
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.AllKraSkillAdapterBinding
import com.seekex.hrmodule.databinding.AllKraSkillAdapterNewBinding
import com.seekex.hrmodule.databinding.ImagelistitemRewardBinding
import com.seekex.hrmodule.grievance.ImagesDTORES
import com.seekex.hrmodule.kra.AllKraListUSkillssRes
import com.seekex.hrmodule.kra.AllKraListUserAssessmentRes
import com.seekex.hrmodule.kra.KraActivity
import com.seekex.hrmodule.markrewarks.RewardsActivity
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*


class AllSkillKraAdapter2(
    private val context: KraActivity?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<AllSkillKraAdapter2.ViewHolder>() {
    private val kraActivity: KraActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<AllKraListUSkillssRes> = ArrayList()

    fun setDataValues(items: ArrayList<AllKraListUSkillssRes>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        kraActivity = context as KraActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AllKraSkillAdapterNewBinding.inflate(inflater)


        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: AllKraSkillAdapterNewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: AllKraListUSkillssRes) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}