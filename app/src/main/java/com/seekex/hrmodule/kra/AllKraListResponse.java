
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.RewardListDTO;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class AllKraListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<AllKraListDTO> data;

    public ArrayList<AllKraListDTO> getData() {
        return data;
    }

    public void setData(ArrayList<AllKraListDTO> data) {
        this.data = data;
    }


}

