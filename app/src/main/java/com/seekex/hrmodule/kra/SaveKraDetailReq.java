package com.seekex.hrmodule.kra;

import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class SaveKraDetailReq extends RequestBase {

    private ArrayList<InputDataRequest> data;
    private String user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public ArrayList<InputDataRequest> getData() {
        return data;
    }

    public void setData(ArrayList<InputDataRequest> data) {
        this.data = data;
    }
}
