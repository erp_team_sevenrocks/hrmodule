package com.sevenrocks.taskapp.appModules.markrewarks


import android.content.Context
import android.media.MediaPlayer
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.databinding.SkillAdapterBinding
import com.seekex.hrmodule.kra.*
import kotlin.collections.ArrayList

class SkillAdapter( private var mainItems: ArrayList<KraListResponse2>, private var pos:Int,
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<SkillAdapter.ViewHolder>() {
    val kraActivity: KraActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<SkillDataResponse2> = ArrayList()
    private var mItems: ArrayList<KraListResponse2> = ArrayList()
    private var instanceList: ArrayList<SkillInsanceResponse> = ArrayList()
    private var moduleList: ArrayList<SkillModuleResponse> = ArrayList()

    private lateinit var adapterinstances: InstancesAdapter
    private lateinit var adaptermodule: ModuleAdapter

    fun setDataValues(items: ArrayList<SkillDataResponse2>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        kraActivity = context as KraActivity?
       mItems = mainItems!!
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SkillAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.skrd1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model!!, "1", position)
                binding.skrd2.isChecked = false
                binding.skrd3.isChecked = false
                binding.skrd4.isChecked = false
                binding.skrd5.isChecked = false

            }
        }
        binding.skrd2.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model!!, "2", position)
                binding.skrd1.isChecked = false
                binding.skrd3.isChecked = false
                binding.skrd4.isChecked = false
                binding.skrd5.isChecked = false


            }
        }
        binding.skrd3.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model!!, "3", position)
                binding.skrd2.isChecked = false
                binding.skrd1.isChecked = false
                binding.skrd4.isChecked = false
                binding.skrd5.isChecked = false
            }
        }
        binding.skrd4.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model!!, "4", position)
                binding.skrd2.isChecked = false
                binding.skrd3.isChecked = false
                binding.skrd1.isChecked = false
                binding.skrd5.isChecked = false

            }
        }
        binding.skrd5.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model!!, "5", position)
                binding.skrd2.isChecked = false
                binding.skrd3.isChecked = false
                binding.skrd1.isChecked = false
                binding.skrd4.isChecked = false

            }
        }


        return ViewHolder(binding)
    }

    private fun setRadioValues(model: SkillDataResponse2, radioid: String, position: Int) {
        var ss= InputDataRequestSkills()

        if (mItems.get(pos).dataRequest.skillAssement == null) {
            mItems.get(pos).dataRequest.skillAssement= ArrayList()

            ss.rating=radioid
            ss.skill_id=model.id
            mItems.get(pos).dataRequest.skillAssement.add(ss)

        } else {
            ss.rating=radioid
            ss.skill_id=model.id
            mItems.get(pos).dataRequest.skillAssement.add(ss)
        }
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(items[position])
    }

    inner class ViewHolder(val binding: SkillAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SkillDataResponse2) {
            binding.model = item
            binding.executePendingBindings()
            instanceList.clear()
            moduleList.clear()
            adapterinstances = InstancesAdapter(kraActivity, object : AdapterListener {
                override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                }
            })
            binding.recShowinstances.setLayoutManager(LinearLayoutManager(kraActivity))
            binding.recShowinstances.adapter = adapterinstances
            Log.e("TAG", "instancesize: " + items.get(position).instance.size)
            instanceList.addAll(items.get(position).instance)
            adapterinstances!!.setDataValues(instanceList)
            adapterinstances!!.notifyDataSetChanged()


            adaptermodule = ModuleAdapter(kraActivity, object : AdapterListener {
                override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                }
            })
            binding.recReccommendlist.setLayoutManager(LinearLayoutManager(kraActivity))
            binding.recReccommendlist.adapter = adaptermodule
            Log.e("TAG", "instancesize: " + items.get(position).instance.size)
            moduleList.addAll(items.get(position).module)
            adaptermodule!!.setDataValues(moduleList)
            adaptermodule!!.notifyDataSetChanged()


        }

    }

}