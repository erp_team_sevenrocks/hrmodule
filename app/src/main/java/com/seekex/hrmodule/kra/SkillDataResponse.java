
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.RewardListDTO;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class SkillDataResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<SkillDataResponse2> data;

    public ArrayList<SkillDataResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<SkillDataResponse2> data) {
        this.data = data;
    }


}

