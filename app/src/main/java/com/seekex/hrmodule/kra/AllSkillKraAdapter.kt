package com.sevenrocks.taskapp.appModules.markrewarks


import android.media.MediaPlayer
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.AllKraSkillAdapterBinding
import com.seekex.hrmodule.kra.*
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.play_audio.*
import java.util.*


class AllSkillKraAdapter(
    private val context: KraActivity?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<AllSkillKraAdapter.ViewHolder>() {
    private val kraActivity: KraActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<AllKraListUserAssessmentRes> = ArrayList()

    fun setDataValues(items: ArrayList<AllKraListUserAssessmentRes>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        kraActivity = context as KraActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AllKraSkillAdapterBinding.inflate(inflater)


        binding.txtReward.setOnClickListener {
            showReward(binding.model!!)
        }
        binding.txtStrike.setOnClickListener {
            showStrike(binding.model!!)
        }
        return ViewHolder(binding)
    }

    private fun showReward(model: AllKraListUserAssessmentRes) {
        val getMasterRequest = GetSkillsDataReq()
        getMasterRequest.user_id =model!!.user_id
        getMasterRequest.kra_id = model!!.id

        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GETREWARDNAME

        kraActivity!!.apiImp.getRewardText(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardnamesResponse
                    if (ss.names.length > 0) {
                        kraActivity.dialogUtil.showCustomAlert(
                            ss.names,
                            "Reward List names",
                            "ok",
                            object : DialogeUtilsCallBack {
                                override fun onDoneClick(clickStatus: Boolean) {
                                }

                            })

                    } else {
                        Toast.makeText(kraActivity, "No Names to show", Toast.LENGTH_SHORT).show()
                    }

                }

            }

        })
    }

    private fun showStrike(model: AllKraListUserAssessmentRes) {
        val getMasterRequest = GetSkillsDataReq()
        getMasterRequest.user_id = model!!.user_id
        getMasterRequest.kra_id = model!!.id

        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GETSTRIKENAME

        kraActivity!!.apiImp.getRewardText(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardnamesResponse
                    if (ss.names.length > 0) {
                    kraActivity.dialogUtil.showCustomAlert(
                        ss.names,
                        "Strike List names",
                        "ok",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                            }

                        })
                    } else {
                        Toast.makeText(kraActivity, "No Names to show", Toast.LENGTH_SHORT).show()
                    }
                }

            }

        })
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: AllKraSkillAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: AllKraListUserAssessmentRes) {
            binding.model = item
            binding.executePendingBindings()

            var adapterskills =
                AllSkillKraAdapter2(kraActivity, object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                    }
                })
            binding.skillsRec.setLayoutManager(
                LinearLayoutManager(
                    kraActivity,
                    LinearLayoutManager.VERTICAL,
                    true
                )
            )
            binding.skillsRec.adapter = adapterskills
            adapterskills.setDataValues(items.get(position).skillAssessment)
            adapterskills.notifyDataSetChanged()
        }

    }

}