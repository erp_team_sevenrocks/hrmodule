package com.sevenrocks.taskapp.appModules.markrewarks


import android.content.Context
import android.media.MediaPlayer
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.ModuleAdapterBinding
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.markrewarks.DropdownModel
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.play_audio.*
import java.util.*

class ModuleAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<ModuleAdapter.ViewHolder>() {
    private val kraActivity: KraActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<SkillModuleResponse> = ArrayList()


    fun setDataValues(items: ArrayList<SkillModuleResponse>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        kraActivity = context as KraActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ModuleAdapterBinding.inflate(inflater)
        Log.e("TAG", "in instance adapter   : ")

        binding.userCardView.setOnClickListener {
            sendRecommendation(binding.model!!)

        }


        return ViewHolder(binding)
    }

    private fun sendRecommendation(model: SkillModuleResponse) {
        val getMasterRequest = SendRecommdationReq()
        val aa = SendRecommdationReq().SendRecommdationReq2()
        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SENDRECOOMENDATION
        aa.user_id = kraActivity?.pref?.get(AppConstants.uid)
        aa.module_id = model.tm.module_id
        getMasterRequest.data=aa
        kraActivity!!.apiImp.sendRecoomedataion(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    items.remove(model)
                    notifyDataSetChanged()
                    Toast.makeText(kraActivity, "Recommendation Send", Toast.LENGTH_SHORT).show()
                }

            }

        })
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        try {
        } catch (e: Exception) {
        }

        holder.bind(items[position])
    }

    inner class ViewHolder(val binding: ModuleAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: SkillModuleResponse) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}