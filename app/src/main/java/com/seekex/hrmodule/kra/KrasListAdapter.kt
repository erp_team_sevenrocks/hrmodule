package com.sevenrocks.taskapp.appModules.markrewarks


import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.widget.AdapterView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.KrasDataAdapterBinding
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.markrewarks.*
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.markrewarks.KraAssismentModule.Companion.reasonList
import java.util.*

class KrasListAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<KrasListAdapter.ViewHolder>() {
    private var reasonId: Boolean? = false
    private val kraActivity: KraActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<KraListResponse2> =  java.util.ArrayList()
    private var skillDataList: ArrayList<SkillDataResponse2> = ArrayList()
    private lateinit var adapterReason: SpinnerAdapter_Users
    private lateinit var adapterskills: SkillAdapter

    fun setDataValues(items: ArrayList<KraListResponse2>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        kraActivity = context as KraActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    @SuppressLint("ClickableViewAccessibility")
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = KrasDataAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.edtKrareason.addTextChangedListener(object : TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                // TODO Auto-generated method stub
            }

            override fun beforeTextChanged(
                s: CharSequence?,
                start: Int,
                count: Int,
                after: Int
            ) {

                // TODO Auto-generated method stub
            }

            override fun afterTextChanged(s: Editable) {

                // filter your list from your input
                if (s.length > 0) {

                    if (items.get(position).dataRequest == null) {
                        items.get(position).dataRequest= InputDataRequest()
                        items.get(position).dataRequest.description=
                            binding.edtKrareason.text.toString()
                    } else {
                        items.get(position).dataRequest.description= binding.edtKrareason.text.toString()

                    }
                }


                //you can use runnable postDelayed like 500 ms to delay search text
            }
        })
        binding.spinReason.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                pos: Int, id: Long
            ) {
                val user: DropdownModel = adapterReason.getItem(pos)!!
                Log.e("TAG", "onItemSelected: " + user.next)
                reasonId = user.next

                setReasonData(user.id, position)
                if (reasonId as Boolean? == true) {
                    binding.recReasonskilllist.visibility = View.VISIBLE
                    getSkillsData(binding.model)

                } else {
                    binding .recReasonskilllist.visibility = View.GONE
                }

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        binding.rd1.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model, "1", position)

                binding.rd2.isChecked = false
                binding.rd3.isChecked = false
                binding.rd4.isChecked = false
                binding.rd5.isChecked = false
                binding.llReasons.visibility = View.VISIBLE
                binding.llSkillset.visibility = View.VISIBLE
            }
        }
        binding.rd2.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model, "2", position)

                binding.rd1.isChecked = false
                binding.rd3.isChecked = false
                binding.rd4.isChecked = false
                binding.rd5.isChecked = false
                binding.llReasons.visibility = View.VISIBLE
                binding.llSkillset.visibility = View.VISIBLE

            }
        }
        binding.rd3.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValues(binding.model, "3", position)
                binding.rd2.isChecked = false
                binding.rd1.isChecked = false
                binding.rd4.isChecked = false
                binding.rd5.isChecked = false
                binding.llReasons.visibility = View.VISIBLE
                binding.llSkillset.visibility = View.VISIBLE
            }
        }
        binding.rd4.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValuesEmpty(binding.model, "4", position)
                binding.rd2.isChecked = false
                binding.rd3.isChecked = false
                binding.rd1.isChecked = false
                binding.rd5.isChecked = false
                binding.llReasons.visibility = View.VISIBLE
                binding.llSkillset.visibility = View.GONE
            }
        }
        binding.rd5.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                setRadioValuesEmpty(binding.model, "5", position)
                binding.rd2.isChecked = false
                binding.rd3.isChecked = false
                binding.rd1.isChecked = false
                binding.rd4.isChecked = false
                binding.llReasons.visibility = View.VISIBLE
                binding.llSkillset.visibility = View.GONE
            }
        }

        return ViewHolder(binding)
    }

    private fun setReasonData(text: String?, position: Int) {

        if (items.get(position).dataRequest == null) {
            items.get(position).dataRequest= InputDataRequest()
            items.get(position).dataRequest.reason = text
        } else {
            items.get(position).dataRequest.reason = text
        }
    }

    private fun setRadioValues(model: KraListResponse2?, radioid: String, position: Int) {
        if (items.get(position).dataRequest == null) {
            items.get(position).dataRequest = InputDataRequest()
            items.get(position).dataRequest.rating = radioid
            items.get(position).dataRequest.kra_id = model!!.id
        } else {
            items.get(position).dataRequest.rating = radioid
            items.get(position).dataRequest.kra_id = model!!.id
        }
    }
    private fun setRadioValuesEmpty(model: KraListResponse2?, radioid: String, position: Int)
    {
        if (items.get(position).dataRequest == null) {
            items.get(position).dataRequest = InputDataRequest()
            items.get(position).dataRequest.rating = radioid
            items.get(position).dataRequest.kra_id = model!!.id
            items.get(position).dataRequest.skillAssement=null


        } else {
            items.get(position).dataRequest.rating = radioid
            items.get(position).dataRequest.kra_id = model!!.id
            items.get(position).dataRequest.skillAssement=null
        }
        notifyDataSetChanged()   }

    private fun getSkillsData(model: KraListResponse2?) {
        val getMasterRequest = GetSkillsDataReq()
        skillDataList.clear()
        getMasterRequest.user_id = kraActivity?.pref?.get(AppConstants.uid)
        getMasterRequest.kra_id = model!!.id

        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GETSKILLSDATA
        kraActivity!!.apiImp.getSkilsData(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    val ss = any as SkillDataResponse
                    skillDataList.addAll(ss.data)
                    adapterskills!!.setDataValues(skillDataList)
                    adapterskills!!.notifyDataSetChanged()

                }

            }

        })
    }

//    private fun getReasonSkillapi() {
//        val getMasterRequest = RequestBase()
//        reasonList.clear()
//        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
//        getMasterRequest.service_name = ApiUtils.GETREASONLIST
//        kraActivity!!.apiImp.getReasonList(getMasterRequest, object : ApiCallBack {
//            override fun onSuccess(status: Boolean, any: Any) {
//                if (status) {
//
//                    val list = any as RewardUsersResponse
//                    var ss = DropdownModel()
//                    ss.id = "0"
//                    ss.name = "Select Reason"
//
//                    reasonList.add(ss)
//                    reasonList.addAll(list.data)
//                    adapterReason.notifyDataSetChanged()
//                }
//
//            }
//
//        })
//    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bind(items[position])
//        holder.binding.spinReason.setOnItemSelectedListener(object : OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>,
//                view: View?,
//                position: Int,
//                id: Long
//            ) {
//                // On selecting a spinner item
//                Log.e("TAG", "onItemSelected: "+position )
////                    val item = parent.getItemAtPosition(position).toString()
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                // todo for nothing selected
//            }
//        })

    }

    inner class ViewHolder(val binding: KrasDataAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: KraListResponse2) {
            binding.model = item
            binding.executePendingBindings()

            adapterReason = SpinnerAdapter_Users(kraActivity, reasonList)
            binding.spinReason.setAdapter(adapterReason)
            adapterskills = SkillAdapter( items,adapterPosition,kraActivity, object : AdapterListener {
                override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                }
            })
            binding.recReasonskilllist.setLayoutManager(LinearLayoutManager(kraActivity))
            binding.recReasonskilllist.adapter = adapterskills
//            getReasonSkillapi()

        }

    }

}