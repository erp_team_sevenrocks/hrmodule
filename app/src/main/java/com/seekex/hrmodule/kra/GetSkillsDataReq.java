package com.seekex.hrmodule.kra;

import com.seekex.hrmodule.markrewarks.RewardFinalAudioSaveReq2;
import com.seekx.webService.models.RequestBase;

public class GetSkillsDataReq extends RequestBase {

    private String  kra_id;
    private String  user_id;

    public String getKra_id() {
        return kra_id;
    }

    public void setKra_id(String kra_id) {
        this.kra_id = kra_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
