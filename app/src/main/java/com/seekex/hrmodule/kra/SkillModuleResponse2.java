package com.seekex.hrmodule.kra;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class SkillModuleResponse2 extends BaseResponse {

    @SerializedName("name")
    private String  name;
    @SerializedName("module_id")
    private String  module_id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModule_id() {
        return module_id;
    }

    public void setModule_id(String module_id) {
        this.module_id = module_id;
    }
}
