
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class AllKraListUSkillssRes extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("user_kra_assessment_id")
    private String user_kra_assessment_id;
    @SerializedName("skill_id")
    private String skill_id;
    @SerializedName("rating")
    private String rating;
    @SerializedName("skill")
    private String skill;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_kra_assessment_id() {
        return user_kra_assessment_id;
    }

    public void setUser_kra_assessment_id(String user_kra_assessment_id) {
        this.user_kra_assessment_id = user_kra_assessment_id;
    }

    public String getSkill_id() {
        return skill_id;
    }

    public void setSkill_id(String skill_id) {
        this.skill_id = skill_id;
    }
    public String getRatingtxt() {
        return "Rating: "+rating;
    }
    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
    public String getSkilltxt() {
        return "Skill: "+skill;
    }
    public String getSkill() {
        return skill;
    }

    public void setSkill(String skill) {
        this.skill = skill;
    }
}

