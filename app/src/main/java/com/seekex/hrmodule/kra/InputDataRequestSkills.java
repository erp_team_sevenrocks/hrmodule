package com.seekex.hrmodule.kra;

import com.seekx.webService.models.RequestBase;

public class InputDataRequestSkills extends RequestBase {

    public String skill_id;
    public String rating;

        public String getSkill_id() {
            return skill_id;
        }

        public void setSkill_id(String skill_id) {
            this.skill_id = skill_id;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }
    }