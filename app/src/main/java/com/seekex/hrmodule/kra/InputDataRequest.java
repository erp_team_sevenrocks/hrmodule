package com.seekex.hrmodule.kra;

import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class InputDataRequest extends RequestBase {

    public String kra_id;
    public String rating;
    public String description;
    public String reason;
    public ArrayList<InputDataRequestSkills> skillAssement;


    public ArrayList<InputDataRequestSkills> getSkillAssement() {
        return skillAssement;
    }

    public void setSkillAssement(ArrayList<InputDataRequestSkills> skillAssement) {
        this.skillAssement = skillAssement;
    }

    public String getKra_id() {
        return kra_id;
    }

    public void setKra_id(String kra_id) {
        this.kra_id = kra_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }


}