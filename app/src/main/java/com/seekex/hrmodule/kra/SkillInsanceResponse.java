package com.seekex.hrmodule.kra;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class SkillInsanceResponse extends BaseResponse {

    @SerializedName("tm")
    private SkillModuleResponse2 tm;
    @SerializedName("ti")
    private SkillInstanceResponseTi ti;
    @SerializedName("u")
    private SkillInstanceResponseU u;

    @SerializedName("score")
    private String score;


    public String getText() {
        return tm.getName()+" "+u.getFirstname()+" "+u.getLastname()+" on "+ti.getStart_time()+"\n"+score;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    public SkillInstanceResponseTi getTi() {
        return ti;
    }

    public void setTi(SkillInstanceResponseTi ti) {
        this.ti = ti;
    }

    public SkillInstanceResponseU getU() {
        return u;
    }

    public void setU(SkillInstanceResponseU u) {
        this.u = u;
    }


    public SkillModuleResponse2 getTm() {
        return tm;
    }

    public void setTm(SkillModuleResponse2 tm) {
        this.tm = tm;
    }
}
