
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class AllKraListUserAssessmentRes extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("user_assessment_id")
    private String user_assessment_id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("reason")
    private String reason;
    @SerializedName("rating")
    private String rating;
    @SerializedName("description")
    private String description;
    @SerializedName("created_by")
    private String created_by;
    @SerializedName("reward")
    private String reward;
    @SerializedName("strike")
    private String strike;
    @SerializedName("kra")
    private String kra;
    @SerializedName("created")
    private String created;
    @SerializedName("SkillAssessment")
    private ArrayList<AllKraListUSkillssRes> SkillAssessment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_assessment_id() {
        return user_assessment_id;
    }

    public void setUser_assessment_id(String user_assessment_id) {
        this.user_assessment_id = user_assessment_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getReasontxt() {
        return "Reason: "+reason;
    }
    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
    public String getRatingtxt() {
        return "Rating: "+rating;
    }
    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
    public String getDescriptiontxt() {
        return "Description: "+description;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }
    public String getRewardtxt() {
        return "Reward: "+reward;
    }
    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }
    public String getStriketxt() {
        return "Strike: "+strike;
    }

    public String getStrike() {
        return strike;
    }

    public void setStrike(String strike) {
        this.strike = strike;
    }
    public String getKratxt() {
        return "Kra: "+kra;
    }
    public String getKra() {
        return kra;
    }

    public void setKra(String kra) {
        this.kra = kra;
    }
    public String getCreatedtxt() {
        return "Created On: "+created;

    }
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public ArrayList<AllKraListUSkillssRes> getSkillAssessment() {
        return SkillAssessment;
    }

    public void setSkillAssessment(ArrayList<AllKraListUSkillssRes> skillAssessment) {
        SkillAssessment = skillAssessment;
    }
}

