package com.seekex.hrmodule.kra;

import com.seekx.webService.models.RequestBase;

public class SendRecommdationReq extends RequestBase {

    private SendRecommdationReq2 data;

    public SendRecommdationReq2 getData() {
        return data;
    }

    public void setData(SendRecommdationReq2 data) {
        this.data = data;
    }

    public class SendRecommdationReq2 extends RequestBase {

        private String user_id;
        private String module_id;

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getModule_id() {
            return module_id;
        }

        public void setModule_id(String module_id) {
            this.module_id = module_id;
        }
    }

}
