package com.sevenrocks.taskapp.appModules.markrewarks

import android.os.Build
import android.os.Bundle
import android.support.v4.media.MediaBrowserCompat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.training.SpinnerAdapter_Department
import com.seekex.hrmodule.training.SpinnerAdapter_Type
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.kra_assessment.view.*
import java.util.*


/**
 */
class KraAssismentModule : Fragment() {
    private lateinit var dModel: KraListResponse2
    private var userId: String? = "0"
    private lateinit var adapterRewarduser: SpinnerAdapter_Users
    private lateinit var adapterKraList: KrasListAdapter

    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var kradatalist: ArrayList<KraListResponse2> = java.util.ArrayList()

    private lateinit var adapterDepartment: SpinnerAdapter_Department
    private lateinit var adapterType: SpinnerAdapter_Type

    public var kraActivity: KraActivity? = null
    public var imageList = ArrayList<ImageDTO>()


    var departmentId: String = "0"
    var tTypeId: String = "0"
    lateinit var apiImp: ApiImp

    private var permissionCallBack: PermissionCallBack? = null
    private var resumeSelectCallback: FileSelectionCallBack? = null


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        val rootView = inflater.inflate(R.layout.kra_assessment, BasicActi, false)
        kraActivity = activity as KraActivity?
        rootView.txtheader.setText("Kra Assessment")
        setAdapters(rootView)
        setlistener(rootView)
        getUsers()
        getReasonSkillapi()
        return rootView
    }

    private fun setlistener(rootView: View?) {

        rootView!!.edt_selectuser.setOnClickListener {
            Log.e("TAG", "setlistener: ")
            DataUtils.openSearchDialogeuser(
                activity!!,
                userlist,
                "Select User",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edt_selectuser.setText(ss.name)
                        userId=ss.id
                        if (!userId.equals("0")) {
                            getKtasList()
                        }
                    }

                })
        }

        rootView!!.btn_krasubmit.setOnClickListener {
            if (userId.equals("0")) {
                Toast.makeText(activity, "Select User", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            var ss = SaveKraDetailReq()
            var aa = ArrayList<InputDataRequest>()

            for (i in kradatalist.indices) {
                if (kradatalist.get(i).getDataRequest() != null) {

                    if (kradatalist.get(i).getDataRequest().description == null) {
                        Toast.makeText(
                            activity,
                            "Add description to editted Kra",
                            Toast.LENGTH_SHORT
                        ).show()
                        return@setOnClickListener
                    } else {

                        if (kradatalist.get(i).getDataRequest().description != null) {
                            aa.add(
                                kradatalist.get(i).getDataRequest()
                            )
                        }
                    }
                }
                Log.e("TAG", "222: " + Gson().toJson(aa))
            }

            if (aa.size > 0) {
                saveData(aa)
            } else {
                Toast.makeText(activity, "No Data Has been modified", Toast.LENGTH_SHORT).show()
            }

        }
    }


    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

    private fun setAdapters(rootView: View) {

        adapterKraList = KrasListAdapter(activity, object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                dModel = any1 as KraListResponse2
                when (i) {
                    2 -> {

                    }
                }
            }

        })

        rootView.rec_kradata!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.rec_kradata!!.adapter = adapterKraList
        adapterKraList.notifyDataSetChanged()

        adapterRewarduser = SpinnerAdapter_Users(activity, userlist)
        rootView.spin_krauser.setAdapter(adapterRewarduser)
        rootView.spin_krauser.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterRewarduser.getItem(position)!!
                userId = user.id
                if (!userId.equals("0")) {
                    getKtasList()
                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })


    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getReasonSkillapi() {
        val getMasterRequest = RequestBase()
        reasonList.clear()
        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GETREASONLIST
        kraActivity!!.apiImp.getReasonList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val list = any as RewardUsersResponse
                    var ss = DropdownModel()
                    ss.id = "0"
                    ss.name = "Select Reason"

                    reasonList.add(ss)
                    reasonList.addAll(list.data)
                }

            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getKtasList() {

        val getMasterRequest = GetKrasReq()

        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.user_id = userId
        getMasterRequest.service_name = ApiUtils.GETKRADATA


        kraActivity!!.apiImp.getKrasData(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as KraListResponse
                    kradatalist.addAll(ss.data)
                    adapterKraList!!.setDataValues(kradatalist)
                    adapterKraList!!.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUsers() {

        val getMasterRequest = GetSalaryReq()
        val get2 = GetSalaryReq2()

        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GUSERS
        getMasterRequest.conditions= get2



        kraActivity!!.apiImp.getkrausers(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
//                    var aa = DropdownModel()
//                    aa.id = "0"
//                    aa.name = "Select User"

//                    userlist.add(aa)

                    userlist.addAll(ss.data)
//                    adapterRewarduser.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveData(aa: ArrayList<InputDataRequest>) {

        val getMasterRequest = SaveKraDetailReq()

        getMasterRequest.token = kraActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SAVEKRA
        getMasterRequest.user_id = userId
        getMasterRequest.data = aa


        kraActivity!!.apiImp.saveeedata(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Toast.makeText(activity, "Data Updated", Toast.LENGTH_SHORT).show()
                    kraActivity!!.finish()
                }


            }

        })
    }

    companion object {

        @JvmField
        var reasonList = ArrayList<DropdownModel>()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = KraAssismentModule()
    }
}