
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.RewardListDTO;
import com.seekx.webService.models.BaseResponse;
import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class KraListResponse2 extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("strike")
    private String strike;
    @SerializedName("reward")
    private String reward;

    private InputDataRequest dataRequest;

    public InputDataRequest getDataRequest() {
        return dataRequest;
    }

    public void setDataRequest(InputDataRequest dataRequest) {
        this.dataRequest = dataRequest;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrikeText() {
        return "Strike: " + strike;
    }

    public String getStrike() {
        return strike;
    }

    public void setStrike(String strike) {
        this.strike = strike;
    }

    public String getRewardTxt() {
        return "Reward: " + reward;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



}

