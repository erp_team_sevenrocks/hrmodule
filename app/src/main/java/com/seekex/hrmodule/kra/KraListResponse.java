
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.RewardListDTO;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class KraListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<KraListResponse2> data;

    public ArrayList<KraListResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<KraListResponse2> data) {
        this.data = data;
    }


}

