
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class RewardnamesResponse extends BaseResponse {

    @SerializedName("names")
    private String names;

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }
}

