
package com.seekex.hrmodule.kra;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class AllKraListDTO extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("created")
    private String created;
    @SerializedName("user")
    private String user;
    @SerializedName("UserKraAssessment")
    private ArrayList<AllKraListUserAssessmentRes> UserKraAssessment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCreatedtxt() {
        return "Created On: "+created;
    }
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getUsertxt() {
        return "User: "+user;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public ArrayList<AllKraListUserAssessmentRes> getUserKraAssessment() {
        return UserKraAssessment;
    }

    public void setUserKraAssessment(ArrayList<AllKraListUserAssessmentRes> userKraAssessment) {
        UserKraAssessment = userKraAssessment;
    }
}
