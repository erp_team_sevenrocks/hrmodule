package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.AllKraAdapterBinding
import com.seekex.hrmodule.databinding.RewardAdapterBinding
import com.seekex.hrmodule.kra.AllKraListDTO
import com.seekex.hrmodule.kra.KraActivity
import com.seekex.hrmodule.markrewarks.RewardListDTO
import com.seekex.hrmodule.markrewarks.RewardsActivity
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*

class AllKraListAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<AllKraListAdapter.ViewHolder>() {
    private val kraActivity: KraActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<AllKraListDTO> = ArrayList()

    fun setDataValues(items: ArrayList<AllKraListDTO>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        kraActivity = context as KraActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AllKraAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.editDelete.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)
        }

        return ViewHolder(binding)
    }
    fun getFileNameFromUrl(url: String): String {
        return url.substring(url.lastIndexOf("/") + 1)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: AllKraAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: AllKraListDTO) {
            binding.model = item
            binding.executePendingBindings()

            var adapterskills =
                AllSkillKraAdapter(kraActivity, object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                    }
                })
            binding.imagesRec.setLayoutManager(
                LinearLayoutManager(
                    kraActivity,
                    LinearLayoutManager.VERTICAL,
                    true
                )
            )
            binding.imagesRec.adapter = adapterskills
            adapterskills.setDataValues(items.get(position).userKraAssessment)
            adapterskills.notifyDataSetChanged()
        }

    }

}