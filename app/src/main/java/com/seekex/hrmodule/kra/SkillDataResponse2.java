package com.seekex.hrmodule.kra;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public  class SkillDataResponse2 extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;

    @SerializedName("module")
    private ArrayList<SkillModuleResponse> module;

    @SerializedName("instance")
    private ArrayList<SkillInsanceResponse> instance;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<SkillModuleResponse> getModule() {
        return module;
    }

    public void setModule(ArrayList<SkillModuleResponse> module) {
        this.module = module;
    }

    public ArrayList<SkillInsanceResponse> getInstance() {
        return instance;
    }

    public void setInstance(ArrayList<SkillInsanceResponse> instance) {
        this.instance = instance;
    }
}
