package com.seekex.hrmodule.kra;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class SkillModuleResponse extends BaseResponse {

    @SerializedName("tm")
    private SkillModuleResponse2 tm;

    public String  getName() {
        return tm.getName();
    }

    public SkillModuleResponse2 getTm() {
        return tm;
    }

    public void setTm(SkillModuleResponse2 tm) {
        this.tm = tm;
    }
}
