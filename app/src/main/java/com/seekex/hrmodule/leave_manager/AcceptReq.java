package com.seekex.hrmodule.leave_manager;


import com.seekx.webService.models.RequestBase;

public class AcceptReq extends RequestBase {

    private AcceptReq2 conditions  ;
    private String is_accepted  ;

    public String getIs_accepted() {
        return is_accepted;
    }

    public void setIs_accepted(String is_accepted) {
        this.is_accepted = is_accepted;
    }

    public AcceptReq2 getConditions() {
        return conditions;
    }

    public void setConditions(AcceptReq2 conditions) {
        this.conditions = conditions;
    }
}
