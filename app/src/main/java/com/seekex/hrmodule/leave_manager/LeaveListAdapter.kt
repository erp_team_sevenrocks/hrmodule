package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Build
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.LeaveAdapterBinding
import com.seekex.hrmodule.grievance.ImagesDTORES
import com.seekex.hrmodule.leave_manager.LeaveActivity
import com.seekex.hrmodule.leave_manager.LeaveListDTO
import com.seekex.hrmodule.markrewarks.DeleteRewardReq
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.vendorreg.MyLeaveList
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*

class LeaveListAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<LeaveListAdapter.ViewHolder>() {
    private val leaveActivity: LeaveActivity?
    private lateinit var mediaadapter: ImageAdapterLeave

    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<LeaveListDTO> = ArrayList()

    fun setDataValues(items: ArrayList<LeaveListDTO>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        leaveActivity = context as LeaveActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LeaveAdapterBinding.inflate(inflater)


        binding.taskEdit.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 3)

        }

        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.editDelete.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)
        }
//        binding.txtPlayaudio.setOnClickListener {
//            openAudioPlayer(ApiUtils.DOMAIN + binding?.model?.audio_file!!)
//        }
//        binding.txtOpenfile.setOnClickListener {
//            Log.e("TAG", "onCreateViewHolder: "+ApiUtils.DOMAIN + binding?.model?.excel_file!! )
//            initializeDownloader(ApiUtils.DOMAIN + binding?.model?.excel_file!!)
//        }

        return ViewHolder(binding)
    }

    fun getFileNameFromUrl(url: String): String {
        return url.substring(url.lastIndexOf("/") + 1)
    }

    fun openDocument(paths: String, context: Context): Boolean {
        Log.e("FileType", "pdf hai")
        try {
            val file = File(paths)
            if (!file.exists())
                return false
            Log.e("FileType", "pdf hai")
            val uri = Uri.fromFile(file)
            var type = "*/*"

            // Check what kind of file you are trying to open, by comparing the paths with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) masterType,
            // so Android knew what application to use to open the file
            if (paths.contains(".doc") || paths.contains(".docx")) {
                // Word document
                type = "application/msword"
            } else if (paths.contains(".pdf")) {
                // PDF file
                type = "application/pdf"
            } else if (paths.contains(".ppt") || paths.contains(".pptx")) {
                // Powerpoint file
                type = "application/vnd.ms-powerpoint"
            } else if (paths.contains(".xls") || paths.contains(".xlsx")) {
                // Excel file
                type = "application/vnd.ms-excel"
            } else if (paths.contains(".zip") || paths.contains(".rar")) {
                // WAV audio file
                type = "application/x-wav"
            } else if (paths.contains(".rtf")) {
                // RTF file
                type = "application/rtf"
            } else if (paths.contains(".wav") || paths.contains(".mp3")) {
                // WAV audio file
                type = "audio/x-wav"
            } else if (paths.contains(".gif")) {
                // GIF file
                type = "image/gif"
            } else if (paths.contains(".jpg") || paths.contains(".jpeg") || paths.contains(".png")) {
                // JPG file
                type = "image/jpeg"
            } else if (paths.contains(".txt")) {
                // Text file
                type = "text/plain"
            } else if (paths.contains(".3gp") || paths.contains(".mpg") || paths.contains(".mpeg") || paths.contains(
                    ".mpe"
                ) || paths.contains(".mp4") || paths.contains(".avi")
            ) {
                // Video files
                type = "video/*"
            }

            Log.e("FileType", type)
            Log.v("FileType", paths)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, type)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        } catch (e: Exception) {
            Log.v("openDocument", e.toString())
            return false
        }
        return true
    }

    fun initializeDownloader(resume: String) {

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
        val config = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .build()

        PRDownloader.initialize(leaveActivity, config)

        val apkName = getFileNameFromUrl(resume)

        val apkPath = AppConstants.downloadDir

        val returnString = "$apkPath/$apkName"
        Log.e("openDocument", returnString)
        if (openDocument(returnString, leaveActivity!!)) {
            return
        }

        PRDownloader.download(resume, apkPath, apkName)
            .build()
            .setOnStartOrResumeListener {}
            .setOnPauseListener {}
            .setOnCancelListener {}
            .setOnProgressListener {}
            .start(object : com.downloader.OnDownloadListener {
                override fun onDownloadComplete() {
                    openDocument(returnString, leaveActivity)
                }

                override fun onError(error: com.downloader.Error?) {
                    TODO("Not yet implemented")
                }

            })
    }

    private fun openAudioPlayer(toString: String) {
        var isPb = true
        var isPaused = false
        val handler = Handler()
        var mediaPlayer = MediaPlayer()
        val dialog = Dialog(leaveActivity!!, R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_audio)

        dialog.setCancelable(true)
        dialog.setOnDismissListener {
            dialog.dismiss()
        }
        mediaPlayer.setOnBufferingUpdateListener { mp, percent ->

            dialog.seekbar.secondaryProgress = percent
        }

        mediaPlayer.setOnCompletionListener {
            isPaused = false
            dialog.seekbar.progress = 100
            Log.v("isPlaying", "complete")
            dialog.dismiss()
        }
        Handler().postDelayed(Runnable {
            dialog.pb_audio.visibility = View.INVISIBLE
        }, 3000)

        dialog.btn_play.setOnClickListener {

            if (isPb) {
                dialog.pb_audio.visibility = View.VISIBLE
            }
            isPb = false
            player = mediaPlayer
            if (!isPaused) {
                count = 0
            }

            dialog.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }


                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
                        mediaPlayer.seekTo(progress * 1000)
                    }
                }
            })


            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(toString) // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare()
                mediaPlayer.prepareAsync()
                // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start()
                dialog.seekbar.max = mediaPlayer.duration / 1000

            } else {

                mediaPlayer.pause()
            }

            updateSeekbar(dialog, handler, mediaPlayer)
        }
        dialog.btn_pause.setOnClickListener {
            isPaused = true
            mediaPlayer.pause()
        }

        dialog.setOnDismissListener {
            count = 0
            if (mediaPlayer.isPlaying || player != null) {
                mediaPlayer.stop()
                player?.stop()
            }


        }
        dialog.show()


    }

    private fun updateSeekbar(
        binding: Dialog,
        handler: Handler,
        mediaPlayer: MediaPlayer
    ) {


        val notification = Runnable {
            val mCurrentPosition = mediaPlayer.currentPosition / 1000
            binding.seekbar.progress = mCurrentPosition

            updateSeekbar(binding, handler, mediaPlayer)
        }

        handler.postDelayed(notification, 1000)


    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: LeaveAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: LeaveListDTO) {
            binding.model = item
            binding.executePendingBindings()


            if (binding.model!!.status.equals("Pending")) {
                binding.editDelete.visibility = View.VISIBLE
                binding.taskEdit.visibility = View.VISIBLE
            } else {
                binding.editDelete.visibility = View.GONE
                binding.taskEdit.visibility = View.GONE
            }
            mediaadapter =
                ImageAdapterLeave(leaveActivity, object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                        var dataModel = any1 as ImagesDTORES
                        when (i) {
                            1 -> {
                                deleteParty(items.get(position).mediaFile, mediaadapter, dataModel)
                            }
                        }
                    }
                })
            binding.imagesRec.setLayoutManager(
                LinearLayoutManager(
                    leaveActivity,
                    LinearLayoutManager.VERTICAL,
                    true
                )
            )
            binding.imagesRec.adapter = mediaadapter
            mediaadapter.setDataValues(items.get(position).mediaFile)
            mediaadapter.notifyDataSetChanged()

        }

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun deleteParty(
        mediaFile: ArrayList<ImagesDTORES>,
        adapterskills: ImageAdapterLeave,
        dataModel: ImagesDTORES
    ) {

        val reqData =
            DeleteRewardReq()


        reqData.service_name = ApiUtils.leave_request_leave_delete
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        leaveActivity?.apiImp?.deleteRecord(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        mediaFile.remove(dataModel)
                        Toast.makeText(leaveActivity!!, "Media Deleted", Toast.LENGTH_SHORT).show()
                        mediaadapter.notifyDataSetChanged()
                    }
                }

            }
        )
    }
}