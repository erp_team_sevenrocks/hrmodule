
package com.seekex.hrmodule.leave_manager;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class PdfResponse extends BaseResponse {

    @SerializedName("url")
    private String url;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}

