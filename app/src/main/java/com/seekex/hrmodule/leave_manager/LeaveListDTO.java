
package com.seekex.hrmodule.leave_manager;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class LeaveListDTO {

    @SerializedName("id")
    private String id;
    @SerializedName("user_id")
    private String user_id;
    @SerializedName("start_date")
    private String start_date;
    @SerializedName("end_date")
    private String end_date;
    @SerializedName("leave_count")
    private String leave_count;
    @SerializedName("status")
    private String status;
    @SerializedName("leave_reason")
    private String leave_reason;
    @SerializedName("approved_remarks")
    private String approved_remarks;
    @SerializedName("decline_reason")
    private String decline_reason;
    @SerializedName("is_paid")
    private String is_paid;
    @SerializedName("is_consume_in_ledger")
    private String is_consume_in_ledger;


    @SerializedName("files")
    private ArrayList<ImagesDTORES> MediaFile;

    public String checkidpaid() {
        if (is_paid.equals("false")) {
            return "Paid : No";
        } else if (is_paid.equals("true")) {
            return "Paid : Yes";
        }
        return "false";
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getTextEnd_date() {
        return "End Date: " + end_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getLeave_count() {
        return leave_count;
    }

    public String getTextLeave_count() {
        return "Leave Count: " + leave_count;
    }

    public void setLeave_count(String leave_count) {
        this.leave_count = leave_count;
    }

    public String getLeave_reason() {
        return leave_reason;
    }

    public void setLeave_reason(String leave_reason) {
        this.leave_reason = leave_reason;
    }


    public String getApproved_remarks() {
        return approved_remarks;
    }

    public void setApproved_remarks(String approved_remarks) {
        this.approved_remarks = approved_remarks;
    }

    public String getDecline_reason() {
        return decline_reason;
    }

    public void setDecline_reason(String decline_reason) {
        this.decline_reason = decline_reason;
    }

    public String getIs_paid() {
        return is_paid;
    }

    public void setIs_paid(String is_paid) {
        this.is_paid = is_paid;
    }

    public String getIs_consume_in_ledger() {
        return is_consume_in_ledger;
    }

    public void setIs_consume_in_ledger(String is_consume_in_ledger) {
        this.is_consume_in_ledger = is_consume_in_ledger;
    }

    public ArrayList<ImagesDTORES> getMediaFile() {
        return MediaFile;
    }

    public void setMediaFile(ArrayList<ImagesDTORES> mediaFile) {
        MediaFile = mediaFile;
    }
}
