package com.seekex.hrmodule.leave_manager;


import com.seekx.webService.models.RequestBase;

public class GetLEaveReq2 extends RequestBase {

    private String start_date  ;
    private String end_date  ;
    private int status  ;

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
