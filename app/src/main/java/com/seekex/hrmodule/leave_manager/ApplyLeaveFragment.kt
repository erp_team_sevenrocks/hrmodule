package com.sevenrocks.taskapp.appModules.markrewarks

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.MediaController
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.adapter.EventParamAdapter
import com.seekex.hrmodule.databinding.*
import com.seekex.hrmodule.feedback.*
import com.seekex.hrmodule.grievance.ImagesGRDTO
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.leave_manager.*
import com.seekex.hrmodule.locationfeedback.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.sop.VideoDTO
import com.seekex.hrmodule.trainingtest.FileDTOTest
import com.seekex.hrmodule.trainingtest.ImageDTO
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.utils.DialogUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.vincent.filepicker.Constant
import com.vincent.filepicker.activity.NormalFilePickActivity
import com.vincent.filepicker.filter.entity.NormalFile
import kotlinx.android.synthetic.main.add_feedback.*
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_loc_feedback.*
import kotlinx.android.synthetic.main.add_loc_feedback.view.*
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.image_view.*
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.*
import kotlinx.android.synthetic.main.add_reminder.*
import kotlinx.android.synthetic.main.add_reminder.view.*
import kotlinx.android.synthetic.main.apply_leave.*
import kotlinx.android.synthetic.main.apply_leave.view.*
import kotlinx.android.synthetic.main.apply_leave.view.addimages
import kotlinx.android.synthetic.main.apply_leave.view.addvideo
import kotlinx.android.synthetic.main.apply_leave.view.btn_addattchment
import kotlinx.android.synthetic.main.apply_leave.view.btn_apply_leave
import kotlinx.android.synthetic.main.apply_leave.view.edt_startdate
import kotlinx.android.synthetic.main.chooseactivity.*
import kotlinx.android.synthetic.main.edit_leave.view.*
import kotlinx.android.synthetic.main.filter_view.*
import kotlinx.android.synthetic.main.fragment_card.*
import kotlinx.android.synthetic.main.play_video.*
import kotlinx.android.synthetic.main.sopauditlist.*
import java.util.*


/**
 */
class ApplyLeaveFragment : Fragment() {

    private lateinit var rootView: View
    private var leaveId: String? = ""
    private var xlFilename: String = ""
    private var call_Counter = 0

    public var leaveActivity: LeaveActivity? = null
    private var excelPath: String = ""
    private var excelName: String = ""
    public var fileList = ArrayList<FileDTOTest>()
    var imagePathList: ArrayList<ImagesGRDTO> = java.util.ArrayList()
    public var videoList = ArrayList<VideoDTO>()

    lateinit var apiImp: ApiImp
    private lateinit var adapter: EventParamAdapter
    var recordlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var durationlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var typelist: ArrayList<DropdownModel> = java.util.ArrayList()
    private var durationid: String? = ""
    private var recordid: String? = ""
    private var typeid: String? = ""
    private var finaldate: String? = ""
    private val cal = Calendar.getInstance()
    public var imageList = ArrayList<ImageDTO>()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.apply_leave, BasicActi, false)
        leaveActivity = activity as LeaveActivity?
        rootView.txtheader.setText("Apply Leave")
        setlistener(rootView)

        return rootView
    }

    private fun pickVideo() {

        val options: Options = Options.init()
            .setRequestCode(110) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Video) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@ApplyLeaveFragment, options)

    }

    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@ApplyLeaveFragment, options)

    }

    private fun setlistener(rootView: View) {

        rootView.btn_addattchment.setOnClickListener {
            val intent4 = Intent(activity, NormalFilePickActivity::class.java)
            intent4.putExtra(Constant.MAX_NUMBER, 1)
            intent4.putExtra(
                NormalFilePickActivity.SUFFIX,
                arrayOf("xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf")
            )
            startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE)
        }
        rootView.addimages.setOnClickListener {
            pickImage()
        }

        rootView.addvideo.setOnClickListener {
            pickVideo()
        }
        rootView.edt_enddate.setOnClickListener {
            val dpd = DatePickerDialog(activity!!, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                rootView.edt_enddate.setText(dob)
                finaldate = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))

//            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }

        rootView.edt_startdate.setOnClickListener {
            val dpd = DatePickerDialog(activity!!, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                rootView.edt_startdate.setText(dob)
                finaldate = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))

//            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }

        rootView.btn_apply_leave.setOnClickListener {

            if (edt_startdate.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter Start date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edt_enddate.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter End date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edt_leavereason.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter Reason", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            saveData(rootView)
        }
        rootView.chk_leave.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {

            }

        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun uploadMedia(rootView: View) {
        val ss1 = UploadmedisReq()

        ss1.token = leaveActivity?.pref?.get(AppConstants.token)
        ss1.service_name = ApiUtils.leave_request_file_save
        ss1.leave_request_id = leaveId
        ss1.files = imagePathList
        leaveActivity!!.apiImp.saveMedia(ss1, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Toast.makeText(leaveActivity, "Leave Applied Successfully", Toast.LENGTH_SHORT).show()
                    leaveActivity!!.finish()
                }

            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun saveData(rootView: View) {
        val ss1 = SaveLeaveDataReq()
        val ss2 = SaveLeaveDataReq2()

        ss1.token = leaveActivity?.pref?.get(AppConstants.token)
        ss1.service_name = ApiUtils.leave_apply

        ss2.start_date = rootView.edt_startdate.text.toString()
        ss2.end_date = rootView.edt_enddate.text.toString()
        ss2.leave_reason = rootView.edt_leavereason.text.toString()
        if (rootView.chk_leave.isChecked) {
            ss2.is_paid = "1"
        } else {
            ss2.is_paid = "0"
        }

        ss1.data = ss2
        leaveActivity!!.apiImp.saveLeave(ss1, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var resdata = any as SaveLEaveResponse
                    leaveId = resdata.leave_request_id

                    if (imageList.size > 0) {
                        uploadImages()
                        return
                    } else if (videoList.size > 0) {
                        uploadVideo()
                        return
                    } else if (fileList.size > 0) {
                        saveExcelFile()
                        return
                    } else {
                        uploadMedia(rootView)
                    }

                }

            }

        })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constant.REQUEST_CODE_PICK_FILE) {
                val list: ArrayList<NormalFile> =
                    intent!!.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE)!!

                for (i in list.indices) {
                    var isExists: Boolean = false
                    excelName = list.get(i).name
                    excelPath = list.get(i).path
//                excelUri = list.get(0).uri
                    if (fileList.size > 0) {
                        for (j in fileList.indices) {
                            if (fileList.get(j).path.toLowerCase()
                                    .equals(excelPath.toLowerCase())
                            ) {
                                isExists = true
                            }
                        }
                        if (!isExists) {
                            fileList.add(
                                FileDTOTest(
                                    excelName, excelPath, ""
                                )
                            )
                        }
                    } else {
                        fileList.add(
                            FileDTOTest(
                                excelName, excelPath, ""
                            )
                        )

                    }


                }
                showAttachmentView("me", ll_attchleave!!,
                    fileList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            fileList = any as ArrayList<FileDTOTest>
                        }
                    })
            } else if (requestCode == 100) {

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!

                val fileUri = intent?.data
                imageList.add(
                    ImageDTO(
                        Uri.parse("file://" + returnValue.get(0)).toString(), "", "me", ""
                    )
                )
                showDynamicImage("me", ll_attach_web!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })
            } else if (requestCode == 110) {
                val bmThumbnail: Bitmap?

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                bmThumbnail = ThumbnailUtils.createVideoThumbnail(
                    returnValue.get(0).toString(),
                    MediaStore.Video.Thumbnails.MICRO_KIND
                )
                Log.e("TAG", "onActivityResult: $bmThumbnail ")
                videoList.add(
                    VideoDTO(bmThumbnail!!, returnValue.get(0))
                )
                showDynamicVideo(ll_attach_video!!,
                    videoList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
//                            videoList = any as ArrayList<ImageDTO>
                        }
                    })
            }
        }
    }

    fun showDynamicImage(
        addedtype: String,
        llAtach: LinearLayout,
        imageList: ArrayList<ImageDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()

        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        if (addedtype.equals("me")) {
            for (model in imageList.indices) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = ImageItemTestBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = imageList[model]

                binding.model = imageDTO

                binding.ivImage.setOnClickListener {
//                    showImageUri(imageDTO.getUri(), context)
                }

                binding.ivCut.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete Image",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {
                                    if (imageDTO.id.equals("")) {
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    } else {

//                                        deleteMediaWeb(imageDTO.id)
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    }


                                }

                            }

                        }
                    )

                }
            }
        } else {
            for (model in imageList.indices) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = ImageItemWebBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = imageList[model]

                binding.model = imageDTO

                binding.ivCut.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete Image",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {

                                    if (imageDTO.id.equals("")) {
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    } else {
//                                        deleteMediaWeb(imageDTO.id)
                                        imageList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(imageList)
                                    }

                                }

                            }

                        }
                    )

                }
            }
        }


    }

    fun showAttachmentView(
        addedtype: String,
        llAtach: LinearLayout,
        fileList: ArrayList<FileDTOTest>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)
        if (addedtype.equals("me")) {
            for (model in 0 until fileList.size) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = FileItemNewBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = fileList[model]

                binding.model = imageDTO



                binding.removefile.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete File",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {
                                    if (imageDTO.id.equals("")) {
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    } else {
//                                        deleteMediaWeb(imageDTO.id)
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    }
                                }
                            }
                        })
                }
            }
        } else {
            for (model in 0 until fileList.size) {

                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                        as LayoutInflater

                val binding = FileItemNewWebBinding.inflate(
                    inflater, llAtach,
                    true
                )

                val imageDTO = fileList[model]

                binding.model = imageDTO



                binding.removefile.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Delete File",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {
                                    if (imageDTO.id.equals("")) {
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    } else {
//                                        deleteMediaWeb(imageDTO.id)
                                        fileList.remove(imageDTO)
                                        llAtach.removeView(binding.root)
                                        selectedMasterCallback.onSelected(fileList)
                                    }
                                }
                            }
                        })
                }
            }
        }

    }

    private fun saveExcelFile() {
        leaveActivity!!.apiImp.showProgressBar()
        leaveActivity!!.apiImp.saveExcelTest(call_Counter, fileList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                leaveActivity!!.apiImp.cancelProgressBar()
                Log.e("TAG", "onSuccess: upload excel")

                if (status) {

                    var ss = any as uploadaudiores
                    xlFilename = ss.data.path + "" + ss.data.filename
                    Log.e("TAG", "onSuccess: upload excel" + xlFilename)

                    var model = ImagesGRDTO()
                    model.type = "excel"
                    model.name = xlFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < fileList.size) {
                        saveExcelFile()
                    }
                }

            }

        })


    }

    private fun uploadImages() {

        leaveActivity!!.apiImp.showProgressBar()
        leaveActivity?.apiImp?.hitApileave(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                leaveActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)


                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "image"
                    model.name = imageFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < imageList.size) {
                        uploadImages()
                    } else {
                        call_Counter = 0
                        if (videoList.size > 0) {
                            uploadVideo()
                        } else if (fileList.size > 0) {
                            saveExcelFile()
                        } else {
                            uploadMedia(rootView)
                        }
                    }
                }
            }
        })
    }

    fun showDynamicVideo(
        llAtach: LinearLayout,
        imageList: ArrayList<VideoDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = VideoItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
//                showImageUri(imageDTO.getUri(), context)
                openVideoPLayer(imageDTO.path)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Video",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    private fun uploadVideo() {

        leaveActivity!!.apiImp.showProgressBar()
        leaveActivity?.apiImp?.hitApivideo(call_Counter, videoList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                leaveActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)
                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "video"
                    model.name = imageFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < videoList.size) {
                        uploadVideo()
                    } else {
                        call_Counter = 0
                        if (fileList.size > 0) {
                            saveExcelFile()
                        } else {
                            uploadMedia(rootView)
                        }

                    }
                }
            }
        })
    }

    private fun openVideoPLayer(toString: String) {

        val dialog = Dialog(activity!!, android.R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_video)

        dialog.setCancelable(true)

        dialog.pb_video.visibility = View.VISIBLE
        val mc = MediaController(activity!!)
        mc.setAnchorView(dialog.videoview)
        mc.setMediaPlayer(dialog.videoview)
        dialog.videoview.setMediaController(mc);
        val video =
            Uri.parse(toString)
        dialog.videoview.setVideoPath(toString)
        dialog.videoview.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
            dialog.pb_video.visibility = View.GONE

            mp.isLooping = true
            dialog.videoview.start()
        })



        dialog.setOnDismissListener {
            dialog.dismiss()
        }


        dialog.btn_play.setOnClickListener {


        }
        dialog.btn_pause.setOnClickListener {

        }

        dialog.setOnDismissListener {
        }
        dialog.show()


    }

    companion object {


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = ApplyLeaveFragment()
    }
}