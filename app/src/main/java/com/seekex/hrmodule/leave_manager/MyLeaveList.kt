package com.sevenrocks.taskapp.appModules.vendorreg

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.grievance.ImagesDTORES
import com.seekex.hrmodule.leave_manager.*
import com.seekex.hrmodule.markrewarks.DeleteRewardReq
import com.seekx.utils.ExtraUtils.Companion.changeFragment
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import com.sevenrocks.taskapp.appModules.markrewarks.EditLeaveFragment
import com.sevenrocks.taskapp.appModules.markrewarks.LeaveListAdapter
import kotlinx.android.synthetic.main.leavelist.view.*
import kotlinx.android.synthetic.main.rewardlist.view.recyclerView
import kotlinx.android.synthetic.main.rewardlist.view.txtheaderre
import java.util.*


/**
 */
class MyLeaveList : Fragment() {
    private lateinit var myAdapter: LeaveListAdapter
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<LeaveListDTO> = java.util.ArrayList()
    var spin_type: String = "0"
    public var leaveActivity: LeaveActivity? = null
    private lateinit var rootView: View
    private val cal = Calendar.getInstance()


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.leavelist, BasicActi, false)
        leaveActivity = activity as LeaveActivity?

        rootView.recyclerView.setLayoutManager(
            LinearLayoutManager(activity)
        )
        rootView.txtclear.setOnClickListener {
            rootView.edt_liststartdate.setText("")
            rootView.edt_listenddate.setText("")
            page = 1
            mydataList.clear()
            getList()

        }
        rootView.spin_type?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Log.e("TAG", "onItemSelected: "+parent!!.getItemAtPosition(position).toString())
                var search_type = parent!!.getItemAtPosition(position).toString()
                when (search_type) {
                    "Pending" -> {
                        spin_type = "0";
                        mydataList.clear()
                        myAdapter.notifyDataSetChanged()
                        page = 1
                        getList()
                    }
                    "Approved" -> {
                        spin_type = "1";
                        mydataList.clear()
                        myAdapter.notifyDataSetChanged()
                        page = 1
                        getList()
                    }
                    "Declined" -> {
                        spin_type = "-1";
                        mydataList.clear()
                        myAdapter.notifyDataSetChanged()
                        page = 1
                        getList()
                    }
                }

            }

        }
        rootView.edt_liststartdate.setOnClickListener {
            val dpd = DatePickerDialog(leaveActivity!!, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                rootView.edt_liststartdate.setText(dob)
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))

//            dpd.datePicker.maxDate = System.currentTimeMillis() - 1000
            dpd.show()
        }
        rootView.edt_listenddate.setOnClickListener {
            val dpd = DatePickerDialog(leaveActivity!!, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                rootView.edt_listenddate.setText(dob)
                page = 1
                mydataList.clear()
                spin_type = "0";
                getList()
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))

            dpd.show()
        }
        rootView.txtheaderre.text = "My Leave"
        setAdapter(rootView)
        page = 1

//        rootView.edt_liststartdate.setText(DataUtils.getCurrentDateformat())
//        rootView.edt_listenddate.setText(DataUtils.getCurrentDateformat())
//        getList()
//        rootView.txtheader.text = "Rewards"


        return rootView
    }


    override fun onResume() {
        super.onResume()
        page = 1
    }


    private fun setAdapter(rootView: View) {
        myAdapter = LeaveListAdapter(activity, object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                dataModel = any1 as LeaveListDTO

                when (i) {
                    2 -> {

                        deleteParty()
                    }
                    3 -> {

                        editTask()
                    }
                }
            }

        })
        rootView.recyclerView!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.recyclerView!!.adapter = myAdapter

        rootView.recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.v("addOnScrollListener", "false")
                    getList()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }

    private fun editTask() {

        for (i in dataModel.mediaFile.indices) {
            if (dataModel.mediaFile.get(i).type.equals("image")) {
                imagelistold.add(dataModel.mediaFile.get(i))
            } else if (dataModel.mediaFile.get(i).type.equals("video")) {
                videolistold.add(dataModel.mediaFile.get(i))
            } else if (dataModel.mediaFile.get(i).type.equals("file")) {
                filelistold.add(dataModel.mediaFile.get(i))
            }

        }

        changeFragment(leaveActivity!!.supportFragmentManager, EditLeaveFragment.instance, true)

    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun deleteParty() {

        val reqData =
            DeleteRewardReq()


        reqData.service_name = ApiUtils.my_leave_delete
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        leaveActivity?.apiImp?.deleteRecord(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        mydataList.remove(dataModel)
                        Toast.makeText(activity, "Record Deleted", Toast.LENGTH_SHORT).show()
                        myAdapter.notifyDataSetChanged()
                    }
                }

            }
        )
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getList() {

        val reqData = GetLEaveReq()
        val reqData2 = GetLEaveReq2()


        reqData.service_name = ApiUtils.get_my_leave_requests
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)
        reqData.page = page;
        reqData2.start_date = rootView.edt_liststartdate.text.toString()
        reqData2.end_date = rootView.edt_listenddate.text.toString()
        reqData2.status = spin_type.toInt()
        reqData.conditions = reqData2


        leaveActivity?.apiImp?.getleavelist(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as LeaveListResponse
                        val datalist = ss.data

                        if (datalist.isEmpty()) {
                            donHit = true
                        } else {
                            mydataList.addAll(datalist)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()
                            page++
                        }
                    }
                }

            }
        )


    }


    companion object {

        @JvmStatic
        var dataModel = LeaveListDTO()

        @JvmStatic
        var imagelistold: ArrayList<ImagesDTORES> = java.util.ArrayList()

        @JvmStatic
        var videolistold: ArrayList<ImagesDTORES> = java.util.ArrayList()

        @JvmStatic
        var filelistold: ArrayList<ImagesDTORES> = java.util.ArrayList()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = MyLeaveList()
    }


}