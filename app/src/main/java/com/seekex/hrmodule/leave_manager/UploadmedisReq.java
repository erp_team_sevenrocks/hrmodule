package com.seekex.hrmodule.leave_manager;


import com.seekex.hrmodule.grievance.ImagesGRDTO;
import com.seekex.hrmodule.trainingtest.ImageDTO;
import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class UploadmedisReq extends RequestBase {

    private String leave_request_id  ;
    private ArrayList<ImagesGRDTO> files  ;

    public String getLeave_request_id() {
        return leave_request_id;
    }

    public void setLeave_request_id(String leave_request_id) {
        this.leave_request_id = leave_request_id;
    }

    public ArrayList<ImagesGRDTO> getFiles() {
        return files;
    }

    public void setFiles(ArrayList<ImagesGRDTO> files) {
        this.files = files;
    }
}
