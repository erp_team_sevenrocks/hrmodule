
package com.seekex.hrmodule.leave_manager;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.RewardListDTO;
import com.seekex.hrmodule.mysalary.SalaryListDTO;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class LeaveListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<LeaveListDTO> data;

    public ArrayList<LeaveListDTO> getData() {
        return data;
    }

    public void setData(ArrayList<LeaveListDTO> data) {
        this.data = data;
    }
}

