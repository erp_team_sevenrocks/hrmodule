package com.seekex.hrmodule.leave_manager;


import com.seekex.hrmodule.remiender.SaveDataReq2;
import com.seekx.webService.models.RequestBase;

public class SaveLeaveDataReq extends RequestBase {

    private SaveLeaveDataReq2 data  ;

    public SaveLeaveDataReq2 getData() {
        return data;
    }

    public void setData(SaveLeaveDataReq2 data) {
        this.data = data;
    }
}
