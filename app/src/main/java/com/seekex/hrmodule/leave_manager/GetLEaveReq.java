package com.seekex.hrmodule.leave_manager;


import com.seekx.webService.models.RequestBase;

public class GetLEaveReq extends RequestBase {

    private GetLEaveReq2 conditions  ;
    private int page  ;

    public GetLEaveReq2 getConditions() {
        return conditions;
    }

    public void setConditions(GetLEaveReq2 conditions) {
        this.conditions = conditions;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
