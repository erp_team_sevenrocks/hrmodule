
package com.seekex.hrmodule.leave_manager;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class SaveLEaveResponse extends BaseResponse {

    @SerializedName("leave_request_id")
    private String leave_request_id;

    public String getLeave_request_id() {
        return leave_request_id;
    }

    public void setLeave_request_id(String leave_request_id) {
        this.leave_request_id = leave_request_id;
    }
}

