package com.sevenrocks.taskapp.appModules.markrewarks

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.EditText
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.config.Configurations
import com.jaiselrahman.filepicker.model.MediaFile
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.adapter.ParticipantAdapter
import com.seekex.hrmodule.adapter.PickedlistAdapter
import com.seekex.hrmodule.databinding.ImageItemBinding
import com.seekex.hrmodule.grievance.GetDesignationReq
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.training.SpinnerAdapter_Department
import com.seekex.hrmodule.training.SpinnerAdapter_Type
import com.seekex.hrmodule.training.TrainingActivity
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import droidninja.filepicker.FilePickerConst
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.ll_attach
import kotlinx.android.synthetic.main.add_reward.ll_viewattach
import kotlinx.android.synthetic.main.add_reward.rec_showlist
import kotlinx.android.synthetic.main.add_reward.txt_excelname
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.add_reward.view.add_rewardbyn
import kotlinx.android.synthetic.main.add_reward.view.addfilerew
import kotlinx.android.synthetic.main.add_reward.view.imagesreward
import kotlinx.android.synthetic.main.add_reward.view.removefile
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.add_trainingmodule.*
import kotlinx.android.synthetic.main.add_trainingmodule.view.*
import kotlinx.android.synthetic.main.grievance_reg.view.*
import kotlinx.android.synthetic.main.image_view.*
import java.io.File
import java.util.*


/**
 */
class AddTrainingModule : Fragment() {
    private var audioFilename: String = ""
    private var rewardId: String = ""
    private var excelPath: String = ""
    private var excelName: String = ""
    private var imageFilename: String = ""
    private var xlFilename: String = ""
    private lateinit var excelUri: Uri
    private var docPaths: ArrayList<Uri> = ArrayList()
    private lateinit var adapterDepartment: SpinnerAdapter_Department
    private lateinit var adapterType: SpinnerAdapter_Type

    public var trainingActivity: TrainingActivity? = null
    public var imageList = ArrayList<ImageDTO>()

    private lateinit var adapter: ParticipantAdapter

    var departmentId: String = "0"
    var tTypeId: String = "0"
    lateinit var apiImp: ApiImp

    private var permissionCallBack: PermissionCallBack? = null
    private var resumeSelectCallback: FileSelectionCallBack? = null
    private var call_Counter = 0

    var depatmentList: ArrayList<DropdownModel> = java.util.ArrayList()
    var typeList: ArrayList<DropdownModel> = java.util.ArrayList()
    var designationList: ArrayList<DropdownModel> = java.util.ArrayList()
    var skillsList: ArrayList<DropdownModel> = java.util.ArrayList()
    var modeldesignationList = ArrayList<ParicipantModel>()
    var modelskillsList = ArrayList<ParicipantModel>()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        val rootView = inflater.inflate(R.layout.add_trainingmodule, BasicActi, false)
        trainingActivity = activity as TrainingActivity?
        rootView.txtheader.setText("Add Training Module")
        alreadySelectedNames.clear()
        namestoShow.clear()
        setListeners(rootView)
        setAdapters(rootView)

        getDepartmentList()
       getTypeList()
        getSkillsList()

        return rootView
    }



    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

    private fun setAdapters(rootView: View) {


        adapterType = SpinnerAdapter_Type(activity, typeList)
        adapterDepartment = SpinnerAdapter_Department(activity, depatmentList)


        rootView.spin_trainingtype.setAdapter(adapterType)
        rootView.spin_department.setAdapter(adapterDepartment)

        rootView.spin_trainingtype.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterType.getItem(position)!!
                tTypeId = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
        rootView.spin_department.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                departmentId = adapterView!!.getItemAtPosition(position).toString()
//                    get reason list
                if (!departmentId.equals("Select Department")) {

                    getDesignationList(departmentId)

                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })



    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getDepartmentList() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = trainingActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.DEPARTMENTLIST


        trainingActivity!!.apiImp.getDropdownList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    var aa = DropdownModel()
                    aa.id = "0"
                    aa.name = "Select Department"

                    designationList.add(aa)

                    designationList.addAll(ss.data)
                    adapterDepartment.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getSkillsList() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = trainingActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SKILLSLIST


        trainingActivity!!.apiImp.getSkillsList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse

                    skillsList.addAll(ss.data)
                    for (i in skillsList.indices) {
                        Log.e("jjg", "names: " + skillsList.get(i).name)

                        modelskillsList.add(
                            ParicipantModel(
                                skillsList.get(i).name,
                                skillsList.get(i).id,
                                skillsList.get(i).name,
                                false
                            )
                        )
                    }
                }


            }

        })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getDesignationList(departmentId: String) {

        val getMasterRequest = GetDesignationReq()

        getMasterRequest.token = trainingActivity?.pref?.get(AppConstants.token)
        getMasterRequest.user_id = departmentId
        getMasterRequest.service_name = ApiUtils.DESIGNATIONLIST


        trainingActivity!!.apiImp.getDesignatinlist(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse

                    designationList.addAll(ss.data)
                    for (i in designationList.indices) {
                        Log.e("jjg", "names: " + designationList.get(i).name)

                        modeldesignationList.add(
                            ParicipantModel(
                                designationList.get(i).name,
                                designationList.get(i).id,
                                designationList.get(i).name,
                                false
                            )
                        )
                    }
                }


            }

        })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getTypeList() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = trainingActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.TRAININGTYPELIST


        trainingActivity!!.apiImp.  getDropdownList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    var aa = DropdownModel()
                    aa.id = "0"
                    aa.name = "Select Training"

                    typeList.add(aa)

                    typeList.addAll(ss.data)
                    adapterType.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveReward() {

        if (imageList.size > 0) {
            uploadImages()
        } else if (bt_save.visibility == View.INVISIBLE) {
            saveaudiofile()
        } else if (excelName.length > 0) {

        }

    }

//    private fun saveRewardDetail() {
//
//        val saveRewardReq = SaveRewardReq()
//        val saveRewardReq2 = SaveRewardReq2()
//
//        var ss = ArrayList<RewardUserSaveReq>()
//        for (i in namestoShow.indices) {
//            var aa = RewardUserSaveReq()
//            aa.id = namestoShow.get(i).id
//            ss.add(aa)
//        }
//
//        val json = Gson().toJson(ss)
//
//        saveRewardReq2.reason = edt_rewreason.text.toString()
//        saveRewardReq2.assign_to_user_id = userId
//        saveRewardReq2.point_type_id = rewardReasonId
//        saveRewardReq2.kra = ss
//        saveRewardReq2.img_file = imageFilename
//        saveRewardReq2.xl = xlFilename
//        saveRewardReq2.audio = audioFilename
//        saveRewardReq2.name = "new"
//
//
//        if (rewardType.equals("Reward")) {
//            saveRewardReq.service_name = ApiUtils.SAVEREWARD
//
//        } else if (rewardType.equals("Strike")) {
//            saveRewardReq.service_name = ApiUtils.SAVESTRIKE
//
//        }
//        saveRewardReq.token = rewardsActivity?.pref?.get(AppConstants.token)
//        saveRewardReq.data = saveRewardReq2
//        rewardsActivity!!.apiImp.saveReward(saveRewardReq, object : ApiCallBack {
//            override fun onSuccess(status: Boolean, any: Any) {
//                if (status) {
//                    var ss = any as RewardSaveResponse
//
//                    Toast.makeText(activity, "Saved Successfully", Toast.LENGTH_SHORT)
//                        .show()
//                    rewardsActivity!!.finish()
//                }
//            }
//
//
//        })
//    }


    private fun saveaudiofile() {

//        rewardsActivity!!.apiImp.saveAudio(file_name, object : ApiCallBack {
//            override fun onSuccess(status: Boolean, any: Any) {
//                if (status) {
//                    var ss = any as uploadaudiores
//                    audioFilename = ss.data.path + "" + ss.data.filename
//                    if (excelName.length > 0) {
//                        saveExcelFile()
//                    } else {
//                        saveRewardDetail()
//
//                    }
//                }
//
//            }
//
//        })


    }

    private fun uploadImages() {


        trainingActivity?.apiImp?.hitApi(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {

                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)

                    imageFilename = ss.data.path + ss.data.filename
                    if (bt_save.visibility == View.INVISIBLE) {
                        saveaudiofile()
                    } else if (excelPath.length > 0) {
//                        saveExcelFile()
                    } else {
//                        saveRewardDetail()

                    }
                }


            }

        })


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun setListeners(rootView: View) {
        rootView.removefile.setOnClickListener {
            excelName = ""
            excelPath = ""
            txt_excelname.setText("")
            ll_viewattach.visibility=View.GONE
        }
        rootView.adddesignations.setOnClickListener {
            val dialog =
                Dialog(activity!!, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
            dialog.setContentView(R.layout.dialog) //layout for dialog
            dialog.setTitle("Add Designation")
            dialog.setCancelable(false) //none-dismiss when touching outside Dialog
            // set the custom dialog components - texts and image
            val recyclerView = dialog.findViewById<View>(R.id.rec_participant) as RecyclerView
            val btnAdd = dialog.findViewById<View>(R.id.btn_ok)
            val btnCancel = dialog.findViewById<View>(R.id.btn_cancel)
            val edt_search = dialog.findViewById<EditText>(R.id.edt_search)
            //set spinner adapter
            adapter = ParticipantAdapter(modeldesignationList, activity)
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.adapter = adapter

            edt_search.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    // TODO Auto-generated method stub
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                    // TODO Auto-generated method stub
                }

                override fun afterTextChanged(s: Editable) {

                    // filter your list from your input
                    filterSearchdesignation(s.toString())
                    //you can use runnable postDelayed like 500 ms to delay search text
                }
            })

            btnAdd.setOnClickListener {
                for (i in modeldesignationList.indices) {
                    Log.e("TAG", "onClick: " + modeldesignationList[i].checked)
                    if (modeldesignationList[i].checked == true) {
                        if (!namestoShow.contains(modeldesignationList[i])) {
                            namestoShow.add(modeldesignationList[i])
                        }
                    } else {
                        if (namestoShow.contains(modeldesignationList[i])) {
                            namestoShow.remove(modeldesignationList[i])
                        }
                    }
                }
                Log.e("TAG", "onClick: " + namestoShow.size)
                showDatainPickList(namestoShow)
                dialog.dismiss()
            }
            btnCancel.setOnClickListener {
                for (i in modeldesignationList.indices) {
                    Log.e("TAG", "onClick: " + modeldesignationList[i].checked)
                    if (namestoShow.contains(modeldesignationList[i])) {
                        modeldesignationList[i].checked = true
                    } else {
                        modeldesignationList[i].checked = false
                    }
                }
                Log.e("TAG", "onClick: " + namestoShow.size)
                showDatainPickList(namestoShow)
                dialog.dismiss()
            }
            dialog.show()
        }
        rootView.addskills.setOnClickListener {
            val dialog =
                Dialog(activity!!, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
            dialog.setContentView(R.layout.dialog) //layout for dialog
            dialog.setTitle("Add Skills")
            dialog.setCancelable(false) //none-dismiss when touching outside Dialog
            // set the custom dialog components - texts and image
            val recyclerView = dialog.findViewById<View>(R.id.rec_participant) as RecyclerView
            val btnAdd = dialog.findViewById<View>(R.id.btn_ok)
            val btnCancel = dialog.findViewById<View>(R.id.btn_cancel)
            val edt_search = dialog.findViewById<EditText>(R.id.edt_search)
            //set spinner adapter
            adapter = ParticipantAdapter(modelskillsList, activity)
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.adapter = adapter

            edt_search.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    // TODO Auto-generated method stub
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                    // TODO Auto-generated method stub
                }

                override fun afterTextChanged(s: Editable) {

                    // filter your list from your input
                    filterSearchskills(s.toString())
                    //you can use runnable postDelayed like 500 ms to delay search text
                }
            })

            btnAdd.setOnClickListener {
                for (i in modelskillsList.indices) {
                    Log.e("TAG", "onClick: " + modelskillsList[i].checked)
                    if (modelskillsList[i].checked == true) {
                        if (!skillstoShow.contains(modelskillsList[i])) {
                            skillstoShow.add(modelskillsList[i])
                        }
                    } else {
                        if (skillstoShow.contains(modelskillsList[i])) {
                            skillstoShow.remove(modelskillsList[i])
                        }
                    }
                }
                Log.e("TAG", "onClick: " + skillstoShow.size)
                showpickedSkills(skillstoShow)
                dialog.dismiss()
            }
            btnCancel.setOnClickListener {
                for (i in modelskillsList.indices) {
                    Log.e("TAG", "onClick: " + modelskillsList[i].checked)
                    if (skillstoShow.contains(modelskillsList[i])) {
                        modelskillsList[i].checked = true
                    } else {
                        modelskillsList[i].checked = false
                    }
                }
                Log.e("TAG", "onClick: " + skillstoShow.size)
                showpickedSkills(skillstoShow)
                dialog.dismiss()
            }
            dialog.show()
        }

        rootView.add_rewardbyn.setOnClickListener {

            saveReward()
        }

        rootView.imagesreward.setOnClickListener {

//            permissionCallBack =
//                PermissionUtils.checkStoragePermission(object : PermissionCallBack {
//                    override fun onPermissionAccessed() {
//                        chooseFile(object : FileSelectionCallBack {
//                            @SuppressLint("SetTextI18n")
//                            override fun onSelection(uri: Uri, name: String) {
////                                val fileName= android.os.FileUtils.getFileNameFromUri(uri,this@BecomeExpertActivity)
//                            }
//                        })
//                    }
//                }, activity!!, PermissionUtils.storagePermission)

//            checkStoragePermission(object : PermissionCallBack {
//                override fun onPermissionAccessed() {
            if (imageList.size == 0) {
                pickImage()
            }


//                }
//
//            })


        }
        rootView.addfilerew.setOnClickListener {

            val intent = Intent(activity, FilePickerActivity::class.java)
            intent.putExtra(
                FilePickerActivity.CONFIGS, Configurations.Builder()
                    .setCheckPermission(true)
                    .setShowImages(false)
                    .enableImageCapture(false)
                    .setMaxSelection(1)
                    .setShowFiles(true)
                    .setSingleChoiceMode(true)
                    .setSkipZeroSizeFiles(true)
                    .build()
            )
            startActivityForResult(intent, 101)

//                checkStoragePermission(object : PermissionCallBack {
//                    override fun onPermissionAccessed() {
//            chooseFile(object : FileSelectionCallBack {
//                @SuppressLint("SetTextI18n")
//                override fun onSelection(uri: Uri, name: String) {
////                        val fileName= android.os.FileUtils.getFileNameFromUri(uri,this@BecomeExpertActivity)
////                        hitUploadApi(uri,AppConstants.PROFILE_RESUME,fileName)
//                }
//            })
//                    }
//                })
        }


    }

    private fun showDatainPickList(list: ArrayList<ParicipantModel>) {
        val adapter = PickedlistAdapter(list, activity)
        rec_showlist!!.layoutManager = LinearLayoutManager(activity)
        rec_showlist!!.adapter = adapter
    }
    private fun showpickedSkills(list: ArrayList<ParicipantModel>) {
        val adapter = PickedlistAdapter(list, activity)
        rec_showskills!!.layoutManager = LinearLayoutManager(activity)
        rec_showskills!!.adapter = adapter
    }
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {

            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }

    fun filterSearchdesignation(text: String?) {
        val temp = ArrayList<ParicipantModel>()
        for (d in modeldesignationList) {

            if (d.name.toLowerCase().contains(text!!.toLowerCase())) {
                temp.add(d)
            }
        }
        //update recyclerview
        adapter.updateList(temp)
    }
    fun filterSearchskills(text: String?) {
        val temp = ArrayList<ParicipantModel>()
        for (d in modelskillsList) {

            if (d.name.toLowerCase().contains(text!!.toLowerCase())) {
                temp.add(d)
            }
        }
        //update recyclerview
        adapter.updateList(temp)
    }




    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@AddTrainingModule, options)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 101) {
                val files: ArrayList<MediaFile> =
                    intent!!.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES)!!
                excelName = files.get(0).name
                excelPath = files.get(0).path
                excelUri = files.get(0).uri
                Log.e(
                    "docPaths",
                    "path :$files.get(0).path ,name=$files.get(0).name ,uri= ${files.get(0).uri}"
                )
                ll_viewattach.visibility=View.VISIBLE
                txt_excelname.setText("File name: " + excelName)

            } else if (requestCode == 42) {
                intent?.data?.path
                val uri = intent?.data

                docPaths = ArrayList()
                docPaths.addAll(intent!!.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_DOCS)!!)

                Log.e("docPaths", "onActivityResult: " + docPaths.get(0))
//                uploadFile(docPaths.get(0)!!, AppConstants.CHAT_FILE)


            } else if (requestCode == 100) {

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!

                val fileUri = intent?.data
                Log.e("TAG", "onActivityResult: " + returnValue + " -- " + fileUri)
//                imageList.add(
//                    ImageDTO(
//                        getCompressedUri(
//                            Uri.parse("file://" + returnValue.get(0)),
//                            activity!!
//                        ).toString()
//                    )
//                )
                imageList.add(
                    ImageDTO(
                        Uri.parse("file://" + returnValue.get(0)).toString()
                    )
                )
                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })
            } else {

                val fileUri = intent?.data
//
//            //You can get File object from intent
//            val file: File = ImagePicker.getFile(intent)!!
//
                imageList.add(ImageDTO(getCompressedUri(fileUri!!, activity!!).toString()))
                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })

            }


        }
    }

    fun showDynamicImage(
        llAtach: LinearLayout,
        imageList: ArrayList<ImageDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = ImageItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
                showImageUri(imageDTO.getUri(), context)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Image",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }

    fun getTimestamp(): String? {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }

    fun getCompressedUri(uri: Uri, context: Context): Uri {
        Log.v("getCompressUri", "${uri.path}")
        val path = "${Environment.getExternalStorageDirectory()}/ERP/ temp"
        val fileName = "${getTimestamp()}.png"

        val myDir = File(path)

        if (!myDir.exists())
            myDir.mkdirs()

        val file = File(myDir, fileName)
        val currentFile = File(uri.path!!)

        val returnString = Uri.parse(file.absolutePath)

        if (file.exists()) file.delete()
        try {
//            val out = FileOutputStream(file)
//            val finalBitmap = Compressor(context).compressToBitmap(currentFile)
//            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
//            out.flush()
//            out.close()
        } catch (e: Exception) {
            Log.v("getCompressUri", "exception $e")
            return uri
        }

        Log.v("getCompressUri", "last ${returnString.path}")

        return returnString

    }


    companion object {

        @JvmField
        var alreadySelectedNames = ArrayList<ParicipantModel>()
        val namestoShow = ArrayList<ParicipantModel>()

        @JvmField
        var alreadySelectedskills = ArrayList<ParicipantModel>()
        val skillstoShow = ArrayList<ParicipantModel>()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddTrainingModule()
    }
}