package com.sevenrocks.taskapp.appModules.grievance

import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.Size
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.camera.core.CameraSelector
import androidx.camera.core.ImageAnalysis
import androidx.camera.core.Preview
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.view.PreviewView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.LifecycleOwner
import butterknife.ButterKnife
import com.google.common.util.concurrent.ListenableFuture
import com.google.zxing.integration.android.IntentIntegrator
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.training.*
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.feedbackform.*
import kotlinx.android.synthetic.main.grievance_reg.view.txtheader
import kotlinx.android.synthetic.main.scancode.view.*
import java.util.concurrent.ExecutionException


/**
 */
class ScanTrainingCodeRegistration : Fragment() {
    private var rating: String = ""
    private var toast: String = ""
    private var isFirst: Boolean = false
    private lateinit var rootView: View
    public var trainingActivity: TrainingActivity? = null
    private val PERMISSION_REQUEST_CAMERA = 0
    private var cameraProviderFuture: ListenableFuture<ProcessCameraProvider>? = null

    private var qrCode: String? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.scancode, BasicActi, false)
        ButterKnife.bind(this, rootView)
        trainingActivity = activity as TrainingActivity?
        rootView.txtheader.text = "Start/ Stop Training"
        openScanner()

        rootView.activity_main_qrCodeFoundButton.setOnClickListener {
            Toast.makeText(activity, qrCode, Toast.LENGTH_SHORT).show()
            Log.e(
                "",
                "QR Code Found: $qrCode"
            )

            saveCodeData()
        }
        return rootView

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveCodeData() {
        isFirst = true
        val greq = CodeSaveReq()
        val greq2 = CodeSaveReq2()

        greq.token = trainingActivity?.pref?.get(AppConstants.token)
        greq.service_name = ApiUtils.SAVECODEDATA

        greq2.training_instance_id = qrCode
        greq2.user_id =trainingActivity?.pref?.get(AppConstants.uid)
        greq2.current_time = System.currentTimeMillis().toString()

        greq.data = greq2
        trainingActivity!!.apiImp.saveCodeData(greq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
//                openFeedbackForm()

                if (status) {
                    var ss = any as CodeGetResponse

                    Toast.makeText(activity, ss.data.task, Toast.LENGTH_SHORT).show()
                    if (ss.data.task.equals("Feedback Form")) {
                        openFeedbackForm()
                    } else {
                        trainingActivity!!.finish()
                    }


                }else{
                    isFirst = false

                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveFeedback(toString: String, dialog: Dialog) {

        val greq = FeedbackSaveReq()
        val greq2 = FeedbackSaveReq2()

        greq.token = trainingActivity?.pref?.get(AppConstants.token)
        greq.service_name = ApiUtils.SAVEFEEDBACK

        greq2.training_instance_id = qrCode
        greq2.user_id = trainingActivity?.pref?.get(AppConstants.uid)
        greq2.feedback = toString
        greq2.rating = rating
        greq.data = greq2
        trainingActivity!!.apiImp.saveFeedback(greq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    isFirst = false
                    Toast.makeText(activity, "Feedback Saved", Toast.LENGTH_SHORT).show()

                    trainingActivity!!.finish()

                }
                else{
                  dialog.dismiss()
                }


            }

        })
    }

    private fun openFeedbackForm() {
        val dialog = Dialog(trainingActivity!!, android.R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.feedbackform)

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.spin_rating.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: String = adapterView!!.getItemAtPosition(position).toString()
                rating = user.removePrefix("Rate ")

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })

        dialog.btn_diasave.setOnClickListener {
            if (rating.equals("0")||rating.equals("")){
                Toast.makeText(activity, "Rating is Mandatory", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            saveFeedback(dialog.edt_feedback.text.toString(),dialog)
        }
        dialog.btn_diacan.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }


    private fun openScanner() {

        cameraProviderFuture = ProcessCameraProvider.getInstance(activity!!)
        requestCamera()

    }

    private fun requestCamera() {
        if (ActivityCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            startCamera()
        } else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(
                    activity!!,
                    Manifest.permission.CAMERA
                )
            ) {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.CAMERA),
                    PERMISSION_REQUEST_CAMERA
                )
            } else {
                ActivityCompat.requestPermissions(
                    activity!!,
                    arrayOf(Manifest.permission.CAMERA),
                    PERMISSION_REQUEST_CAMERA
                )
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        if (requestCode == PERMISSION_REQUEST_CAMERA) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                startCamera()
            } else {
                Toast.makeText(activity, "Camera Permission Denied", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun startCamera() {
        cameraProviderFuture!!.addListener({
            try {
                val cameraProvider = cameraProviderFuture!!.get()
                bindCameraPreview(cameraProvider)
            } catch (e: ExecutionException) {
                Toast.makeText(activity, "Error starting camera " + e.message, Toast.LENGTH_SHORT)
                    .show()
            } catch (e: InterruptedException) {
                Toast.makeText(activity, "Error starting camera " + e.message, Toast.LENGTH_SHORT)
                    .show()
            }
        }, ContextCompat.getMainExecutor(activity))
    }

    private fun bindCameraPreview(cameraProvider: ProcessCameraProvider) {
        rootView.activity_main_previewView.setPreferredImplementationMode(PreviewView.ImplementationMode.SURFACE_VIEW)
        val preview = Preview.Builder()
            .build()
        val cameraSelector = CameraSelector.Builder()
            .requireLensFacing(CameraSelector.LENS_FACING_BACK)
            .build()
        preview.setSurfaceProvider(rootView.activity_main_previewView.createSurfaceProvider())
        val imageAnalysis = ImageAnalysis.Builder()
            .setTargetResolution(Size(1280, 720))
            .setBackpressureStrategy(ImageAnalysis.STRATEGY_KEEP_ONLY_LATEST)
            .build()
        imageAnalysis.setAnalyzer(
            ContextCompat.getMainExecutor(activity),
            QRCodeImageAnalyzer(object : QRCodeFoundListener {
                override fun onQRCodeFound(_qrCode: String) {
                    if (!isFirst) {
                        qrCode = _qrCode
                        Toast.makeText(activity, qrCode, Toast.LENGTH_SHORT).show()

                        saveCodeData()
                    }


                }

                override fun qrCodeNotFound() {
                }
            })
        )
        val camera = cameraProvider.bindToLifecycle(
            (this as LifecycleOwner),
            cameraSelector,
            imageAnalysis,
            preview
        )
    }

    private fun displayToast() {
        if (activity != null && toast != null) {
            Toast.makeText(activity, toast, Toast.LENGTH_LONG).show()
            toast = ""
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (result != null) {
            if (result.contents == null) {
                toast = "Cancelled from fragment"
            } else {
                toast = "Scanned from fragment: " + result.contents
            }
            Log.e("TAG", "onActivityResult: " + result.contents)
            trainingActivity?.finish()
            // At this point we may or may not have a reference to the activity
            displayToast()
        }
    }

    companion object {
        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = ScanTrainingCodeRegistration()
    }
}