package com.seekex.hrmodule.training;

public class FeedbackSaveReq2 {
        private String training_instance_id;
        private String user_id;

        private String feedback;
        private String rating;

    public String getTraining_instance_id() {
        return training_instance_id;
    }

    public void setTraining_instance_id(String training_instance_id) {
        this.training_instance_id = training_instance_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}