
package com.seekex.hrmodule.training;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class CodeGetResponse2 extends BaseResponse {
    @SerializedName("task")
    private String task;

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }
}

