package com.seekex.hrmodule.training;

import com.seekex.hrmodule.grievance.GreivanceSaveReq2;
import com.seekx.webService.models.RequestBase;

public class CodeSaveReq extends RequestBase {

    private CodeSaveReq2 data;

    public CodeSaveReq2 getData() {
        return data;
    }

    public void setData(CodeSaveReq2 data) {
        this.data = data;
    }



}
