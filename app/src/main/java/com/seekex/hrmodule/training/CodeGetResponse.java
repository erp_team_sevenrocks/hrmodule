
package com.seekex.hrmodule.training;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.login.LoginResponse2;
import com.seekx.webService.models.BaseResponse;

public class CodeGetResponse extends BaseResponse {
    @SerializedName("data")
    private CodeGetResponse2 data;

    public CodeGetResponse2 getData() {
        return data;
    }

    public void setData(CodeGetResponse2 data) {
        this.data = data;
    }
}

