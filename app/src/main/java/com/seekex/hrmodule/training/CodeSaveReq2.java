package com.seekex.hrmodule.training;

public class CodeSaveReq2 {
        private String training_instance_id;
        private String user_id;

        private String current_time;

    public String getTraining_instance_id() {
        return training_instance_id;
    }

    public void setTraining_instance_id(String training_instance_id) {
        this.training_instance_id = training_instance_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getCurrent_time() {
        return current_time;
    }

    public void setCurrent_time(String current_time) {
        this.current_time = current_time;
    }
}