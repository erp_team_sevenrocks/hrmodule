package com.seekex.hrmodule.training;

import com.seekx.webService.models.RequestBase;

public class FeedbackSaveReq extends RequestBase {

    private FeedbackSaveReq2 data;

    public FeedbackSaveReq2 getData() {
        return data;
    }

    public void setData(FeedbackSaveReq2 data) {
        this.data = data;
    }



}
