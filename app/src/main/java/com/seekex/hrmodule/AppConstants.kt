package com.seekex.hrmodule

import android.os.Environment

object AppConstants {


    const val SPACES =" "
    const val SPACES_DOTS =" . "
    const val isNull="null"
    var baseDir= "${Environment.getExternalStorageDirectory().absolutePath }/Seekex/"

    var downloadDir= "$baseDir Downloads/"

    const val BASEURL="baseurl"
    const val LIVEURL="https://erp.sevenrocks.in/"
    const val TESTURL="https://test-live.sevenrocks.in/"
    const val STAGGINGURL="https://stagging.sevenrocks.in/"
    const val MASTERURL="https://master.sevenrocks.in/"



    const val EMPTY=""
    const val NA="NA"
    const val uid="uid"
    const val isLogin="isLogin"
    const val pic_url="pic_url"
    const val user_name="user_name"
    const val password="password"
    const val token="token"
    const val name="name"
    const val que_count="que_count"
    const val cand_id="cand_id"
    const val test_id="test_id"
    const val timer="timer"

    const val TYPE_MCQ="1"
    const val TYPE_FILE="2"
    const val TYPE_TEXT="3"

    const val QUE_IMAGE="1"
    const val QUE_AUDIO="2"
    const val QUE_VIDEO="3"
    const val QUE_FILE="4"


}