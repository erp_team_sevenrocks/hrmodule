package com.seekex.hrmodule.remiender;


import com.seekex.hrmodule.locationfeedback.SaveLocFeedbackReq2;
import com.seekx.webService.models.RequestBase;

public class GetRecordReq extends RequestBase {

    private String type  ;
    private String user_id  ;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
