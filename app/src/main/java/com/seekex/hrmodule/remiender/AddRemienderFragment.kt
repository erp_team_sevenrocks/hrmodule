package com.sevenrocks.taskapp.appModules.markrewarks

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.adapter.EventParamAdapter
import com.seekex.hrmodule.feedback.*
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.locationfeedback.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.remiender.GetRecordReq
import com.seekex.hrmodule.remiender.RemienderActivity
import com.seekex.hrmodule.remiender.SaveDataReq
import com.seekex.hrmodule.remiender.SaveDataReq2
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.utils.DataUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import kotlinx.android.synthetic.main.add_feedback.*
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_loc_feedback.*
import kotlinx.android.synthetic.main.add_loc_feedback.view.*
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.image_view.*
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.*
import kotlinx.android.synthetic.main.add_reminder.*
import kotlinx.android.synthetic.main.add_reminder.view.*
import kotlinx.android.synthetic.main.filter_view.*
import kotlinx.android.synthetic.main.play_video.*
import kotlinx.android.synthetic.main.sopauditlist.*
import java.util.*


/**
 */
class AddRemienderFragment : Fragment() {


    public var remienderActivity: RemienderActivity? = null

    lateinit var apiImp: ApiImp
    private lateinit var adapter: EventParamAdapter
    var recordlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var durationlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var typelist: ArrayList<DropdownModel> = java.util.ArrayList()
    private var durationid: String? = ""
    private var recordid: String? = ""
    private var typeid: String? = ""
    private var finaldate: String? = ""
    private val cal = Calendar.getInstance()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        val rootView = inflater.inflate(R.layout.add_reminder, BasicActi, false)
        remienderActivity = activity as RemienderActivity?
        rootView.txtheader.setText("Add Reminder")
        setlistener(rootView)
        gettype()
        getDuration()
        return rootView
    }

    private fun setlistener(rootView: View) {

        rootView.edtrecrem.setOnClickListener {

            if (typeid.equals("")) {
                Toast.makeText(activity, "Select reminder type ", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            DataUtils.openSearchDialoge(
                activity!!,
                recordlist,
                "Select Record",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtrecrem.setText(ss.name)
                        recordid = ss.id
                    }

                })
        }
        rootView.edtremtype.setOnClickListener {


            DataUtils.openSearchDialoge(
                activity!!,
                typelist,
                "Select Type",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtremtype.setText(ss.name)
                        typeid = ss.id

                        getrecord()
                    }

                })
        }
        rootView.edtdurrem.setOnClickListener {
            DataUtils.openSearchDialoge(
                activity!!,
                durationlist,
                "Select Duration",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtdurrem.setText(ss.name)
                        durationid = ss.id
                    }

                })
        }

        rootView.edt_finaldate.setOnClickListener {
            val dpd = DatePickerDialog(activity!!, { _, year, month, date ->
                val dob = String.format("%02d", date) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%d", year)
                var startDate = String.format("%d", year) + "-" + String.format(
                    "%02d",
                    month + 1
                ) + "-" + String.format("%02d", date)
                rootView.edt_finaldate.setText(dob)
                finaldate = startDate
            }, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DATE))

            dpd.datePicker.minDate = System.currentTimeMillis() - 1000
            dpd.show()
        }

        rootView.save_reminder.setOnClickListener {

            if (edt_remname.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter Name", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edt_desrem.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter Description", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edt_finaldate.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter Date", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (typeid.equals("0")) {
                Toast.makeText(activity, "Select Type", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (durationid.equals("0")||durationid.equals("")) {
                Toast.makeText(activity, "Select Duration", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (recordid.equals("0")||recordid.equals("")) {
                Toast.makeText(activity, "Select record", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            saveData()
        }

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun saveData() {
        val ss1 = SaveDataReq()
        val ss2 = SaveDataReq2()

        ss1.token = remienderActivity?.pref?.get(AppConstants.token)
        ss1.service_name = ApiUtils.save_reminder

        ss2.name=edt_remname.text.toString()
        ss2.description=edt_desrem.text.toString()
        ss2.duration=durationid
        ss2.final_date=finaldate
        ss2.record_id=recordid
        ss2.type=typeid

        ss1.data=ss2
        remienderActivity!!.apiImp.savereminder(ss1, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    Toast.makeText(activity, "Record saved", Toast.LENGTH_SHORT).show()
                    remienderActivity!!.finish()
                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getDuration() {
        durationlist.clear()
        val getMasterRequest = RequestBase()
        getMasterRequest.token = remienderActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.reminder_duration_list
        remienderActivity!!.apiImp.getFeedbackType(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val list = any as RewardUsersResponse

                    durationlist.addAll(list.data)

//                    adapterFeedbacktype.notifyDataSetChanged()
                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun gettype() {
        typelist.clear()
        val getMasterRequest = RequestBase()
        getMasterRequest.token = remienderActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.reminder_type_list
        remienderActivity!!.apiImp.getFeedbackType(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val list = any as RewardUsersResponse

                    typelist.addAll(list.data)
                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getrecord() {
        recordlist.clear()
        val getMasterRequest = GetRecordReq()
        getMasterRequest.token = remienderActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.get_list_on_type

        getMasterRequest.type = typeid
        getMasterRequest.user_id = remienderActivity?.pref?.get(AppConstants.uid)
        remienderActivity!!.apiImp.getrecord(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val list = any as RewardUsersResponse
                    recordlist.addAll(list.data)
                }

            }

        })
    }

    companion object {


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddRemienderFragment()
    }
}