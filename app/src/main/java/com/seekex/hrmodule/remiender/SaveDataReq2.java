package com.seekex.hrmodule.remiender;


import com.seekex.hrmodule.grievance.ImagesGRDTO;
import com.seekex.hrmodule.markrewarks.RewardUserSaveReq;
import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class SaveDataReq2 extends RequestBase {

    private String name   ;
    private String description  ;
    private String type  ;
    private String record_id  ;
    private String final_date  ;
    private String duration  ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    public String getFinal_date() {
        return final_date;
    }

    public void setFinal_date(String final_date) {
        this.final_date = final_date;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }
}
