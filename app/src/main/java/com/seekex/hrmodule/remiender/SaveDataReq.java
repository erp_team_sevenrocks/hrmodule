package com.seekex.hrmodule.remiender;


import com.seekex.hrmodule.locationfeedback.SaveLocFeedbackReq2;
import com.seekx.webService.models.RequestBase;

public class SaveDataReq extends RequestBase {

    private SaveDataReq2 data  ;

    public SaveDataReq2 getData() {
        return data;
    }

    public void setData(SaveDataReq2 data) {
        this.data = data;
    }
}
