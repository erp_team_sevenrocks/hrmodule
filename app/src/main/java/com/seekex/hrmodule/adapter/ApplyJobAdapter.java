package com.seekex.hrmodule.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.markrewarks.DropdownModel;
import com.seekex.hrmodule.markrewarks.ParicipantModel;
import com.sevenrocks.taskapp.appModules.markrewarks.AddRewards;

import java.util.ArrayList;

public class ApplyJobAdapter extends RecyclerView.Adapter<ApplyJobAdapter.ViewHolder> {
    private ArrayList<DropdownModel> listdata;
    Context context;

    // RecyclerView recyclerView;
    private ArrayList<Integer> selectCheck = new ArrayList<>();

    public ApplyJobAdapter(ArrayList<DropdownModel> listdata, Context context) {
        this.listdata = listdata;
        this.context = context;

    }

    public void updateList(ArrayList<DropdownModel> list) {
        listdata = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item_job, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {


        final DropdownModel myListData = listdata.get(position);
        holder.textView.setText("Kra: " + listdata.get(position).getId());
        holder.departmentname.setText("Skills: " + listdata.get(position).getName());
//        holder.chk.setChecked(listdata.get(position).getChecked());
    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public TextView departmentname;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            this.departmentname = (TextView) itemView.findViewById(R.id.departmentname);
            this.setIsRecyclable(false);

        }
    }
}  