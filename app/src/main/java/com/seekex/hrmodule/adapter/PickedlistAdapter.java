package com.seekex.hrmodule.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.markrewarks.ParicipantModel;

import java.util.ArrayList;

public class PickedlistAdapter extends RecyclerView.Adapter<PickedlistAdapter.ViewHolder> {
    private ArrayList<ParicipantModel> listdata;
    Context context;

    // RecyclerView recyclerView;
    public PickedlistAdapter(ArrayList<ParicipantModel> listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.pickedlist_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final ParicipantModel myListData = listdata.get(position);
        holder.txt_showlist.setText(listdata.get(position).getName());
        holder.txt_deptname.setText(listdata.get(position).getDepatmentname());
//        holder.chk.setChecked(listdata.get(position).getChecked());

    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_showlist;
        public TextView txt_deptname;

        public ViewHolder(View itemView) {
            super(itemView);
            this.txt_showlist = (TextView) itemView.findViewById(R.id.txt_showlist);
            this.txt_deptname = (TextView) itemView.findViewById(R.id.txt_deptname);


        }
    }
}  