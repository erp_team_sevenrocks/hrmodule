package com.seekex.hrmodule.adapter;

import static com.sevenrocks.taskapp.appModules.markrewarks.AddTrainingModule.alreadySelectedNames;
import static com.sevenrocks.taskapp.appModules.markrewarks.AddTrainingModule.alreadySelectedskills;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.markrewarks.ParicipantModel;
import com.sevenrocks.taskapp.appModules.markrewarks.AddRewards;

import java.util.ArrayList;

public class EventParamAdapterParam extends RecyclerView.Adapter<EventParamAdapterParam.ViewHolder> {
    private ArrayList<ParicipantModel> listdata;
    Context context;

    // RecyclerView recyclerView;
    private ArrayList<Integer> selectCheck = new ArrayList<>();

    public EventParamAdapterParam(ArrayList<ParicipantModel> listdata, Context context) {
        this.listdata = listdata;
        this.context = context;
        for (int i = 0; i < listdata.size(); i++) {
            selectCheck.add(0);
        }

    }

    public void updateList(ArrayList<ParicipantModel> list) {
        listdata = list;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, @SuppressLint("RecyclerView") final int position) {

        if (selectCheck.get(position) == 1) {
            holder.chk.setChecked(true);
        } else {
            holder.chk.setChecked(false);
        }


        final ParicipantModel myListData = listdata.get(position);
        holder.textView.setText("Name: " + listdata.get(position).getName());
        holder.departmentname.setText("Dept.name: " + listdata.get(position).getDepatmentname());
//        holder.chk.setChecked(listdata.get(position).getChecked());

        holder.chk.setOnCheckedChangeListener(null);
        holder.chk.setTag(listdata.get(position));

        if (listdata.get(position).getChecked() == true) {
            holder.chk.setChecked(true);
        }
        holder.chk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ParicipantModel chkModel = (ParicipantModel) holder.chk.getTag();
                String data = "";
                ParicipantModel fruits1 = (ParicipantModel) holder.chk.getTag();
                if (holder.chk.isChecked()) {
                    fruits1.setChecked(false);
                    listdata.get(position).setChecked(false);

                    AddRewards.alreadySelecteParam.remove(chkModel);
                } else {
                    fruits1.setChecked(true);
                    AddRewards.alreadySelecteParam.add(chkModel);
                    listdata.get(position).setChecked(false);

                }
                notifyDataSetChanged();
                for (int k = 0; k < selectCheck.size(); k++) {
                    if (k == position) {
                        selectCheck.set(k, 1);
                        AddRewards.alreadySelecteParam.clear();
                        AddRewards.alreadySelecteParam.add(chkModel);

                    } else {

                        selectCheck.set(k, 0);
                    }
                }
//                listdata.get(position).setChecked(holder.chk.isChecked());


                notifyDataSetChanged();


//                listdata.get(position).setChecked(holder.chk.isChecked());
//                Log.e("TAG", "onClick: " + listdata.size());
//                for (int j = 0; j < listdata.size(); j++) {
//
//                    if (listdata.get(j).getChecked() == true) {
//                        if (!AddRewards.alreadySelecteParam.contains(listdata.get(j))) {
//                            AddRewards.alreadySelecteParam.add(listdata.get(j));
//
//                        }
//                        data = data + "\n" + listdata.get(j).getName().toString() + "   " + listdata.get(j).getId().toString();
//                    } else {
//                        if (AddRewards.alreadySelecteParam.contains(listdata.get(j))) {
//                            AddRewards.alreadySelecteParam.remove(listdata.get(j));
//                            fruits1.setChecked(false);
//
//                            listdata.get(position).setChecked(false);
//                        }
//                    }
//                    notifyDataSetChanged();
//                }
//                Toast.makeText(context, "Selected  : \n " + data, Toast.LENGTH_SHORT).show();
//                Toast.makeText(context, "list size = " + CreateMeeting.alreadySelectedNames.size(), Toast.LENGTH_SHORT).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return listdata.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public TextView departmentname;
        public CheckBox chk;

        public ViewHolder(View itemView) {
            super(itemView);
            this.textView = (TextView) itemView.findViewById(R.id.textView);
            this.departmentname = (TextView) itemView.findViewById(R.id.departmentname);
            this.chk = (CheckBox) itemView.findViewById(R.id.chk);
            this.setIsRecyclable(false);

        }
    }
}  