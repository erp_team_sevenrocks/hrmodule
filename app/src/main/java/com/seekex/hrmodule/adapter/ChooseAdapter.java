package com.seekex.hrmodule.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;


import com.seekex.hrmodule.R;
import com.seekex.hrmodule.retrofitclasses.models.DashboardModel;

import java.util.ArrayList;

public abstract class ChooseAdapter extends BaseAdapter {

        private Context context;

    private ArrayList<DashboardModel> alist;

        private View.OnClickListener onViewclick;

        protected ChooseAdapter(Context context, ArrayList<DashboardModel> alist){
            this.alist=alist;
            this.context=context;
            onViewclick = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    onViewClicked(v,String.valueOf(v.getTag(R.string.id)));

                }

            };

        }



    protected abstract void onViewClicked(View v, String s);

        @Override
        public int getCount() {
            return alist.size();
        }

        @Override
        public Object getItem(int position) {
            return alist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View vi =convertView;
            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                vi = mInflater.inflate(R.layout.choose_adapter, null);
            }

            DashboardModel dashboardModel=alist.get(position);

            final ImageView  imageView=vi.findViewById(R.id.i);
            final CardView cardView= vi.findViewById(R.id.wstatus);
            final TextView tvLable=vi.findViewById(R.id.t);

            imageView.setImageResource(dashboardModel.getDrawableId());
            tvLable.setText(dashboardModel.getLabel());

            cardView.setOnClickListener(onViewclick);
            cardView.setTag(R.string.id,dashboardModel.getId());

            return vi;
        }




    }
