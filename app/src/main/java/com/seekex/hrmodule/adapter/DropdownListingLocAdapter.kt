package com.seekex.hrmodule.adapter


import android.annotation.SuppressLint
import android.content.Context
import android.media.MediaPlayer
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.databinding.SearchViewBinding
import com.seekex.hrmodule.databinding.SearchViewLocBinding
import com.seekex.hrmodule.markrewarks.DropdownModel
import com.seekex.hrmodule.markrewarks.ParicipantModel

import com.seekx.interfaces.AdapterListener


class DropdownListingLocAdapter(
    val context: Context,
    private val type: String,

    private val adapterListener: AdapterListener
) : RecyclerView.Adapter<DropdownListingLocAdapter.ViewHolder>() {

    private var items: ArrayList<DropdownModel> = ArrayList()

    private var count: Long = 0L

    private var player: MediaPlayer? = null

    fun setDataValues(items: ArrayList<DropdownModel>) {
        this.items = items
        notifyDataSetChanged()
    }
    fun addDataValues(item: ArrayList<DropdownModel>) {
        this.items.addAll(item)
        notifyDataSetChanged()
    }
    fun stopMediaPlayer() {
        player?.stop()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = SearchViewLocBinding.inflate(inflater)
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
//        try {
//            if (items.get(position).get!!) {
//                holder.binding.status.setImageResource(R.mipmap.online)
//            } else {
//                holder.binding.status.setImageResource(R.mipmap.offline)
//            }
//        } catch (ex: Exception) {
//
//        }

        holder.bind(items[position])
    }

    inner class ViewHolder(private val binding: SearchViewLocBinding) :
        RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("RtlHardcoded")
        fun bind(item: DropdownModel) {
            binding.model = item
            binding.rrView.setOnClickListener {
                adapterListener.onSelection(binding?.model!!, null, 0)
            }
            binding.executePendingBindings()
        }
    }


    override fun getItemViewType(position: Int): Int {
        return position
    }


}