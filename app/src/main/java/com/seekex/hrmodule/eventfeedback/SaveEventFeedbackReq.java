package com.seekex.hrmodule.eventfeedback;

import com.seekex.hrmodule.feedback.SaveFeedbackReq2;
import com.seekx.webService.models.RequestBase;

public class SaveEventFeedbackReq extends RequestBase {

    private SaveFeedbackReq2 data;

    public SaveFeedbackReq2 getData() {
        return data;
    }

    public void setData(SaveFeedbackReq2 data) {
        this.data = data;
    }
}
