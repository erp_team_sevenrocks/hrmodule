package com.seekex.hrmodule.eventfeedback;

import com.seekx.webService.models.RequestBase;

public class SaveEventFeedbackReq2 extends RequestBase {

    private String  user_id;
    private String  team_event_instance_id;
    private String  feedback;
    private String  rating;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getTeam_event_instance_id() {
        return team_event_instance_id;
    }

    public void setTeam_event_instance_id(String team_event_instance_id) {
        this.team_event_instance_id = team_event_instance_id;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }
}
