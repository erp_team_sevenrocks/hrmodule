package com.sevenrocks.taskapp.appModules.markrewarks

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.eventfeedback.SaveEventFeedbackReq
import com.seekex.hrmodule.eventfeedback.SaveEventFeedbackReq2
import com.seekex.hrmodule.feedback.*
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import kotlinx.android.synthetic.main.add_event_feedback.*
import kotlinx.android.synthetic.main.add_event_feedback.view.*
import kotlinx.android.synthetic.main.add_feedback.*
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_feedback.view.edtuserfeed
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.kra_assessment.view.btn_krasubmit
import java.util.*


/**
 */
class AddEventFeedbackFragment : Fragment() {
    private var userId: String? = "0"
    private var feedbacktypeid: String? = "0"
    private var feedid: String? = "0"
    private lateinit var adapterUsers: SpinnerAdapter_Users
    private lateinit var adapterFeedbacktype: SpinnerAdapter_FeedbackType
    private lateinit var adapterFeed: SpinnerAdapter_Feed

    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var feedbacktypelist: ArrayList<DropdownModel> = java.util.ArrayList()
    var feedlist: ArrayList<DropdownModel> = java.util.ArrayList()

    public var feedbackActivity: FeedbackActivity? = null

    lateinit var apiImp: ApiImp


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        val rootView = inflater.inflate(R.layout.add_event_feedback, BasicActi, false)
        feedbackActivity = activity as FeedbackActivity?
        rootView.txtheader.setText("Add Event Feedback")
//        setAdapters(rootView)
        setlistener(rootView)
        getUsers()

        return rootView
    }

    private fun setlistener(rootView: View?) {

        rootView!!.edtuserfeed.setOnClickListener {
            DataUtils.openSearchDialogeuser(
                activity!!,
                userlist,
                "Select User",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtuserfeed.setText(ss.name)
                        userId=ss.id

                    }

                })
        }
        rootView!!.edtfeedtype.setOnClickListener {
            DataUtils.
            openSearchDialoge(
                activity!!,
                feedbacktypelist,
                "Select Feedback Type",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtfeedtype.setText(ss.name)
                        feedbacktypeid = ss.id


                    }

                })
        }


        rootView!!.saveeventfeedback.setOnClickListener {

            if (edt_event_feedback.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter Feedback", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }


            saveData(edt_event_feedback.text.toString().trim())

        }
    }



    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUsers() {

        val getMasterRequest = GetSalaryReq()
        val get2 = GetSalaryReq2()

        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GUSERS
        getMasterRequest.conditions= get2

        feedbackActivity!!.apiImp.getkrausers(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    userlist.addAll(ss.data)
//                    adapterUsers.notifyDataSetChanged()
                }
            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveData(remark: String) {

        val getMasterRequest = SaveEventFeedbackReq()
        val ss = SaveEventFeedbackReq2()

        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.team_event_feedback_save
//        ss.feedback_type_id = feedbacktypeid
//        ss.feedback_value = feedid
//        ss.remarks = remark
//        ss.to_user_id = userId
//        getMasterRequest.data = ss
//        feedbackActivity!!.apiImp.saveNewFeedback(getMasterRequest, object : ApiCallBack {
//            override fun onSuccess(status: Boolean, any: Any) {
//                if (status) {
//                    Toast.makeText(activity, "Feedback Saved", Toast.LENGTH_LONG).show()
//                    feedbackActivity!!.finish()
//                }
//
//
//            }
//
//        })
    }

    companion object {


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddEventFeedbackFragment()
    }
}