package com.seekx.webService.models.apiRequest

import com.seekx.webService.models.RequestBase

data class LoginReq(
    var gcm_reg_no: String,
    var otp: String):RequestBase(){

    constructor():this("","")
}
