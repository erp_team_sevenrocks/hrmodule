
package com.seekex.hrmodule.login;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class LoginResponse extends BaseResponse {
    @SerializedName("User")
    private LoginResponse2 User;

    @SerializedName("token")
    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoginResponse2 getUser() {
        return User;
    }

    public void setUser(LoginResponse2 user) {
        User = user;
    }


}

