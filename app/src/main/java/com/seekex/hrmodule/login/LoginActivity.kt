package com.seekex.hrmodule.login

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekex.hrmodule.R
import com.seekex.hrmodule.activities.DashboardActivity
import com.seekex.hrmodule.utils.ActivityUtils
import com.seekx.utils.*
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.apiRequest.LoginReq
import kotlinx.android.synthetic.main.loginactivity.*

class LoginActivity : AppCompatActivity() {
    lateinit var pref: Preferences
    lateinit var apiImp: ApiImp
    lateinit var dialogUtils: DialogUtils

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.loginactivity)

        init()
        setListener()

    }

    private fun init() {
        dialogUtils = DialogUtils(this)
        apiImp = ApiImp(this)
        pref = Preferences(this)

    }


    private fun setListener() {

        login_button.setOnClickListener {
            apiImp?.showProgressBar()
            if (check())
                validateUser(
                    ExtraUtils.getVal(et_username),
                    ExtraUtils.getVal(et_pass)
                )
            else
                apiImp?.cancelProgressBar()
        }


    }

    fun validateUser(username: String, pass: String) {
        val loginReq = LoginReq()

        loginReq.username = username
        loginReq.password = pass
        loginReq.service_name=ApiUtils.LOGIN

        Log.e("TAG", "validateUser: " + username)
        apiImp.login(loginReq,object :ApiCallBack{
            override fun onSuccess(status: Boolean, any: Any) {
                Log.e("TAG", "onSuccess: " )

                if (status){
                    val detail = any as LoginResponse
                    pref.set(AppConstants.uid, detail.user.id!!.toString())
//                    pref.set(AppConstants.pic_url, detail.user.profile_image_thumbnail)
                    pref.set(AppConstants.name, detail.user.name)
                    pref.setBoolean(AppConstants.isLogin, true)
                    pref.set(AppConstants.user_name, et_username.text.toString())
                    pref.set(AppConstants.password, et_pass.text.toString())
                    pref.set(AppConstants.token, detail.token)
        ActivityUtils.navigate(this@LoginActivity, DashboardActivity::class.java, false)
                }
            }

        })

    }

    override fun onResume() {
        super.onResume()
    }

    private fun check(): Boolean {
        return when {
            ValidationUtils.isEmpty(et_username) -> {
                dialogUtils.showAlert(getString(R.string.error_username))
                false
            }
            ValidationUtils.isContainsSpace(et_username) -> {
                dialogUtils.showAlert(getString(R.string.error_username_space))
                false
            }
            ValidationUtils.isEmpty(et_pass) -> {
                dialogUtils.showAlert(getString(R.string.err_password))
                false
            }
            else -> true
        }

    }



}