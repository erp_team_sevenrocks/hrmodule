package com.seekex.hrmodule.sop;

import android.widget.EditText;
import android.widget.LinearLayout;

public class InflateModel {
        private LinearLayout layout;

        private EditText editText;

    public InflateModel(LinearLayout layout, EditText editText) {
        this.layout = layout;
        this.editText = editText;
    }

    public LinearLayout getLayout() {
        return layout;
    }

    public void setLayout(LinearLayout layout) {
        this.layout = layout;
    }

    public EditText getEditText() {
        return editText;
    }

    public void setEditText(EditText editText) {
        this.editText = editText;
    }
}