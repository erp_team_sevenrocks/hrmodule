package com.seekex.hrmodule.sop;

import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class SaveAuditDataReq2 extends RequestBase {

    private String sop_audit_id;
    private String work_instruction_id;
    private String rating;
    private String remarks;
    private ArrayList<SaveAuditMediaDOP> AuditMedia;
    private ArrayList<SaveAuditFailureDOP> FailurePoints;
    private ArrayList<SaveAudidUsersDOP> Users;

    public ArrayList<SaveAudidUsersDOP> getUsers() {
        return Users;
    }

    public void setUsers(ArrayList<SaveAudidUsersDOP> users) {
        Users = users;
    }

    public String getSop_audit_id() {
        return sop_audit_id;
    }

    public void setSop_audit_id(String sop_audit_id) {
        this.sop_audit_id = sop_audit_id;
    }

    public String getWork_instruction_id() {
        return work_instruction_id;
    }

    public void setWork_instruction_id(String work_instruction_id) {
        this.work_instruction_id = work_instruction_id;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public ArrayList<SaveAuditMediaDOP> getAuditMedia() {
        return AuditMedia;
    }

    public void setAuditMedia(ArrayList<SaveAuditMediaDOP> auditMedia) {
        AuditMedia = auditMedia;
    }

    public ArrayList<SaveAuditFailureDOP> getFailurePoints() {
        return FailurePoints;
    }

    public void setFailurePoints(ArrayList<SaveAuditFailureDOP> failurePoints) {
        FailurePoints = failurePoints;
    }
}
