package com.seekex.hrmodule.sop;

import com.seekx.webService.models.RequestBase;

public class SaveAuditdataReq extends RequestBase {

    private SaveAuditDataReq2 data;
    private String subsection_id;

    public SaveAuditDataReq2 getData() {
        return data;
    }

    public void setData(SaveAuditDataReq2 data) {
        this.data = data;
    }
}
