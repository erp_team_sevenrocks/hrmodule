package com.seekex.hrmodule.sop;

import com.seekex.hrmodule.training.CodeSaveReq2;
import com.seekx.webService.models.RequestBase;

public class SubSectionListReq extends RequestBase {

    private String sop_master_id;


    public String getSop_master_id() {
        return sop_master_id;
    }

    public void setSop_master_id(String sop_master_id) {
        this.sop_master_id = sop_master_id;
    }
}
