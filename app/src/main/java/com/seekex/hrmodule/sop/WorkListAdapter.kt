package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.WorkAdapterBinding
import com.seekex.hrmodule.sop.MediaFilesAdapter
import com.seekex.hrmodule.sop.SopActivity
import com.seekex.hrmodule.sop.WorkListResponse2
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*

class WorkListAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<WorkListAdapter.ViewHolder>() {
    private val sopActivity: SopActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<WorkListResponse2> = ArrayList()

    fun setDataValues(items: ArrayList<WorkListResponse2>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        sopActivity = context as SopActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = WorkAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.txtAudit.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)
        }
        return ViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: WorkAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: WorkListResponse2) {
            binding.model = item
            binding.executePendingBindings()
            var adapterskills =
                MediaFilesAdapter(sopActivity, object :AdapterListener{
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                    }

                })
            binding.mediaRec.setLayoutManager(
                LinearLayoutManager(
                    sopActivity,
                    LinearLayoutManager.VERTICAL,
                    true
                )
            )
            binding.mediaRec.adapter = adapterskills
            adapterskills.setDataValues(items.get(position).workInstructionMedia)
            adapterskills.notifyDataSetChanged()

        }

    }

}