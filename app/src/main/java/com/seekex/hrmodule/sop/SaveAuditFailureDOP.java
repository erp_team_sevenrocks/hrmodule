package com.seekex.hrmodule.sop;

import com.seekx.webService.models.RequestBase;

public class SaveAuditFailureDOP extends RequestBase {

    private String point;

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }
}
