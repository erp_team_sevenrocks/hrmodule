
package com.seekex.hrmodule.sop;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.training.CodeGetResponse2;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class WorkListResponse extends BaseResponse {
    @SerializedName("data")
    private ArrayList<WorkListResponse2> data;

    public ArrayList<WorkListResponse2> getData() {
        return data;
    }

    public void setData(ArrayList<WorkListResponse2> data) {
        this.data = data;
    }
}

