
package com.seekex.hrmodule.sop;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class SavePrimaryResponse extends BaseResponse {
    @SerializedName("saved_id")
    private String saved_id;

    public String getSaved_id() {
        return saved_id;
    }

    public void setSaved_id(String saved_id) {
        this.saved_id = saved_id;
    }
}

