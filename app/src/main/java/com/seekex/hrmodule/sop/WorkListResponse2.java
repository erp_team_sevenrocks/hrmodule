
package com.seekex.hrmodule.sop;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class WorkListResponse2 extends BaseResponse {

    @SerializedName("WorkInstructionMedia")
    private ArrayList<WorkMediaDTO> WorkInstructionMedia;
    @SerializedName("audited")
    private String audited;
    @SerializedName("id")
    private String id;
    @SerializedName("sop_subsection_id")
    private String sop_subsection_id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("remarks")
    private String remarks;
    @SerializedName("created")
    private String created;
    @SerializedName("modified")
    private String modified;
    @SerializedName("sop_subsection")
    private String sop_subsection;
    @SerializedName("contributer")
    private String contributer;
    @SerializedName("primary_desig_name")
    private String primary_desig_name;
    @SerializedName("secondary_desig_name")
    private String secondary_desig_name;
    @SerializedName("strike")
    private String strike;
    @SerializedName("reward")
    private String reward;
    @SerializedName("current_status")
    private String current_status;

    public String getAudited() {
        return audited;
    }
    public Boolean showButton() {
        if(audited.equals("0")){
            return true;
        }else{
            return false;
        }
    }

    public void setAudited(String audited) {
        this.audited = audited;
    }

    public ArrayList<WorkMediaDTO> getWorkInstructionMedia() {
        return WorkInstructionMedia;
    }

    public void setWorkInstructionMedia(ArrayList<WorkMediaDTO> workInstructionMedia) {
        WorkInstructionMedia = workInstructionMedia;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSop_subsection_id() {
        return sop_subsection_id;
    }

    public void setSop_subsection_id(String sop_subsection_id) {
        this.sop_subsection_id = sop_subsection_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getSop_subsection() {
        return sop_subsection;
    }

    public void setSop_subsection(String sop_subsection) {
        this.sop_subsection = sop_subsection;
    }

    public String getContributer() {
        return contributer;
    }

    public void setContributer(String contributer) {
        this.contributer = contributer;
    }

    public String getPrimary_desig_name() {
        return primary_desig_name;
    }

    public void setPrimary_desig_name(String primary_desig_name) {
        this.primary_desig_name = primary_desig_name;
    }

    public String getSecondary_desig_name() {
        return secondary_desig_name;
    }

    public void setSecondary_desig_name(String secondary_desig_name) {
        this.secondary_desig_name = secondary_desig_name;
    }

    public String getStrike() {
        return strike;
    }

    public void setStrike(String strike) {
        this.strike = strike;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getCurrent_status() {
        return current_status;
    }

    public void setCurrent_status(String current_status) {
        this.current_status = current_status;
    }
}

