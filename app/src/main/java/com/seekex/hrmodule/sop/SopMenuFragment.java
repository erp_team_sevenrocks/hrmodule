package com.seekex.hrmodule.sop;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.adapter.ChooseAdapter;
import com.seekex.hrmodule.custombinding.ExpandableHeightGridView;
import com.seekex.hrmodule.utils.Data.DataUtils;
import com.seekx.utils.ExtraUtils;
import com.sevenrocks.taskapp.appModules.grievance.ScanTrainingCodeRegistration;
import com.sevenrocks.taskapp.appModules.markrewarks.SopAuditList;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SopMenuFragment extends Fragment {




    @BindView(R.id.gridview_menu)
    ExpandableHeightGridView grid_views;

    @BindView(R.id.txtheaderre)
    TextView tvHeader;

    private SopActivity sopActivity  ;

    public SopMenuFragment() {
        // Required empty public constructor
    }

    public static Fragment getInstance(){
        return new SopMenuFragment();
    }

    @SuppressLint("SetTextI18n")
    void setTvHeader(){
        tvHeader.setText("Manage Training");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup BasicActi, Bundle savedInstanceState) {

        // Inflate the layout for getActivity() fragment
        View rootView = inflater.inflate(R.layout.choose_activty, BasicActi, false);
        ButterKnife.bind(this,rootView);

        init();

        setListener();

        setTvHeader();

        return rootView;

    }

    private  void init(){

        sopActivity =((SopActivity)getActivity());
        setAapter();
    }


    private void setListener(){


    }

    private void setAapter() {
        ChooseAdapter chooseAdapter = new ChooseAdapter(getContext(), DataUtils.getSopData(Objects.requireNonNull(getActivity()))) {

            @Override
            protected void onViewClicked(View v, String s) {
                switch (s) {

                    case "1":
                        ExtraUtils.changeFragment(sopActivity.getSupportFragmentManager(), SopAuditList.getInstance(), true);
                        break;


                }
            }
        };
        grid_views.setAdapter(chooseAdapter);
        grid_views.setExpanded(true);
    }




}
