package com.seekex.hrmodule.sop;

import com.seekx.webService.models.RequestBase;

public class SavepredatReq extends RequestBase {

    private SavepredatReq2 data;

    public SavepredatReq2 getData() {
        return data;
    }

    public void setData(SavepredatReq2 data) {
        this.data = data;
    }
}
