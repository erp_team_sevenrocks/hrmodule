package com.seekex.hrmodule.sop;

import com.seekx.webService.models.RequestBase;

public class WorkListReq extends RequestBase {

    private String subsection_id;
    private String record_id;

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }

    public String getSubsection_id() {
        return subsection_id;
    }

    public void setSubsection_id(String subsection_id) {
        this.subsection_id = subsection_id;
    }
}
