package com.seekex.hrmodule.sop;

import com.seekx.webService.models.RequestBase;

public class SOpUsersReq extends RequestBase {

    private String type;
    private String type_id;
    private String location_id;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }
}
