package com.seekex.hrmodule.sop;

import com.seekx.webService.models.RequestBase;

public class SavepredatReq2 extends RequestBase {

    private String sop_master_id;
    private String sop_subsection_id;
    private String name;
    private String description;
    private String record_id;
    private String location_id;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getSop_master_id() {
        return sop_master_id;
    }

    public void setSop_master_id(String sop_master_id) {
        this.sop_master_id = sop_master_id;
    }

    public String getSop_subsection_id() {
        return sop_subsection_id;
    }

    public void setSop_subsection_id(String sop_subsection_id) {
        this.sop_subsection_id = sop_subsection_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRecord_id() {
        return record_id;
    }

    public void setRecord_id(String record_id) {
        this.record_id = record_id;
    }
}
