package com.sevenrocks.taskapp.appModules.markrewarks

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.databinding.ImageItemBinding
import com.seekex.hrmodule.databinding.VideoItemBinding
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.sop.*
import com.seekex.hrmodule.training.SpinnerAdapter_Department
import com.seekex.hrmodule.training.SpinnerAdapter_Type
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.bt_save
import kotlinx.android.synthetic.main.add_reward.ll_attach
import kotlinx.android.synthetic.main.add_reward.tvTimers
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.failure_item.view.*
import kotlinx.android.synthetic.main.image_view.*
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.play_audio.*
import kotlinx.android.synthetic.main.play_video.*
import kotlinx.android.synthetic.main.play_video.btn_pause
import kotlinx.android.synthetic.main.play_video.btn_play
import kotlinx.android.synthetic.main.rewardlist.view.*
import kotlinx.android.synthetic.main.sopauditlist.*
import kotlinx.android.synthetic.main.sopauditlist.view.*
import net.alhazmy13.mediapicker.Video.VideoPicker
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


/**
 */
class SopAuditList : Fragment() {
    private lateinit var workModel: WorkListResponse2
    private var ratingValue: Float = 0.0f
    private var userId: String? = "0"
    private var masterId: String? = ""
    private var teamId: String? = ""
    private var sectionId: String? = ""
    private var recordId: String? = ""
    private var userType: String? = ""
    private var userTypeVal: String? = "1"
    var inflateList = ArrayList<InflateModel>()
    private lateinit var myAdapter: WorkListAdapter
    var mediaRecorder: MediaRecorder? = null
    var countDownTimer: CountDownTimer? = null
    var isRecorded = 0
    private var count: Long? = null
    private var timer = ""
    private var file_name = ""
    private var permissionCallBack: PermissionCallBack? = null
    private var call_Counter = 0
    var failureList: ArrayList<SaveAuditFailureDOP> = java.util.ArrayList()
    var auditMedia: ArrayList<SaveAuditMediaDOP> = java.util.ArrayList()
    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var selUserlist: ArrayList<SaveAudidUsersDOP> = java.util.ArrayList()
    var mulUserlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var locationList: ArrayList<DropdownModel> = java.util.ArrayList()
    var teamList: ArrayList<DropdownModel> = java.util.ArrayList()
    var masterList: ArrayList<DropdownModel> = java.util.ArrayList()
    var subsectinList: ArrayList<DropdownModel> = java.util.ArrayList()
    var departMentList: ArrayList<DropdownModel> = java.util.ArrayList()
    var worklist: ArrayList<WorkListResponse2> = java.util.ArrayList()

    private lateinit var adapterDepartment: SpinnerAdapter_Department
    private lateinit var adapterType: SpinnerAdapter_Type

    public var sopActivity: SopActivity? = null
    public var imageList = ArrayList<ImageDTO>()
    public var videoList = ArrayList<VideoDTO>()
    private lateinit var rtview: View


    var departmentId: String = "0"
    var locationId: String = ""
    var tTypeId: String = ""
    lateinit var apiImp: ApiImp
    lateinit var rootView: View


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.sopauditlist, BasicActi, false)
        sopActivity = activity as SopActivity?
        rootView.txtheader.setText("Sop Audit")
        setAdapters(rootView)
        setlistener(rootView)
        setVisibility(rootView, false)
        getLocation()
        return rootView

    }


    fun addlayout(
        llAtach: LinearLayout
    ) {

        val context = llAtach.context

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                as LayoutInflater

        rtview = inflater.inflate(R.layout.failure_item, null)

        inflateList.add(
            InflateModel(
                rtview as LinearLayout?,
                rtview.findViewById(R.id.edt_topics),
            )
        )
        rtview.cancel_topics.setTag(inflateList.size - 1)
        llAtach.addView(rtview)

        rtview.cancel_topics.setOnClickListener {
            var ss: Int = it.getTag() as Int
            var deleteModel = inflateList.get(ss)
            llAtach.removeView(deleteModel.layout)


        }

    }

    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {

            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    fun showDynamicImage(
        llAtach: LinearLayout,
        imageList: ArrayList<ImageDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = ImageItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
                showImageUri(imageDTO.getUri(), context)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Image",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showDynamicVideo(
        llAtach: LinearLayout,
        imageList: ArrayList<VideoDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = VideoItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
//                showImageUri(imageDTO.getUri(), context)
                openVideoPLayer(imageDTO.path)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Video",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

        private fun openVideoPLayer(toString: String) {

        val dialog = Dialog(activity!!, android.R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_video)

        dialog.setCancelable(true)

        dialog.pb_video.visibility=View.VISIBLE
        val mc = MediaController(activity!!)
        mc.setAnchorView(dialog.videoview)
        mc.setMediaPlayer(dialog.videoview)
        dialog.videoview.setMediaController(mc);
        val video =
            Uri.parse(toString)
        dialog.videoview.setVideoPath(toString)
        dialog.videoview.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
            dialog.pb_video.visibility = View.GONE

            mp.isLooping = true
            dialog.videoview.start()
        })



        dialog.setOnDismissListener {
            dialog.dismiss()
        }


        dialog.btn_play.setOnClickListener {


        }
        dialog.btn_pause.setOnClickListener {

        }

        dialog.setOnDismissListener {
        }
        dialog.show()


    }
    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }

    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@SopAuditList, options)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 110) {
                val bmThumbnail: Bitmap?

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                bmThumbnail = ThumbnailUtils.createVideoThumbnail(
                    returnValue.get(0).toString(),
                    MediaStore.Video.Thumbnails.MICRO_KIND
                )
                Log.e("TAG", "onActivityResult: $bmThumbnail ")
                videoList.add(
                    VideoDTO(bmThumbnail!!, returnValue.get(0))
                )
                showDynamicVideo(ll_attachvideo!!,
                    videoList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
//                            videoList = any as ArrayList<ImageDTO>
                        }
                    })
            }

            if (requestCode == 100) {
                val bmThumbnail: Bitmap?

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                bmThumbnail = ThumbnailUtils.createVideoThumbnail(
                    returnValue.get(0).toString(),
                    MediaStore.Video.Thumbnails.MICRO_KIND
                )

                imageList.add(
                    ImageDTO(
                        Uri.parse("file://" + returnValue.get(0)).toString()
                    )
                )
                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })
            } else {
                //                val fileUri = intent?.data
//                imageList.add(ImageDTO(getCompressedUri(fileUri!!, activity!!).toString()))
//                showDynamicImage(ll_attach!!,
//                    imageList, object : SelectedMasterCallback {
//                        override fun onSelected(any: Any) {
//                            imageList = any as ArrayList<ImageDTO>
//                        }
//                    })
            }
        }
    }

    private fun stopMedia() {
        try {
            mediaRecorder!!.stop()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun startRecording() {
        mediaRecorderReady()
        try {
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startCountdownTimer()
    }


    private fun startCountdownTimer() {
        count = 0L
        countDownTimer = object : CountDownTimer(10800000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // Used for formatting digit to be in 2 digits only
                count = count!! + 1
                tvTimers!!.text = ExtraUtils.convertSecondToHHMMString(count!!)
            }

            // When the task is over it will print 00:00:00 there
            override fun onFinish() {
                tvTimers!!.text = "00:00:00"
            }
        }.start()
    }

    private fun mediaRecorderReady() {
        val currentTime = System.currentTimeMillis()
        val extBaseDir = activity?.externalCacheDir
        val file = File(extBaseDir!!.absolutePath + "/SeekSop/" + "")
        if (!file.exists()) {
            if (!file.mkdirs()) {
            }
        }
        file_name = "$file/myRecording(${System.currentTimeMillis()}).mp3"
        Log.e("TAG", "mediaRecorderReady: $file_name")
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        mediaRecorder!!.setOutputFile(file_name)
    }

    private fun setlistener(rootView: View?) {

        rootView!!.sopratingbar.onRatingBarChangeListener =
            RatingBar.OnRatingBarChangeListener { _, rating, _ ->

                ratingValue = rating
                if (rating < 4) {
                    rootView.ll_failurepoints.visibility = View.VISIBLE
                } else {
                    rootView.ll_failurepoints.visibility = View.GONE
                }

            }

        rootView!!.img_addfailure.setOnClickListener {
            addlayout(rootView!!.ll_attachsop)

        }


        rootView!!.img_sopback.setOnClickListener {
            setVisibility(rootView!!, false)
        }

        rootView!!.imagessop.setOnClickListener {
            pickImage()
        }
        rootView!!.videossop.setOnClickListener {
            pickVideo()
        }
        rootView!!.iv_actionsop.setOnClickListener {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    if (isRecorded == 0) {
                        isRecorded = 1
                        playBeep()
                        startRecording()
                        iv_actionrew!!.setImageResource(android.R.drawable.ic_media_pause)
                    } else if (isRecorded == 1) {
                        bt_save!!.visibility = View.INVISIBLE
                        stopMedia()
                        playBeep()
                        isRecorded = 2
                        countDownTimer!!.cancel()
                        timer = tvTimers!!.text.toString()
                        iv_actionrew!!.setImageResource(android.R.drawable.ic_media_play)
                    } else if (isRecorded == 2) {
//                        FileUtils.openDocument(file_name, activity)
                    }

                }

            })


        }
        rootView!!.bt_resetsop.setOnClickListener {
            try {
                isRecorded = 0
                stopMedia()
                playBeep()
                countDownTimer!!.cancel()
                tvTimers!!.text = ""
                bt_save!!.visibility = View.GONE
                iv_actionrew!!.setImageDrawable(activity!!.resources.getDrawable(R.drawable.audiorec))
            } catch (e: Exception) {
            }
        }



        rootView!!.edtteam.setOnClickListener {

            if (userType.equals("Team")) {
                DataUtils.openSearchDialoge(
                    activity!!,
                    teamList,
                    "Select Team",
                    object : SearchCallback {
                        override fun onTextTyped(
                            txt: String,
                            searchResultCallback: SearchResultCallback
                        ) {
                        }

                        override fun onSelected(any: Any?) {
                            var ss = any as DropdownModel
                            Log.e("TAG", "onSelected: " + ss.name)
                            rootView.edtteam.setText(ss.name)
                            teamId = ss.id
                            tTypeId = teamId.toString()
                            getMaster("1", teamId!!)
                        }

                    })
            } else if (userType.equals("Department")) {
                DataUtils.openSearchDialoge(
                    activity!!,
                    departMentList,
                    "Select Department",
                    object : SearchCallback {
                        override fun onTextTyped(
                            txt: String,
                            searchResultCallback: SearchResultCallback
                        ) {
                        }

                        override fun onSelected(any: Any?) {
                            var ss = any as DropdownModel
                            Log.e("TAG", "onSelected: " + ss.name)
                            rootView.edtteam.setText(ss.name)
                            departmentId = ss.id
                            tTypeId = departmentId.toString()
                            getMaster("0", departmentId)
                        }

                    })

            }
        }

        rootView!!.edt_sopmaster.setOnClickListener {

            if (rootView!!.edt_namee.text.toString().trim().equals("")) {
                rootView!!.edt_namee.setError("Enter Name")
                return@setOnClickListener
            }

            if (rootView!!.edt_description.text.toString().trim().equals("")) {
                rootView!!.edt_description.setError("Enter Description")
                return@setOnClickListener
            }

            if (userTypeVal.equals("1") && teamId.equals("")) {
                Toast.makeText(activity, "Select Team", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (userTypeVal.equals("0") && departmentId.equals("")) {
                Toast.makeText(activity, "Select Department", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            DataUtils.openSearchDialoge(
                activity!!,
                masterList,
                "Select Master",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edt_sopmaster.setText(ss.name)
                        masterId = ss.id
                        if (!masterId.equals("")) {
                            getSubsection(masterId!!)

                        }
                    }

                })
        }
        rootView!!.edt_subsection.setOnClickListener {

            if (masterId.equals("")) {
                Toast.makeText(activity, "select master first", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            DataUtils.openSearchDialoge(
                activity!!,
                subsectinList,
                "Select Sub Section",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edt_subsection.setText(ss.name)
                        sectionId = ss.id
                        if (!sectionId.equals("")) {

                        }
                    }

                })
        }
        rootView!!.edt_soplocation.setOnClickListener {
            if (masterId.equals("")) {
                Toast.makeText(activity, "select master first", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            DataUtils.openSearchDialoge(
                activity!!,
                locationList,
                "Select Location",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edt_soplocation.setText(ss.name)
                        locationId = ss.id
                        getUsers()
                        savePrimaryData(rootView!!)

                    }

                })
        }
        rootView!!.edt_sopuser.setOnClickListener {

            DataUtils.openSearchDialogeMultiSelect(
                activity!!,
                mulUserlist,
                userlist,
                "Select User",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        selUserlist.clear()
                        val list: ArrayList<String> = ArrayList()

                        rootView.txtusersname.setText("")
                        var ss = any as ArrayList<DropdownModel>
                        Log.e("TAG", "onSelected: " + ss.size)

                        for (dd in ss) {
                            var aa = SaveAudidUsersDOP()
                            aa.user_id = dd.id
                            list.add(dd.name)
                            selUserlist.add(aa)

                        }
                        if (ss.size > 1) {
                            rootView.txtusersname.setText(TextUtils.join(", ", list))
                        } else {
                            rootView.edt_sopuser.setText(TextUtils.join(", ", list))

                        }

//                        userId = ss.id

                    }

                })
        }
        rootView!!.btn_savedata.setOnClickListener {
            if (selUserlist.size == 0) {
                Toast.makeText(activity, "Select User First", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (ratingValue == 0.0f) {
                Toast.makeText(activity, "Add rating", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (imageList.size > 0) {
                uploadImages()
            } else if (videoList.size > 0) {
                uploadVideo()
            } else if (bt_save.visibility == View.INVISIBLE) {
                saveaudiofile()
            } else {
                saveauditdata(rootView!!)
            }


        }
    }

    private fun pickVideo() {

        val options: Options = Options.init()
            .setRequestCode(110) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Video) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@SopAuditList, options)
    }

    private fun saveaudiofile() {
        sopActivity!!.apiImp.showProgressBar()
        sopActivity!!.apiImp.saveAudio(file_name, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                sopActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    var audioFilename = ss.data.path + "" + ss.data.filename
                    var model = SaveAuditMediaDOP()
                    model.type = "audio"
                    model.name = audioFilename
                    auditMedia.add(model)


                    saveauditdata(rootView)


                }

            }

        })


    }

    private fun uploadImages() {

        sopActivity!!.apiImp.showProgressBar()
        sopActivity?.apiImp?.hitApi(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                sopActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)


                    var imageFilename = ss.data.path + ss.data.filename
                    var model = SaveAuditMediaDOP()
                    model.type = "image"
                    model.name = imageFilename
                    auditMedia.add(model)

                    call_Counter++

                    if (call_Counter < imageList.size) {
                        uploadImages()
                    } else {
                        call_Counter = 0

                        if (videoList.size > 0) {
                            uploadVideo()
                        } else
                            if (bt_save.visibility == View.INVISIBLE) {
                                saveaudiofile()
                            } else {
                                saveauditdata(rootView)
                            }
                    }
                }
            }
        })
    }

    private fun uploadVideo() {

        sopActivity!!.apiImp.showProgressBar()
        sopActivity?.apiImp?.hitApivideo(call_Counter, videoList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                sopActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)


                    var imageFilename = ss.data.path + ss.data.filename
                    var model = SaveAuditMediaDOP()
                    model.type = "video"
                    model.name = imageFilename
                    auditMedia.add(model)

                    call_Counter++

                    if (call_Counter < videoList.size) {
                        uploadVideo()
                    } else {
                        call_Counter = 0
                        if (bt_save.visibility == View.INVISIBLE) {
                            saveaudiofile()
                        } else {
                            saveauditdata(rootView)
                        }
                    }
                }
            }
        })
    }

    private fun setAdapters(rootView: View) {
        myAdapter = WorkListAdapter(activity, object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                workModel = any1 as WorkListResponse2
                when (i) {
                    2 -> {
                        setVisibility(rootView!!, true)


                    }

                }
            }

        })
        rootView.rec_worklist!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.rec_worklist!!.adapter = myAdapter

        myAdapter!!.notifyDataSetChanged()
        rootView.spin_sopteam.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: String = adapterView!!.getItemAtPosition(position).toString()!!
                userType = user

                if (userType.equals("Team")) {
                    userTypeVal = "1"
                    rootView!!.edtteam.setText("")
                    getTeam()
                } else if (userType.equals("Department")) {
                    userTypeVal = "0"
                    rootView!!.edtteam.setText("")
                    getdepartment()
                }
            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })


    }

    private fun setVisibility(rootView: View, isshow: Boolean) {
        if (isshow) {
            rootView.ll_sopmain.visibility = View.GONE
            rootView.ll_sopaudit.visibility = View.VISIBLE
            rootView.img_sopback.visibility = View.VISIBLE
        } else {
            rootView.ll_sopaudit.visibility = View.GONE
            rootView.img_sopback.visibility = View.GONE
            rootView.ll_sopmain.visibility = View.VISIBLE
        }


    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getsoplist() {

        val getMasterRequest = GetKrasReq()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.user_id = userId
        getMasterRequest.service_name = ApiUtils.GETKRADATA


//        sopActivity!!.apiImp.getKrasData(getMasterRequest, object : ApiCallBack {
//            override fun onSuccess(status: Boolean, any: Any) {
//                if (status) {
//                    var ss = any as KraListResponse
//                    kradatalist.addAll(ss.data)
//                    adapterKraList!!.setDataValues(kradatalist)
//                    adapterKraList!!.notifyDataSetChanged()
//
//                }
//
//
//            }
//
//        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUsers() {

        val getMasterRequest = SOpUsersReq()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SOPUSERS
        getMasterRequest.type = userTypeVal
        getMasterRequest.type_id = tTypeId
        getMasterRequest.location_id = locationId


        sopActivity!!.apiImp.getUsersList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    userlist.addAll(ss.data)

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getLocation() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SOPLOCATION


        sopActivity!!.apiImp.getDropdownList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    locationList.addAll(ss.data)

                }


            }

        })
    }

    fun getPoints(rootView: View): ArrayList<SaveAuditFailureDOP> {
        var total = ""

        var sstopics = ArrayList<SaveAuditFailureDOP>()

        //get each ViewGroup
        for (i in 0 until rootView.ll_attachsop.childCount) {
            var aa = SaveAuditFailureDOP()

            val vg: View = rootView.ll_attachsop.get(i)
            val e = vg.findViewById(R.id.edt_topics) as EditText
            val str = e.text.toString()
            aa.point = str
            sstopics.add(aa)
        }
        return sstopics
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveauditdata(rootView: View) {

        val getMasterRequest = SaveAuditdataReq()
        val req2 = SaveAuditDataReq2()


        req2.remarks = rootView.edt_remark.text.toString()
        req2.rating = ratingValue.toString()
        req2.work_instruction_id = workModel.id
        req2.sop_audit_id = recordId
        req2.failurePoints = getPoints(rootView!!)
        req2.users = selUserlist
        req2.auditMedia = auditMedia

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SAVEAUDIT
        getMasterRequest.data = req2


        sopActivity!!.apiImp.saveAuditData(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Toast.makeText(activity, "Audit Saved", Toast.LENGTH_SHORT).show()
                    sopActivity!!.finish()
                }


            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun savePrimaryData(rootView: View) {

        val getMasterRequest = SavepredatReq()
        val req2 = SavepredatReq2()
        req2.description = rootView.edt_description.text.toString()
        req2.name = rootView.edt_namee.text.toString()
        req2.record_id = recordId
        req2.sop_master_id = masterId
        req2.sop_subsection_id = sectionId
        req2.location_id = locationId

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SAVESOPWORK
        getMasterRequest.data = req2


        sopActivity!!.apiImp.savePrimaryData(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as SavePrimaryResponse
                    recordId = ss.saved_id

                    getWorkInstructions(sectionId!!)
                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getWorkInstructions(sectionId: String) {
        worklist.clear()
        val getMasterRequest = WorkListReq()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SOPWORK
        getMasterRequest.subsection_id = sectionId
        getMasterRequest.record_id = recordId


        sopActivity!!.apiImp.getWorkInst(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as WorkListResponse
                    worklist.addAll(ss.data)
                    myAdapter!!.setDataValues(worklist!!)
                    myAdapter!!.notifyDataSetChanged()
                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getMaster(s: String, teamId: String) {
        masterList.clear()
        val getMasterRequest = MasterListReq()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SOPMASTER
        getMasterRequest.type = s
        getMasterRequest.type_id = teamId


        sopActivity!!.apiImp.getMasterList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    masterList.addAll(ss.data)

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getTeam() {
        teamList.clear()
        val getMasterRequest = RequestBase()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GETTEAM


        sopActivity!!.apiImp.getDropdownList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    teamList.addAll(ss.data)

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getSubsection(masterId: String) {
        subsectinList.clear()
        val getMasterRequest = SubSectionListReq()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SOPSUBSECTION
        getMasterRequest.sop_master_id = masterId


        sopActivity!!.apiImp.getSubSection(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    subsectinList.addAll(ss.data)

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getdepartment() {
        departMentList.clear()
        val getMasterRequest = RequestBase()

        getMasterRequest.token = sopActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GETDEPARTMENTSOP


        sopActivity!!.apiImp.getDropdownList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
                    departMentList.addAll(ss.data)

                }


            }

        })
    }


    companion object {

        @JvmField
        var reasonList = ArrayList<DropdownModel>()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = SopAuditList()
    }
}