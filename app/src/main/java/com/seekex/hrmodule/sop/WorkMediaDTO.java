
package com.seekex.hrmodule.sop;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class WorkMediaDTO extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("work_instruction_id")
    private String work_instruction_id;
    @SerializedName("media_type")
    private String media_type;
    @SerializedName("url")
    private String url;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWork_instruction_id() {
        return work_instruction_id;
    }

    public void setWork_instruction_id(String work_instruction_id) {
        this.work_instruction_id = work_instruction_id;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Boolean checkIsAudio() {

        if (media_type.equals("audio")){
            return true;
        }else{
            return false;
        }
    }
    public Boolean checkIsExcel() {
        if (media_type.equals("file")){
            return true;
        }else{
            return false;
        }
    }
    public Boolean checkIsVideo() {
        if (media_type.equals("video")){
            return true;
        }else{
            return false;
        }
    }
    public Boolean checkIsImage() {
        if (media_type.equals("image")){
            return true;
        }else{
            return false;
        }
    }

}
