
package com.seekex.hrmodule.mysalary;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;

import java.util.ArrayList;

public class DeclareDTO {

    @SerializedName("salary_payment_mode")
    private String salary_payment_mode;
    @SerializedName("basic_salary")
    private String basic_salary;
    @SerializedName("hra")
    private String hra;
    @SerializedName("others")
    private String others;
    @SerializedName("shift")
    private String shift;

    @SerializedName("one_hour_salary")
    private String one_hour_salary;
    @SerializedName("one_day_salary")
    private String one_day_salary;
    public void setSalary_payment_mode(String salary_payment_mode) {
        this.salary_payment_mode = salary_payment_mode;
    }

    public String getSalary_payment_mode() {
        return salary_payment_mode;
    }

    public String getBasic_salary() {
        return basic_salary;
    }

    public void setBasic_salary(String basic_salary) {
        this.basic_salary = basic_salary;
    }

    public String getHra() {
        return hra;
    }

    public void setHra(String hra) {
        this.hra = hra;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getOne_hour_salary() {
        return one_hour_salary;
    }

    public void setOne_hour_salary(String one_hour_salary) {
        this.one_hour_salary = one_hour_salary;
    }

    public String getOne_day_salary() {
        return one_day_salary;
    }

    public void setOne_day_salary(String one_day_salary) {
        this.one_day_salary = one_day_salary;
    }
}
