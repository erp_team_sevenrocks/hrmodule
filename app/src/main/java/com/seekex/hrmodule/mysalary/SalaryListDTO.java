
package com.seekex.hrmodule.mysalary;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class SalaryListDTO extends BaseResponse {

    @SerializedName("declare")
    private DeclareDTO declare;
    @SerializedName("calculate")
    private CalculateDTO calculate;

    public DeclareDTO getDeclare() {
        return declare;
    }

    public void setDeclare(DeclareDTO declare) {
        this.declare = declare;
    }

    public CalculateDTO getCalculate() {
        return calculate;
    }

    public void setCalculate(CalculateDTO calculate) {
        this.calculate = calculate;
    }
}
