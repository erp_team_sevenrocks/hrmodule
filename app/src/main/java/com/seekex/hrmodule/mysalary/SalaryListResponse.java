
package com.seekex.hrmodule.mysalary;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.leave_manager.LeaveListDTO;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class SalaryListResponse extends BaseResponse {

    @SerializedName("data")
    private SalaryListDTO data;

    public SalaryListDTO getData() {
        return data;
    }

    public void setData(SalaryListDTO data) {
        this.data = data;
    }
}

