package com.seekex.hrmodule.mysalary;


import com.seekex.hrmodule.leave_manager.GetLEaveReq2;
import com.seekx.webService.models.RequestBase;

public class GetSalaryReq2 extends RequestBase {

    private String month  ;
    private String year  ;

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }
}
