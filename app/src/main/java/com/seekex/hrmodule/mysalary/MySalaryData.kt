package com.sevenrocks.taskapp.appModules.vendorreg

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.leave_manager.AcceptReq
import com.seekex.hrmodule.leave_manager.AcceptReq2
import com.seekex.hrmodule.leave_manager.PdfResponse
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.mysalary.SalaryActivity
import com.seekex.hrmodule.mysalary.SalaryListResponse
import com.seekex.hrmodule.utils.FileOpener
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import kotlinx.android.synthetic.main.rewardlist.view.txtheaderre
import kotlinx.android.synthetic.main.salary_list.view.*
import java.io.File


/**
 */
class MySalaryData : Fragment() {

    private var pdfUrl: String? = ""
    private var csvUrl: String? = ""
    public var leaveActivity: SalaryActivity? = null
    private var rootView: View? = null
    var check = 0
    var monthpos: String = "1"
    var monthtext: String = "January"
    var yearpos: String = "2020"

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.salary_list, BasicActi, false)
        leaveActivity = activity as SalaryActivity?
        rootView!!.btn_salaryaccept.setOnClickListener {
            acceptReject("1")
        }

        rootView!!.btn_salaryreject.setOnClickListener {
            acceptReject("0")
        }

        rootView!!.btn_downloadpdf.setOnClickListener {
//            initializeDownloader(pdfUrl!!)
            FileOpener.open(leaveActivity, pdfUrl!!);
        }
        rootView!!.btn_downloadcsv.setOnClickListener {
//            initializeDownloader(csvUrl!!)
            FileOpener.open(leaveActivity, csvUrl!!);
        }
        rootView!!.spin_year.setSelection(5)

        rootView!!.spin_months.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (++check > 1) {
                        try {
                            var search_type = parent!!.getItemAtPosition(position).toString()
                            if (!search_type.equals(monthtext)) {
                                monthpos = (position + 1).toString()
                                monthtext = search_type
                                clearData()
                                getList()
                            }


                        } catch (e: Exception) {
                        }

                    }

                }

            }
        rootView!!.spin_year.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (++check > 1) {
                    try {
                        var search_type = parent!!.getItemAtPosition(position).toString()
                        if (!yearpos.equals(search_type)) {
                            yearpos = search_type
                            clearData()
                            getList()
                        }

                    } catch (e: Exception) {
                    }
                }


            }

        }
        val adapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.months_list,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        rootView?.spin_months?.setAdapter(adapter)
//

        val countries: ArrayList<String> = ArrayList()
//        countries.add("2015")
//        countries.add("2016")
//        countries.add("2017")
//        countries.add("2018")
//        countries.add("2019")
        countries.add("2020")
        countries.add("2021")
        countries.add("2022")
        countries.add("2023")

        val yadapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.years_list,
            android.R.layout.simple_spinner_item
        )
        yadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        val aa = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, countries)

        rootView?.spin_year?.setAdapter(aa)

        rootView?.txtheaderre?.text = "My Salary"

        getList()
        return rootView
    }

    private fun clearData() {
        rootView!!.txt_monthyear.setText("")
        rootView!!.txt_basicsalary.setText("")
        rootView!!.txt_onedayhour.setText("")
        rootView!!.txt_paymentmode.setText("")
        rootView!!.txt_hra.setText("")
        rootView!!.txt_totalmonthshift.setText("")
        rootView!!.txt_onehoursalary.setText("")

        rootView!!.txt_basicsalarycal.setText("")
        rootView!!.txt_esi.setText("")
        rootView!!.txt_working.setText("")
        rootView!!.txt_leave.setText("")
        rootView!!.txt_payableamount.setText("")
        rootView!!.txt_bookedsalary.setText("")
        rootView!!.txt_adjustmentforbookedsalary.setText("")
        rootView!!.txt_paidsalary.setText("")
        rootView!!.txt_adjustmentforpaidsalary.setText("")
        rootView!!.txt_holiday.setText("")
        rootView!!.txt_ignore.setText("")
    }


    override fun onResume() {
        super.onResume()
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getList() {

        val reqData = GetSalaryReq()
        val reqData2 = GetSalaryReq2()


        reqData.service_name = ApiUtils.my_salary
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)

        reqData2.month = monthpos
        reqData2.year = yearpos
        reqData.conditions = reqData2

        leaveActivity?.apiImp?.getsalarydata(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        rootView?.let { rootView ->
                            val ss = any as SalaryListResponse
                            getsalarycsv()
                            if (ss.data.calculate.is_accepted.equals("1")) {
                                rootView.ll_buttons.visibility = View.GONE
                                rootView.btn_downloadpdf.visibility = View.VISIBLE
                                getsalarypdf()

                            } else {
                                rootView.ll_buttons.visibility = View.VISIBLE
                                rootView.btn_downloadpdf.visibility = View.GONE
                            }

                            rootView.txt_monthyear.setText(monthtext + "-" + yearpos)
                            rootView.txt_basicsalary.setText(ss.data.declare.basic_salary)
                            rootView.txt_onedayhour.setText(ss.data.declare.shift)
                            rootView.txt_paymentmode.setText(ss.data.declare.salary_payment_mode)
                            rootView.txt_hra.setText(ss.data.declare.hra)
                            rootView.txt_totalmonthshift.setText(ss.data.declare.shift)
                            rootView.txt_onehoursalary.setText(ss.data.declare.one_hour_salary)

                            rootView.txt_basicsalarycal.setText(ss.data.calculate.basic_salary)
                            rootView.txt_esi.setText(ss.data.calculate.esi)
                            rootView.txt_working.setText(ss.data.calculate.working)
                            rootView.txt_leave.setText(ss.data.calculate.leave)
                            rootView.txt_payableamount.setText(ss.data.calculate.payable_salary)
                            rootView.txt_bookedsalary.setText(ss.data.calculate.booked_salary)
                            rootView.txt_adjustmentforbookedsalary.setText(ss.data.calculate.adjustment_for_booked_salary)
                            rootView.txt_paidsalary.setText(ss.data.calculate.paid_salary)
                            rootView.txt_adjustmentforpaidsalary.setText(ss.data.calculate.adjustment_for_paid_salary)
                            rootView.txt_holiday.setText(ss.data.calculate.holiday)
                            rootView.txt_ignore.setText(ss.data.calculate.ignore)


                        }
                    }
                }

            }
        )


    }

    fun initializeDownloader(resume: String) {
        Log.e("TAG", "initializeDownloader: "+resume)
        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
        val config = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .build()
        val extBaseDir = leaveActivity?.externalCacheDir

        val file = File(extBaseDir!!.absolutePath + "/Rewaurdoc/" + "")
        PRDownloader.initialize(leaveActivity, config)

        val apkName = getFileNameFromUrl(resume)

        val apkPath = file.absolutePath//AppConstants.downloadDir

        val returnString = "$apkPath/$apkName"

        if (openDocument(returnString, activity!!)) {
            return
        }

        PRDownloader.download(resume, apkPath, apkName)
            .build()
            .setOnStartOrResumeListener {}
            .setOnPauseListener {}
            .setOnCancelListener {}
            .setOnProgressListener {}
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    openDocument(returnString, activity!!)
                }

                override fun onError(error: com.downloader.Error?) {
                }

            })
    }

    fun getFileNameFromUrl(url: String): String {
        return url.substring(url.lastIndexOf("/") + 1)
    }

    fun openDocument(paths: String, context: Context): Boolean {
        Log.e("FileType", "pdf hai")
        try {
            val file = File(paths)
            if (!file.exists())
                return false
            Log.e("FileType", "pdf hai")
            val uri = Uri.fromFile(file)
            var type = "*/*"

            // Check what kind of file you are trying to open, by comparing the paths with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) masterType,
            // so Android knew what application to use to open the file
            if (paths.contains(".doc") || paths.contains(".docx")) {
                // Word document
                type = "application/msword"
            } else if (paths.contains(".pdf")) {
                // PDF file
                type = "application/pdf"
            } else if (paths.contains(".ppt") || paths.contains(".pptx")) {
                // Powerpoint file
                type = "application/vnd.ms-powerpoint"
            } else if (paths.contains(".xls") || paths.contains(".xlsx")) {
                // Excel file
                type = "application/vnd.ms-excel"
            } else if (paths.contains(".zip") || paths.contains(".rar")) {
                // WAV audio file
                type = "application/x-wav"
            } else if (paths.contains(".rtf")) {
                // RTF file
                type = "application/rtf"
            } else if (paths.contains(".wav") || paths.contains(".mp3")) {
                // WAV audio file
                type = "audio/x-wav"
            } else if (paths.contains(".gif")) {
                // GIF file
                type = "image/gif"
            } else if (paths.contains(".jpg") || paths.contains(".jpeg") || paths.contains(".png")) {
                // JPG file
                type = "image/jpeg"
            } else if (paths.contains(".txt")) {
                // Text file
                type = "text/plain"
            } else if (paths.contains(".3gp") || paths.contains(".mpg") || paths.contains(".mpeg") || paths.contains(
                    ".mpe"
                ) || paths.contains(".mp4") || paths.contains(".avi")
            ) {
                // Video files
                type = "video/*"
            }

            Log.e("FileType", type)
            Log.v("FileType", paths)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, type)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            context.startActivity(intent)
        } catch (e: Exception) {
            Log.v("openDocument", e.toString())
            return false
        }
        return true
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getsalarypdf() {
        val reqData = GetSalaryReq()
        val reqData2 = GetSalaryReq2()


        reqData.service_name = ApiUtils.my_salary_pdf
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)

        reqData2.month = monthpos
        reqData2.year = yearpos
        reqData.conditions = reqData2


        leaveActivity?.apiImp?.getsalarypdf(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        var data = any as PdfResponse
                        pdfUrl = data.url
                    }
                }

            }
        )


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getsalarycsv() {
        val reqData = GetSalaryReq()
        val reqData2 = GetSalaryReq2()


        reqData.service_name = ApiUtils.my_salary_csv
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)

        reqData2.month = monthpos
        reqData2.year = yearpos
        reqData.conditions = reqData2


        leaveActivity?.apiImp?.getsalarypdf(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        var data = any as PdfResponse
                        csvUrl = data.url
                    }
                }

            }
        )


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun acceptReject(s: String) {
        val reqData = AcceptReq()
        val reqData2 = AcceptReq2()


        reqData.service_name = ApiUtils.my_salary_accept_or_reject
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)

        reqData2.month = monthpos
        reqData2.year = yearpos
        reqData.is_accepted = s
        reqData.conditions = reqData2


        leaveActivity?.apiImp?.acceptreject(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        clearData()
                        getList()
                    }
                }

            }
        )


    }

    companion object {


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = MySalaryData()
    }


}