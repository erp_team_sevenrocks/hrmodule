
package com.seekex.hrmodule.mysalary;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;

import java.util.ArrayList;

public class CalculateDTO {

    @SerializedName("is_accepted")
    private String is_accepted;
    @SerializedName("basic_salary")
    private String basic_salary;
    @SerializedName("hra")
    private String hra;
    @SerializedName("working")
    private String working;
    @SerializedName("ignore")
    private String ignore;
    @SerializedName("holiday")
    private String holiday;
    @SerializedName("payable_salary")
    private String payable_salary;
    @SerializedName("paid_salary")
    private String paid_salary;
    @SerializedName("paid_datetime")
    private String paid_datetime;
    @SerializedName("paid_by")
    private String paid_by;
    @SerializedName("esi")
    private String esi;
    @SerializedName("pf")
    private String pf;
    @SerializedName("tds")
    private String tds;
    @SerializedName("others")
    private String others;
    @SerializedName("booked_salary")
    private String booked_salary;
    @SerializedName("leave")
    private String leave;
    @SerializedName("adjustment_for_paid_salary")
    private String adjustment_for_paid_salary;
    @SerializedName("adjustment_for_booked_salary")
    private String adjustment_for_booked_salary;

    public String getBooked_salary() {
        return booked_salary;
    }

    public void setBooked_salary(String booked_salary) {
        this.booked_salary = booked_salary;
    }

    public String getAdjustment_for_paid_salary() {
        return adjustment_for_paid_salary;
    }

    public void setAdjustment_for_paid_salary(String adjustment_for_paid_salary) {
        this.adjustment_for_paid_salary = adjustment_for_paid_salary;
    }

    public String getAdjustment_for_booked_salary() {
        return adjustment_for_booked_salary;
    }

    public void setAdjustment_for_booked_salary(String adjustment_for_booked_salary) {
        this.adjustment_for_booked_salary = adjustment_for_booked_salary;
    }

    public String getLeave() {
        return leave;
    }

    public void setLeave(String leave) {
        this.leave = leave;
    }

    public String getEsi() {
        return esi;
    }

    public void setEsi(String esi) {
        this.esi = esi;
    }

    public String getPf() {
        return pf;
    }

    public void setPf(String pf) {
        this.pf = pf;
    }

    public String getTds() {
        return tds;
    }

    public void setTds(String tds) {
        this.tds = tds;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    public String getIs_accepted() {
        return is_accepted;
    }

    public void setIs_accepted(String is_accepted) {
        this.is_accepted = is_accepted;
    }

    public String getBasic_salary() {
        return basic_salary;
    }

    public void setBasic_salary(String basic_salary) {
        this.basic_salary = basic_salary;
    }

    public String getHra() {
        return hra;
    }

    public void setHra(String hra) {
        this.hra = hra;
    }

    public String getWorking() {
        return working;
    }

    public void setWorking(String working) {
        this.working = working;
    }

    public String getIgnore() {
        return ignore;
    }

    public void setIgnore(String ignore) {
        this.ignore = ignore;
    }

    public String getHoliday() {
        return holiday;
    }

    public void setHoliday(String holiday) {
        this.holiday = holiday;
    }

    public String getPayable_salary() {
        return payable_salary;
    }

    public void setPayable_salary(String payable_salary) {
        this.payable_salary = payable_salary;
    }

    public String getPaid_salary() {
        return paid_salary;
    }

    public void setPaid_salary(String paid_salary) {
        this.paid_salary = paid_salary;
    }

    public String getPaid_datetime() {
        return paid_datetime;
    }

    public void setPaid_datetime(String paid_datetime) {
        this.paid_datetime = paid_datetime;
    }

    public String getPaid_by() {
        return paid_by;
    }

    public void setPaid_by(String paid_by) {
        this.paid_by = paid_by;
    }
}
