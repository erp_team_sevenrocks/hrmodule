package com.seekex.hrmodule.mysalary;


import com.seekex.hrmodule.leave_manager.GetLEaveReq2;
import com.seekx.webService.models.RequestBase;

public class GetSalaryReq extends RequestBase {

    private GetSalaryReq2 conditions  ;

    public GetSalaryReq2 getConditions() {
        return conditions;
    }

    public void setConditions(GetSalaryReq2 conditions) {
        this.conditions = conditions;
    }
}
