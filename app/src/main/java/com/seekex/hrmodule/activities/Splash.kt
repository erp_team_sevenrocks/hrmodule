package com.seekex.hrmodule.activities

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekex.hrmodule.R
import kotlinx.android.synthetic.main.splashactivity.*


class Splash : AppCompatActivity() {
    lateinit var pref: Preferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splashactivity)
        pref = Preferences(this)
  Glide.with(this).load(R.raw.hrgif).into(imagesplash)

        Handler().postDelayed({ // This method will be executed once the timer is over

            if (pref.getBoolean(AppConstants.isLogin)){
                val i = Intent(this@Splash, DashboardActivity::class.java)
                startActivity(i)
                finish()
            }else{
                val i = Intent(this@Splash, ChooseActivity::class.java)
                startActivity(i)
                finish()
            }

        }, 3000)



    }
}