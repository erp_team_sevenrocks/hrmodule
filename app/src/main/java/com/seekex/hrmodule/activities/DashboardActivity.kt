package com.seekex.hrmodule.activities

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekex.hrmodule.R
import com.seekex.hrmodule.attendence.AttendenceActivity
import com.seekex.hrmodule.criticalevent.EventActivity
import com.seekex.hrmodule.custombinding.DashboardAdapter
import com.seekex.hrmodule.databinding.AnnounceMentImagesBinding
import com.seekex.hrmodule.eventfeedback.EventFeedbackActivity
import com.seekex.hrmodule.feedback.FeedbackActivity
import com.seekex.hrmodule.grievance.GrievanceActivity
import com.seekex.hrmodule.jobposting.JobPostingActivity
import com.seekex.hrmodule.kra.KraActivity
import com.seekex.hrmodule.leave_manager.LeaveActivity
import com.seekex.hrmodule.locationfeedback.LocationFeedbackActivity
import com.seekex.hrmodule.login.LoginActivity
import com.seekex.hrmodule.markrewarks.RewardsActivity
import com.seekex.hrmodule.mysalary.SalaryActivity
import com.seekex.hrmodule.remiender.RemienderActivity
import com.seekex.hrmodule.retrofitclasses.models.AnnouncementResponse
import com.seekex.hrmodule.retrofitclasses.models.DashboardModel
import com.seekex.hrmodule.sop.SopActivity
import com.seekex.hrmodule.training.TrainingActivity
import com.seekex.hrmodule.utils.Data.DataUtils
import com.seekex.trainingtestapp.PasscodeActivity
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ImageUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import kotlinx.android.synthetic.main.announcement_bottomsheet.*
import kotlinx.android.synthetic.main.dashboardactivity.*
import java.util.*

class DashboardActivity : AppCompatActivity() {
    var dashboardAdapter: DashboardAdapter? = null

    var apiImp: ApiImp? = null
    lateinit var pref: Preferences

    var dialogUtil: DialogUtils? = null

    var aList: ArrayList<*> = ArrayList<Any?>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dashboardactivity)
        txtheaderre.setText("Dashboard")
        apiImp = ApiImp(this)
        pref = Preferences(this)

        txtlogout.setOnClickListener {
            pref = Preferences(this)

            pref.setBoolean(AppConstants.isLogin, false)

            val i = Intent(this, LoginActivity::class.java)
// set the new task and clear flags
            i.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(i)
        }
        setAdapter()
//        getAnnouncement()
    }

    fun setAdapter() {
        aList = DataUtils.getEntranceData(this)
        dashboardAdapter = object : DashboardAdapter(this, aList as ArrayList<DashboardModel>?) {
            protected override fun onViewClicked(v: View?, s: String?) {
                when (s) {
                    "1" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        RewardsActivity::class.java
                    )
                    "2" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        GrievanceActivity::class.java
                    )
                    "3" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        TrainingActivity::class.java
                    )
                    "4" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        KraActivity::class.java
                    )
                    "5" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        FeedbackActivity::class.java
                    )
                    "6" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        PasscodeActivity::class.java
                    )
                    "7" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        JobPostingActivity::class.java
                    )
                    "8" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        SopActivity::class.java

                    )

                    "9" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        EventActivity::class.java

                    )
                    "10" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        LocationFeedbackActivity::class.java

                    )
                    "11" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        RemienderActivity::class.java

                    )
                    "12" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        LeaveActivity::class.java

                    )
                    "13" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        SalaryActivity::class.java
                    )
                    "14" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        AttendenceActivity::class.java

                    )
                    "15" -> ExtraUtils.navigateWithoutFinish(
                        this@DashboardActivity,
                        EventFeedbackActivity::class.java

                    )
                }
            }
        }
        gv.setAdapter(dashboardAdapter)
        gv.setExpanded(true)
    }

    fun seenAnnouncement(id: Int) {
        val getMasterRequest = RequestBase()
        getMasterRequest.service_name = "announcement_seen"
        getMasterRequest.announcement_id = id.toString()
        getMasterRequest.username = (pref.get(AppConstants.user_name))
        getMasterRequest.password = (pref.get(AppConstants.password))
        getMasterRequest.app_version = "2.2.0"
        getMasterRequest.secret_key = ImageUtils.getSecretKey()
        apiImp!!.seenAccouncement(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                getAnnouncement()
            }

        })
    }

    fun getAnnouncement() {
        val getMasterRequest = RequestBase()
        getMasterRequest.service_name = "announcement_get"
        getMasterRequest.username = (pref.get(AppConstants.user_name))
        getMasterRequest.password = (pref.get(AppConstants.password))
        getMasterRequest.app_version = "2.2.0"
        getMasterRequest.secret_key = ImageUtils.getSecretKey()
        apiImp!!.getannpuncement(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
             if (status){
                    var data =
                    any as AnnouncementResponse

                if (data == null) {
                    bottomsheet.setVisibility(View.GONE)

                }

                llimages.removeAllViews()
                bottomsheet.setVisibility(View.VISIBLE)

                tv_name.setText(data.data.announcement.name)
                tv_detail.setText(data.data.announcement.detail)
                tv_created_by.setText(data.data.announcement.created_by)
                tv_date.setText(data.data.announcement.created)

                setBackGroundColorByPriority(data.data.announcement.priority)

                rr_delte.setOnClickListener {
                    seenAnnouncement(data.data.announcement.id)
                }

                for (i in 0 until data.data.announcementImage.size) {
                    val inflater = getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
                    val binding: AnnounceMentImagesBinding = DataBindingUtil.inflate(
                        inflater,
                        R.layout.announce_ment_images,
                        llimages,
                        true
                    )
                    binding.setData(data.data.announcementImage.get(i))
//                    binding.ivImage.setOnClickListener(View.OnClickListener {
//                        DialogUtil(this@DashboardActivity).showImage(
//                            data.getAnnouncementImage().get(
//                                i
//                            ).getImage()
//                        )
//                    })
                }
             }


            }

        })
    }

    private fun setBackGroundColorByPriority(priority: Int) {
        if (priority < 5) rr_background.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark)) else if (priority < 8) rr_background.setBackgroundColor(
            resources.getColor(R.color.colorOrange)
        ) else rr_background.setBackgroundColor(resources.getColor(R.color.red))
    }

}