package com.seekex.hrmodule.feedback;

import com.seekx.webService.models.RequestBase;

public class SaveFeedbackReq extends RequestBase {

    private SaveFeedbackReq2  data;

    public SaveFeedbackReq2 getData() {
        return data;
    }

    public void setData(SaveFeedbackReq2 data) {
        this.data = data;
    }
}
