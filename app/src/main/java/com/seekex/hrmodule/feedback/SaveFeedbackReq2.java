package com.seekex.hrmodule.feedback;

import com.seekx.webService.models.RequestBase;

public class SaveFeedbackReq2 extends RequestBase {

    private String  feedback_type_id;
    private String  feedback_value;
    private String  remarks;
    private String  to_user_id;

    public String getFeedback_type_id() {
        return feedback_type_id;
    }

    public void setFeedback_type_id(String feedback_type_id) {
        this.feedback_type_id = feedback_type_id;
    }

    public String getFeedback_value() {
        return feedback_value;
    }

    public void setFeedback_value(String feedback_value) {
        this.feedback_value = feedback_value;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getTo_user_id() {
        return to_user_id;
    }

    public void setTo_user_id(String to_user_id) {
        this.to_user_id = to_user_id;
    }
}
