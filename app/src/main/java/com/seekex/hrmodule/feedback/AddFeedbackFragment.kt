package com.sevenrocks.taskapp.appModules.markrewarks

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.feedback.*
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import kotlinx.android.synthetic.main.add_feedback.*
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.kra_assessment.view.btn_krasubmit
import java.util.*


/**
 */
class AddFeedbackFragment : Fragment() {
    private var userId: String? = "0"
    private var feedbacktypeid: String? = "0"
    private var feedid: String? = "0"
    private lateinit var adapterUsers: SpinnerAdapter_Users
    private lateinit var adapterFeedbacktype: SpinnerAdapter_FeedbackType
    private lateinit var adapterFeed: SpinnerAdapter_Feed

    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var feedbacktypelist: ArrayList<DropdownModel> = java.util.ArrayList()
    var feedlist: ArrayList<DropdownModel> = java.util.ArrayList()

    public var feedbackActivity: FeedbackActivity? = null

    lateinit var apiImp: ApiImp


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        val rootView = inflater.inflate(R.layout.add_feedback, BasicActi, false)
        feedbackActivity = activity as FeedbackActivity?
        rootView.txtheader.setText("Add Feedback")
//        setAdapters(rootView)
        setlistener(rootView)
        getUsers()
        getFeedbacktype()
        getFeedlist()
        return rootView
    }

    private fun setlistener(rootView: View?) {

        rootView!!.edtuserfeed.setOnClickListener {
            DataUtils.openSearchDialogeuser(
                activity!!,
                userlist,
                "Select User",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtuserfeed.setText(ss.name)
                        userId=ss.id

                    }

                })
        }
        rootView!!.edtfeedtype.setOnClickListener {
            DataUtils.
            openSearchDialoge(
                activity!!,
                feedbacktypelist,
                "Select Feedback Type",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtfeedtype.setText(ss.name)
                        feedbacktypeid = ss.id


                    }

                })
        }
        rootView!!.edtfeedlist.setOnClickListener {
            DataUtils.openSearchDialoge(
                activity!!,
                feedlist,
                "Select Value",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtfeedlist.setText(ss.name)
                        feedid = ss.id

                    }

                })
        }


        rootView!!.savefeedback.setOnClickListener {

            if (edt_feed_remark.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter remark", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            if (userId.equals("0")) {
                Toast.makeText(activity, "Select User", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (feedbacktypeid.equals("0")) {
                Toast.makeText(activity, "Select Feedback Type", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (feedid.equals("0")) {
                Toast.makeText(activity, "Select Feedback Value ", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }

            saveData(edt_feed_remark.text.toString().trim())

        }
    }


    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

   /* private fun setAdapters(rootView: View) {


        adapterUsers = SpinnerAdapter_Users(activity, userlist)
        rootView.spin_feeduser.setAdapter(adapterUsers)

        adapterFeed = SpinnerAdapter_Feed(activity, feedlist)
        rootView.spin_feedlist.setAdapter(adapterFeed)

        adapterFeedbacktype = SpinnerAdapter_FeedbackType(activity, feedbacktypelist)
        rootView.spin_feedbacktype.setAdapter(adapterFeedbacktype)

        rootView.spin_feedbacktype.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterFeedbacktype.getItem(position)!!
                feedbacktypeid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
        rootView.spin_feedlist.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterFeed.getItem(position)!!
                feedid = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })
        rootView.spin_feeduser.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?, view: View,
                position: Int, id: Long
            ) {
                val user: DropdownModel = adapterUsers.getItem(position)!!
                userId = user.id

            }

            override fun onNothingSelected(adapter: AdapterView<*>?) {}
        })


    }*/


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getFeedbacktype() {
        val getMasterRequest = RequestBase()
        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.FEEDBACKTYPE
        feedbackActivity!!.apiImp.getFeedbackType(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val list = any as RewardUsersResponse

                    feedbacktypelist.addAll(list.data)

//                    adapterFeedbacktype.notifyDataSetChanged()
                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getFeedlist() {
        val getMasterRequest = RequestBase()
        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.FEEDBACKVALUE
        feedbackActivity!!.apiImp.getFeedbackType(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val list = any as RewardUsersResponse

                    feedlist.addAll(list.data)

//                    adapterFeed.notifyDataSetChanged()
                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUsers() {

        val getMasterRequest = GetSalaryReq()
        val get2 = GetSalaryReq2()

        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GUSERS
        getMasterRequest.conditions= get2

        feedbackActivity!!.apiImp.getkrausers(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse


                    userlist.addAll(ss.data)
//                    adapterUsers.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveData(remark: String) {

        val getMasterRequest = SaveFeedbackReq()
        val ss = SaveFeedbackReq2()

        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.SAVEFEEDBACKk
        ss.feedback_type_id = feedbacktypeid
        ss.feedback_value = feedid
        ss.remarks = remark
        ss.to_user_id = userId

        getMasterRequest.data = ss
        feedbackActivity!!.apiImp.saveNewFeedback(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Toast.makeText(activity, "Feedback Saved", Toast.LENGTH_LONG).show()
                    feedbackActivity!!.finish()
                }


            }

        })
    }

    companion object {


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddFeedbackFragment()
    }
}