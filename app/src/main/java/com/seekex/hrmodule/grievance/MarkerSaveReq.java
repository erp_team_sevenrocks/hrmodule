package com.seekex.hrmodule.grievance;


import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class MarkerSaveReq extends RequestBase {

    private ArrayList<MarkerSaveReq2>  data;

    public ArrayList<MarkerSaveReq2> getData() {
        return data;
    }

    public void setData(ArrayList<MarkerSaveReq2> data) {
        this.data = data;
    }
}
