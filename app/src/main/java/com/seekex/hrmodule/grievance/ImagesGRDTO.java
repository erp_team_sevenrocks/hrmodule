
package com.seekex.hrmodule.grievance;

import com.google.gson.annotations.SerializedName;

public class ImagesGRDTO  {

    @SerializedName("name")
    private String name;
    @SerializedName("type")
    private String type;
    @SerializedName("duration")
    private String duration;

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
