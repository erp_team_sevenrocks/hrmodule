package com.seekex.hrmodule.grievance;


import com.seekx.webService.models.RequestBase;

public class GetAudioDetailReq extends RequestBase {

    private String  grievance_id;

    public String getGrievance_id() {
        return grievance_id;
    }

    public void setGrievance_id(String grievance_id) {
        this.grievance_id = grievance_id;
    }
}
