package com.seekex.hrmodule.grievance;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.adapter.ChooseAdapter;
import com.seekex.hrmodule.custombinding.ExpandableHeightGridView;
import com.seekex.hrmodule.utils.Data.DataUtils;
import com.seekx.utils.ExtraUtils;
import com.sevenrocks.taskapp.appModules.grievance.GrievanceRegistration;
import com.sevenrocks.taskapp.appModules.vendorreg.GreivanceList;
import com.sevenrocks.taskapp.appModules.vendorreg.MyAssignedList;
import com.sevenrocks.taskapp.appModules.vendorreg.My_AddedList;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GrievanceMenuFragment extends Fragment {


    @BindView(R.id.gridview_menu)
    ExpandableHeightGridView grid_views;

    @BindView(R.id.txtheaderre)
    TextView tvHeader;

    private GrievanceActivity grievanceActivity;

    public GrievanceMenuFragment() {
        // Required empty public constructor
    }

    public static Fragment getInstance() {
        return new GrievanceMenuFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup BasicActi, Bundle savedInstanceState) {

        // Inflate the layout for getActivity() fragment
        View rootView = inflater.inflate(R.layout.choose_activty, BasicActi, false);
        ButterKnife.bind(this, rootView);

        init();

        setListener();

        tvHeader.setText("Choose Options");

        return rootView;

    }

    private void init() {

        grievanceActivity = ((GrievanceActivity) getActivity());
        setAapter();
    }


    private void setListener() {


    }

    private void setAapter() {
        ChooseAdapter chooseAdapter = new ChooseAdapter(getContext(), DataUtils.getGrievanceData(Objects.requireNonNull(getActivity()))) {

            @Override
            protected void onViewClicked(View v, String s) {
                switch (s) {

                    case "1":
                        ExtraUtils.changeFragment(grievanceActivity.getSupportFragmentManager(), GrievanceRegistration.getInstance(), true);
                        break;

                    case "2":
                        ExtraUtils.changeFragment(grievanceActivity.getSupportFragmentManager(), GreivanceList.getInstance(), true);
                        break;
                    case "3":
                        ExtraUtils.changeFragment(grievanceActivity.getSupportFragmentManager(), My_AddedList.getInstance(), true);
                        break;
//                        case "4":
//                        ExtraUtils.changeFragment(grievanceActivity.getSupportFragmentManager(), MyAssignedList.getInstance(), true);
//                        break;
                }
            }
        };
        grid_views.setAdapter(chooseAdapter);
        grid_views.setExpanded(true);
    }


}
