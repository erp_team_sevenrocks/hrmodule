package com.seekex.hrmodule.grievance;

import com.google.gson.annotations.SerializedName;

public class AudioDetailResponse2 {
    @SerializedName("id")
    private String  id;
    @SerializedName("meeting_id")
    private String meeting_id;
    @SerializedName("duration")
    private String duration;
    @SerializedName("is_upload_to_s3")
    private String is_upload_to_s3;
    @SerializedName("File")
    private String File;
    @SerializedName("url")
    private String url;

    public String getFile() {
        return File;
    }

    public void setFile(String file) {
        File = file;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(String meeting_id) {
        this.meeting_id = meeting_id;
    }



    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getIs_upload_to_s3() {
        return is_upload_to_s3;
    }

    public void setIs_upload_to_s3(String is_upload_to_s3) {
        this.is_upload_to_s3 = is_upload_to_s3;
    }
}
