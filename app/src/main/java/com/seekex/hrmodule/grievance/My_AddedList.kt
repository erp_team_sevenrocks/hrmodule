package com.sevenrocks.taskapp.appModules.vendorreg

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.grievance.GreivanceListDTO
import com.seekex.hrmodule.grievance.GreivanceListResponse
import com.seekex.hrmodule.grievance.GrievanceActivity
import com.seekex.hrmodule.grievance.ManageGreivanceReq
import com.seekex.hrmodule.markrewarks.*
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import com.sevenrocks.taskapp.appModules.markrewarks.*


import kotlinx.android.synthetic.main.rewardlist.view.*

class My_AddedList : Fragment() {
    private lateinit var myAdapter: GListAdapterMyAdded
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<GreivanceListDTO> = java.util.ArrayList()

    public var rewardsActivity: GrievanceActivity? = null
    private lateinit var rootView: View


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.rewardlist, BasicActi, false)
        rewardsActivity = activity as GrievanceActivity?

        rootView.recyclerView.setLayoutManager(
            LinearLayoutManager(activity)
        )
        rootView.txtheaderre.text = "View Grievance"
        setAdapter(rootView)
        page = 1
        gettypeList()


//        rootView.txtheader.text = "Rewards"


        return rootView
    }


    override fun onResume() {
        super.onResume()
        page = 1
    }


    private fun setAdapter(rootView: View) {
        myAdapter = GListAdapterMyAdded(activity, object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                dataModel = any1 as GreivanceListDTO

                when (i) {
                    2 -> {
                        deleteParty()
                    }
                    3 -> {
                        acceptGre()
                    }
                    4 -> {
                        rejectGre()
                    }

                }
            }

        })
        rootView.recyclerView!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.recyclerView!!.adapter = myAdapter

        rootView.recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.v("addOnScrollListener", "false")
                    getList()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun gettypeList() {
        gtypelist.clear()
        val reqData =
            RequestBase()


        reqData.service_name = ApiUtils.GSTATUS
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        rewardsActivity?.apiImp?.getGtypelist(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as RewardUsersResponse
                        gtypelist.addAll(ss.data)
                        getList()
                    }
                }

            }
        )


    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun acceptGre() {

        val reqData =
            ManageGreivanceReq()
        reqData.service_name = ApiUtils.GACCEPT
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        rewardsActivity?.apiImp?.manageRecord(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        Toast.makeText(activity, "Record Updated", Toast.LENGTH_SHORT).show()
                        mydataList.clear()
                        myAdapter.notifyDataSetChanged()
                        page = 1
                        getList()
                    }
                }

            }
        )
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun rejectGre() {

        val reqData =
            ManageGreivanceReq()
        reqData.service_name = ApiUtils.GREJECT
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        rewardsActivity?.apiImp?.manageRecord(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        Toast.makeText(activity, "Record Updated", Toast.LENGTH_SHORT).show()
                        mydataList.clear()
                        myAdapter.notifyDataSetChanged()
                        page = 1
                        getList()
                    }
                }

            }
        )
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun deleteParty() {

        val reqData =
            DeleteRewardReq()
        reqData.service_name = ApiUtils.GDELETE
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        rewardsActivity?.apiImp?.deleteRecord(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        mydataList.remove(dataModel)
                        Toast.makeText(activity, "Record Deleted", Toast.LENGTH_SHORT).show()
                        myAdapter.notifyDataSetChanged()
                    }
                }

            }
        )
    }

    private fun getGreivanceReqdata(): GetListReq {
        val reqData =
            GetListReq()


        reqData.service_name = ApiUtils.GGET
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.page = page;
        reqData.user_id = rewardsActivity?.pref?.get(AppConstants.uid)

        return reqData
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getList() {


        rewardsActivity?.apiImp?.getGreivancelist(
            getGreivanceReqdata(), object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as GreivanceListResponse
                        val datalist = ss.data

                        if (datalist.isEmpty()) {
                            donHit = true
                        } else {
                            mydataList.addAll(datalist)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()
                            page++
                        }
                    }
                }

            }
        )


    }


    companion object {

        @JvmStatic
        var dataModel = GreivanceListDTO()

        @JvmStatic
        var gtypelist: ArrayList<DropdownModel> = java.util.ArrayList()


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = My_AddedList()
    }


}