package com.seekex.hrmodule.grievance;

import java.util.ArrayList;

public class GreivanceSaveReq2 {
    private String description;
    private String name;

    private String grievance_type_id;
    private ArrayList<ImagesGRDTO> media;
    private String user_id;
    private String location_id;
    private String occurrence;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public ArrayList<ImagesGRDTO> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<ImagesGRDTO> media) {
        this.media = media;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGrievance_type_id() {
        return grievance_type_id;
    }

    public void setGrievance_type_id(String grievance_type_id) {
        this.grievance_type_id = grievance_type_id;
    }


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOccurrence() {
        return occurrence;
    }

    public void setOccurrence(String occurrence) {
        this.occurrence = occurrence;
    }
}