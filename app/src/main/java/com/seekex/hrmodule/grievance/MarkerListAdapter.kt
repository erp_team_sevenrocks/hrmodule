package com.sevenrocks.taskapp.appModules.meetings

import android.content.Context
import android.media.MediaPlayer
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.R
import com.seekex.hrmodule.databinding.MarkerListAdapterBinding
import com.seekex.hrmodule.grievance.GrievanceActivity
import com.seekex.hrmodule.grievance.MarkerDataDTO
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import kotlin.collections.ArrayList

class MarkerListAdapter(
    context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<MarkerListAdapter.ViewHolder>() /*implements DialogeclickListener, AdapterApiCallBack*/ {
    private var mDataset: ArrayList<MarkerDataDTO>? = null
    private val mInflater: LayoutInflater
    private val greivanceactivity: GrievanceActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null

    inner class ViewHolder(  // each data item is just a string in this case
        val binding: MarkerListAdapterBinding
    ) : RecyclerView.ViewHolder(binding.root)

    fun setmDataset(outGoingScannble: ArrayList<MarkerDataDTO>?) {
        mDataset = outGoingScannble
        notifyDataSetChanged()
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MarkerListAdapterBinding = DataBindingUtil.inflate(
            mInflater,
            R.layout.marker_list_adapter, parent, false
        )
        binding.deleteRecord.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)

        }
        binding.playaudio.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)

        }
        return ViewHolder(binding)
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val batchData = mDataset!![position]
        holder.binding.model = batchData
        holder.binding.executePendingBindings()
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return if (mDataset != null) mDataset!!.size else 0
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    init {
        greivanceactivity = context as GrievanceActivity?
        mInflater = LayoutInflater.from(context)
    }
}