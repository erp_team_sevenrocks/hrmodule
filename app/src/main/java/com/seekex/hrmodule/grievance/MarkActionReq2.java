package com.seekex.hrmodule.grievance;

import com.seekx.webService.models.RequestBase;

public class MarkActionReq2 extends RequestBase {

        private String grievance_id  ;
        private String assignee_id  ;
        private String action  ;

        public String getGrievance_id() {
            return grievance_id;
        }

        public void setGrievance_id(String grievance_id) {
            this.grievance_id = grievance_id;
        }

        public String getAssignee_id() {
            return assignee_id;
        }

        public void setAssignee_id(String assignee_id) {
            this.assignee_id = assignee_id;
        }

        public String getAction() {
            return action;
        }

        public void setAction(String action) {
            this.action = action;
        }
    }