
package com.seekex.hrmodule.grievance;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.RewardListDTO;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class GreivanceListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<GreivanceListDTO> data;

    public ArrayList<GreivanceListDTO> getData() {
        return data;
    }

    public void setData(ArrayList<GreivanceListDTO> data) {
        this.data = data;
    }


}

