package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.databinding.GreivanceAdapterMyAssignedBinding
import com.seekex.hrmodule.grievance.GreivanceListDTO
import com.seekex.hrmodule.grievance.GrievanceActivity
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*

class GListAdapterMyAssigned(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<GListAdapterMyAssigned.ViewHolder>() {
    private val rewardsActivity: GrievanceActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<GreivanceListDTO> = ArrayList()

    fun setDataValues(items: ArrayList<GreivanceListDTO>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        rewardsActivity = context as GrievanceActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = GreivanceAdapterMyAssignedBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }
        binding.editDelete.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 2)
        }
        binding.txtPlayaudio.setOnClickListener {
            openAudioPlayer(ApiUtils.DOMAIN + binding?.model?.url!!)
        }
        binding.txtSendformark.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 3)
        }
        binding.addAction.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 4)
        }

        return ViewHolder(binding)
    }
    fun getFileNameFromUrl(url: String): String {
        return url.substring(url.lastIndexOf("/") + 1)
    }


    private fun openAudioPlayer(toString: String) {
        var isPb = true
        var isPaused = false
        val handler = Handler()
        var mediaPlayer = MediaPlayer()
        val dialog = Dialog(rewardsActivity!!, R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_audio)

        dialog.setCancelable(true)
        dialog.setOnDismissListener {
            dialog.dismiss()
        }
        mediaPlayer.setOnBufferingUpdateListener { mp, percent ->

            dialog.seekbar.secondaryProgress = percent
        }

        mediaPlayer.setOnCompletionListener {
            isPaused = false
            dialog.seekbar.progress = 100
            Log.v("isPlaying", "complete")
            dialog.dismiss()
        }
        Handler().postDelayed(Runnable {
            dialog.pb_audio.visibility = View.INVISIBLE
        }, 3000)

        dialog.btn_play.setOnClickListener {

            if (isPb) {
                dialog.pb_audio.visibility = View.VISIBLE
            }
            isPb = false
            player = mediaPlayer
            if (!isPaused) {
                count = 0
            }

            dialog.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }


                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
                        mediaPlayer.seekTo(progress * 1000)
                    }
                }
            })


            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(toString) // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare()
                mediaPlayer.prepareAsync()
                // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start()
                dialog.seekbar.max = mediaPlayer.duration / 1000

            } else {

                mediaPlayer.pause()
            }

            updateSeekbar(dialog, handler, mediaPlayer)
        }
        dialog.btn_pause.setOnClickListener {
            isPaused = true
            mediaPlayer.pause()
        }

        dialog.setOnDismissListener {
            count = 0
            if (mediaPlayer.isPlaying || player != null) {
                mediaPlayer.stop()
                player?.stop()
            }


        }
        dialog.show()


    }

    private fun updateSeekbar(
        binding: Dialog,
        handler: Handler,
        mediaPlayer: MediaPlayer
    ) {


        val notification = Runnable {
            val mCurrentPosition = mediaPlayer.currentPosition / 1000
            binding.seekbar.progress = mCurrentPosition

//            val total =
//                binding.seekbar.max.toString()
//            val played = TimeUtils.convertSecondToHHMMString(count)
//
//            Log.e("totalplayed", "updateSeekbar: "+total+" -- "+played )
//            val displayDuration = "$played / $total"
//
//            ++count
//
//            binding.durationtxt.text = displayDuration

//            if (total == played) {
//                return@Runnable
//            }

            updateSeekbar(binding, handler, mediaPlayer)
        }

        handler.postDelayed(notification, 1000)


    }
    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: GreivanceAdapterMyAssignedBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: GreivanceListDTO) {
            binding.model = item
            binding.executePendingBindings()
            var adapterskills =
                ImageAdapter(rewardsActivity, object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                    }
                })
            binding.imagesRec.setLayoutManager(
                LinearLayoutManager(
                    rewardsActivity,
                    LinearLayoutManager.HORIZONTAL,
                    true
                )
            )
            binding.imagesRec.adapter = adapterskills
            adapterskills.setDataValues(items.get(position).grievanceMediaFile)
            adapterskills.notifyDataSetChanged()


            var adapteractions =
                ActionAdapter(rewardsActivity, object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                    }
                })
            binding.actionsRec.setLayoutManager(
                LinearLayoutManager(
                    rewardsActivity,
                    LinearLayoutManager.VERTICAL,
                    true
                )
            )
            binding.actionsRec.adapter = adapteractions
            adapteractions.setDataValues(items.get(position).grievanceAction)
            adapteractions.notifyDataSetChanged()



        }

    }

}