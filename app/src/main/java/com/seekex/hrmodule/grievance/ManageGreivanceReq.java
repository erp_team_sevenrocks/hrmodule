package com.seekex.hrmodule.grievance;

import com.seekx.webService.models.RequestBase;

public class ManageGreivanceReq extends RequestBase {

    private String   id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
