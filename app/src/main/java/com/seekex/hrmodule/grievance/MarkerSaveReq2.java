package com.seekex.hrmodule.grievance;


import com.seekx.webService.models.RequestBase;

public class MarkerSaveReq2 extends RequestBase {

    private String  grievance_recording_id;
    private String  from;
    private String  to;
    private String  description;

    public String getGrievance_recording_id() {
        return grievance_recording_id;
    }

    public void setGrievance_recording_id(String grievance_recording_id) {
        this.grievance_recording_id = grievance_recording_id;
    }

    public String getFromtxt() {
        return "From Time: "+from;
    }
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }
    public String getTotxt() {
        return "To Time:"+to;
    }
    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
