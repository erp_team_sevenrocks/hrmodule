package com.seekex.hrmodule.grievance;


import com.seekx.webService.models.RequestBase;

public class MarkerDataDTO extends RequestBase {

    private String fromtime;
    private String totime;
    private String  text;


    public MarkerDataDTO(String fromtime, String totime, String text) {
        this.fromtime = fromtime;
        this.totime = totime;
        this.text = text;
    }

    public MarkerDataDTO() {

    }

    public String getFromtime() {
        return fromtime;
    }

    public void setFromtime(String fromtime) {
        this.fromtime = fromtime;
    }

    public String getTotime() {
        return totime;
    }

    public void setTotime(String totime) {
        this.totime = totime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
