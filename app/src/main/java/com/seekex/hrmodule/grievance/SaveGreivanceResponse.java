
package com.seekex.hrmodule.grievance;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class SaveGreivanceResponse extends BaseResponse {

    @SerializedName("grievance_id")
    private String grievance_id;

    public String getGrievance_id() {
        return grievance_id;
    }

    public void setGrievance_id(String grievance_id) {
        this.grievance_id = grievance_id;
    }
}

