
package com.seekex.hrmodule.grievance;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.ApiUtils;

import java.util.ArrayList;

public class ImagesDTORES {

    @SerializedName("url")
    private String url;
    @SerializedName("type")
    private String type;
    @SerializedName("id")
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("GrievanceRecordingMarker")
    private ArrayList<MarkerSaveReq2> GrievanceRecordingMarker;

    public ArrayList<MarkerSaveReq2> getGrievanceRecordingMarker() {
        return GrievanceRecordingMarker;
    }

    public void setGrievanceRecordingMarker(ArrayList<MarkerSaveReq2> grievanceRecordingMarker) {
        GrievanceRecordingMarker = grievanceRecordingMarker;
    }

    public Boolean showMarker() {
        if (GrievanceRecordingMarker.size() > 0) {
            return true;
        } else {
            return false;
        }

    }

    public String getUrl() {
        return url;
    }

    public Boolean showUrl() {
        if (type.equals("audio") || type.equals("video")|| type.equals("excel")) {
            return true;
        }
        return false;
    }

    public Boolean showImage() {
        if (type.equals("image")) {
            return true;
        }
        return false;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
