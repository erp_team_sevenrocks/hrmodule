
package com.seekex.hrmodule.grievance;

import android.util.Log;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.DropdownModel;
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO;
import com.seekx.webService.models.BaseResponse;
import com.sevenrocks.taskapp.appModules.vendorreg.MyAssignedList;
import com.sevenrocks.taskapp.appModules.vendorreg.My_AddedList;

import java.util.ArrayList;

public class GreivanceListDTO extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("name")
    private String name;
    @SerializedName("description")
    private String description;
    @SerializedName("occurrence")
    private String occurrence;
    @SerializedName("expected_date")
    private String expected_date;
    @SerializedName("grievance_type")
    private String grievance_type;
    @SerializedName("assign_to")
    private String assign_to;
    @SerializedName("grievance_user")
    private String grievance_user;
    @SerializedName("url")
    private String url;

    @SerializedName("gstatus")
    private String gstatus;
    @SerializedName("grievance_reporter")
    private String grievance_reporter;

    @SerializedName("GrievanceMediaFile")
    private ArrayList<ImagesDTORES> GrievanceMediaFile;


    @SerializedName("GrievanceAction")
    private ArrayList<ActionsDTO> GrievanceAction;

    public ArrayList<ActionsDTO> getGrievanceAction() {
        return GrievanceAction;
    }

    public void setGrievanceAction(ArrayList<ActionsDTO> grievanceAction) {
        GrievanceAction = grievanceAction;
    }

    public ArrayList<ImagesDTORES> getGrievanceMediaFile() {
        return GrievanceMediaFile;
    }

    public void setGrievanceMediaFile(ArrayList<ImagesDTORES> grievanceMediaFile) {
        GrievanceMediaFile = grievanceMediaFile;
    }

    public String getGrievance_reportertxt() {
        return "Reporter: " + grievance_reporter;
    }


    public String getGrievance_reporter() {
        return grievance_reporter;
    }

    public void setGrievance_reporter(String grievance_reporter) {
        this.grievance_reporter = grievance_reporter;
    }

    @SerializedName("imagelist")
    private ArrayList<ImageDTO> imagelist;

    public ArrayList<ImageDTO> getImagelist() {
        return imagelist;
    }

    public void setImagelist(ArrayList<ImageDTO> imagelist) {
        this.imagelist = imagelist;
    }

    public String getGstatus() {
        return gstatus;
    }

    public void setGstatus(String gstatus) {
        this.gstatus = gstatus;
    }


    public Boolean showAccepted() {
        if (gstatus==null){
            return false;

        }

        if (gstatus.equals("1") ||gstatus.equals("0") || gstatus.equals("2") || gstatus.equals("3") || gstatus.equals("4")) {
            return true;

        }
        return false;
    }
    public Boolean showButtons() {

        if (gstatus.equals("0")) {
            return true;

        }
        return false;
    }
    public String showText() {
        Log.e("tag", "showText: " + gstatus + " listsize= " + My_AddedList.getGtypelist().size());
        DropdownModel ss = new DropdownModel(gstatus);
        if (My_AddedList.getGtypelist().contains(ss)) {
            int index = My_AddedList.getGtypelist().indexOf(ss);
            return My_AddedList.getGtypelist().get(index).getName();
        }
        if (MyAssignedList.getGtypelist().contains(ss)) {
            int index = MyAssignedList.getGtypelist().indexOf(ss);
            return MyAssignedList.getGtypelist().get(index).getName();
        }
//        if (gstatus.equals("0")) {
//            return "Open";
//        } else if (gstatus.equals("1")) {
//            return "Send for marked";
//        } else if (gstatus.equals("2")) {
//            return "Accepted by User";
//        } else if (gstatus.equals("3")) {
//            return "Rejected by User";
//        } else if (gstatus.equals("4")) {
//            return "Solution Not Found";
//        }
        return "";
    }

    public Boolean showRejected() {

        if (gstatus.equals("2")) {
            return true;

        }
        return false;
    }



    public String getUrl() {
        return url;
    }

    public String getUrltxt() {
        if (url.length() > 0) {
            return "Audio path: " + url;
        }
        return "";
    }
    public Boolean getUrlVisibility() {
        if (url.length() > 0) {
            return true;
        }
        return false;
    }
    public Boolean showAction() {
        if (GrievanceAction.size() > 0) {
            return true;
        }
        return false;
    }

    public Boolean showImages() {
        if (GrievanceMediaFile.size() > 0) {
            return true;
        }
        return false;
    }
    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNametxt() {
        return "Name: " + name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescriptiontxt() {
        return "Description: " + description;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getOccurrencetxt() {
        return "Occurence: " + occurrence;
    }

    public String getOccurrence() {
        return occurrence;
    }

    public void setOccurrence(String occurrence) {
        this.occurrence = occurrence;
    }

    public String getExpected_datetxt() {
        return "Expected Date: " + expected_date;
    }

    public String getExpected_date() {
        return expected_date;
    }

    public void setExpected_date(String expected_date) {
        this.expected_date = expected_date;
    }

    public String getGrievance_typetxt() {
        return "Gtype: " + grievance_type;
    }

    public String getGrievance_type() {
        return grievance_type;
    }

    public void setGrievance_type(String grievance_type) {
        this.grievance_type = grievance_type;
    }

    public String getAssign_totext() {
        return "Assigned to: " + assign_to;
    }

    public String getAssign_to() {
        return assign_to;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }

    public String getGrievance_user() {
        return grievance_user;
    }

    public String getGrievance_usertxt() {
        return "Greivance User: " + grievance_user;
    }

    public void setGrievance_user(String grievance_user) {
        this.grievance_user = grievance_user;
    }
}
