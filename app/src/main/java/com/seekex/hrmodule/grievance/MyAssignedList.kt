package com.sevenrocks.taskapp.appModules.vendorreg


import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.grievance.*
import com.seekex.hrmodule.markrewarks.*
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import com.sevenrocks.taskapp.appModules.markrewarks.*
import kotlinx.android.synthetic.main.dialog_addaction.*
import kotlinx.android.synthetic.main.rewardlist.view.*


/**
 */
class MyAssignedList : Fragment() {
    private lateinit var myAdapter: GListAdapterMyAssigned
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<GreivanceListDTO> = java.util.ArrayList()

    public var rewardsActivity: GrievanceActivity? = null
    private lateinit var rootView: View


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.rewardlist, BasicActi, false)
        rewardsActivity = activity as GrievanceActivity?

        rootView.recyclerView.setLayoutManager(
            LinearLayoutManager(activity)
        )
        rootView.txtheaderre.text = "View Grievance"
        setAdapter(rootView)
        page = 1
        gettypeList()



        return rootView
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun gettypeList() {
        gtypelist.clear()
        val reqData =
            RequestBase()


        reqData.service_name = ApiUtils.GSTATUS
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        rewardsActivity?.apiImp?.getGtypelist(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as RewardUsersResponse
                        gtypelist.addAll(ss.data)
                        getList()
                    }
                }

            }
        )


    }

    override fun onResume() {
        super.onResume()
        page = 1
    }


    private fun setAdapter(rootView: View) {
        myAdapter = GListAdapterMyAssigned(activity, object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                dataModel = any1 as GreivanceListDTO

                when (i) {
                    2 -> {
                        deleteParty()
                    }
                    3 -> {
                        sendForMark()
                    }
                    4 -> {
                        addActionDialog()
                    }

                }
            }

        })
        rootView.recyclerView!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.recyclerView!!.adapter = myAdapter

        rootView.recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.v("addOnScrollListener", "false")
                    getList()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }

    private fun addActionDialog() {

        val dialog = Dialog(rewardsActivity!!, android.R.style.Theme_Light)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_addaction)
        dialog.getWindow()!!.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.MATCH_PARENT
        );

        dialog.setCancelable(true)
        dialog.setOnDismissListener {
            dialog.dismiss()
        }
        dialog.btn_save.setOnClickListener {
            if (dialog.edtaction.text.toString().length == 0) {
                dialog.edtaction.setError("Enter Action")
                return@setOnClickListener
            }
            saveAction(dialog, dialog.edtaction.text.toString().trim())
        }
        dialog.btn_cancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()

    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun saveAction(dialog: Dialog, text: String) {

        val reqData =
            MarkActionReq()
        val reqData2 =
            MarkActionReq2()

        reqData.service_name = ApiUtils.SAVEACTION
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData2.grievance_id = dataModel.id
        reqData2.assignee_id = rewardsActivity?.pref?.get(AppConstants.uid)
        reqData2.action = text

        reqData.data = reqData2
        rewardsActivity?.apiImp?.markAction(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        Toast.makeText(activity, "Data Saved", Toast.LENGTH_SHORT).show()
                        if (dialog != null)
                            dialog.dismiss()
                        page = 1
                        mydataList.clear()
                        getList()
                    }
                }

            }
        )
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun deleteParty() {

        val reqData =
            DeleteRewardReq()
        reqData.service_name = ApiUtils.GDELETE
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        rewardsActivity?.apiImp?.deleteRecord(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        mydataList.remove(dataModel)
                        Toast.makeText(activity, "Record Deleted", Toast.LENGTH_SHORT).show()
                        myAdapter.notifyDataSetChanged()
                    }
                }

            }
        )
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun sendForMark() {

        val reqData =
            DeleteRewardReq()
        reqData.service_name = ApiUtils.SENDFORMARK
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        rewardsActivity?.apiImp?.sendformark(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {

                        Toast.makeText(activity, "Marked Successfully", Toast.LENGTH_SHORT).show()
                        page = 1
                        mydataList.clear()
                        myAdapter.notifyDataSetChanged()

                        getList()
                    }
                }

            }
        )
    }

    private fun getGreivanceReqdata(): GetListReq {
        val reqData =
            GetListReq()


        reqData.service_name = ApiUtils.GGETMYASSIGNED
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.page = page;
        reqData.assignee_id = rewardsActivity?.pref?.get(AppConstants.uid)

        return reqData
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getList() {


        rewardsActivity?.apiImp?.getGreivancelist(
            getGreivanceReqdata(), object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        val ss = any as GreivanceListResponse
                        val datalist = ss.data

                        if (datalist.isEmpty()) {
                            donHit = true
                        } else {
                            mydataList.addAll(datalist)
                            myAdapter!!.setDataValues(mydataList)
                            myAdapter!!.notifyDataSetChanged()
                            page++
                        }
                    }
                }

            }
        )


    }


    companion object {

        @JvmStatic
        var dataModel = GreivanceListDTO()

        @JvmStatic
        var gtypelist: ArrayList<DropdownModel> = java.util.ArrayList()

        @JvmStatic
        var imageList = ArrayList<String>()

        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = MyAssignedList()
    }


}