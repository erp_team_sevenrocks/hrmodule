package com.sevenrocks.taskapp.appModules.grievance

import android.Manifest
import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.net.Uri
import android.os.*
import android.preference.PreferenceManager
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.ButterKnife
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.github.hiteshsondhi88.libffmpeg.ExecuteBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.FFmpeg
import com.github.hiteshsondhi88.libffmpeg.LoadBinaryResponseHandler
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegCommandAlreadyRunningException
import com.github.hiteshsondhi88.libffmpeg.exceptions.FFmpegNotSupportedException
import com.google.gson.Gson
import com.rizlee.rangeseekbar.RangeSeekBar
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.databinding.ImageItemBinding
import com.seekex.hrmodule.grievance.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import com.sevenrocks.taskapp.appModules.meetings.MarkerListAdapter
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.dialog_addmarker.*
import kotlinx.android.synthetic.main.grievance_reg.*
import kotlinx.android.synthetic.main.grievance_reg.bt_save
import kotlinx.android.synthetic.main.grievance_reg.ll_attach
import kotlinx.android.synthetic.main.grievance_reg.tvTimers
import kotlinx.android.synthetic.main.grievance_reg.view.*
import kotlinx.android.synthetic.main.grievance_reg.view.txtheader
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.play_audio.*
import org.florescu.android.rangeseekbar.RangeSeekBar.OnRangeSeekBarChangeListener
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit


/**
 */
@Suppress("UNREACHABLE_CODE")
class GrievanceRegistration : Fragment(), RangeSeekBar.OnRangeSeekBarRealTimeListener,
    RatingBar.OnRatingBarChangeListener {
    private var ffmpeg: FFmpeg? = null

    private var mLeftProgressPos: Long = 0
    private var mRightProgressPos: kotlin.Long = 0
    private var mRedProgressBarPos: Long = 0
    private val scrollPos: Long = 0
    private val mScaledTouchSlop = 0
    private val lastScrollX = 0
    private var isSeeking = false

    private var toduration: String = ""
    private var mediaRecordingId: String? = ""

    private var recordingPath: String? = ""
    private var grievanceId: String? = ""
    public var grievanceActivity: GrievanceActivity? = null
    public var imageList = ArrayList<ImageDTO>()
    private lateinit var rootView: View
    var markerList = ArrayList<MarkerDataDTO>()
    var player: MediaPlayer? = null

    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var imagePathList: ArrayList<ImagesGRDTO> = java.util.ArrayList()
    private lateinit var adapterRewarduser: SpinnerAdapter_Users
    private var permissionCallBack: PermissionCallBack? = null
    var gtypelist: ArrayList<DropdownModel> = java.util.ArrayList()
    var locationlist: ArrayList<DropdownModel> = java.util.ArrayList()
    private lateinit var adapterGtype: SpinnerAdapter_gtype
    var gtypeid: String = "0"
    var userid: String = "0"
    var locationid: String = ""
    var audioFilename: String = ""
    lateinit var apiImp: ApiImp
    var mediaRecorder: MediaRecorder? = null
    var countDownTimer: CountDownTimer? = null
    var isRecorded = 0
    private var count: Long? = null
    private var timer = ""
    private var file_name = ""
    private lateinit var markerAdapter: MarkerListAdapter
    var apiFrom: Int = 0
    var apiTo: Int = 0
    private var call_Counter = 0
    var mediaPlayer = MediaPlayer()
    var rangeMediaPlayer = MediaPlayer()
    private var rangeseekbar: org.florescu.android.rangeseekbar.RangeSeekBar<Int>? = null

    var wasPlaying = false
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.grievance_reg, BasicActi, false)
        ButterKnife.bind(this, rootView)
        grievanceActivity = activity as GrievanceActivity?
        rootView.txtheader.text = "Grievance reg"
//        rootView.rangeseekbar.listenerRealTime = this
        rangeseekbar = rootView.rangeseekbar as org.florescu.android.rangeseekbar.RangeSeekBar<Int>?
        setListeners()
        setAdapters()
        getGtype()
        getUserList()
        getLocationList()
        loadFFMpegBinary()

        rootView.play_range.setOnClickListener {
            try {
                rangeMediaPlayer!!.setDataSource(recordingPath)
                rangeMediaPlayer.prepare()
                rangeMediaPlayer.prepareAsync()
            } catch (e: Exception) {
            }

            if (!rangeMediaPlayer.isPlaying) {
                rangeMediaPlayer.start()
                rangeMediaPlayer.setOnPreparedListener { mp ->
                    var duration = mp.duration / 1000
                    rootView.txtfroom.setText("00:00:00")
                    rootView.txtto.setText(getTime(mp.duration / 1000))
                    mp.isLooping = true
                    rangeseekbar!!.setRangeValues(0, duration)
                    rangeseekbar!!.setSelectedMinValue(0)
                    rangeseekbar!!.setSelectedMaxValue(duration)
                    rangeseekbar!!.setEnabled(true)
                    rangeseekbar!!.setOnRangeSeekBarChangeListener(OnRangeSeekBarChangeListener<Int> { bar, minValue, maxValue ->
                        rangeMediaPlayer.seekTo(minValue * 1000)
                        Log.e("TAG", "onCreateView: " + bar.selectedMinValue as Int)
                        Log.e("TAG", "onCreateView: minValue" + minValue)
                        apiFrom = minValue.toInt()
                        apiTo = maxValue.toInt()
                        rootView.txtfroom.setText(getTime((bar.selectedMinValue as Int)!!))
                        rootView.txtto.setText(getTime((bar.selectedMaxValue as Int)!!))
                    })

                    val handler = Handler()

                    val ss: Runnable = object : Runnable {
                        override fun run() {
                            if (rangeMediaPlayer.currentPosition >= rangeseekbar!!.getSelectedMaxValue() * 1000) rangeMediaPlayer.seekTo(
                                rangeseekbar!!.getSelectedMinValue() * 1000
                            )
                            handler.postDelayed(this, 100)
                        }
                    }
                    handler.postDelayed(ss, 100)
                }


            } else {
                rangeMediaPlayer.pause()

            }

        }
        rootView.pause_range.setOnClickListener {
            if (rangeMediaPlayer.isPlaying) {
                rangeMediaPlayer.pause()

            }
        }

        managerVisibility(rootView.ll_main, rootView.ll_audiotrack)

        rootView.submit.setOnClickListener {
            call_Counter = 0
            if (isRecorded == 1) {
                bt_save.setVisibility(View.INVISIBLE)
                stopMedia()
                playBeep()
                isRecorded = 2
                countDownTimer!!.cancel()
                timer = tvTimers.getText().toString()
                iv_action.setImageResource(android.R.drawable.ic_media_play)
            }
            if (edt_name.text.isEmpty()) {
                Toast.makeText(activity, "Enter Description", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edt_occ.text.isEmpty()) {
                Toast.makeText(activity, "Enter Occurance", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (gtypeid.equals("0")) {
                Toast.makeText(activity, "Pick Grievance Type", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (userid.equals("0")) {
                Toast.makeText(activity, "Pick User", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (locationid.equals("")) {
                Toast.makeText(activity, "Pick Location", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            imagePathList.clear()

            if (bt_save.visibility == View.INVISIBLE) {
                saveaudiofile()
            } else if (imageList.size > 0) {

                uploadImages()
            } else {
                saveGrievance()
            }

        }
        rootView.iv_action.setOnClickListener(View.OnClickListener {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    if (isRecorded == 0) {
                        isRecorded = 1
                        playBeep()
                        startRecording()
                        iv_action.setImageResource(android.R.drawable.ic_media_pause)
                    } else if (isRecorded == 1) {
                        bt_save.setVisibility(View.INVISIBLE)
                        stopMedia()
                        playBeep()
                        isRecorded = 2
                        countDownTimer!!.cancel()
                        timer = tvTimers.getText().toString()
                        iv_action.setImageResource(android.R.drawable.ic_media_play)

//                iv_action.setImageDrawable(activity!!.resources.getDrawable(R.drawable.aar_ic_play))
                    } else if (isRecorded == 2) {
//                FileUtils.openDocument(file_name, activity)
                    }
                }
            })


        })
        rootView.bt_reset.setOnClickListener(View.OnClickListener {
            try {
                isRecorded = 0
                stopMedia()
                playBeep()
                countDownTimer!!.cancel()
                tvTimers.setText("")
                bt_save.setVisibility(View.GONE)
                iv_action.setImageDrawable(activity!!.resources.getDrawable(R.drawable.audiorec))
            } catch (e: java.lang.Exception) {
            }
        })

        return rootView
    }

    override fun onPause() {
        super.onPause()
        try {
            if (rangeMediaPlayer.isPlaying) {
                rangeMediaPlayer.pause()

            }
        } catch (e: Exception) {
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    private fun saveaudiofile() {

        grievanceActivity!!.apiImp.saveAudio(file_name, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as uploadaudiores
                    audioFilename = ss.data.path + "" + ss.data.filename
                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "audio"
                    model.name = audioFilename
                    model.duration = count.toString()//getAudioDuration().toString()
                    imagePathList.add(model)
                    if (imageList.size > 0) {

                        uploadImages()
                    } else {
                        saveGrievance()

                    }

                }

            }

        })


    }

    private fun getAudioDuration(): Int {
        val uri: Uri = Uri.parse(file_name)
        val mmr = MediaMetadataRetriever()
        mmr.setDataSource(activity, uri)
        val durationStr: String =
            mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)!!
        val millSecond = durationStr.toInt()
        val hours: Int
        val minutes: Int
//        var seconds = millSecond / 1000
        val dur: Long = durationStr.toLong()
        var seconds: String? = (dur % 60000 / 1000).toString()
//        hours = seconds / 3600
//        minutes = seconds / 60 % 60
//        seconds = seconds % 60

        Log.e("TAG", "getAudioDuration: " + seconds)
        return seconds!!.toInt()
    }

    private fun stopMedia() {
        try {
            mediaRecorder!!.stop()
        } catch (e: java.lang.Exception) {
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUserList() {

        val getMasterRequest = GetSalaryReq()
        val get2 = GetSalaryReq2()

        getMasterRequest.token = grievanceActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GUSERS
        getMasterRequest.conditions = get2

        grievanceActivity!!.apiImp.getkrausers(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
//                    var aa = DropdownModel()
//                    aa.id = "0"
//                    aa.name = "Select User"
//                    userlist.add(aa)

                    userlist.addAll(ss.data)
//                    adapterRewarduser.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getGtype() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = grievanceActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GTYPE


        grievanceActivity!!.apiImp.getGlist(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse
//                    var aa = DropdownModel()
//                    aa.id = "0"
//                    aa.name = "Select Type"
//                    gtypelist.add(aa)

                    gtypelist.addAll(ss.data)
//                    adapterGtype.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getLocationList() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = grievanceActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.get_location


        grievanceActivity!!.apiImp.getGlist(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse

                    locationlist.addAll(ss.data)

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveGrievance() {
        Log.e("TAG", "saveGrievance: " + Gson().toJson(imagePathList))
        val greq = GreivanceSaveReq()
        val greq2 = GreivanceSaveReq2()

        greq.token = grievanceActivity?.pref?.get(AppConstants.token)
        greq.service_name = ApiUtils.GSAVE

        greq2.description = edt_description.text.toString()
        greq2.occurrence = edt_occ.text.toString()
        greq2.grievance_type_id = gtypeid
        greq2.user_id = userid
        greq2.media = imagePathList
        greq2.location_id = locationid
        greq2.name = edt_name.text.toString()

        greq.data = greq2
        grievanceActivity!!.apiImp.saveglsit(greq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as SaveGreivanceResponse
                    grievanceId = ss.grievance_id

                    if (bt_save.visibility == View.INVISIBLE) {
                        managerVisibility(rootView.ll_audiotrack, rootView.ll_main)
                        getAudioFileDetails()

                    } else {
                        Toast.makeText(activity, "Record Saved", Toast.LENGTH_SHORT).show()

                        grievanceActivity!!.finish()
                    }


                }


            }

        })
    }

    private fun getTimeValues(minValue: Float): String {
        val day = TimeUnit.SECONDS.toDays(minValue.toLong())
        val hours: Long = TimeUnit.SECONDS.toHours(minValue.toLong()) - day * 24
        val minute: Long =
            TimeUnit.SECONDS.toMinutes(minValue.toLong()) - TimeUnit.SECONDS.toHours(minValue.toLong()) * 60
        val second: Long =
            TimeUnit.SECONDS.toSeconds(minValue.toLong()) - TimeUnit.SECONDS.toMinutes(minValue.toLong()) * 60
        var data = hours.toString() + ":" + minute + ":" + second
        Log.e("TAG", "getTimeValues: " + data)

        return data
    }

    private fun getAudioFileDetails() {

        val itemReq = GetAudioDetailReq()

        itemReq.grievance_id = grievanceId
        itemReq.service_name = ApiUtils.GETRECORDINGDETAILS
        itemReq.token = grievanceActivity?.pref?.get(AppConstants.token)

        grievanceActivity!!.apiImp.getAudioFileDetails(itemReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {

                if (status) {
                    var ss = any as AudioDetailResponse
                    mediaRecordingId = ss.data.id
                    recordingPath = ApiUtils.DOMAIN + ss.data.url
                    Toast.makeText(activity, "recordingPath", Toast.LENGTH_SHORT).show()

                    toduration = ss.data.duration


                }

            }

        })


    }

    private fun getTime(seconds: Int): String? {
        val hr = seconds / 3600
        val rem = seconds % 3600
        val mn = rem / 60
        val sec = rem % 60
        return String.format("%02d", hr) + ":" + String.format(
            "%02d",
            mn
        ) + ":" + String.format("%02d", sec)
    }


    private fun updateRangeSeekbar(

        handler: Handler,
        mediaPlayer: MediaPlayer
    ) {


        val notification = Runnable {
            val mCurrentPosition = mediaPlayer.currentPosition / 1000
            rangeseekbar!!.setRangeValues(
                mCurrentPosition.toInt(),
                toduration.toInt()
            ) //= mCurrentPosition
            updateRangeSeekbar(handler, mediaPlayer)
        }

        handler.postDelayed(notification, 1000)


    }

    private fun openAudioPlayer(toString: String, fromtime: String, totime: String) {
        Log.e("TAG", "openAudioPlayer: " + fromtime + " -- " + totime)
        var isPb = true
        var isPaused = false
        val handler = Handler()
        var mediaPlayer = MediaPlayer()
        val dialog = Dialog(grievanceActivity!!, android.R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_audio)

        dialog.setCancelable(true)
        dialog.setOnDismissListener {
            dialog.dismiss()
        }
        mediaPlayer.setOnBufferingUpdateListener { mp, percent ->

            dialog.seekbar.secondaryProgress = percent
        }

        mediaPlayer.setOnCompletionListener {
            isPaused = false
            dialog.seekbar.progress = 100
            Log.v("isPlaying", "complete")
            dialog.dismiss()
        }
        Handler().postDelayed(Runnable {
            dialog.pb_audio.visibility = View.INVISIBLE
        }, 3000)

        dialog.btn_play.setOnClickListener {

            if (isPb) {
                dialog.pb_audio.visibility = View.VISIBLE
            }
            isPb = false
            player = mediaPlayer
            if (!isPaused) {
                count = 0
            }

            dialog.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }


                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
//                        val value: Double = fromtime.toDouble() + progress * 1
                        mediaPlayer.seekTo(progress)
                    }
                }
            })
//            mediaPlayer.seekTo(fromtime.toInt())
            dialog.seekbar.progress = fromtime.toInt()
            dialog.seekbar.max = totime.toInt()

            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(toString) // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare()
                mediaPlayer.prepareAsync()
                // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (e: Exception) {
                e.printStackTrace()
            }

            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start()
//                dialog.seekbar.max = mediaPlayer.duration / 1000

//

            } else {

                mediaPlayer.pause()
            }

            updateSeekbar(dialog, handler, mediaPlayer, fromtime, totime)
        }
        dialog.btn_pause.setOnClickListener {
            isPaused = true
            mediaPlayer.pause()
        }

        dialog.setOnDismissListener {
            count = 0
            if (mediaPlayer.isPlaying || player != null) {
                mediaPlayer.stop()
                player?.stop()
            }


        }
        dialog.show()


    }

    private fun updateSeekbar(
        binding: Dialog,
        handler: Handler,
        mediaPlayer: MediaPlayer,
        fromtime: String,
        totime: String
    ) {


        val notification = Runnable {
            val mCurrentPosition = mediaPlayer.currentPosition / 1000
            binding.seekbar.progress = mCurrentPosition
            updateSeekbar(binding, handler, mediaPlayer, fromtime, totime)
        }

        handler.postDelayed(notification, 1000)


    }

    private fun startRecording() {
        mediaRecorderReady()
        try {
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startCountdownTimer()
    }

    fun convertSecondToHHMMString(count: Long): String? {
        val df = SimpleDateFormat("mm:ss")
        df.timeZone = TimeZone.getTimeZone("UTC")
        return df.format(Date(count * 1000L))
    }

    private fun startCountdownTimer() {
        count = 0L
        countDownTimer = object : CountDownTimer(10800000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // Used for formatting digit to be in 2 digits only
                count = count!! + 1
                tvTimers.setText(convertSecondToHHMMString(count!!))
            }

            // When the task is over it will print 00:00:00 there
            override fun onFinish() {
                tvTimers.setText("00:00:00")
            }
        }.start()
    }

    private fun mediaRecorderReady() {
        val currentTime = System.currentTimeMillis()
//        val extBaseDir = Environment.getExternalStorageDirectory()

        val extBaseDir = activity?.externalCacheDir

        val file = File(extBaseDir!!.absolutePath + "/SeekMeeting/" + "")
        if (!file.exists()) {
            if (!file.mkdirs()) {
            }
        }
        file_name = "$file/myRecording.mp3"
        Log.e("TAG", "mediaRecorderReady: $file_name")
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        mediaRecorder!!.setOutputFile(file_name)
    }

    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

    private fun setAdapters() {

        rootView.edtgtype.setOnClickListener {
            DataUtils.openSearchDialoge(
                activity!!,
                gtypelist,
                "Select Type",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtgtype.setText(ss.name)
                        gtypeid = ss.id

                    }

                })
        }
        rootView.edtguser.setOnClickListener {
            DataUtils.openSearchDialogeuser(
                activity!!,
                userlist,
                "Select User",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtguser.setText(ss.name)
                        userid = ss.id

                    }

                })

        }
        rootView.edtglocation.setOnClickListener {
            DataUtils.openSearchDialogeuser(
                activity!!,
                locationlist,
                "Select Location",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtglocation.setText(ss.name)
                        locationid = ss.id

                    }

                })

        }

        /* adapterGtype = SpinnerAdapter_gtype(activity, gtypelist)
         adapterRewarduser = SpinnerAdapter_Users(activity, userlist)


         rootView.spin_gtype.setAdapter(adapterGtype)
         rootView.spin_guser.setAdapter(adapterRewarduser)

         rootView.spin_guser.setOnItemSelectedListener(object :
             AdapterView.OnItemSelectedListener {
             override fun onItemSelected(
                 adapterView: AdapterView<*>?, view: View,
                 position: Int, id: Long
             ) {
                 val user: DropdownModel = adapterRewarduser.getItem(position)!!
                 userid = user.id

             }

             override fun onNothingSelected(adapter: AdapterView<*>?) {}
         })

         rootView.spin_gtype.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {
             override fun onItemSelected(
                 adapterView: AdapterView<*>?, view: View,
                 position: Int, id: Long
             ) {
                 val user: DropdownModel = adapterGtype.getItem(position)!!
                 gtypeid = user.id

             }

             override fun onNothingSelected(adapter: AdapterView<*>?) {}
         })*/
    }


//    fun getListingData() {
//        val getMasterRequest = VendorRegReq()
//
//        getMasterRequest.serial_code = rootView.edt_capacity.getText().toString().trim({ it <= ' ' })
//        getMasterRequest.setService_name("serviceName")
//
//        vendorActivity!!.apiImp.saveVendorDetails(getMasterRequest)
//    }

    private fun setListeners() {


        markerAdapter = MarkerListAdapter(activity, object : AdapterListener {
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {

                var markerdatadto = any1 as MarkerDataDTO

                when (i) {
                    1 -> {
                        markerList.remove(markerdatadto)
                        markerAdapter.notifyDataSetChanged()
                    }
                    2 -> {
                        openAudioPlayer(
                            recordingPath!!,
                            markerdatadto.fromtime,
                            markerdatadto.totime
                        )
                    }
                }
            }

        })
        rootView.recmarkers.layoutManager = LinearLayoutManager(activity)
        rootView.recmarkers.adapter = markerAdapter

        rootView.save_marker.setOnClickListener {


            savemarkers(rootView)
        }
        rootView.add_markers.setOnClickListener {
            addMarkerDialog()
        }


        rootView.add_images!!.setOnClickListener { pickImage() }

    }

    fun savemarkers(rootView: View) {


        var ss = ArrayList<MarkerSaveReq2>()
        for (i in markerList.indices) {
            var aa = MarkerSaveReq2()


            aa.grievance_recording_id = mediaRecordingId
            aa.to = markerList.get(i).totime
            aa.from = markerList.get(i).fromtime
            aa.description = markerList.get(i).text
            ss.add(aa)
        }

        val json = Gson().toJson(ss)

        val itemReq = MarkerSaveReq()
        itemReq.data = ss
        itemReq.token = grievanceActivity?.pref?.get(AppConstants.token)

        itemReq.service_name = (ApiUtils.SAVEMARKERS)


        grievanceActivity!!.apiImp.savemarkers(itemReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Toast.makeText(activity, "Saved Successfully", Toast.LENGTH_SHORT).show()
                    grievanceActivity!!.finish()
                }

            }

        })
    }

    private fun managerVisibility(first: View, second: View) {
        first.visibility = View.VISIBLE
        second.visibility = View.GONE

    }

    private fun addMarkerDialog() {
        val dialog =
            Dialog(activity!!)
        dialog.setContentView(R.layout.dialog_addmarker) //layout for dialog
        dialog.setTitle("Add Markers")
        dialog.setCancelable(false) //none-dismiss when touching outside Dialog
        // set the custom dialog components - texts and image
        val btnAdd = dialog.findViewById<View>(R.id.btn_ok)
        val btnCancel = dialog.findViewById<View>(R.id.btn_cancel)
        val edt_markertext = dialog.findViewById<EditText>(R.id.edt_markertext)
        //set spinner adapter

        dialog.tttt.setText("time From: " + txtfroom.text.toString())
        dialog.fff.setText("time To: " + txtto.text.toString())
        btnAdd.setOnClickListener {
            markerList.add(
                MarkerDataDTO(
                    apiFrom.toString(),
                    apiTo.toString(),
                    dialog.edt_markertext.text.toString()
                )
            )
            rootView.save_marker.visibility = View.VISIBLE
            markerAdapter.setmDataset(markerList)
            markerAdapter.notifyDataSetChanged()
            dialog.dismiss()

//            executeCutAudioCommand(apiFrom, apiTo)
        }
        btnCancel.setOnClickListener {
            dialog.dismiss()
        }
        dialog.show()
    }


    private fun loadFFMpegBinary() {
        try {
            if (ffmpeg == null) {
                Log.e("", "ffmpeg : era nulo")
                ffmpeg = FFmpeg.getInstance(activity)
            }
            ffmpeg!!.loadBinary(object : LoadBinaryResponseHandler() {
                override fun onFailure() {
                    showUnsupportedExceptionDialog()
                }

                override fun onSuccess() {
                    Log.e(
                        "",
                        "ffmpeg : correct Loaded"
                    )
                }
            })
        } catch (e: FFmpegNotSupportedException) {
            showUnsupportedExceptionDialog()
        } catch (e: java.lang.Exception) {
            Log.e(
                "",
                "EXception no controlada : $e"
            )
        }
    }

    private fun showUnsupportedExceptionDialog() {
        AlertDialog.Builder(activity)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Not Supported")
            .setMessage("Device Not Supported")
            .setCancelable(false)
            .setPositiveButton(android.R.string.ok,
                DialogInterface.OnClickListener { dialog, which -> grievanceActivity?.finish() })
            .create()
            .show()
    }

    private fun executeCutAudioCommand(startMs: Int, endMs: Int) {
        val moviesDir = Environment.getExternalStoragePublicDirectory(
            Environment.DIRECTORY_MUSIC
        )
        val filePrefix = "cut_audio"
        val fileExtn = ".mp3"
        val yourRealPath: String = file_name
        var dest = File(moviesDir, filePrefix + fileExtn)
        var fileNo = 0
        while (dest.exists()) {
            fileNo++
            dest = File(moviesDir, filePrefix + fileNo + fileExtn)
        }
        var filePath = dest.absolutePath
        Log.e("TAG", "executeCutAudioCommand: " + filePath)

        val complexCommand = arrayOf(
            "-i",
            yourRealPath,
            "-ss",
            "" + startMs / 1000,
            "-t",
            "" + (endMs - startMs) / 1000,
            "-acodec",
            "copy",
            filePath
        )
        execFFmpegBinary(complexCommand)
    }

    private fun execFFmpegBinary(command: Array<String>) {
        try {

            ffmpeg?.execute(command, object : ExecuteBinaryResponseHandler() {
                override fun onFailure(s: String) {
                    Log.e(
                        "",
                        "FAILED with output : $s"
                    )
                }

                override fun onSuccess(s: String) {
                    Log.e(
                        "",
                        "SUCCESS with output : $s"
                    )
//                    val intent = Intent(this@AudioCutterActivity, AudioPreviewActivity::class.java)
//                    intent.putExtra(
//                        video.cutter.mp3.activity.AudioCutterActivity.FILEPATH,
//                        filePath
//                    )
//                    startActivity(intent)
                }

                override fun onProgress(s: String) {
                    Log.e(
                        "",
                        "progress : $s"
                    )
                }

                override fun onStart() {
                    Log.e(
                        "",
                        "Started command : ffmpeg " + Arrays.toString(command)
                    )
                }

                override fun onFinish() {
                    Log.e(
                        "",
                        "Finished command : ffmpeg " + Arrays.toString(command)
                    )
                }
            })
        } catch (e: FFmpegCommandAlreadyRunningException) {
            // do nothing for now
        }
    }


    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@GrievanceRegistration, options)

    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {

            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }

    private fun check(): Boolean {

//        if (edt_agenda.getText().toString().equalsIgnoreCase("")) {
//            toast("Please Enter agenda!!");
//            return false;
//        } else {
//            items.setName(edt_agenda.getText().toString());
//        }
        return true
    }

    private fun uploadImages() {

        grievanceActivity?.apiImp?.hitApi(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {

                if (status) {


                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)


                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "image"
                    model.name = imageFilename
                    model.duration = ""
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < imageList.size) {

                        uploadImages()

                    } else {
                        saveGrievance()
                    }


                }


            }

        })


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == 100) {

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
//                getImageContentUri(activity!!,returnValue.get(0))

                val fileUri = intent?.data
                Log.e("TAG", "onActivityResult: " + returnValue + " -- " + fileUri)
                imageList.add(
                    ImageDTO(
                        Uri.parse("file://" + returnValue.get(0)).toString()
                    )
                )
                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })
            }
        }
    }

    fun showDynamicImage(
        llAtach: LinearLayout,
        imageList: ArrayList<ImageDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = ImageItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
                showImageUri(imageDTO.getUri(), context)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Image",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

//        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }

    fun getTimestamp(): String? {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }

    fun getCompressedUri(uri: Uri, context: Context): Uri {
        Log.v("getCompressUri", "${uri.path}")
        val path = "${Environment.getExternalStorageDirectory()}/ERP/ temp"
        val fileName = "${getTimestamp()}.png"

        val myDir = File(path)

        if (!myDir.exists())
            myDir.mkdirs()

        val file = File(myDir, fileName)
        val currentFile = File(uri.path!!)

        val returnString = Uri.parse(file.absolutePath)

        if (file.exists()) file.delete()
        try {
//            val out = FileOutputStream(file)
//            val finalBitmap = Compressor(context).compressToBitmap(currentFile)
//            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
//            out.flush()
//            out.close()
        } catch (e: Exception) {
            Log.v("getCompressUri", "exception $e")
            return uri
        }

        Log.v("getCompressUri", "last ${returnString.path}")

        return returnString

    }


    companion object {
        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = GrievanceRegistration()
    }

    override fun onValuesChanging(minValue: Float, maxValue: Float) {
        apiFrom = minValue.toInt()
        apiTo = maxValue.toInt()
        txtfroom.setText(getTimeValues(minValue))
        txtto.setText(getTimeValues(maxValue))
    }

    override fun onValuesChanging(minValue: Int, maxValue: Int) {
    }

    override fun onRatingChanged(ratingBar: RatingBar?, rating: Float, fromUser: Boolean) {

    }


}