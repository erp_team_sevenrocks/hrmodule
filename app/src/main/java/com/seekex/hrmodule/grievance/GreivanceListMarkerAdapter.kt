package com.seekex.hrmodule.grievance

import android.content.Context
import android.media.MediaPlayer
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.R
import com.seekex.hrmodule.databinding.MarkerListAdapterMeetingBinding
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import kotlin.collections.ArrayList

class GreivanceListMarkerAdapter(
    context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<GreivanceListMarkerAdapter.ViewHolder>() /*implements DialogeclickListener, AdapterApiCallBack*/ {
    private var mDataset: ArrayList<MarkerSaveReq2>? = null
    private val mInflater: LayoutInflater
    private val inventoryScanActivity: GrievanceActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null

    inner class ViewHolder(  // each data item is just a string in this case

        val binding: MarkerListAdapterMeetingBinding
    ) : RecyclerView.ViewHolder(binding.root)

    fun setmDataset(outGoingScannble: ArrayList<MarkerSaveReq2>?) {
        mDataset = outGoingScannble
        notifyDataSetChanged()
    }

    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: MarkerListAdapterMeetingBinding = DataBindingUtil.inflate(
            mInflater,
            R.layout.marker_list_adapter_meeting, parent, false
        )
        
        return ViewHolder(binding)
    }


    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val batchData = mDataset!![position]
        holder.binding.model = batchData
        holder.binding.executePendingBindings()
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return if (mDataset != null) mDataset!!.size else 0
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    init {
        inventoryScanActivity = context as GrievanceActivity?
        mInflater = LayoutInflater.from(context)
    }
}