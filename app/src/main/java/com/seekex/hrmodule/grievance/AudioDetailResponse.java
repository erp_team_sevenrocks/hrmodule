
package com.seekex.hrmodule.grievance;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class AudioDetailResponse extends BaseResponse {

    @SerializedName("data")
    private AudioDetailResponse2 data;

    public AudioDetailResponse2 getData() {
        return data;
    }

    public void setData(AudioDetailResponse2 data) {
        this.data = data;
    }



}

