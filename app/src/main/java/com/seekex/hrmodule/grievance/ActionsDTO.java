
package com.seekex.hrmodule.grievance;

import com.google.gson.annotations.SerializedName;

public class ActionsDTO {

    @SerializedName("assignee_name")
    private String assignee_name;
    @SerializedName("id")
    private String id;
    @SerializedName("created")
    private String created;
    @SerializedName("action")
    private String action;

    public String getAssignee_nametext() {
        return "Assignee name: "+assignee_name;
    }
    public String getAssignee_name() {
        return assignee_name;
    }
    public void setAssignee_name(String assignee_name) {
        this.assignee_name = assignee_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getCreatedtext() {
        return "Created: "+created;
    }
    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }
    public String getActiontext() {
        return "Action: "+action;
    }
    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }
}
