package com.seekex.hrmodule.custombinding

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import com.seekex.hrmodule.R


/**
 * Created by admin on 4/9/2016.
 */


class CustomLoader : Dialog {

    constructor(context: Context) : super(context, android.R.style.Theme_Translucent_NoTitleBar) {
        // TODO Auto-generated constructor stub
        setContentView(R.layout.custom_progress)
        setCancelable(false)
        setCanceledOnTouchOutside(false)



    }


    constructor(
        context: Context, cancelable: Boolean,
        cancelListener: DialogInterface.OnCancelListener
    ) : super(context, cancelable, cancelListener) {
        // TODO Auto-generated constructor stub
    }

}
