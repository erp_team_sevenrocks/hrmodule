package com.seekex.hrmodule.custombinding;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.retrofitclasses.models.DashboardModel;


import java.util.ArrayList;

public abstract class DashboardAdapter extends BaseAdapter {

        Context context;

    ArrayList<DashboardModel> alist;

        public View.OnClickListener onViewclick;

        public DashboardAdapter(Context context, ArrayList<DashboardModel> alist){
            this.alist=alist;
            this.context=context;
            onViewclick = new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub

                    onViewClicked(v,String.valueOf(v.getTag(R.string.id)));


                }

            };

        }



    protected abstract void onViewClicked(View v, String s);

        @Override
        public int getCount() {
            return alist.size();
        }

        @Override
        public Object getItem(int position) {
            return alist.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            View vi =convertView;
            LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                vi = mInflater.inflate(R.layout.home_adapter, null);
            }

            Log.v("getViewaaa",alist.toString());

            DashboardModel dashboardModel=alist.get(position);

            final ImageView  imageView=(ImageView)vi.findViewById(R.id.iv);
            final CardView cardView=(CardView) vi.findViewById(R.id.adduser);
            final TextView tvLable=(TextView)vi.findViewById(R.id.l);
            final TextView tvDetail=(TextView)vi.findViewById(R.id.d);

            imageView.setImageResource(dashboardModel.getDrawableId());
            tvLable.setText(dashboardModel.getLabel());
            tvDetail.setText(dashboardModel.getDetail());

            cardView.setOnClickListener(onViewclick);
            cardView.setTag(R.string.id,dashboardModel.getId());

            return vi;
        }

        }
