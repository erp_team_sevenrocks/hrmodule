package com.seekex.hrmodule.custombinding

import android.content.Context
import android.graphics.*
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

class RoundedImageViw : AppCompatImageView {

    constructor(context: Context) : super(context) {
        //TODO Auto-generated constructor stub
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {}

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {}

    override fun onDraw(canvas: Canvas) {

        val drawable = getDrawable() ?: return

        if (getWidth() === 0 || getHeight() === 0) {
            return
        }
        val b = (drawable as BitmapDrawable).bitmap
        val bitmap = b.copy(Bitmap.Config.ARGB_8888, true)

        val w = getWidth()
        val h = getHeight()
        val radius = if (w < h) w else h

        val roundBitmap = getCroppedBitmap(bitmap, radius, w, h)
        // roundBitmap= ImageUtils.setCircularInnerGlow(roundBitmap, 0xFFBAB399,
        // 4, 1);
        canvas.drawBitmap(roundBitmap, 0f, 0f, null)

    }

    companion object {

        fun getCroppedBitmap(bmp: Bitmap, radius: Int, w: Int, h: Int): Bitmap {
            val sbmp: Bitmap
            if (bmp.width != radius || bmp.height != radius) {
                val _w_rate = 1.0f * radius / bmp.width
                val _h_rate = 1.0f * radius / bmp.height
                val _rate = if (_w_rate < _h_rate) _h_rate else _w_rate
                sbmp = Bitmap.createScaledBitmap(bmp, (bmp.width * _rate).toInt(), (bmp.height * _rate).toInt(), false)
            } else
                sbmp = bmp

            val output = Bitmap.createBitmap(
                sbmp.width, sbmp.height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(output)

            val color = -0x5e688c
            val paint = Paint()
            val rect = Rect(0, 0, sbmp.width, sbmp.height)

            paint.isAntiAlias = true
            paint.isFilterBitmap = true
            paint.isDither = true
            canvas.drawARGB(0, 0, 0, 0)
            paint.color = Color.parseColor("#BAB399")
            canvas.drawCircle((w / 2).toFloat(), (h / 2).toFloat(), ((if (w < h) w else h) / 2).toFloat(), paint)
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            canvas.drawBitmap(sbmp, rect, rect, paint)

            return output
        }


        fun getCroppedBitmap(bmp: Bitmap, radius: Int): Bitmap {
            val sbmp: Bitmap
            if (bmp.width != radius || bmp.height != radius)
                sbmp = Bitmap.createScaledBitmap(bmp, radius, radius, false)
            else
                sbmp = bmp
            val output = Bitmap.createBitmap(
                sbmp.width,
                sbmp.height, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(output)

            val color = -0x5e688c
            val paint = Paint()
            val rect = Rect(0, 0, sbmp.width, sbmp.height)

            paint.isAntiAlias = true
            paint.isFilterBitmap = true
            paint.isDither = true
            canvas.drawARGB(0, 0, 0, 0)
            paint.color = Color.parseColor("#BAB399")
            canvas.drawCircle(
                sbmp.width / 2 + 0.7f, sbmp.height / 2 + 0.7f,
                sbmp.width / 2 + 0.1f, paint
            )
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            canvas.drawBitmap(sbmp, rect, rect, paint)


            return output
        }
    }
}