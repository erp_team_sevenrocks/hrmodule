package com.seekex.hrmodule.custombinding

import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.SystemClock
import android.text.TextUtils
import android.view.Window
import android.widget.Chronometer
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.seekex.hrmodule.R
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ImageUtils
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.image_view.*

class CustonViewBindings {

    companion object {

        @JvmStatic
        @BindingAdapter("retriveVideoFrameFromVideo")
        fun retriveVideoFrameFromVideo(view: ImageView?,videoPath: String?) {
            val requestOptions = RequestOptions()
            Glide.with(view!!.context)
                .load(videoPath)
                .apply(requestOptions)
                .thumbnail(Glide.with(view.context).load(videoPath))
                .into(view!!)
        }
        @JvmStatic
        @BindingAdapter("uriImage")
        fun uriImage(view: ImageView?, uriImage: String?) {

            if (view == null || uriImage == null)
                return

            view.setImageURI(Uri.parse(uriImage))

            view.setOnClickListener {
                showImageUri(Uri.parse(uriImage), view.context)
            }

        }

        @JvmStatic
        @BindingAdapter("bitmapImage")
        fun bitmapImage(view: ImageView?, uriImage: Bitmap?) {

            if (view == null || uriImage == null)
                return

            view.setImageBitmap(uriImage)
//
//            view.setOnClickListener {
//                showImageUri(Uri.parse(uriImage), view.context)
//            }

        }
        fun showImageUri(uri: Uri, ctx: Context) {
            val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.image_view)

            dialog.tiv.setImageURI(uri)

            dialog.show()
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)

        }

        @JvmStatic
        @BindingAdapter("choronmeterSetting")
        fun choronmeterSetting(ch_meter: Chronometer?, timieinmillis: String?) {


            try {
                if (timieinmillis!!.equals("") || timieinmillis == null || ch_meter == null)
                    return
// 60*1000*2  2 mint
                val timeInMil = 60 * 60 * 1000  // 1 hour
                val timeInMilSeconds = System.currentTimeMillis() - timieinmillis.toLong()
//        Log.e("", "setListener: "+System.currentTimeMillis() +" -- "+SystemClock.elapsedRealtime())
//        Log.e("", "timeInMilSeconds: "+timeInMilSeconds)

                ch_meter.base = SystemClock.elapsedRealtime() - timeInMilSeconds
                ch_meter.start()
            } catch (e: Exception) {
            }

        }

        @JvmStatic
        @BindingAdapter("openImage")
        fun openImage(view: ImageView?, image: String?) {
            if (view == null || image == null)
                return

            try {
                Glide.with(view.context)
                    .load(ApiUtils.DOMAIN+image)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)

                view.setOnClickListener {
                    ImageUtils.showImage(image, view.context)
                }
            } catch (e: Exception) {
            }
        }
        @JvmStatic
        @BindingAdapter("leaveImage")
        fun leaveImage(view: ImageView?, image: String?) {
            if (view == null || image == null)
                return

            try {
                Glide.with(view.context)
                    .load(image)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)

                view.setOnClickListener {
                    ImageUtils.showImage(image, view.context)
                }
            } catch (e: Exception) {
            }
        }
        @JvmStatic
        @BindingAdapter("rewardImage")
        fun rewardImage(view: ImageView?, image: String?) {
            if (view == null || image == null)
                return

            try {
                Glide.with(view.context)
                    .load(ApiUtils.DOMAIN +image)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)

                view.setOnClickListener {
                    ImageUtils.showImage(ApiUtils.DOMAIN +image, view.context)
                }
            } catch (e: Exception) {
            }
        }

        @JvmStatic
        @BindingAdapter("closedImage")
        fun closedImage(view: ImageView?, image: String?) {

            try {
                if (view == null || image == null)
                    return

                Glide.with(view.context)
                    .load(image)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)
            } catch (e: Exception) {
            }
        }


        @JvmStatic
        @BindingAdapter("capitalizeText")
        fun capitalizeText(view: TextView?, str: String?) {

            if (view == null || str == null)
                return


            view.text = ExtraUtils.capitalizeText(str)
            view.setEllipsize(TextUtils.TruncateAt.MARQUEE)
            view.isSelected = true

        }

        fun setDrawableImage(view: RoundedImageViw?, drawableId: Drawable) {
            try {
                Glide.with(view!!.context)
                    .load(drawableId)
                    .placeholder(R.drawable.defaultimg)
                    .into(view)
            } catch (e: Exception) {
            }
        }


        @JvmStatic
        @BindingAdapter("audioRating")
        fun audioRating(textView: TextView, amt: Double?) {
            textView.text = textView.context.getString(
                R.string.audio_rating, ExtraUtils.getFormatedNumber(
                    amt
                ).toString()
            )
        }

        @JvmStatic
        @BindingAdapter("rupee")
        fun rupee(textView: TextView?, rate: Double?) {

            if (textView == null || rate == null)
                return

            textView.text = textView.context.getString(
                R.string.rupee, ExtraUtils.coolFormat(
                    rate, 0
                ).toString()
            )
        }


        fun setIntAndExpert(view: TextView?, expert: Boolean?) {

            if (expert!!)
                view?.text = view?.context?.getString(R.string.expertise)
            else
                view?.text = view?.context?.getString(R.string.interest)

        }

    }


}
