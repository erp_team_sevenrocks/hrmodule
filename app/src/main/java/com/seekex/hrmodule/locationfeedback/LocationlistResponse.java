package com.seekex.hrmodule.locationfeedback;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.markrewarks.ParicipantModel;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class LocationlistResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<ParicipantModel> data;

    public ArrayList<ParicipantModel> getData() {
        return data;
    }

    public void setData(ArrayList<ParicipantModel> data) {
        this.data = data;
    }


}

