package com.sevenrocks.taskapp.appModules.markrewarks

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.media.MediaPlayer
import android.media.MediaRecorder
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.preference.PreferenceManager
import android.provider.MediaStore
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.MediaController
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.zxing.integration.android.IntentIntegrator
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.adapter.EventParamAdapter
import com.seekex.hrmodule.adapter.PickedlistAdapter
import com.seekex.hrmodule.criticalevent.ParamlistResponse
import com.seekex.hrmodule.databinding.ImageItemBinding
import com.seekex.hrmodule.databinding.VideoItemBinding
import com.seekex.hrmodule.feedback.*
import com.seekex.hrmodule.grievance.ImagesGRDTO
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.locationfeedback.*
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.sop.SaveAuditMediaDOP
import com.seekex.hrmodule.sop.VideoDTO
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import com.sevenrocks.taskapp.appModules.grievance.SaveEvent
import kotlinx.android.synthetic.main.add_feedback.*
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_feedback.view.savefeedback
import kotlinx.android.synthetic.main.add_loc_feedback.*
import kotlinx.android.synthetic.main.add_loc_feedback.ll_attach
import kotlinx.android.synthetic.main.add_loc_feedback.view.*
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.image_view.*
import kotlinx.android.synthetic.main.kra_assessment.view.*
import kotlinx.android.synthetic.main.*
import kotlinx.android.synthetic.main.add_loc_feedback.ll_attachvideo
import kotlinx.android.synthetic.main.play_video.*
import kotlinx.android.synthetic.main.sopauditlist.*
import java.io.File
import java.io.IOException
import java.util.*


/**
 */
class AddLocFeedbackFragment : Fragment() {
    private var location_id_txt: String = ""
    private var audioFilename: String = ""
    var mediaRecorder: MediaRecorder? = null
    var countDownTimer: CountDownTimer? = null
    var isRecorded = 0
    private var count: Long? = null
    private var timer = ""
    private var file_name = ""
    private var permissionCallBack: PermissionCallBack? = null
    private var resumeSelectCallback: FileSelectionCallBack? = null
    private var call_Counter = 0
    private var intentIntegrator: IntentIntegrator? = null
    public var videoList = ArrayList<VideoDTO>()

    private var userId: String? = "0"
    private var feedbacktypeid: String? = "0"
    private var sopId: String? = "0"
    private var feedid: String? = "0"
    private lateinit var adapterUsers: SpinnerAdapter_Users
    private lateinit var adapterFeedbacktype: SpinnerAdapter_FeedbackType
    private lateinit var adapterFeed: SpinnerAdapter_Feed
    public var imageList = ArrayList<ImageDTO>()
    var imagePathList: ArrayList<ImagesGRDTO> = java.util.ArrayList()

    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var feedbacktypelist: ArrayList<DropdownModel> = java.util.ArrayList()
    var feedlist: ArrayList<DropdownModel> = java.util.ArrayList()

    public var feedbackActivity: LocationFeedbackActivity? = null

    lateinit var apiImp: ApiImp
    private lateinit var adapter: EventParamAdapter


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        val rootView = inflater.inflate(R.layout.add_loc_feedback, BasicActi, false)
        feedbackActivity = activity as LocationFeedbackActivity?
        rootView.txtheader.setText("Add Suggestions")
//        setAdapters(rootView)
        setlistener(rootView)
        getFeedbacktype()

        return rootView
    }

    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@AddLocFeedbackFragment, options)

    }

    private fun pickVideo() {


        val options: Options = Options.init()
            .setRequestCode(110) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span count for gallery min 1 & max 5
            .setMode(Options.Mode.Video) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@AddLocFeedbackFragment, options)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == 110) {
                val bmThumbnail: Bitmap?

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!
                bmThumbnail = ThumbnailUtils.createVideoThumbnail(
                    returnValue.get(0).toString(),
                    MediaStore.Video.Thumbnails.MICRO_KIND
                )
                Log.e("TAG", "onActivityResult: $bmThumbnail ")
                videoList.add(
                    VideoDTO(bmThumbnail!!, returnValue.get(0))
                )
                showDynamicVideo(ll_attachvideo!!,
                    videoList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
//                            videoList = any as ArrayList<ImageDTO>
                        }
                    })
            } else if (requestCode == 100) {

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!

                val fileUri = intent?.data
                imageList.add(
                    ImageDTO(
                        Uri.parse("file://" + returnValue.get(0)).toString()
                    )
                )
                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })
            } else {

                Log.e("TAG", "onActivityResult:  in else")

                val intentResult =
                    IntentIntegrator.parseActivityResult(requestCode, resultCode, intent)
                Log.e("TAG", "onActivityResult:  in else intentResult" + intentResult)

                if (intentResult != null) {
                    if (intentResult.contents == null) {
                        Toast.makeText(activity, "Cancelled", Toast.LENGTH_SHORT).show()
                    } else {

                        getlocationdetails(intentResult.contents)
                        // if the intentResult is not null we'll set
                        // the content and format of scan message
//                    Toast.makeText(activity, intentResult.contents, Toast.LENGTH_SHORT).show()

                    }

                }
            }
        }
    }

    fun showDynamicVideo(
        llAtach: LinearLayout,
        imageList: ArrayList<VideoDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = VideoItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
//                showImageUri(imageDTO.getUri(), context)
                openVideoPLayer(imageDTO.path)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Video",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    private fun openVideoPLayer(toString: String) {

        val dialog = Dialog(activity!!, android.R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_video)

        dialog.setCancelable(true)

        dialog.pb_video.visibility = View.VISIBLE
        val mc = MediaController(activity!!)
        mc.setAnchorView(dialog.videoview)
        mc.setMediaPlayer(dialog.videoview)
        dialog.videoview.setMediaController(mc);
        val video =
            Uri.parse(toString)
        dialog.videoview.setVideoPath(toString)
        dialog.videoview.setOnPreparedListener(MediaPlayer.OnPreparedListener { mp ->
            dialog.pb_video.visibility = View.GONE

            mp.isLooping = true
            dialog.videoview.start()
        })



        dialog.setOnDismissListener {
            dialog.dismiss()
        }


        dialog.btn_play.setOnClickListener {


        }
        dialog.btn_pause.setOnClickListener {

        }

        dialog.setOnDismissListener {
        }
        dialog.show()


    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {

            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }

    private fun stopMedia() {
        try {
            mediaRecorder!!.stop()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun startRecording() {
        mediaRecorderReady()
        try {
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startCountdownTimer()
    }


    private fun startCountdownTimer() {
        count = 0L
        countDownTimer = object : CountDownTimer(10800000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // Used for formatting digit to be in 2 digits only
                count = count!! + 1
                tvTimersfeed!!.text = ExtraUtils.convertSecondToHHMMString(count!!)
            }

            // When the task is over it will print 00:00:00 there
            override fun onFinish() {
                tvTimersfeed!!.text = "00:00:00"
            }
        }.start()
    }

    private fun mediaRecorderReady() {
        val currentTime = System.currentTimeMillis()
        val extBaseDir = activity?.externalCacheDir
        val file = File(extBaseDir!!.absolutePath + "/SeekReward/" + "")
        if (!file.exists()) {
            if (!file.mkdirs()) {
            }
        }
        file_name = "$file/myRecording.mp3"
        Log.e("TAG", "mediaRecorderReady: $file_name")
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        mediaRecorder!!.setOutputFile(file_name)
    }

    private fun setlistener(rootView: View?) {


        rootView!!.iv_actionfeed.setOnClickListener {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    if (isRecorded == 0) {
                        isRecorded = 1
                        playBeep()
                        startRecording()
                        iv_actionfeed!!.setImageResource(android.R.drawable.ic_media_pause)
                    } else if (isRecorded == 1) {
                        bt_savefeed!!.visibility = View.INVISIBLE
                        stopMedia()
                        playBeep()
                        isRecorded = 2
                        countDownTimer!!.cancel()
                        timer = tvTimersfeed!!.text.toString()
                        iv_actionfeed!!.setImageResource(android.R.drawable.ic_media_play)
                    } else if (isRecorded == 2) {
//                        FileUtils.openDocument(file_name, activity)
                    }

                }

            })


        }
        rootView!!.bt_resetfeed.setOnClickListener {
            try {
                isRecorded = 0
                stopMedia()
                playBeep()
                countDownTimer!!.cancel()
                tvTimersfeed!!.text = ""
                bt_savefeed!!.visibility = View.GONE
                iv_actionfeed!!.setImageDrawable(activity!!.resources.getDrawable(R.drawable.audiorec))
            } catch (e: Exception) {
            }
        }

        rootView!!.edtlocfeedtype.setOnClickListener {

            if (location_id_txt.equals("")){
                feedbackActivity!!.dialogUtil.showAlert("Scan location first ")
                return@setOnClickListener
            }

            DataUtils.openSearchDialoge(
                activity!!,
                feedbacktypelist,
                "Select Suggestion Type",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtlocfeedtype.setText(ss.name)
                        feedbacktypeid = ss.id
                        if (ss.id.equals("1")) {
                            rootView.ll_sop.visibility = View.GONE
                            rootView.ll_userloc.visibility = View.VISIBLE
                            rootView.txtsuggestionuserselected.visibility = View.VISIBLE

                            getdataontype("1")
                        } else if (ss.id.equals("2")) {
                            rootView.ll_userloc.visibility = View.GONE
                            rootView.ll_sop.visibility = View.VISIBLE
                            rootView.txtsuggestionuserselected.visibility = View.GONE

                            getdataontype("2")
                        } else if (ss.id.equals("3")) {
                            rootView.ll_userloc.visibility = View.GONE
                            rootView.ll_sop.visibility = View.GONE
                            rootView.txtsuggestionuserselected.visibility = View.GONE

                        }

                    }

                })
        }
        rootView!!.edtlocsoplist.setOnClickListener {
            DataUtils.openSearchDialogeuser(
                activity!!,
                userlist,
                "Select SOP",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtlocsoplist.setText(ss.name)
                        sopId = ss.id
                    }

                })
        }
        rootView!!.edt_feedlocation.setOnClickListener {
            openScannner()
        }

        rootView!!.imagesloc.setOnClickListener {
            pickImage()
        }
        rootView!!.videosfeed.setOnClickListener {
            pickVideo()
        }
        rootView!!.btn_selectuser.setOnClickListener {
/*
            val dialog =
                Dialog(activity!!, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
            dialog.setContentView(R.layout.dialog) //layout for dialog
            dialog.setTitle("Add user")
            dialog.setCancelable(false) //none-dismiss when touching outside Dialog
            // set the custom dialog components - texts and image
            val recyclerView = dialog.findViewById<View>(R.id.rec_participant) as RecyclerView
            val btnAdd = dialog.findViewById<View>(R.id.btn_ok)
            val btnCancel = dialog.findViewById<View>(R.id.btn_cancel)
            val edt_search = dialog.findViewById<EditText>(R.id.edt_search)
            //set spinner adapter
            adapter = EventParamAdapter(userlist, activity)
            recyclerView.layoutManager = LinearLayoutManager(activity)
            recyclerView.adapter = adapter

            edt_search.addTextChangedListener(object : TextWatcher {
                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                    // TODO Auto-generated method stub
                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                        count: Int,
                    after: Int
                ) {

                    // TODO Auto-generated method stub
                }

                override fun afterTextChanged(s: Editable) {

                    // filter your list from your input
                    filterSearch(s.toString())
                    //you can use runnable postDelayed like 500 ms to delay search text
                }
            })

            btnAdd.setOnClickListener {
                Log.e("TAG", "setlisteners: " + userlist.size)

                for (i in userlist.indices) {
                    Log.e("TAG", "onClick: " + userlist[i].checked)
                    if (userlist[i].checked == true) {
                        if (!namestoShow.contains(userlist[i])) {
                            namestoShow.add(userlist[i])
                        }
                    } else {
                        if (namestoShow.contains(userlist[i])) {
                            namestoShow.remove(userlist[i])
                        }
                    }
                }
                Log.e("TAG", "onClick: " + namestoShow.size)
                showDatainPickList(namestoShow)
                dialog.dismiss()
            }
            btnCancel.setOnClickListener {
                for (i in userlist.indices) {
                    Log.e("TAG", "onClick: " + userlist[i].checked)
                    if (namestoShow.contains(userlist[i])) {
                        userlist[i].checked = true
                    } else {
                        userlist[i].checked = false
                    }
                }
                Log.e("TAG", "onClick: " + namestoShow.size)
                showDatainPickList(namestoShow)
                dialog.dismiss()
            }
            dialog.show()
*/


            DataUtils.openSearchDialogeMultiSelect(
                activity!!,
                mulUserlist,
                userlist,
                "Select user",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        val list: ArrayList<String> = ArrayList()

                        rootView.txtsuggestionuserselected.setText("")
                        var ss = any as ArrayList<DropdownModel>
                        Log.e("TAG", "onSelected: " + ss.size)

                        for (dd in ss) {
                            var aa = ParicipantModel("")
                            aa.id = dd.id
                            list.add(dd.name)
                            namestoShow.add(aa)

                        }
                        if (ss.size > 1) {
                            rootView.txtsuggestionuserselected.setText(TextUtils.join(", ", list))
                        } else {
                            rootView.txtsuggestionuserselected.setText(TextUtils.join(", ", list))

                        }

//                        userId = ss.id

                    }

                })

        }
        rootView!!.add_feedbacknew.setOnClickListener {


            if (feedbacktypeid.equals("0")) {
                Toast.makeText(activity, "Select Feedback Type", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (edt_rewfeedback.text.toString().trim().equals("")) {
                Toast.makeText(activity, "Enter remark", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (feedbacktypeid.equals("1")) {
                if (namestoShow.size == 0) {
                    Toast.makeText(activity, "Select User or Sop", Toast.LENGTH_SHORT).show()
                    return@setOnClickListener
                }
            }
            if (feedbacktypeid.equals("2")) {
    if (sopId.equals("")){
        Toast.makeText(activity, "Select Sop", Toast.LENGTH_SHORT).show()
        return@setOnClickListener
    }
            }

            if (imageList.size > 0) {
                uploadImages()
            } else if (videoList.size > 0) {
                uploadVideo()
            } else if (bt_savefeed.visibility == View.INVISIBLE) {
                saveaudiofile()
            } else {
                saveData()
            }


        }
    }

    var mulUserlist: ArrayList<DropdownModel> = java.util.ArrayList()

    private fun uploadVideo() {

        feedbackActivity!!.apiImp.showProgressBar()
        feedbackActivity?.apiImp?.hitApivideo(call_Counter, videoList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                feedbackActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)
                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "video"
                    model.name = imageFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < videoList.size) {
                        uploadVideo()
                    } else {
                        call_Counter = 0
                        if (bt_savefeed.visibility == View.INVISIBLE) {
                            saveaudiofile()
                        } else {
                            saveData()
                        }
                    }
                }
            }
        })
    }

    private fun uploadImages() {

        feedbackActivity!!.apiImp.showProgressBar()
        feedbackActivity?.apiImp?.hitApi(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                feedbackActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)


                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "image"
                    model.name = imageFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < imageList.size) {
                        uploadImages()
                    } else if (videoList.size > 0) {
                        call_Counter = 0
                        uploadVideo()
                    } else {
                        call_Counter = 0
                        if (bt_savefeed.visibility == View.INVISIBLE) {
                            saveaudiofile()
                        } else {
                            saveData()
                        }
                    }
                }
            }
        })
    }

    private fun saveaudiofile() {
        feedbackActivity!!.apiImp.showProgressBar()
        feedbackActivity!!.apiImp.saveAudio(file_name, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                feedbackActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    audioFilename = ss.data.path + "" + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "audio"
                    model.name = audioFilename
                    imagePathList.add(model)
                    saveData()
                }
            }
        })
    }

    private fun openScannner() {
        intentIntegrator = IntentIntegrator.forSupportFragment(this@AddLocFeedbackFragment)
        intentIntegrator!!.setPrompt("Scan Barcode")
        intentIntegrator!!.setOrientationLocked(false)
        intentIntegrator!!.setBeepEnabled(true)
        intentIntegrator!!.initiateScan(); // or QR_CODE_TYPES if you need to scan QR
    }

    private fun showDatainPickList(list: ArrayList<ParicipantModel>) {
        val adapter = PickedlistAdapter(list, activity)
        rec_selecteduser!!.layoutManager = LinearLayoutManager(activity)
        rec_selecteduser!!.adapter = adapter
    }

    /* fun filterSearch(text: String?) {
         val temp = ArrayList<ParicipantModel>()
         for (d in userlist) {

             if (d.name.toLowerCase().contains(text!!.toLowerCase())) {
                 temp.add(d)
             }
         }
         //update recyclerview
         adapter.updateList(temp)
     }*/


    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }

    /* private fun setAdapters(rootView: View) {


         adapterUsers = SpinnerAdapter_Users(activity, userlist)
         rootView.spin_feeduser.setAdapter(adapterUsers)

         adapterFeed = SpinnerAdapter_Feed(activity, feedlist)
         rootView.spin_feedlist.setAdapter(adapterFeed)

         adapterFeedbacktype = SpinnerAdapter_FeedbackType(activity, feedbacktypelist)
         rootView.spin_feedbacktype.setAdapter(adapterFeedbacktype)

         rootView.spin_feedbacktype.setOnItemSelectedListener(object :
             AdapterView.OnItemSelectedListener {
             override fun onItemSelected(
                 adapterView: AdapterView<*>?, view: View,
                 position: Int, id: Long
             ) {
                 val user: DropdownModel = adapterFeedbacktype.getItem(position)!!
                 feedbacktypeid = user.id

             }

             override fun onNothingSelected(adapter: AdapterView<*>?) {}
         })
         rootView.spin_feedlist.setOnItemSelectedListener(object :
             AdapterView.OnItemSelectedListener {
             override fun onItemSelected(
                 adapterView: AdapterView<*>?, view: View,
                 position: Int, id: Long
             ) {
                 val user: DropdownModel = adapterFeed.getItem(position)!!
                 feedid = user.id

             }

             override fun onNothingSelected(adapter: AdapterView<*>?) {}
         })
         rootView.spin_feeduser.setOnItemSelectedListener(object :
             AdapterView.OnItemSelectedListener {
             override fun onItemSelected(
                 adapterView: AdapterView<*>?, view: View,
                 position: Int, id: Long
             ) {
                 val user: DropdownModel = adapterUsers.getItem(position)!!
                 userId = user.id

             }

             override fun onNothingSelected(adapter: AdapterView<*>?) {}
         })


     }*/


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getFeedbacktype() {
        feedbacktypelist.clear()
        val getMasterRequest = RequestBase()
        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.suggestion_type_list
        feedbackActivity!!.apiImp.getFeedbackType(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    val list = any as RewardUsersResponse

                    feedbacktypelist.addAll(list.data)

//                    adapterFeedbacktype.notifyDataSetChanged()
                }

            }

        })
    }

    fun showDynamicImage(
        llAtach: LinearLayout,
        imageList: ArrayList<ImageDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = ImageItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
                showImageUri(imageDTO.getUri(), context)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Image",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }

    private fun getdataontype(type: String) {
        userlist.clear()
        val getMasterRequest = DataonTypeReq()
        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.get_data_on_type
        getMasterRequest.location_id = location_id_txt
        getMasterRequest.type_id = type
        feedbackActivity!!.apiImp.getdataontype(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {

                    var ss = any as LocationlistResponse

                    var list = ArrayList<ParicipantModel>()
                    list.addAll(ss.data)
                    for (i in list.indices) {
                        userlist.add(
                            DropdownModel(
                                list.get(i).id,
                                list.get(i).name,
                                false
                            )
                        )
                    }

                }

            }

        })
    }

    private fun getlocationdetails(contents: String) {

        val getMasterRequest = LocationdetailReq()
        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.get_location_info
        getMasterRequest.location_id = contents
        feedbackActivity!!.apiImp.getlocationdetails(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    val list = any as LocationResponse
                    location_id_txt = contents
                    txt_locationname.setText(list.data.name)
                    ll_typelayout.visibility - View.VISIBLE
                }

            }

        })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveData() {

        val getMasterRequest = SaveLocFeedbackReq()
        val ss = SaveLocFeedbackReq2()

        var qq = ArrayList<RewardUserSaveReq>()
        for (i in namestoShow.indices) {
            var aa = RewardUserSaveReq()
            aa.id = namestoShow.get(i).id
            qq.add(aa)
        }




        getMasterRequest.token = feedbackActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.save_suggestion
        ss.location_id = location_id_txt
        ss.type_id = feedbacktypeid
        ss.description = edt_rewfeedback.text.toString().trim()
        ss.media = imagePathList
        ss.record_id = qq

        getMasterRequest.data = ss
        feedbackActivity!!.apiImp.savelocFeedback(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    Toast.makeText(activity, "Suggestion Saved", Toast.LENGTH_LONG).show()
                    feedbackActivity!!.finish()
                }


            }

        })
    }

    companion object {
        @JvmStatic
        val namestoShow = ArrayList<ParicipantModel>()

        @JvmStatic
        var alreadySelectedNames = ArrayList<ParicipantModel>()


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddLocFeedbackFragment()
    }
}