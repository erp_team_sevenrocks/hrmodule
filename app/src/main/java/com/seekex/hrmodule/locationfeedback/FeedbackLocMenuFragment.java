
package com.seekex.hrmodule.locationfeedback;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.adapter.ChooseAdapter;
import com.seekex.hrmodule.custombinding.ExpandableHeightGridView;
import com.seekex.hrmodule.feedback.FeedbackActivity;
import com.seekex.hrmodule.utils.Data.DataUtils;
import com.seekx.utils.ExtraUtils;
import com.sevenrocks.taskapp.appModules.markrewarks.AddLocFeedbackFragment;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedbackLocMenuFragment extends Fragment {




    @BindView(R.id.gridview_menu)
    ExpandableHeightGridView grid_views;

    @BindView(R.id.txtheaderre)
    TextView tvHeader;

    private LocationFeedbackActivity feedbackActivity   ;

    public FeedbackLocMenuFragment() {
        // Required empty public constructor
    }

    public static Fragment getInstance(){
        return new FeedbackLocMenuFragment();
    }

    @SuppressLint("SetTextI18n")
    void setTvHeader(){
        tvHeader.setText("Add suggestions");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup BasicActi, Bundle savedInstanceState) {

        // Inflate the layout for getActivity() fragment
        View rootView = inflater.inflate(R.layout.choose_activty, BasicActi, false);
        ButterKnife.bind(this,rootView);

        init();

        setListener();

        setTvHeader();

        return rootView;

    }

    private  void init(){

        feedbackActivity =((LocationFeedbackActivity)getActivity());
        setAapter();
    }


    private void setListener(){


    }

    private void setAapter() {
        ChooseAdapter chooseAdapter = new ChooseAdapter(getContext(), DataUtils.getLocFeedback(Objects.requireNonNull(getActivity()))) {

            @Override
            protected void onViewClicked(View v, String s) {
                switch (s) {

                    case "1":
                        ExtraUtils.changeFragment(feedbackActivity.getSupportFragmentManager(), AddLocFeedbackFragment.getInstance(), true);
                        break;


                }
            }
        };
        grid_views.setAdapter(chooseAdapter);
        grid_views.setExpanded(true);
    }




}
