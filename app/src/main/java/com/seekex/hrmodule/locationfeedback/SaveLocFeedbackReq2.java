package com.seekex.hrmodule.locationfeedback;


import com.seekex.hrmodule.grievance.ImagesDTORES;
import com.seekex.hrmodule.grievance.ImagesGRDTO;
import com.seekex.hrmodule.markrewarks.RewardUserSaveReq;
import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class SaveLocFeedbackReq2 extends RequestBase {

    private String location_id  ;
    private String type_id  ;
    private String description  ;
    private ArrayList<RewardUserSaveReq> record_id  ;
    private ArrayList<ImagesGRDTO> media  ;

    public ArrayList<RewardUserSaveReq> getRecord_id() {
        return record_id;
    }

    public void setRecord_id(ArrayList<RewardUserSaveReq> record_id) {
        this.record_id = record_id;
    }

    public ArrayList<ImagesGRDTO> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<ImagesGRDTO> media) {
        this.media = media;
    }

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
