package com.seekex.hrmodule.locationfeedback;


import com.seekx.webService.models.RequestBase;

public class DataonTypeReq extends RequestBase {

    private String location_id  ;
    private String type_id  ;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }

    public String getType_id() {
        return type_id;
    }

    public void setType_id(String type_id) {
        this.type_id = type_id;
    }
}
