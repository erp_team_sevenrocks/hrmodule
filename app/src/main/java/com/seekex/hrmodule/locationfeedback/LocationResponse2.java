
package com.seekex.hrmodule.locationfeedback;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class LocationResponse2 extends BaseResponse {

    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

