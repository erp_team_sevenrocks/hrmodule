package com.seekex.hrmodule.locationfeedback;


import com.seekx.webService.models.RequestBase;

public class SaveLocFeedbackReq extends RequestBase {

    private SaveLocFeedbackReq2 data  ;

    public SaveLocFeedbackReq2 getData() {
        return data;
    }

    public void setData(SaveLocFeedbackReq2 data) {
        this.data = data;
    }
}
