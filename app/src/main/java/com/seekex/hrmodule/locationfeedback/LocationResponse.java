
package com.seekex.hrmodule.locationfeedback;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class LocationResponse extends BaseResponse {

    @SerializedName("data")
    private LocationResponse2 data;

    public LocationResponse2 getData() {
        return data;
    }

    public void setData(LocationResponse2 data) {
        this.data = data;
    }
}

