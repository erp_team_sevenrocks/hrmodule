package com.seekex.hrmodule.locationfeedback;


import com.seekx.webService.models.RequestBase;

public class LocationdetailReq extends RequestBase {

    private String location_id  ;

    public String getLocation_id() {
        return location_id;
    }

    public void setLocation_id(String location_id) {
        this.location_id = location_id;
    }
}
