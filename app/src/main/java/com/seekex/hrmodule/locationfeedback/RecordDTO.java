package com.seekex.hrmodule.locationfeedback;


import com.seekx.webService.models.RequestBase;

public class RecordDTO extends RequestBase {

    private String id  ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
