package com.seekx.utils

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.Window
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.seekex.hrmodule.R
import kotlinx.android.synthetic.main.image_view.*
import java.text.SimpleDateFormat
import java.util.*


@Suppress("RECEIVER_NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class ImageUtils {


    companion object {
        fun getSecretKey(): String? {
            var returnStr = ""
            val alphabets = arrayOf(
                "A",
                "B",
                "C",
                "D",
                "E",
                "F",
                "G",
                "H",
                "I",
                "J",
                "K",
                "L",
                "M",
                "N",
                "O",
                "P",
                "Q",
                "R",
                "S",
                "T",
                "U",
                "V",
                "W",
                "X",
                "Y",
                "Z"
            )
            val datebook = Date()
            val pattern = "yyyyMMdd"
            @SuppressLint("SimpleDateFormat") val sdf = SimpleDateFormat(pattern)
            var date = sdf.format(datebook).toInt()
            while (date > 0) {
                val mod = date % alphabets.size
                date = date / alphabets.size
                returnStr = alphabets[mod] + returnStr
            }
            Log.v("getSecretKey", returnStr)
            return returnStr
        }
        @JvmStatic

        fun getLetters(text: String): String {
            return if(text.contains(" ")){
                val splitText=text.split(" ")
                var returnText=""
                returnText= splitText[0][0].toString()+splitText[1][0].toString()
                returnText.toUpperCase()
            }else{
                text[0].toString()+text[1].toString().toUpperCase()
            }

        }

        fun setImageUrl(url: String?, iv_profile: ImageView, context: Context) {

            if(url==null)
                return


            Glide.with(context)
                .load(url)
                .into(iv_profile)
        }


        fun drawableToBitmap(drawable: Drawable): Bitmap? {
            var bitmap: Bitmap? = null
            if (drawable is BitmapDrawable) {
                val bitmapDrawable = drawable
                if (bitmapDrawable.bitmap != null) {
                    return bitmapDrawable.bitmap
                }
            }
            bitmap = if (drawable.intrinsicWidth <= 0 || drawable.intrinsicHeight <= 0) {
                Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888) // Single color bitmap will be created of 1x1 pixel
            } else {
                Bitmap.createBitmap(
                    drawable.intrinsicWidth,
                    drawable.intrinsicHeight,
                    Bitmap.Config.ARGB_8888
                )
            }
            val canvas = Canvas(bitmap)
            drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight())
            drawable.draw(canvas)
            return bitmap
        }



        fun  showImage(url: String, ctx: Context) {
            Log.v("showImage", url)
            val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(R.layout.image_view)

            Glide.with(ctx)
                .load(url)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .into(dialog.tiv)

            dialog.show()
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)

        }

    }

}