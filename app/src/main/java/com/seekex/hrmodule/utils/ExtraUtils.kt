package com.seekx.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.content.res.Resources
import android.media.MediaPlayer
import android.net.ConnectivityManager
import android.provider.Settings
import android.util.Log
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.google.firebase.ktx.Firebase
import com.seekex.hrmodule.R
import org.apache.commons.lang3.StringUtils
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.io.UnsupportedEncodingException
import java.math.RoundingMode
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.ln
import kotlin.math.pow


class ExtraUtils {

    companion object {
      /*  fun String.trace(body: () -> Unit) {
            val myTrace = Firebase.performance.newTrace(this)
            myTrace.start()
            body.invoke()
            myTrace.stop()
        }*/

        fun convertSecondToHHMMString(count: Long): String? {
            val df = SimpleDateFormat("mm:ss")
            df.timeZone = TimeZone.getTimeZone("UTC")
            return df.format(Date(count * 1000L))
        }

        fun openFragment(
            fragmentManager: FragmentManager,
            fragment: Fragment,
            isBack: Boolean
        ) {
            val tx = fragmentManager.beginTransaction()
            if (!isBack) tx.setCustomAnimations(
                R.anim.move_right_in_activity,
                R.anim.move_left_out_activity
            ) else tx.setCustomAnimations(
                R.anim.move_left_in_activity,
                R.anim.move_right_out_activity
            )
            tx.replace(R.id.framcontainer, fragment)
            tx.commit()
        }



        fun navigateWithoutFinish(from: Activity, to: Class<*>) {
            checkAndOpenActivity(from, to, false, false)
        }

        private fun openActivity(from: Activity, to: Class<*>, isBack: Boolean, isFinish: Boolean) {
            val intent = Intent(from, to)
            from.startActivity(intent)
            if (isFinish) {
                if (isBack) from.overridePendingTransition(
                    R.anim.move_left_in_activity,
                    R.anim.move_right_out_activity
                ) else from.overridePendingTransition(
                    R.anim.move_right_in_activity,
                    R.anim.move_left_out_activity
                )
                from.finish()
            } else from.overridePendingTransition(R.anim.activity_up, R.anim.no_change)
        }

        private fun checkAndOpenActivity(
            from: Activity,
            to: Class<*>,
            isBack: Boolean,
            isFinish: Boolean
        ) {
        openActivity(from, to, isBack, isFinish)

            /*Preferences pref=new Preferences(from);
		String simpleName=to.getSimpleName();

		if(Daos.permissionDao.CheckMobilePermission(pref.get(Constants.group_id),simpleName)==null
				&& !AppPermission.activityNames.contains(simpleName))
			new DialogUtil(from).showAlert(from.getString(R.string.permission_error));
		else{
			openActivity(from,to,isBack,isFinish);
		}*/
        }
        fun getRandomNumberString(): String { // It will generate 6 digit random Number.
// from 0 to 999999
            val rnd = Random()
            val number = rnd.nextInt(999999)
            // this will convert any number sequence into 6 character.
            return String.format("%06d", number)
        }
        fun getVal(editText: EditText): String {
            return editText.text.toString()
        }

        @JvmStatic
        fun isYourServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
            val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
            for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.name == service.service.className) {
                    Log.i("getLocation", "Running")
                    return true
                }
            }

            Log.i("getLocation", "Not running")

            return false
        }

        fun roundTwoDecimals(d: Double?): Double? {
            if (d == null)
                return d

            val twoDForm = DecimalFormat("#.##")
            return java.lang.Double.valueOf(twoDForm.format(d))
        }

        fun roundOneDecimals(d: Double): Double {


            val twoDForm = DecimalFormat("#.#")
            return java.lang.Double.valueOf(twoDForm.format(d))
        }    private fun getFormat(count: Double): String {
            if(count<0)
                return "#.###"
            return "#.##"
        }
        fun roundOffDecimals(d: Double): String {
            val df = DecimalFormat("#.##")
            df.roundingMode = RoundingMode.CEILING
            return df.format(d).toString()

        }

        fun getFormatedNumber(count: Double?): String? {
            if (count == null)
                return count
            if (count < 1000)
                return roundOffDecimals(count)

            val exp = (ln(count.toDouble()) / ln(1000.0)).toInt()
            return String.format("%.1f %c", count / 1000.0.pow(exp.toDouble()), "kMGTPE"[exp - 1])
        }

        private val c = charArrayOf('k', 'm', 'b', 't')

        /**
         * Recursive implementation, invokes itself for each factor of a thousand, increasing the class on each invokation.
         * @param n the number to format
         * @param iteration in fact this is the class from the array c
         * @return a String representing the number n formatted in a cool looking way.
         */
         fun coolFormat(n: Double, iteration: Int): String? {
            if (n == null)
                return n
            if (n < 1000){
                return "" + roundTwoDecimals(n)
            }
            var d = n.toDouble() / 100 / 10.0
            d= roundTwoDecimals(d)!!
            val isRound =
                d * 10 % 10 == 0.0
            return (if (d < 1000)
                (if (d > 99.9 ||  d > 9.99)
                    d.toDouble() * 10 / 10 else d.toString() + ""
                        ).toString() + "" + c[iteration] else coolFormat(roundOneDecimals(d), iteration + 1))
        }









        @JvmStatic
        fun isNetworkConnectedMainThread(ctx: Context): Boolean {

            val cm = ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val ni = cm.activeNetworkInfo
            return ni != null
        }

        @SuppressLint("HardwareIds")
        fun getDeviceID(ctx: Context): String {
            // return "123456";
            return Settings.Secure.getString(ctx.contentResolver, Settings.Secure.ANDROID_ID)
        }


        /*   @SuppressLint("PackageManagerGetSignatures")
           fun printHashKey(pContext: Context) {

               try {
                   val info = pContext.packageManager.getPackageInfo(pContext.packageName, PackageManager.GET_SIGNATURES)
                   for (signature in info.signatures) {
                       val md = MessageDigest.getInstance("SHA")
                       md.update(signature.toByteArray())
                       val hashKey = String(Base64.encode(md.digest(), 0))
                       Log.i("printHashKey", "printHashKey() Hash Key: $hashKey")
                   }
               } catch (e: NoSuchAlgorithmException) {
                   Log.e("printHashKey", "printHashKey()", e)
               } catch (e: Exception) {
                   Log.e("printHashKey", "printHashKey()", e)
               }

           }
   */

        @SuppressLint("PackageManagerGetSignatures")
        fun printKeyHash(context: Activity): String? {
            val packageInfo: PackageInfo
            var key: String? = null
            try { //getting application package name, as defined in manifest
                val packageName = context.applicationContext.packageName
                //Retriving package info
                packageInfo = context.packageManager.getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES
                )
                Log.e(
                    "Package Name=",
                    context.applicationContext.packageName
                )
                for (signature in packageInfo.signatures) {
                    val md =
                        MessageDigest.getInstance("SHA")
                    md.update(signature.toByteArray())
                    key = String(android.util.Base64.encode(md.digest(), 0))
                    // String key = new String(Base64.encodeBytes(md.digest()));
                    Log.e("dsfgdsfdsf=", key)
                }
            } catch (e1: PackageManager.NameNotFoundException) {
                Log.e("dsfgdsfdsf", e1.toString())
            } catch (e: NoSuchAlgorithmException) {
                Log.e("dsfgdsfdsf", e.toString())
            } catch (e: Exception) {
                Log.e("dsfgdsfdsf", e.toString())
            }
            return key
        }



        fun capitalizeText(str: String): String? {
            val strList=str.split(" ")

            var capitalizeWord = ""
            for(list in strList){
                if(!capitalizeWord.isEmpty())
                    capitalizeWord+=" "

                capitalizeWord+= StringUtils.capitalize(list)
            }
            return capitalizeWord.trim()
        }
         fun getStringFromRawRes(rawRes: Int,context: Context): String? {
            val inputStream: InputStream
            inputStream = try {
                context.resources.openRawResource(rawRes)
            } catch (e: Resources.NotFoundException) {
                e.printStackTrace()
                return null
            }
            val byteArrayOutputStream = ByteArrayOutputStream()
            val buffer = ByteArray(1024)
            var length: Int
            try {
                while (inputStream.read(buffer).also { length = it } != -1) {
                    byteArrayOutputStream.write(buffer, 0, length)
                }
            } catch (e: IOException) {
                e.printStackTrace()
                return null
            } finally {
                try {
                    inputStream.close()
                    byteArrayOutputStream.close()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
            val resultString: String
            resultString = try {
                byteArrayOutputStream.toString("UTF-8")
            } catch (e: UnsupportedEncodingException) {
                e.printStackTrace()
                return null
            }
            return resultString
        }

        @JvmStatic
        fun changeFragment(fragmentManager: FragmentManager, fragment: Fragment, isBack: Boolean) {
            openFragment(fragmentManager, fragment, isBack)
        }

        fun playASounds(context: Context?, beep: Int) {
            try {
                val mp = MediaPlayer.create(context, beep)
                mp.start()
            } catch (ignore: java.lang.Exception) {
            }
        }

    }

}