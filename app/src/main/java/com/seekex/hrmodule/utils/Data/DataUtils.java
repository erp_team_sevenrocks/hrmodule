 package com.seekex.hrmodule.utils.Data;


import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.EditText;

import com.seekex.hrmodule.R;
import com.seekex.hrmodule.retrofitclasses.models.DashboardModel;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DataUtils {
    public static ArrayList<DashboardModel> getEntranceData(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.rewards),context.getString(R.string.rewards),R.drawable.expence_manager,1));
        dashboardModels.add(new DashboardModel(context.getString(R.string.grievance),context.getString(R.string.grievance),R.drawable.expence_manager,2));
        dashboardModels.add(new DashboardModel(context.getString(R.string.training),context.getString(R.string.managetraining),R.drawable.expence_manager,3));
        dashboardModels.add(new DashboardModel(context.getString(R.string.kra),context.getString(R.string.kra),R.drawable.expence_manager,4));
        dashboardModels.add(new DashboardModel(context.getString(R.string.feedbackpb),context.getString(R.string.feedbackpb),R.drawable.expence_manager,5));
        dashboardModels.add(new DashboardModel(context.getString(R.string.trainingTest),context.getString(R.string.feedbackpb),R.drawable.expence_manager,6));
        dashboardModels.add(new DashboardModel(context.getString(R.string.jobposting),context.getString(R.string.jobposting),R.drawable.expence_manager,7));
        dashboardModels.add(new DashboardModel(context.getString(R.string.sopaudit),context.getString(R.string.sopaudit),R.drawable.expence_manager,8));
        dashboardModels.add(new DashboardModel(context.getString(R.string.criticalevent),context.getString(R.string.criticalevent),R.drawable.expence_manager,9));
        dashboardModels.add(new DashboardModel(context.getString(R.string.newfeedback),context.getString(R.string.newfeedback),R.drawable.expence_manager,10));
        dashboardModels.add(new DashboardModel(context.getString(R.string.remiender),context.getString(R.string.remiender),R.drawable.expence_manager,11));
        dashboardModels.add(new DashboardModel(context.getString(R.string.leave_request),context.getString(R.string.leave_request),R.drawable.expence_manager,12));
        dashboardModels.add(new DashboardModel(context.getString(R.string.my_salary),context.getString(R.string.my_salary),R.drawable.expence_manager,13));
        dashboardModels.add(new DashboardModel(context.getString(R.string.my_atten),context.getString(R.string.my_atten),R.drawable.expence_manager,14));
        dashboardModels.add(new DashboardModel(context.getString(R.string.event_feedback),context.getString(R.string.event_feedback),R.drawable.expence_manager,15));
        return dashboardModels;
    }

    public  static  ArrayList<DashboardModel> getGrievanceData(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.addgrievance),context.getString(R.string.addgrievance),R.drawable.expence_manager,1));
        dashboardModels.add(new DashboardModel(context.getString(R.string.listgrievance),context.getString(R.string.listgrievance),R.drawable.expence_manager,2));
        dashboardModels.add(new DashboardModel(context.getString(R.string.listgrievancemy),context.getString(R.string.listgrievancemy),R.drawable.expence_manager,3));
//        dashboardModels.add(new DashboardModel(context.getString(R.string.listgrievanceassignedme),context.getString(R.string.listgrievanceassignedme),R.drawable.expence_manager,4));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getevent(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();

        dashboardModels.add(new DashboardModel(context.getString(R.string.saveevent),context.getString(R.string.saveevent),R.drawable.expence_manager,1));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getTrainingData(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
//        dashboardModels.add(new DashboardModel(context.getString(R.string.addtmodule),context.getString(R.string.addtmodule),R.drawable.expence_manager,1));
        dashboardModels.add(new DashboardModel(context.getString(R.string.starttraining),context.getString(R.string.starttraining),R.drawable.expence_manager,2));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getSopData(Context context) {
    ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.sopaudit),context.getString(R.string.sopaudit),R.drawable.expence_manager,1));
        return dashboardModels;
}

    public  static  ArrayList<DashboardModel> getKradata(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.kra),context.getString(R.string.kra),R.drawable.expence_manager,1));
        dashboardModels.add(new DashboardModel(context.getString(R.string.kra_list),context.getString(R.string.kra_list),R.drawable.expence_manager,2));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getFeedbackdata(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.addfeedbackpb),context.getString(R.string.addfeedbackpb),R.drawable.expence_manager,1));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getLocFeedback(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.addlocfeedback),context.getString(R.string.addlocfeedback),R.drawable.expence_manager,1));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getremiender(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.remiender),context.getString(R.string.remiender),R.drawable.expence_manager,1));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getleave(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.apply_leave),context.getString(R.string.apply_leave),R.drawable.expence_manager,1));
        dashboardModels.add(new DashboardModel(context.getString(R.string.my_leave),context.getString(R.string.my_leave),R.drawable.expence_manager,2));
        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getSalary(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.my_salary),context.getString(R.string.my_salary),R.drawable.expence_manager,1));

        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getAtten(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.my_atten),context.getString(R.string.my_atten),R.drawable.expence_manager,1));

        return dashboardModels;
    }
    public  static  ArrayList<DashboardModel> getRewardData(Context context) {
        ArrayList<DashboardModel> dashboardModels = new ArrayList<>();
        dashboardModels.add(new DashboardModel(context.getString(R.string.addrewards),context.getString(R.string.addrewards),R.drawable.expence_manager,1));
        dashboardModels.add(new DashboardModel(context.getString(R.string.viewrewards),context.getString(R.string.viewrewards),R.drawable.expence_manager,2));
        dashboardModels.add(new DashboardModel(context.getString(R.string.viewstrike),context.getString(R.string.viewstrike),R.drawable.expence_manager,3));
        return dashboardModels;
    }




    public static boolean isCorrectValue(String value){
        return !value.equalsIgnoreCase("select") && !value.equalsIgnoreCase("");

    }


}
