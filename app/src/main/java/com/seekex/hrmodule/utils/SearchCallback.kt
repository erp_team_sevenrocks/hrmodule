package com.seekex.hrmodule.utils

import androidx.recyclerview.widget.RecyclerView

interface SearchCallback {
    fun onTextTyped(txt:String,searchResultCallback: SearchResultCallback)
    fun onSelected(any: Any?)
//    fun onCallAgain(any: Any?, recyclerView: RecyclerView, userAdapter: UserTaggedAdapter)
}