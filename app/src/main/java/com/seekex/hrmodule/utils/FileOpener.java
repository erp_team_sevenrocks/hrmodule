package com.seekex.hrmodule.utils;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.webkit.MimeTypeMap;

import com.seekex.hrmodule.markrewarks.FileDTO;

public class FileOpener {


    private FileOpener() {
    }

    private static String url;
    private static Uri uri;
    private static Intent intent;

    public static void open(Context context, String fileDTO) {
//        fileDTO.setUrl("https://calibre-ebook.com/downloads/demos/demo.docx");
        setupBase(fileDTO);
        checkFileTypeByUrl();

        try {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static void checkFileTypeByUrl() {

    String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(MimeTypeMap.getFileExtensionFromUrl(url));
    intent.setDataAndType(uri, mimeType);
    }

    private static void setupBase(String fileDTO) {
        url = fileDTO;
        uri = Uri.parse(url);
        intent = new Intent(Intent.ACTION_VIEW, uri);
    }
}