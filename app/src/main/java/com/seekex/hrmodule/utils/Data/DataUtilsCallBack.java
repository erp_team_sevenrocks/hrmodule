package com.seekex.hrmodule.utils.Data;

public interface DataUtilsCallBack {

    void onInsertComplete();
    void onInsertFailed(String msg);
}
