package com.seekx.utils

import android.content.Context
import android.text.Html
import android.util.Log
import androidx.appcompat.app.AlertDialog
import com.seekex.hrmodule.R

import com.seekx.interfaces.DialogeUtilsCallBack


class DialogUtils(val context: Context) {



    fun showAlert(message: String) {
        val alertDialog = AlertDialog.Builder(context, R.style.AlertDialog)

        alertDialog.setTitle(context.getString(R.string.alert))

        alertDialog.setMessage(message)


        alertDialog.setCancelable(false)

        alertDialog.setPositiveButton(
            Html.fromHtml("<font color='#FF7F27'>OK</font>")
        ) { dialog, which -> dialog.dismiss() }
        alertDialog.show()

    }

    fun showCustomAlert(
        message: String,
        titile: String,
        okText: String,
        dialogeUtilsCallBack: DialogeUtilsCallBack
    ) {
        val alertDialog = AlertDialog.Builder(context, R.style.AlertDialog)

        alertDialog.setTitle(titile)

        alertDialog.setMessage(message)

        alertDialog.setPositiveButton(
            okText
        ) { dialog, which ->
            run {
                dialog.dismiss()
                dialogeUtilsCallBack.onDoneClick(true)
            }
        }

        alertDialog.setNegativeButton(context.getString(R.string.cancel))
        { dialog, _ ->
            run {
                dialogeUtilsCallBack.onDoneClick(false)
                dialog.dismiss()
            }
        }
        alertDialog.show()
    }

    fun showOkAlertWithCallBack(message: String, dialogeUtilsCallBack: DialogeUtilsCallBack) {
        val alertDialog = AlertDialog.Builder(context, R.style.AlertDialog)

        alertDialog.setTitle(context.getString(R.string.alert))

        alertDialog.setMessage(message)

        alertDialog.setCancelable(false)

        alertDialog.setPositiveButton(
            context.getString(R.string.ok)
        ) { dialog, which ->
            run {
                Log.e("showOkAlertWithCallBack", ": dismiss")
                dialog.dismiss()
                dialogeUtilsCallBack.onDoneClick(true)
            }
        }
        alertDialog.show()
    }
    fun showAlertWithCallBack(
        message: String,
        okText: String,
        dialogeUtilsCallBack: DialogeUtilsCallBack
    ) {
        val alertDialog = AlertDialog.Builder(context, R.style.AlertDialog)

        alertDialog.setTitle(context.getString(R.string.alert))

        alertDialog.setMessage(message)

        alertDialog.setCancelable(false)

        alertDialog.setNegativeButton(context.getString(R.string.cancel)) { dialog, _ ->
            run {
                dialogeUtilsCallBack.onDoneClick(false)
                dialog.dismiss()
            }
        }

        alertDialog.setPositiveButton(okText)
        { dialog, _ ->
            run {
                dialogeUtilsCallBack.onDoneClick(true)
                dialog.dismiss()
            }
        }

        alertDialog.show()
    }

    fun showAlertWithCallBack(
        message: String,
        okText: String,
        cancelTExt: String,
        dialogeUtilsCallBack: DialogeUtilsCallBack
    ) {
        val alertDialog = AlertDialog.Builder(context, R.style.AlertDialog)

        alertDialog.setTitle(context.getString(R.string.alert))

        alertDialog.setMessage(message)

        alertDialog.setCancelable(false)

        alertDialog.setNegativeButton(cancelTExt) { dialog, _ ->
            run {
                dialogeUtilsCallBack.onDoneClick(false)
                dialog.dismiss()
            }
        }

        alertDialog.setPositiveButton(okText)
        { dialog, _ ->
            run {
                dialogeUtilsCallBack.onDoneClick(true)
                dialog.dismiss()
            }
        }

        alertDialog.show()
    }
    fun showAlertWithCallBackCancellable(
        message: String,
        okText: String,
        cancelTExt: String,
        dialogeUtilsCallBack: DialogeUtilsCallBack
    ) {
        val alertDialog = AlertDialog.Builder(context, R.style.AlertDialog)

        alertDialog.setTitle(context.getString(R.string.alert))

        alertDialog.setMessage(message)

        alertDialog.setNegativeButton(cancelTExt) { dialog, _ ->
            run {
                dialogeUtilsCallBack.onDoneClick(false)
                dialog.dismiss()
            }
        }

        alertDialog.setPositiveButton(okText)
        { dialog, _ ->
            run {
                dialogeUtilsCallBack.onDoneClick(true)
                dialog.dismiss()
            }
        }

        alertDialog.show()
    }
}


