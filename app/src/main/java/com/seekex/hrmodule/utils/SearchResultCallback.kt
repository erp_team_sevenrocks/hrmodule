package com.seekex.hrmodule.utils

interface SearchResultCallback {
    fun onSearchResult(any: Any)
}