package com.seekx.utils

import android.R
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.Dialog
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.LinearLayout
import androidx.core.content.ContextCompat.getSystemService
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import com.seekex.hrmodule.adapter.DropdownListingAdapter
import com.seekex.hrmodule.adapter.DropdownListingLocAdapter
import com.seekex.hrmodule.adapter.DropdownListingUserAdapter
import com.seekex.hrmodule.databinding.MasterDataViewBinding
import com.seekex.hrmodule.databinding.SearchViewBinding
import com.seekex.hrmodule.markrewarks.DropdownModel
import com.seekex.hrmodule.markrewarks.ParicipantModel
import com.seekex.hrmodule.markrewarks.SelectedMasterCallback
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.AdapterListener
import com.seekx.interfaces.DialogeUtilsCallBack
import kotlinx.android.synthetic.main.multi_select_dialoge.*
import kotlinx.android.synthetic.main.multi_select_dialoge.et_serach
import kotlinx.android.synthetic.main.multi_select_dialoge.tv_header
import kotlinx.android.synthetic.main.multi_select_dialoge.tv_ok
import kotlinx.android.synthetic.main.multi_select_dialoge_new.*
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class DataUtils {
    val selectedName = ArrayList<String>()

    companion object {

        private fun twoDigitString(number: Long): String? {
            if (number == 0L) {
                return "00"
            }
            return if (number / 10 == 0L) {
                "0$number"
            } else number.toString()
        }

        fun formatMilliSecondsToTime(milliseconds: Long): String? {
            val seconds = (milliseconds / 1000).toInt() % 60
            val minutes = (milliseconds / (1000 * 60) % 60).toInt()
            val hours = (milliseconds / (1000 * 60 * 60) % 24).toInt()
            return (twoDigitString(hours.toLong()).toString() + " : " + twoDigitString(minutes.toLong()) + " : "
                    + twoDigitString(seconds.toLong()))
        }

        @SuppressLint("PrivateResource")
        fun openSearchDialogeMultiSelect(
            context: Context,
            selectedList: ArrayList<DropdownModel>?,
            dataList: ArrayList<DropdownModel>?,
            header: String,
            searchCallback: SearchCallback
        ) {

            val dialog = Dialog(context, android.R.style.Theme_Material_Dialog_MinWidth)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(com.seekex.hrmodule.R.layout.multi_select_dialoge_new)

            dialog.tv_header.text = header

            if (selectedList != null)


                setSelectionAdapter(
                    context,
                    dialog.lld_yanamicnew,
                    selectedList,
                    object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            searchCallback.onSelected(any)
                        }
                    })


            dialog.tv_header.text = header
            val userAdapter = DropdownListingAdapter(
                context,
                header,
                object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                        val masterGodown: DropdownModel = any1 as DropdownModel
                        if (selectedList == null) {
                            dialog.dismiss()
                            searchCallback.onSelected(masterGodown)
                        } else {
                            if (!selectedName.contains(masterGodown.name)) {
                                Log.e("checkskills", "55: " + selectedList?.size)
                                selectedList.add(0, masterGodown)
                                setSelectionAdapter(context,
                                    dialog.lld_yanamicnew,
                                    selectedList,
                                    object : SelectedMasterCallback {
                                        override fun onSelected(any: Any) {
                                            searchCallback.onSelected(any)
                                        }
                                    })
                            }
                        }


                    }

                })

            dialog.rec_usersssss.layoutManager = LinearLayoutManager(context)
//     dialog.recyclerview.setHasFixedSize(true);
            dialog.rec_usersssss.adapter = userAdapter

            userAdapter.setDataValues(dataList!!)
            userAdapter.notifyDataSetChanged()

            dialog.et_serach.addTextChangedListener {


                val temp = ArrayList<DropdownModel>()
                for (d in dataList!!) {

                    if (d.name.toLowerCase().contains(it!!.toString().toLowerCase())) {
                        temp.add(d)
                    }
                }

                userAdapter.setDataValues(temp)
                userAdapter.notifyDataSetChanged()

            }

            dialog.et_serach.setText("")

            dialog.tv_ok.setOnClickListener {
                searchCallback.onSelected(selectedList)
                dialog.dismiss()
            }

            if (dataList == null)
                dialog.tv_ok.visibility = View.GONE



            dialog.show()
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)
        }

        private fun checkForVisibility(
            selectedList: ArrayList<DropdownModel>,
            llDynamic: LinearLayout
        ) {
            if (selectedList.size > 0) {
                llDynamic.visibility = View.VISIBLE
            } else {
                llDynamic.visibility = View.GONE
            }
        }

        val selectedName = ArrayList<String>()

        fun setSelectionAdapter(
            context: Context,
            llDynamic: LinearLayout,
            selectedList: ArrayList<DropdownModel>,
            param: SelectedMasterCallback?
        ) {
            val dialogUtils = DialogUtils(context)

            selectedName.clear()
            checkForVisibility(selectedList, llDynamic)

            llDynamic.removeAllViews()
            for (model in 0 until selectedList.size) {

                val inflater =
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val binding = MasterDataViewBinding.inflate(inflater, llDynamic, true)
                val masterDTO = selectedList[model]
                Log.e("checkskill", "setSelectionAdapter: " + masterDTO.name)
                binding.model = masterDTO
                selectedName.add(masterDTO.name!!)
                if (param == null)
                    binding.ivCut.visibility = View.GONE

                binding.ivCut.setOnClickListener {
                    dialogUtils.showAlertWithCallBack(
                        "Do you want to delete ?",
                        "Delete",
                        object : DialogeUtilsCallBack {
                            override fun onDoneClick(clickStatus: Boolean) {
                                if (clickStatus) {
                                    selectedList.remove(masterDTO)
                                    llDynamic.removeView(binding.root)
                                    param?.onSelected(selectedList)
                                    checkForVisibility(selectedList, llDynamic)
                                }
                            }

                        }
                    )
                }

            }
        }

        fun openSearchDialogeuser(
            context: Context,
            dataList: ArrayList<DropdownModel>?,
            header: String,
            searchCallback: SearchCallback
        ) {
            Log.e("TAG", "openSearchDialoge: " + dataList!!.size)
            val dialog = Dialog(context, R.style.Theme_Material_Dialog_MinWidth)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(com.seekex.hrmodule.R.layout.multi_select_dialoge)

            dialog.tv_header.text = header
            val userAdapter = DropdownListingUserAdapter(
                context,
                header,
                object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                        val masterGodown: DropdownModel = any1 as DropdownModel
                        dialog.dismiss()
                        searchCallback.onSelected(masterGodown)

                    }

                })

            dialog.rec_usersss.layoutManager = LinearLayoutManager(context)
//     dialog.recyclerview.setHasFixedSize(true);
            dialog.rec_usersss.adapter = userAdapter

            userAdapter.setDataValues(dataList)
            userAdapter.notifyDataSetChanged()

            dialog.et_serach.addTextChangedListener {


                val temp = ArrayList<DropdownModel>()
                for (d in dataList) {

                    if (d.name.toLowerCase().contains(it!!.toString().toLowerCase())) {
                        temp.add(d)
                    }
                }

                userAdapter.setDataValues(temp)
                userAdapter.notifyDataSetChanged()

            }

            dialog.et_serach.setText("")

            dialog.tv_ok.setOnClickListener {
//                searchCallback.onSelected(dataList)
                dialog.dismiss()
            }

            if (dataList == null)
                dialog.tv_ok.visibility = View.GONE



            dialog.show()
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)

        }
        fun openSearchDialoge(
            context: Context,
            dataList: ArrayList<DropdownModel>?,
            header: String,
            searchCallback: SearchCallback
        ) {
            Log.e("TAG", "openSearchDialoge: " + dataList!!.size)
            val dialog = Dialog(context, R.style.Theme_Material_Dialog_MinWidth)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(com.seekex.hrmodule.R.layout.multi_select_dialoge)
            dialog.tv_header.text = header
            val userAdapter = DropdownListingAdapter(
                context,
                header,
                object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                        val masterGodown: DropdownModel = any1 as DropdownModel
                        dialog.dismiss()
                        searchCallback.onSelected(masterGodown)

                    }

                })

            dialog.rec_usersss.layoutManager = LinearLayoutManager(context)
//     dialog.recyclerview.setHasFixedSize(true);
            dialog.rec_usersss.adapter = userAdapter

            userAdapter.setDataValues(dataList)
            userAdapter.notifyDataSetChanged()

            dialog.et_serach.addTextChangedListener {


                val temp = ArrayList<DropdownModel>()
                for (d in dataList) {

                    if (d.name.toLowerCase().contains(it!!.toString().toLowerCase())) {
                        temp.add(d)
                    }
                }

                userAdapter.setDataValues(temp)
                userAdapter.notifyDataSetChanged()

            }

            dialog.et_serach.setText("")

            dialog.tv_ok.setOnClickListener {
//                searchCallback.onSelected(dataList)
                dialog.dismiss()
            }

            if (dataList == null)
                dialog.tv_ok.visibility = View.GONE



            dialog.show()
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)

        }
        fun openSearchDialogee(
            context: Context,
            dataList: ArrayList<DropdownModel>?,
            header: String,
            searchCallback: SearchCallback
        ) {
            Log.e("TAG", "openSearchDialoge: " + dataList!!.size)
            val dialog = Dialog(context, R.style.Theme_Material_Dialog_MinWidth)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setContentView(com.seekex.hrmodule.R.layout.multi_select_dialoge)

            dialog.tv_header.text = header
            val userAdapter = DropdownListingLocAdapter(
                context,
                header,
                object : AdapterListener {
                    override fun onSelection(any1: Any?, any2: Any?, i: Int) {
                        val masterGodown: DropdownModel = any1 as DropdownModel
                        dialog.dismiss()
                        searchCallback.onSelected(masterGodown)

                    }

                })

            dialog.rec_usersss.layoutManager = LinearLayoutManager(context)
//     dialog.recyclerview.setHasFixedSize(true);
            dialog.rec_usersss.adapter = userAdapter

            userAdapter.setDataValues(dataList)
            userAdapter.notifyDataSetChanged()

            dialog.et_serach.addTextChangedListener {


                val temp = ArrayList<DropdownModel>()
                for (d in dataList) {

                    if (d.name.toLowerCase().contains(it!!.toString().toLowerCase())) {
                        temp.add(d)
                    }
                }

                userAdapter.setDataValues(temp)
                userAdapter.notifyDataSetChanged()

            }

            dialog.et_serach.setText("")

            dialog.tv_ok.setOnClickListener {
//                searchCallback.onSelected(dataList)
                dialog.dismiss()
            }

            if (dataList == null)
                dialog.tv_ok.visibility = View.GONE



            dialog.show()
            dialog.setCanceledOnTouchOutside(true)
            dialog.setCancelable(true)

        }

        fun attachDynamicView(
            context: Context,
            ll_view: LinearLayout,
            llDynamic: LinearLayout,
            internalList: List<DropdownModel>,
            searchCallback: SearchCallback,
            dialog: Dialog,
            dataList: MutableList<DropdownModel>?
        ) {
            ll_view.removeAllViews()

            for (element in dataList!!) {
                val inflater =
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                val binding = SearchViewBinding.inflate(inflater, ll_view, true)
                binding.model = element
                Log.e("checkskills", "11: " + element.name)
                Log.e("checkskills", "22: " + dataList?.size)
                binding.rrView.setOnClickListener {

                    if (dataList == null) {
                        Log.e("checkskills", "33: " + dataList?.size)
                        dialog.dismiss()
                        searchCallback.onSelected(element)
                    } else {

                        dataList.add(0, element)

                    }


                }
            }
        }


        fun getCurrentDate(): String {
            val sdf = SimpleDateFormat("yyyy-MM-dd")
            val currentDate = sdf.format(Date())
            System.out.println(" C DATE is  " + currentDate)
            return currentDate
        }
        fun getCurrentDateformat(): String {
            val sdf = SimpleDateFormat("dd-MM-yyyy")
            val currentDate = sdf.format(Date())
            System.out.println(" C DATE is  " + currentDate)
            return currentDate
        }
        fun getYesterdayDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -1)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getDaysAgo(daysAgo: Int): Date {
            val calendar = Calendar.getInstance()
            calendar.add(Calendar.DAY_OF_YEAR, -daysAgo)

            return calendar.time
        }

        fun getthreeDaysDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -3)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getsevenDaysDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -7)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getLastThirtyDaysStartDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.DATE, -30)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getLastMonthStartDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()
            cal.add(Calendar.MONTH, -1)
            cal.set(Calendar.DAY_OF_MONTH, 1)
            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getLastMonthLastDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()

            cal.set(Calendar.DAY_OF_MONTH, 1)
            cal.add(Calendar.DAY_OF_MONTH, -1)

            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getCurrentMonthFirstDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()

            cal.set(Calendar.DAY_OF_MONTH, 1)

            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        fun getCurrentMonthLastDate(): String {
            val dateFormat: DateFormat = SimpleDateFormat("yyyy-MM-dd")
            val cal = Calendar.getInstance()

            cal.set(Calendar.DAY_OF_MONTH, 1)
            cal.add(Calendar.MONTH, 1)
            cal.add(Calendar.DAY_OF_MONTH, -1)

            val yesDate = dateFormat.format(cal.time) //your formatted date here
            return yesDate
        }

        public fun isMyServiceRunning(context: Context, serviceClass: Class<*>): Boolean {
            val manager: ActivityManager =
                getSystemService(context, serviceClass) as ActivityManager
            for (service in manager.getRunningServices(Int.MAX_VALUE)) {
                if (serviceClass.name == service.service.getClassName()) {
                    Log.i("isMyServiceRunning?", true.toString() + "")
                    return true
                }
            }
            Log.i("isMyServiceRunning?", false.toString() + "")
            return false
        }


    }

}
