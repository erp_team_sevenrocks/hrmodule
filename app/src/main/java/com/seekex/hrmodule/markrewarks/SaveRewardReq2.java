package com.seekex.hrmodule.markrewarks;

import com.seekex.hrmodule.grievance.ImagesGRDTO;
import com.seekx.webService.models.RequestBase;

import java.util.ArrayList;

public class SaveRewardReq2 extends RequestBase {

    private String   point_type_id;
    private String   assign_to_user_id;
    private String   reason;
    private String   name;
    private String   img_file;
    private String   xl;
    private String   audio;
    private ArrayList<RewardUserSaveReq> kra;
    private ArrayList<SaveRewardpart> parameter;
    private ArrayList<ImagesGRDTO> media;

    public ArrayList<SaveRewardpart> getParameter() {
        return parameter;
    }

    public void setParameter(ArrayList<SaveRewardpart> parameter) {
        this.parameter = parameter;
    }

    public ArrayList<ImagesGRDTO> getMedia() {
        return media;
    }

    public void setMedia(ArrayList<ImagesGRDTO> media) {
        this.media = media;
    }

    public ArrayList<RewardUserSaveReq> getKra() {
        return kra;
    }

    public void setKra(ArrayList<RewardUserSaveReq> kra) {
        this.kra = kra;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg_file() {
        return img_file;
    }

    public void setImg_file(String img_file) {
        this.img_file = img_file;
    }

    public String getXl() {
        return xl;
    }

    public void setXl(String xl) {
        this.xl = xl;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }



    public String getPoint_type_id() {
        return point_type_id;
    }

    public void setPoint_type_id(String point_type_id) {
        this.point_type_id = point_type_id;
    }

    public String getAssign_to_user_id() {
        return assign_to_user_id;
    }

    public void setAssign_to_user_id(String assign_to_user_id) {
        this.assign_to_user_id = assign_to_user_id;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }
}
