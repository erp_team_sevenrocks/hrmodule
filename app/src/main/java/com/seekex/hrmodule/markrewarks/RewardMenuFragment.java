package com.seekex.hrmodule.markrewarks;
/**
 * Created by Mohammad aqil on 1/12/2016.
 */

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;


import com.seekex.hrmodule.R;
import com.seekex.hrmodule.adapter.ChooseAdapter;
import com.seekex.hrmodule.custombinding.ExpandableHeightGridView;
import com.seekex.hrmodule.utils.Data.DataUtils;
import com.seekx.utils.ExtraUtils;
import com.sevenrocks.taskapp.appModules.markrewarks.AddRewards;
import com.sevenrocks.taskapp.appModules.vendorreg.RewardsList;
import com.sevenrocks.taskapp.appModules.vendorreg.StrikesList;

import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;


public class RewardMenuFragment extends Fragment {

    @BindView(R.id.gridview_menu)
    ExpandableHeightGridView grid_views;

    @BindView(R.id.txtheaderre)
    TextView tvHeader;
    private RewardsActivity rewardsActivity;

    public RewardMenuFragment() {
        // Required empty public constructor
    }

    public static Fragment getInstance() {
        return new RewardMenuFragment();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup BasicActi, Bundle savedInstanceState) {

        // Inflate the layout for getActivity() fragment
        View rootView = inflater.inflate(R.layout.choose_activty, BasicActi, false);
        ButterKnife.bind(this, rootView);

        init(rootView);
        setListener(rootView);
        tvHeader.setText("Choose Options");
        return rootView;

    }


    private void init(View rootView) {

        rewardsActivity = ((RewardsActivity) getActivity());
        setAapter(rootView);
    }


    private void setListener(View rootView) {


    }


    private void setAapter(View rootView) {
        ChooseAdapter chooseAdapter = new ChooseAdapter(getContext(), DataUtils.getRewardData(Objects.requireNonNull(getActivity()))) {

            @Override
            protected void onViewClicked(View v, String s) {
                switch (s) {

                    case "1":
                        ExtraUtils.changeFragment(rewardsActivity.getSupportFragmentManager(), AddRewards.getInstance(), true);
                        break;
                    case "2":
                        ExtraUtils.changeFragment(rewardsActivity.getSupportFragmentManager(), RewardsList.getInstance(), true);
                        break;
                    case "3":
                        ExtraUtils.changeFragment(rewardsActivity.getSupportFragmentManager(), StrikesList.getInstance(), true);
                        break;
                }
            }
        };

        grid_views.setAdapter(chooseAdapter);
        grid_views.setExpanded(true);
    }


}
