
package com.seekex.hrmodule.markrewarks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class RewardListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<RewardListDTO> data;

    public ArrayList<RewardListDTO> getData() {
        return data;
    }

    public void setData(ArrayList<RewardListDTO> data) {
        this.data = data;
    }


}

