
package com.seekex.hrmodule.markrewarks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class RewardSaveResponse extends BaseResponse {

    @SerializedName("rewid")
    private String rewid;

    public String getRewid() {
        return rewid;
    }

    public void setRewid(String rewid) {
        this.rewid = rewid;
    }
}

