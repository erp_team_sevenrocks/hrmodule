package com.seekex.hrmodule.markrewarks;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class DropdownModel {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    @SerializedName("next")
    private Boolean next;

    public DropdownModel(String id, String name, Boolean next) {
        this.id = id;
        this.name = name;
        this.next = next;
    }

    public DropdownModel(String id) {
        this.id = id;
    }
    public DropdownModel() {
        this.id = id;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DropdownModel that = (DropdownModel) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Boolean getNext() {
        return next;
    }

    public void setNext(Boolean next) {
        this.next = next;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }
    public String getNameId() {
        return name+" - "+id;
    }

    public void setName(String name) {
        this.name = name;
    }
}