package com.seekex.hrmodule.markrewarks;

public interface OnDownloadListener {

    void onDownloadComplete();

    void onError(Error error);

}