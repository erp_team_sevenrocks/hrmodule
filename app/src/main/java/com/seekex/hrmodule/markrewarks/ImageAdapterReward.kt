package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.core.content.ContextCompat.getSystemService
import androidx.recyclerview.widget.RecyclerView
import com.downloader.Error
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.databinding.ImagelistitemRewardBinding
import com.seekex.hrmodule.grievance.ImagesDTORES
import com.seekex.hrmodule.markrewarks.RewardsActivity
import com.seekex.hrmodule.utils.FileOpener
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.BufferedInputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.net.URL
import java.net.URLConnection


class ImageAdapterReward(
    private val context: RewardsActivity?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<ImageAdapterReward.ViewHolder>() {
    private val rewardsActivity: RewardsActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    var manager: DownloadManager? = null

    private var items: ArrayList<ImagesDTORES> = ArrayList()

    fun setDataValues(items: ArrayList<ImagesDTORES>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        rewardsActivity = context as RewardsActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ImagelistitemRewardBinding.inflate(inflater)

        binding.txturl.setOnClickListener {

            if (binding?.model?.type.equals("audio")) {
                openAudioPlayer(ApiUtils.DOMAIN + binding?.model?.url!!)
            } else {
//                initializeDownloader(ApiUtils.DOMAIN + binding?.model?.url!!)
//                Log.e("TAG", "onCreateViewHolder: " + ApiUtils.DOMAIN + binding?.model?.url!!)
//                manager = context!!.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
//                val uri =
//                    Uri.parse(ApiUtils.DOMAIN + binding?.model?.url!!)
//                val request = DownloadManager.Request(uri)
//                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE)
//                val reference = manager!!.enqueue(request)
                FileOpener.open(context, ApiUtils.DOMAIN + binding?.model?.url!!);
            }
        }

        return ViewHolder(binding)
    }

    fun getFileNameFromUrl(url: String): String {
        return url.substring(url.lastIndexOf("/") + 1)
    }

    fun downloadFile(path: String?): Boolean {
        try {
            val apkName = getFileNameFromUrl(path!!)
            val url = URL(path)
            val ucon: URLConnection = url.openConnection()
            ucon.setReadTimeout(5000)
            ucon.setConnectTimeout(10000)
            val `is`: InputStream = ucon.getInputStream()
            val inStream = BufferedInputStream(`is`, 1024 * 5)
            val file: File =
                File(context!!.getDir("filesdir", Context.MODE_PRIVATE).toString() + "/" + apkName)
            if (file.exists()) {
                file.delete()
            }
            file.createNewFile()
            Log.e("TAG", "downloadFile:" + file.path)

            val outStream = FileOutputStream(file)
            val buff = ByteArray(5 * 1024)
            var len: Int
            while (inStream.read(buff).also { len = it } != -1) {
                outStream.write(buff, 0, len)
            }
            outStream.flush()
            outStream.close()
            inStream.close()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
            return false
        }
        return true
    }

    fun openDocument(paths: String, context: Context): Boolean {
        Log.e("FileType", "pdf hai")
        try {
            val file = File(paths)
            if (!file.exists())
                return false
            Log.e("FileType", "pdf hai" + paths)
            val uri = Uri.fromFile(file)
            var type = "*/*"

            // Check what kind of file you are trying to open, by comparing the paths with extensions.
            // When the if condition is matched, plugin sets the correct intent (mime) masterType,
            // so Android knew what application to use to open the file
            if (paths.contains(".doc") || paths.contains(".docx")) {
                // Word document
                type = "application/msword"
            } else if (paths.contains(".pdf")) {
                // PDF file
                type = "application/pdf"
            } else if (paths.contains(".ppt") || paths.contains(".pptx")) {
                // Powerpoint file
                type = "application/vnd.ms-powerpoint"
            } else if (paths.contains(".xls") || paths.contains(".xlsx")) {
                // Excel file
                type = "application/vnd.ms-excel"
            } else if (paths.contains(".zip") || paths.contains(".rar")) {
                // WAV audio file
                type = "application/x-wav"
            } else if (paths.contains(".rtf")) {
                // RTF file
                type = "application/rtf"
            } else if (paths.contains(".wav") || paths.contains(".mp3")) {
                // WAV audio file
                type = "audio/x-wav"
            } else if (paths.contains(".gif")) {
                // GIF file
                type = "image/gif"
            } else if (paths.contains(".jpg") || paths.contains(".jpeg") || paths.contains(".png")) {
                // JPG file
                type = "image/jpeg"
            } else if (paths.contains(".txt")) {
                // Text file
                type = "text/plain"
            } else if (paths.contains(".3gp") || paths.contains(".mpg") || paths.contains(".mpeg") || paths.contains(
                    ".mpe"
                ) || paths.contains(".mp4") || paths.contains(".avi")
            ) {
                // Video files
                type = "video/*"
            }

            Log.e("FileType", type)
            Log.v("FileType", paths)
            val intent = Intent(Intent.ACTION_VIEW)
            intent.setDataAndType(uri, "application/pdf")
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            context.startActivity(intent)
        } catch (e: Exception) {
            Log.v("openDocument", e.toString())
            return false
        }
        return true
    }

    fun initializeDownloader(resume: String) {
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()

        val config = PRDownloaderConfig.newBuilder()
            .setDatabaseEnabled(true)
            .build()
        val extBaseDir = context?.externalCacheDir

        val file = File(extBaseDir!!.absolutePath + "/Rewarddoc/" + "")
        PRDownloader.initialize(rewardsActivity, config)

        val apkName = getFileNameFromUrl(resume)

        val apkPath = file.absolutePath//AppConstants.downloadDir

        val returnString = "$apkPath/$apkName"
        Log.e("openDocument", returnString)

        if (openDocument(returnString, rewardsActivity!!)) {
            return
        }

        PRDownloader.download(
            resume,
            apkPath, apkName
        )
            .build()
            .setOnStartOrResumeListener {


            }
            .setOnPauseListener { }
            .setOnCancelListener { }
            .setOnProgressListener { progress ->

            }
            .start(object : OnDownloadListener {
                override fun onDownloadComplete() {
                    Log.e("TAG", "onDownloadComplete: ")
                    openDocument(returnString, rewardsActivity)

                }

                override fun onError(error: Error) {

                }

            })
    }

    private fun openAudioPlayer(toString: String) {
        var isPb = true
        var isPaused = false
        val handler = Handler()
        var mediaPlayer = MediaPlayer()
        val dialog = Dialog(rewardsActivity!!, R.style.Theme_Material_Dialog_MinWidth)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(com.seekex.hrmodule.R.layout.play_audio)

        dialog.setCancelable(true)
        dialog.setOnDismissListener {
            dialog.dismiss()
        }
        mediaPlayer.setOnBufferingUpdateListener { mp, percent ->

            dialog.seekbar.secondaryProgress = percent
        }

        mediaPlayer.setOnCompletionListener {
            isPaused = false
            dialog.seekbar.progress = 100
            Log.v("isPlaying", "complete")
            dialog.dismiss()
        }
        Handler().postDelayed(Runnable {
            dialog.pb_audio.visibility = View.INVISIBLE
        }, 3000)

        dialog.btn_play.setOnClickListener {

            if (isPb) {
                dialog.pb_audio.visibility = View.VISIBLE
            }
            isPb = false
            player = mediaPlayer
            if (!isPaused) {
                count = 0
            }

            dialog.seekbar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {

                override fun onStopTrackingTouch(seekBar: SeekBar) {

                }


                override fun onStartTrackingTouch(seekBar: SeekBar) {

                }

                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    if (fromUser) {
                        mediaPlayer.seekTo(progress * 1000)
                    }
                }
            })


            /** ImageButton onClick event handler. Method which start/pause mediaplayer playing */
            try {
                mediaPlayer.setDataSource(toString) // setup song from http://www.hrupin.com/wp-content/uploads/mp3/testsong_20_sec.mp3 URL to mediaplayer data source
                mediaPlayer.prepare()
                mediaPlayer.prepareAsync()
                // you must call this method after setup the datasource in setDataSource method. After calling prepare() the instance of MediaPlayer starts load data from URL to internal buffer.
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (!mediaPlayer.isPlaying) {
                mediaPlayer.start()
                dialog.seekbar.max = mediaPlayer.duration / 1000

            } else {

                mediaPlayer.pause()
            }

            updateSeekbar(dialog, handler, mediaPlayer)
        }
        dialog.btn_pause.setOnClickListener {
            isPaused = true
            mediaPlayer.pause()
        }

        dialog.setOnDismissListener {
            count = 0
            if (mediaPlayer.isPlaying || player != null) {
                mediaPlayer.stop()
                player?.stop()
            }


        }
        dialog.show()


    }

    private fun updateSeekbar(
        binding: Dialog,
        handler: Handler,
        mediaPlayer: MediaPlayer
    ) {


        val notification = Runnable {
            val mCurrentPosition = mediaPlayer.currentPosition / 1000
            binding.seekbar.progress = mCurrentPosition

//            val total =
//                binding.seekbar.max.toString()
//            val played = TimeUtils.convertSecondToHHMMString(count)
//
//            Log.e("totalplayed", "updateSeekbar: "+total+" -- "+played )
//            val displayDuration = "$played / $total"
//
//            ++count
//
//            binding.durationtxt.text = displayDuration

//            if (total == played) {
//                return@Runnable
//            }

            updateSeekbar(binding, handler, mediaPlayer)
        }

        handler.postDelayed(notification, 1000)


    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: ImagelistitemRewardBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: ImagesDTORES) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}