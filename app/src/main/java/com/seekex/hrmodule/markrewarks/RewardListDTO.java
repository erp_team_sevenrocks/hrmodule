
package com.seekex.hrmodule.markrewarks;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.grievance.ImagesDTORES;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class RewardListDTO extends BaseResponse {

    @SerializedName("id")
    private String id;
    @SerializedName("assign_to_user_id")
    private String assign_to_user_id;
    @SerializedName("reward_point_type_id")
    private String reward_point_type_id;
    @SerializedName("reason")
    private String reason;
    @SerializedName("photo")
    private String photo;
    @SerializedName("excel_file")
    private String excel_file;
    @SerializedName("audio_file")
    private String audio_file;
    @SerializedName("point_type")
    private String point_type;
    @SerializedName("assign_to")
    private String assign_to;
    @SerializedName("Kra")
    private String Kra;
    @SerializedName("AssessmentParameter")
    private String AssessmentParameter;

    @SerializedName("MediaFile")
    private ArrayList<ImagesDTORES> MediaFile;

    public String getKra() {
        return Kra;
    }

    public void setKra(String kra) {
        Kra = kra;
    }

    public String getAssessmentParameter() {
        return AssessmentParameter;
    }

    public void setAssessmentParameter(String assessmentParameter) {
        AssessmentParameter = assessmentParameter;
    }

    public ArrayList<ImagesDTORES> getMediaFile() {
        return MediaFile;
    }

    public void setMediaFile(ArrayList<ImagesDTORES> mediaFile) {
        MediaFile = mediaFile;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAssign_to_user_id() {
        return assign_to_user_id;
    }

    public void setAssign_to_user_id(String assign_to_user_id) {
        this.assign_to_user_id = assign_to_user_id;
    }

    public String getReward_point_type_id() {
        return reward_point_type_id;
    }

    public void setReward_point_type_id(String reward_point_type_id) {
        this.reward_point_type_id = reward_point_type_id;
    }

    public String getReasonTxt() {
        return "Reason: " + reason;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
    public String getExcel_filetxt()
    {
        return "Excel file: "+excel_file;
    }
    public String getKratxt()
    {
        return "Kra : "+Kra;
    }
    public String getParamtext()
    {
        return "Parametre : "+AssessmentParameter;
    }

    public String getExcel_file() {
        return excel_file;
    }

    public void setExcel_file(String excel_file) {
        this.excel_file = excel_file;
    }


    public String getAudio_filetxt() {
        return "Audio File: "+audio_file;
    }
    public Boolean checkIsAudio() {
        if (audio_file.length()>0){
            return true;
        }else{
            return false;
        }
    }
    public Boolean checkIsExcel() {
        if (excel_file.length()>0){
            return true;
        }else{
            return false;
        }
    }
    public Boolean checkIsImage() {
        if (photo.length()>0){
            return true;
        }else{
            return false;
        }
    }
    public String getAudio_file() {
        return audio_file;
    }

    public void setAudio_file(String audio_file) {
        this.audio_file = audio_file;
    }

    public String getPoint_typeTxt() {
        return "Point Type: " + point_type;
    }

    public String getPoint_type() {
        return point_type;
    }

    public void setPoint_type(String point_type) {
        this.point_type = point_type;
    }

    public String getAssign_toTxt() {
        return "Assign to: " + assign_to;
    }

    public String getAssign_to() {
        return assign_to;
    }

    public void setAssign_to(String assign_to) {
        this.assign_to = assign_to;
    }
}
