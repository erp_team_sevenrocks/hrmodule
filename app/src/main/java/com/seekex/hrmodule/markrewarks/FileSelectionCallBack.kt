package com.sevenrocks.taskapp.appModules.markrewarks

import android.net.Uri

interface FileSelectionCallBack {
    fun onSelection(uri: Uri, name:String)
}