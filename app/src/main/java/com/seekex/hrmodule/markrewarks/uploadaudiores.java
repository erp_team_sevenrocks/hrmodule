package com.seekex.hrmodule.markrewarks;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

public class uploadaudiores extends BaseResponse {

    @SerializedName("data")
    private uploadaudiores2 data;

    public uploadaudiores2 getData() {
        return data;
    }

    public void setData(uploadaudiores2 data) {
        this.data = data;
    }


}
