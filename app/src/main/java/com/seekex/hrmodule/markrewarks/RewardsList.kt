package com.sevenrocks.taskapp.appModules.vendorreg

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.markrewarks.*
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import com.sevenrocks.taskapp.appModules.markrewarks.RewardListAdapter

import kotlinx.android.synthetic.main.rewardlist.view.*
import java.util.*


/**
 */
class RewardsList : Fragment() {
    private lateinit var myAdapter: RewardListAdapter
    private var page = 1
    private var donHit = false
    var mydataList: ArrayList<RewardListDTO> = java.util.ArrayList()

    public var rewardsActivity: RewardsActivity? = null
    private lateinit var rootView: View


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.rewardlist, BasicActi, false)
        rewardsActivity = activity as RewardsActivity?

        rootView.recyclerView.setLayoutManager(
            LinearLayoutManager(activity)
        )
        rootView.txtheaderre.text = "View Rewards"
        setAdapter(rootView)
        page = 1
        getList()
//        rootView.txtheader.text = "Rewards"


        return rootView
    }


    override fun onResume() {
        super.onResume()
        page = 1
    }


    private fun setAdapter(rootView: View) {
        myAdapter = RewardListAdapter(activity,object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {
               dataModel=  any1 as RewardListDTO

                when (i) {
                    2 -> {

                        deleteParty()
                    }

                }
            }

        })
        rootView.recyclerView!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.recyclerView!!.adapter = myAdapter

        rootView.recyclerView!!.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (!recyclerView.canScrollVertically(1) && !donHit) {
                    Log.v("addOnScrollListener", "false")
                    getList()
                }
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
            }
        })


        myAdapter!!.notifyDataSetChanged()
    }

    private fun deletePartyRequestData(): DeleteRewardReq {
        val reqData =
            DeleteRewardReq()


        reqData.service_name= ApiUtils.DELETEREWARD
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.id = dataModel.id

        return reqData
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun deleteParty() {
        rewardsActivity?.apiImp?.deleteRecord(
            deletePartyRequestData(),object :ApiCallBack{
                override fun onSuccess(status: Boolean, any: Any) {
                  if (status){
                      mydataList.remove(dataModel)
                      Toast.makeText(activity, "Record Deleted", Toast.LENGTH_SHORT).show()
                      myAdapter.notifyDataSetChanged()
                  }
                }

            }
        )
    }

    private fun getAllvendorReqdata(): GetListReq {
        val reqData =
            GetListReq()


        reqData.service_name=ApiUtils.GETREWARDS
        reqData.token = rewardsActivity?.pref?.get(AppConstants.token)
        reqData.page = page;

        return reqData
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getList() {


        rewardsActivity?.apiImp?.getrewardlist(
            getAllvendorReqdata(),object :ApiCallBack{
                override fun onSuccess(status: Boolean, any: Any) {
                   if (status){
                       val ss = any as RewardListResponse
                       val datalist = ss.data

                       if (datalist.isEmpty()) {
                           donHit = true
                       } else {
                           mydataList.addAll(datalist)
                           myAdapter!!.setDataValues(mydataList)
                           myAdapter!!.notifyDataSetChanged()
                           page++
                       }
                   }
                }

            }
        )


    }


    companion object {

        @JvmStatic
        var dataModel = RewardListDTO()


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = RewardsList()
    }


}