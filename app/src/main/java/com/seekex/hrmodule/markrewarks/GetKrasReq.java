package com.seekex.hrmodule.markrewarks;


import com.seekx.webService.models.RequestBase;

public class GetKrasReq extends RequestBase {

    private String   user_id;

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
