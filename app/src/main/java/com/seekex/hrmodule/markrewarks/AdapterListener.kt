package com.sevenrocks.taskapp.appModules.markrewarks

interface AdapterListener {
    fun onSelection(any1: Any?, any2: Any?, i: Int)
}