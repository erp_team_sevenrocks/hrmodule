package com.sevenrocks.taskapp.appModules.markrewarks

import android.Manifest
import android.R.attr.data
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaRecorder
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Environment
import android.preference.PreferenceManager
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.fxn.pix.Options
import com.fxn.pix.Pix
import com.google.gson.Gson
import com.jaiselrahman.filepicker.activity.FilePickerActivity
import com.jaiselrahman.filepicker.model.MediaFile
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.adapter.EventParamAdapter
import com.seekex.hrmodule.adapter.EventParamAdapterParam
import com.seekex.hrmodule.adapter.ParticipantAdapter
import com.seekex.hrmodule.adapter.PickedlistAdapter
import com.seekex.hrmodule.criticalevent.ParamlistResponse
import com.seekex.hrmodule.databinding.FileItemBinding
import com.seekex.hrmodule.databinding.ImageItemBinding
import com.seekex.hrmodule.grievance.ImagesGRDTO
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.sop.SaveAudidUsersDOP
import com.seekex.hrmodule.utils.SearchCallback
import com.seekex.hrmodule.utils.SearchResultCallback
import com.seekx.interfaces.DialogeUtilsCallBack
import com.seekx.interfaces.PermissionCallBack
import com.seekx.utils.DataUtils
import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.webService.ApiImp
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import com.sevenrocks.taskapp.appModules.grievance.SaveEvent
import com.vincent.filepicker.Constant
import com.vincent.filepicker.activity.NormalFilePickActivity
import com.vincent.filepicker.filter.entity.NormalFile
import droidninja.filepicker.FilePickerBuilder
import droidninja.filepicker.FilePickerConst
import droidninja.filepicker.models.sort.SortingTypes
import kotlinx.android.synthetic.main.add_feedback.view.*
import kotlinx.android.synthetic.main.add_reward.*
import kotlinx.android.synthetic.main.add_reward.view.*
import kotlinx.android.synthetic.main.add_reward.view.txtheader
import kotlinx.android.synthetic.main.dialog.*
import kotlinx.android.synthetic.main.grievance_reg.view.*
import kotlinx.android.synthetic.main.image_view.*
import kotlinx.android.synthetic.main.saveevent.view.*
import kotlinx.android.synthetic.main.sopauditlist.view.*
import java.io.File
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList


class AddRewards : Fragment() {
    private var audioFilename: String = ""
    private var rewardId: String = ""
    private var excelPath: String = ""
    private var excelName: String = ""
    private var imageFilename: String = ""
    private var xlFilename: String = ""
    private lateinit var excelUri: Uri
    private var docPaths: ArrayList<Uri> = ArrayList()
    private lateinit var adapterRewardreason: SpinnerAdapter_RewardReason
    private lateinit var adapterRewarduser: SpinnerAdapter_Users

    public var rewardsActivity: RewardsActivity? = null
    public var imageList = ArrayList<ImageDTO>()
    public var fileList = ArrayList<FileDTO>()

    private lateinit var adapter: ParticipantAdapter
    private lateinit var evadapter: EventParamAdapterParam

    var rewardReasonId: String = "0"
    var rewardType: String = "0"
    var userId: String = "0"
    var kraid: String = "0"
    lateinit var apiImp: ApiImp
    var mediaRecorder: MediaRecorder? = null
    var countDownTimer: CountDownTimer? = null
    var isRecorded = 0
    private var count: Long? = null
    private var timer = ""
    private var file_name = ""
    private var permissionCallBack: PermissionCallBack? = null
    private var resumeSelectCallback: FileSelectionCallBack? = null
    private var call_Counter = 0

    var reasonlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var userlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var participantlist: ArrayList<DropdownModel> = java.util.ArrayList()
    var kraslist: ArrayList<DropdownModel> = java.util.ArrayList()
    var modelArrayList = ArrayList<ParicipantModel>()
    var imagePathList: ArrayList<ImagesGRDTO> = java.util.ArrayList()
    var selpartlist: ArrayList<SaveRewardpart> = java.util.ArrayList()
    var paramlist: ArrayList<DropdownModel> = java.util.ArrayList()


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for getActivity() fragment
        val rootView = inflater.inflate(R.layout.add_reward, BasicActi, false)
        rewardsActivity = activity as RewardsActivity?
        rootView.txtheader.setText("Add Reward/Strike")
        alreadySelectedNames.clear()
        namestoShow.clear()
        namestoShowpart.clear()
        setListeners(rootView)
//        setAdapters(rootView)
        getparamlist()
        getUserList()

        return rootView
    }

    private fun chooseFile(param: FileSelectionCallBack) {
        resumeSelectCallback = param
        FilePickerBuilder.instance
            .setMaxCount(1) //optional
            .enableDocSupport(true)
            .setActivityTheme(R.style.LibAppTheme_Dark) //optional
            .enableSelectAll(true)
            .sortDocumentsBy(SortingTypes.NAME)
            .setActivityTitle("Please select doc")
            .pickFile(this, 42)

    }

    private fun playBeep() {
        ExtraUtils.playASounds(activity, R.raw.beep)
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUserList() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = rewardsActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.GETUSERSLIST


        rewardsActivity!!.apiImp.getDropdownList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse


                    userlist.addAll(ss.data)
//                    adapterRewarduser.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getKtasList() {

        val getMasterRequest = GetKrasReq()

        getMasterRequest.token = rewardsActivity?.pref?.get(AppConstants.token)
        getMasterRequest.user_id = userId
        getMasterRequest.service_name = ApiUtils.GETKRASLIST


        rewardsActivity!!.apiImp.getKrasList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as RewardUsersResponse

                    kraslist.addAll(ss.data)
                    Log.e("jjg", "names: " + kraslist.size)

                    for (i in kraslist.indices) {
                        Log.e("jjg", "names: " + kraslist.get(i).name)

                        modelArrayList.add(
                            ParicipantModel(
                                kraslist.get(i).name,
                                kraslist.get(i).id,
                                kraslist.get(i).name,
                                false
                            )
                        )
                    }

//                    adapter = ParticipantAdapter(modelArrayList, activity)
//                    rec_showlist.layoutManager = LinearLayoutManager(activity)
//                    rec_showlist.adapter = adapter

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getResonList() {
        reasonlist.clear()
//        adapterRewardreason.notifyDataSetChanged()


        if (rewardType.equals("Reward")) {
            hitReasonListApi("reward")
        } else if (rewardType.equals("Strike")) {
            hitReasonListApi("strike")
        }

    }

    private fun hitReasonListApi(type: String) {

        val getMasterRequest = RequestBase()
        Log.e("TAG", "hitReasonListApi: " + type)
        getMasterRequest.token = rewardsActivity?.pref?.get(AppConstants.token)
        if (type.equals("reward")) {
            getMasterRequest.service_name = ApiUtils.GETREWARDREASONLIST
        } else {
            getMasterRequest.service_name = ApiUtils.GETSTRIKEPOINTREASONLIST
        }

        rewardsActivity!!.apiImp.getRewardReasonList(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {

                if (status) {
                    var ss = any as RewardReasonResponse
//                    var aa = DropdownModel()
//                    aa.id = "0"
//                    aa.name = "Select Reason"
//
//                    reasonlist.add(aa)

                    reasonlist.addAll(ss.data)
//                    adapterRewardreason.notifyDataSetChanged()
                }

            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveReward() {

        if (rewardType.equals("0")) {
            rewardsActivity?.dialogUtil?.showAlert("Please Type")
            return
        }

        if (userId.equals("0") || userId.equals("")) {
            rewardsActivity?.dialogUtil?.showAlert("Please Pick User")
            return
        }
        if (rewardReasonId.equals("0")) {
            rewardsActivity?.dialogUtil?.showAlert("Pick Reason Type")
            return
        }

        if (edt_rewreason.equals("") || edt_rewreason.length() == 0) {
            rewardsActivity?.dialogUtil?.showAlert("Add Reason ")
            return
        }
        if (edt_rewname.equals("") || edt_rewname.length() == 0) {
            rewardsActivity?.dialogUtil?.showAlert("Enter Name ")
            return
        }

        if (kraslist.size == 0) {
            rewardsActivity?.dialogUtil?.showAlert("Select User with Kra")
            return
        }
        if (kraid.equals("0") || kraid.equals("")) {
            rewardsActivity?.dialogUtil?.showAlert("Select Kra  ")
            return
        }
        if (imageList.size > 0) {
            uploadImages()
        } else if (bt_save.visibility == View.INVISIBLE) {
            saveaudiofile()
        } else if (fileList.size > 0) {
            saveExcelFile()
        } else {

            saveRewardDetail()
        }

    }

    private fun saveRewardDetail() {
        rewardsActivity!!.apiImp.showProgressBar()
        val saveRewardReq = SaveRewardReq()
        val saveRewardReq2 = SaveRewardReq2()

        var ss = ArrayList<RewardUserSaveReq>()
        for (i in alreadySelectedNames.indices) {
            var aa = RewardUserSaveReq()
            aa.id = alreadySelectedNames.get(i).id
            ss.add(aa)
        }
        var sspart = ArrayList<SaveRewardpart>()
        for (i in alreadySelecteParam.indices) {
            var aa = SaveRewardpart()
            aa.id = alreadySelecteParam.get(i).id
            sspart.add(aa)
        }
        val json = Gson().toJson(ss)

        saveRewardReq2.reason = edt_rewreason.text.toString()
        saveRewardReq2.assign_to_user_id = userId
        saveRewardReq2.point_type_id = rewardReasonId
        saveRewardReq2.kra = ss
        saveRewardReq2.parameter = sspart
//        saveRewardReq2.img_file = imageFilename
//        saveRewardReq2.xl = xlFilename
//        saveRewardReq2.audio = audioFilename
        saveRewardReq2.name =edt_rewname.text.toString()
        saveRewardReq2.media = imagePathList


        if (rewardType.equals("Reward")) {
            saveRewardReq.service_name = ApiUtils.SAVEREWARD

        } else if (rewardType.equals("Strike")) {
            saveRewardReq.service_name = ApiUtils.SAVESTRIKE

        }
        saveRewardReq.token = rewardsActivity?.pref?.get(AppConstants.token)
        saveRewardReq.data = saveRewardReq2
        rewardsActivity!!.apiImp.saveReward(saveRewardReq, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                rewardsActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as RewardSaveResponse

                    Toast.makeText(activity, "Saved Successfully", Toast.LENGTH_SHORT)
                        .show()
                    rewardsActivity!!.finish()
                }
            }


        })
    }

    private fun saveExcelFile() {
        rewardsActivity!!.apiImp.showProgressBar()
        rewardsActivity!!.apiImp.saveExcel(call_Counter, fileList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                rewardsActivity!!.apiImp.cancelProgressBar()
                Log.e("TAG", "onSuccess: upload excel")

                if (status) {

                    var ss = any as uploadaudiores
                    xlFilename = ss.data.path + "" + ss.data.filename
                    Log.e("TAG", "onSuccess: upload excel" + xlFilename)

                    var model = ImagesGRDTO()
                    model.type = "excel"
                    model.name = xlFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < fileList.size) {
                        saveExcelFile()
                    } else {
                        call_Counter = 0
                        saveRewardDetail()
                    }
                }

            }

        })


    }

    private fun saveaudiofile() {
        rewardsActivity!!.apiImp.showProgressBar()
        rewardsActivity!!.apiImp.saveAudio(file_name, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                rewardsActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    audioFilename = ss.data.path + "" + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "audio"
                    model.name = audioFilename
                    imagePathList.add(model)
//
                    if (fileList.size > 0) {
                        saveExcelFile()
                    } else {
                        saveRewardDetail()

                    }
                }

            }

        })


    }

    private fun uploadImages() {

        rewardsActivity!!.apiImp.showProgressBar()
        rewardsActivity?.apiImp?.hitApi(call_Counter, imageList, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                rewardsActivity!!.apiImp.cancelProgressBar()
                if (status) {
                    var ss = any as uploadaudiores
                    Log.e("TAG", "onSuccess: filepath=" + ss.data.path + ss.data.filename)


                    var imageFilename = ss.data.path + ss.data.filename
                    var model = ImagesGRDTO()
                    model.type = "image"
                    model.name = imageFilename
                    imagePathList.add(model)

                    call_Counter++

                    if (call_Counter < imageList.size) {
                        uploadImages()
                    } else {
                        call_Counter = 0
                        if (bt_save.visibility == View.INVISIBLE) {
                            saveaudiofile()
                        } else if (fileList.size > 0) {
                            saveExcelFile()
                        } else {
                            saveRewardDetail()
                        }
                    }
                }
            }
        })
    }

    var mulUserlist: ArrayList<DropdownModel> = java.util.ArrayList()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getparamlist() {

        val getMasterRequest = RequestBase()

        getMasterRequest.token = rewardsActivity?.pref?.get(AppConstants.token)
        getMasterRequest.service_name = ApiUtils.EVENTPARAMLIST


        rewardsActivity!!.apiImp.getparamlist(getMasterRequest, object : ApiCallBack {
            override fun onSuccess(status: Boolean, any: Any) {
                if (status) {
                    var ss = any as ParamlistResponse
//                    var aa = DropdownModel()
//                    aa.id = "0"
//                    aa.name = "Select User"
//                    userlist.add(aa)

                    var list = ArrayList<DropdownModel>()
                    list.addAll(ss.data)
                    for (i in list.indices) {
                        paramlist.add(
                            DropdownModel(
                                list.get(i).id,
                                list.get(i).name,
                                false
                            )
                        )
                    }
//                    adapterRewarduser.notifyDataSetChanged()

                }


            }

        })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun setListeners(rootView: View) {

        rootView.btn_addparamrew.setOnClickListener {
            /*  Log.e("TAG", "setlisteners: " + paramlist.size)
              val dialog =
                  Dialog(activity!!, android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen)
              dialog.setContentView(R.layout.dialog) //layout for dialog
              dialog.setTitle("Add Param Type")
              dialog.setCancelable(false) //none-dismiss when touching outside Dialog
              // set the custom dialog components - texts and image
              val recyclerView = dialog.findViewById<View>(R.id.rec_participant) as RecyclerView
              val btnAdd = dialog.findViewById<View>(R.id.btn_ok)
              val btnCancel = dialog.findViewById<View>(R.id.btn_cancel)
              val edt_search = dialog.findViewById<EditText>(R.id.edt_search)
              //set spinner adapter
              evadapter = EventParamAdapterParam(paramlist, activity)
              recyclerView.layoutManager = LinearLayoutManager(activity)
              recyclerView.adapter = evadapter

              edt_search.addTextChangedListener(object : TextWatcher {
                  override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                      // TODO Auto-generated method stub
                  }

                  override fun beforeTextChanged(
                      s: CharSequence?,
                      start: Int,
                      count: Int,
                      after: Int
                  ) {

                      // TODO Auto-generated method stub
                  }

                  override fun afterTextChanged(s: Editable) {

                      // filter your list from your input
                      filterSearch(s.toString())
                      //you can use runnable postDelayed like 500 ms to delay search text
                  }
              })

              btnAdd.setOnClickListener {
                  Log.e("TAG", "setlisteners: " + paramlist.size)

                  for (i in paramlist.indices) {
                      Log.e("TAG", "onClick: " + paramlist[i].checked)
                      if (paramlist[i].checked == true) {
                          if (!namestoShowpart.contains(paramlist[i])) {
                              namestoShowpart.add(paramlist[i])
                          }
                      } else {
                          if (namestoShowpart.contains(paramlist[i])) {
                              namestoShowpart.remove(paramlist[i])
                          }
                      }
                  }
                  Log.e("TAG", "onClick: " + namestoShowpart.size)
                  showDataforpart(namestoShowpart)
                  dialog.dismiss()
              }
              btnCancel.setOnClickListener {
                  for (i in paramlist.indices) {
                      Log.e("TAG", "onClick: " + paramlist[i].checked)
                      if (namestoShowpart.contains(paramlist[i])) {
                          paramlist[i].checked = true
                      } else {
                          paramlist[i].checked = false
                      }
                  }
                  Log.e("TAG", "onClick: " +namestoShowpart.size)
                  showDataforpart(namestoShowpart)
                  dialog.dismiss()
              }
              dialog.show()

          }


        */

            DataUtils.openSearchDialogeMultiSelect(
                activity!!,
                mulUserlist,
                paramlist,
                "Select Parameter",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        alreadySelecteParam.clear()
                        val list: ArrayList<String> = ArrayList()

                        rootView.txtparamselected.setText("")
                        var ss = any as ArrayList<DropdownModel>
                        Log.e("TAG", "onSelected: " + ss.size)

                        for (dd in ss) {
                            var aa = ParicipantModel("")
                            aa.id = dd.id
                            list.add(dd.name)
                            alreadySelecteParam.add(aa)

                        }
                        if (ss.size > 1) {
                            rootView.txtparamselected.setText(TextUtils.join(", ", list))
                        } else {
                            rootView.txtparamselected.setText(TextUtils.join(", ", list))

                        }

//                        userId = ss.id

                    }

                })

        }
        rootView.edtcreasontype.setOnClickListener {
            DataUtils.openSearchDialoge(
                activity!!,
                reasonlist,
                "Select Reason",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtcreasontype.setText(ss.name)
                        rewardReasonId = ss.id


                    }

                })
        }
        rootView.edtchooserew.setOnClickListener {
            var ss = DropdownModel()
            var ssList = ArrayList<DropdownModel>()
            ss.name = "Reward"
            ss.id = "0"
            ssList.add(ss)

            var ss1 = DropdownModel()
            ss1.name = "Strike"
            ss1.id = "1"
            ssList.add(ss1)

            DataUtils.openSearchDialoge(
                activity!!,
                ssList,
                "Select Type",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        rootView.edtchooserew.setText(ss.name)

                        rewardType = ss.name
                        getResonList()


                    }

                })
        }
        rootView.edtkrarew.setOnClickListener {

            if (userId.equals("0") || userId.equals("")) {
                rewardsActivity!!.dialogUtil.showAlert("Select User first !!")
                return@setOnClickListener
            }
            DataUtils.openSearchDialogeuser(
                activity!!,
                kraslist,
                "Select Kra",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)

                        rootView.edtkrarew.setText(ss.name)
                        kraid = ss.id
                        alreadySelectedNames.add(
                            ParicipantModel(
                                ss.name,
                                kraid,
                                "",
                                false
                            )
                        )
                    }

                })
        }
        rootView.edtuserrew.setOnClickListener {
            DataUtils.openSearchDialogeuser(
                activity!!,
                userlist,
                "Select User",
                object : SearchCallback {
                    override fun onTextTyped(
                        txt: String,
                        searchResultCallback: SearchResultCallback
                    ) {
                    }

                    override fun onSelected(any: Any?) {
                        var ss = any as DropdownModel
                        Log.e("TAG", "onSelected: " + ss.name)
                        rootView.edtuserrew.setText(ss.name)
                        userId = ss.id
                        if (!userId.equals("0")) {
                            getKtasList()
                        } else {
                        }

                    }

                })
        }
        rootView.removefile.setOnClickListener {
            excelName = ""
            excelPath = ""
            txt_excelname.setText("")
            ll_viewattach.visibility = View.GONE
        }

        rootView.add_rewardbyn.setOnClickListener {
            if (isRecorded == 1) {
                rootView.iv_actionrew.performClick()
            }


            saveReward()
        }

        rootView.imagesreward.setOnClickListener {


//            permissionCallBack =
//                PermissionUtils.checkStoragePermission(object : PermissionCallBack {
//                    override fun onPermissionAccessed() {
//                        chooseFile(object : FileSelectionCallBack {
//                            @SuppressLint("SetTextI18n")
//                            override fun onSelection(uri: Uri, name: String) {
////                                val fileName= android.os.FileUtils.getFileNameFromUri(uri,this@BecomeExpertActivity)
//                            }
//                        })
//                    }
//                }, activity!!, PermissionUtils.storagePermission)

//            checkStoragePermission(object : PermissionCallBack {
//                override fun onPermissionAccessed() {
            pickImage()


//                }
//
//            })


        }
        rootView.addfilerew.setOnClickListener {

            val intent4 = Intent(activity, NormalFilePickActivity::class.java)
            intent4.putExtra(Constant.MAX_NUMBER, 4)
            intent4.putExtra(
                NormalFilePickActivity.SUFFIX,
                arrayOf("xlsx", "xls", "doc", "docx", "ppt", "pptx", "pdf")
            )
            startActivityForResult(intent4, Constant.REQUEST_CODE_PICK_FILE)

//            val intent = Intent(activity, FilePickerActivity::class.java)
//            intent.putExtra(
//                FilePickerActivity.CONFIGS, Configurations.Builder()
//                    .setCheckPermission(true)
//                    .setShowImages(false)
//                    .enableImageCapture(false)
//                    .setMaxSelection(1)
//                    .setShowFiles(true)
//                    .setSingleChoiceMode(true)
//                    .setSkipZeroSizeFiles(true)
//                    .build()
//            )
//            startActivityForResult(intent, 101)

//                checkStoragePermission(object : PermissionCallBack {
//                    override fun onPermissionAccessed() {
//            chooseFile(object : FileSelectionCallBack {
//                override fun onSelection(uri: Uri, name: String) {
////                        val fileName= android.os.FileUtils.getFileNameFromUri(uri,this@BecomeExpertActivity)
////                        hitUploadApi(uri,AppConstants.PROFILE_RESUME,fileName)
//                }
//            })
//                    }
//                })
        }
        rootView.iv_actionrew.setOnClickListener {
            checkStoragePermission(object : PermissionCallBack {
                override fun onPermissionAccessed() {
                    if (isRecorded == 0) {
                        isRecorded = 1
                        playBeep()
                        startRecording()
                        iv_actionrew!!.setImageResource(android.R.drawable.ic_media_pause)
                    } else if (isRecorded == 1) {
                        bt_save!!.visibility = View.INVISIBLE
                        stopMedia()
                        playBeep()
                        isRecorded = 2
                        countDownTimer!!.cancel()
                        timer = tvTimers!!.text.toString()
                        iv_actionrew!!.setImageResource(android.R.drawable.ic_media_play)
                    } else if (isRecorded == 2) {
//                        FileUtils.openDocument(file_name, activity)
                    }

                }

            })


        }
        rootView.bt_resetrew.setOnClickListener {
            try {
                isRecorded = 0
                stopMedia()
                playBeep()
                countDownTimer!!.cancel()
                tvTimers!!.text = ""
                bt_save!!.visibility = View.GONE
                iv_actionrew!!.setImageDrawable(activity!!.resources.getDrawable(R.drawable.audiorec))
            } catch (e: Exception) {
            }
        }

    }

    private fun showDatainPickList(list: ArrayList<ParicipantModel>) {
        val adapter = PickedlistAdapter(list, activity)
        rec_showlist!!.layoutManager = LinearLayoutManager(activity)
        rec_showlist!!.adapter = adapter
    }

    private fun showDataforpart(list: ArrayList<ParicipantModel>) {
        val adapter = PickedlistAdapter(list, activity)

        rec_paramlistrew!!.layoutManager = LinearLayoutManager(activity)
        rec_paramlistrew!!.adapter = adapter
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        try {
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putBoolean("request_permissions", false)
                .apply()
            var permissionGranted = true
            try {
                for (i in permissions.indices) {
                    if (grantResults[i] == -1)
                        permissionGranted = false
                }
            } catch (e: Exception) {
            }
            if (permissionGranted)
                permissionCallBack!!.onPermissionAccessed()
        } catch (e: Exception) {
        }
    }

    fun checkStoragePermission(permissionCallBack: PermissionCallBack) {
        this.permissionCallBack = permissionCallBack
        if (android.os.Build.VERSION.SDK_INT > 22) {

            requestPermissions(
                arrayOf(
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.RECORD_AUDIO,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                ),
                0
            )
        } else
            permissionCallBack.onPermissionAccessed()
    }

    fun filterSearch(text: String?) {
        val temp = ArrayList<ParicipantModel>()
        for (d in modelArrayList) {

            if (d.name.toLowerCase().contains(text!!.toLowerCase())) {
                temp.add(d)
            }
        }
        //update recyclerview
        adapter.updateList(temp)
    }


    private fun stopMedia() {
        try {
            mediaRecorder!!.stop()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun startRecording() {
        mediaRecorderReady()
        try {
            mediaRecorder!!.prepare()
            mediaRecorder!!.start()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        startCountdownTimer()
    }


    private fun startCountdownTimer() {
        count = 0L
        countDownTimer = object : CountDownTimer(10800000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                // Used for formatting digit to be in 2 digits only
                count = count!! + 1
                tvTimers!!.text = ExtraUtils.convertSecondToHHMMString(count!!)
            }

            // When the task is over it will print 00:00:00 there
            override fun onFinish() {;
                tvTimers!!.text = "00:00:00"
            }
        }.start()
    }

    private fun mediaRecorderReady() {
        val currentTime = System.currentTimeMillis()
        val extBaseDir = activity?.externalCacheDir
        val file = File(extBaseDir!!.absolutePath + "/SeekReward/" + "")
        if (!file.exists()) {
            if (!file.mkdirs()) {
            }
        }
        file_name = "$file/myRecording.mp3"
        Log.e("TAG", "mediaRecorderReady: $file_name")
        mediaRecorder = MediaRecorder()
        mediaRecorder!!.setAudioSource(MediaRecorder.AudioSource.MIC)
        mediaRecorder!!.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
        mediaRecorder!!.setAudioEncoder(MediaRecorder.OutputFormat.AMR_NB)
        mediaRecorder!!.setOutputFile(file_name)
    }

    private fun pickImage() {

        val options: Options = Options.init()
            .setRequestCode(100) //Request code for activity results
            .setCount(1) //Number of images to restict selection count
            .setFrontfacing(false) //Front Facing camera on start
//                .setPreSelectedUrls(returnValue) //Pre selected Image Urls
            .setSpanCount(4) //Span co

            .setMode(Options.Mode.Picture) //Option to select only pictures or videos or both
            .setScreenOrientation(Options.SCREEN_ORIENTATION_PORTRAIT) //Orientaion
            .setPath("/pix/images") //Custom Path For media Storage


        Pix.start(this@AddRewards, options)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, intent: Intent?) {
        super.onActivityResult(requestCode, resultCode, intent)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == Constant.REQUEST_CODE_PICK_FILE) {
                val list: ArrayList<NormalFile> =
                    intent!!.getParcelableArrayListExtra(Constant.RESULT_PICK_FILE)!!

                for (i in list.indices) {
                    var isExists: Boolean = false
                    excelName = list.get(i).name
                    excelPath = list.get(i).path
//                excelUri = list.get(0).uri
                    if (fileList.size > 0) {
                        for (j in fileList.indices) {
                            if (fileList.get(j).path.toLowerCase()
                                    .equals(excelPath.toLowerCase())
                            ) {
                                isExists = true
                            }
                        }
                        if (!isExists) {
                            fileList.add(
                                FileDTO(
                                    excelName, excelPath
                                )
                            )
                        }
                    } else {
                        fileList.add(
                            FileDTO(
                                excelName, excelPath
                            )
                        )

                    }


                }


                showAttachmentView(ll_viewattach!!,
                    fileList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            fileList = any as ArrayList<FileDTO>
                        }
                    })
            } else if (requestCode == 101) {
                val files: ArrayList<MediaFile> =
                    intent!!.getParcelableArrayListExtra(FilePickerActivity.MEDIA_FILES)!!
                excelName = files.get(0).name
                excelPath = files.get(0).path
                excelUri = files.get(0).uri

                fileList.add(
                    FileDTO(
                        excelName, excelPath
                    )
                )
                showAttachmentView(ll_viewattach!!,
                    fileList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            fileList = any as ArrayList<FileDTO>
                        }
                    })
//              var   fileUri = intent!!.getData();
//                var filePath = fileUri!!.getPath();
//
//
//                fileList.add(
//                    FileDTO(
//                        filePath, filePath
//                    )
//                )
//                showAttachmentView(ll_viewattach!!,
//                    fileList, object : SelectedMasterCallback {
//                        override fun onSelected(any: Any) {
//                            fileList = any as ArrayList<FileDTO>
//                        }
//                    })

//                Log.e("TAG", "onActivityResult: "+filePath )

            } else if (requestCode == 42) {
                intent?.data?.path
                val uri = intent?.data

                docPaths = ArrayList()
                docPaths.addAll(intent!!.getParcelableArrayListExtra<Uri>(FilePickerConst.KEY_SELECTED_DOCS)!!)
                Log.e("TAG", "onActivityResult: 42" + docPaths.get(0))
                fileList.add(
                    FileDTO(
                        docPaths.get(0).toString(), docPaths.get(0).toString()
                    )
                )
                showAttachmentView(ll_viewattach!!,
                    fileList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            fileList = any as ArrayList<FileDTO>
                        }
                    })

            } else if (requestCode == 100) {

                val returnValue: ArrayList<String> =
                    intent?.getStringArrayListExtra(Pix.IMAGE_RESULTS)!!

                val fileUri = intent?.data
                imageList.add(
                    ImageDTO(
                        Uri.parse("file://" + returnValue.get(0)).toString()
                    )
                )
                showDynamicImage(ll_attach!!,
                    imageList, object : SelectedMasterCallback {
                        override fun onSelected(any: Any) {
                            imageList = any as ArrayList<ImageDTO>
                        }
                    })
            } else {
                //                val fileUri = intent?.data
//                imageList.add(ImageDTO(getCompressedUri(fileUri!!, activity!!).toString()))
//                showDynamicImage(ll_attach!!,
//                    imageList, object : SelectedMasterCallback {
//                        override fun onSelected(any: Any) {
//                            imageList = any as ArrayList<ImageDTO>
//                        }
//                    })
            }
        }
    }

    fun showAttachmentView(
        llAtach: LinearLayout,
        fileList: ArrayList<FileDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until fileList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = FileItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = fileList[model]

            binding.model = imageDTO



            binding.removefile.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete File",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                fileList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(fileList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showDynamicImage(
        llAtach: LinearLayout,
        imageList: ArrayList<ImageDTO>,
        selectedMasterCallback: SelectedMasterCallback
    ) {

        llAtach.removeAllViews()
        val context = llAtach.context
        val dialogUtils = DialogUtils(llAtach.context)

        for (model in 0 until imageList.size) {

            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)
                    as LayoutInflater

            val binding = ImageItemBinding.inflate(
                inflater, llAtach,
                true
            )

            val imageDTO = imageList[model]

            binding.model = imageDTO

            binding.ivImage.setOnClickListener {
                showImageUri(imageDTO.getUri(), context)
            }

            binding.ivCut.setOnClickListener {
                dialogUtils.showAlertWithCallBack(
                    "Delete Image",
                    "Delete",
                    object : DialogeUtilsCallBack {
                        override fun onDoneClick(clickStatus: Boolean) {
                            if (clickStatus) {
                                imageList.remove(imageDTO)
                                llAtach.removeView(binding.root)
                                selectedMasterCallback.onSelected(imageList)
                            }

                        }

                    }
                )

            }
        }
    }

    fun showImageUri(uri: Uri, ctx: Context) {
        val dialog = Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar_Fullscreen)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.image_view)

        dialog.tiv.setImageURI(uri)

        dialog.show()
        dialog.setCanceledOnTouchOutside(true)
        dialog.setCancelable(true)

    }

    fun getTimestamp(): String? {
        val tsLong = System.currentTimeMillis()
        return tsLong.toString()
    }

    fun getCompressedUri(uri: Uri, context: Context): Uri {
        Log.v("getCompressUri", "${uri.path}")
        val path = "${Environment.getExternalStorageDirectory()}/ERP/ temp"
        val fileName = "${getTimestamp()}.png"

        val myDir = File(path)

        if (!myDir.exists())
            myDir.mkdirs()

        val file = File(myDir, fileName)
        val currentFile = File(uri.path!!)

        val returnString = Uri.parse(file.absolutePath)

        if (file.exists()) file.delete()
        try {
//            val out = FileOutputStream(file)
//            val finalBitmap = Compressor(context).compressToBitmap(currentFile)
//            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out)
//            out.flush()
//            out.close()
        } catch (e: Exception) {
            Log.v("getCompressUri", "exception $e")
            return uri
        }

        Log.v("getCompressUri", "last ${returnString.path}")

        return returnString

    }


    companion object {

        @JvmField
        var alreadySelectedNames = ArrayList<ParicipantModel>()
        val namestoShow = ArrayList<ParicipantModel>()
        val namestoShowpart = ArrayList<ParicipantModel>()

        @JvmField
        var alreadySelecteParam = ArrayList<ParicipantModel>()


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = AddRewards()
    }
}