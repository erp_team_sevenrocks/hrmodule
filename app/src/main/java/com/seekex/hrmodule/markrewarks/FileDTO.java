
package com.seekex.hrmodule.markrewarks;

import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.Objects;

public class FileDTO extends BaseResponse {

    @SerializedName("name")
    private String name;
    @SerializedName("path")
    private String path;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileDTO fileDTO = (FileDTO) o;
        return Objects.equals(path, fileDTO.path);
    }

    @Override
    public int hashCode() {
        return Objects.hash(path);
    }

    public FileDTO(String name, String path) {
        this.name = name;
        this.path = path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
