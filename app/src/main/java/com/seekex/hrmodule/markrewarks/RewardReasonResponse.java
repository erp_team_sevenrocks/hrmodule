
package com.seekex.hrmodule.markrewarks;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;

import java.util.ArrayList;

public class RewardReasonResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<DropdownModel> data;

    public ArrayList<DropdownModel> getData() {
        return data;
    }

    public void setData(ArrayList<DropdownModel> data) {
        this.data = data;
    }


}

