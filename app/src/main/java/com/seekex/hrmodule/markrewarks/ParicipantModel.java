package com.seekex.hrmodule.markrewarks;

import java.util.Objects;

public class ParicipantModel {

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParicipantModel that = (ParicipantModel) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    private String name;
    private String id;

    public String getDepatmentname() {
        return depatmentname;
    }

    public void setDepatmentname(String depatmentname) {
        this.depatmentname = depatmentname;
    }

    private String depatmentname;
    private Boolean isChecked;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParicipantModel(String name, String id, String depatmentname, Boolean isChecked) {
        this.name = name;
        this.id = id;
        this.depatmentname = depatmentname;
        this.isChecked = isChecked;
    }

    public ParicipantModel(String name) {
        this.name = name;
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}
