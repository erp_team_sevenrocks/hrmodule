package com.seekex.hrmodule.markrewarks;

import com.seekx.webService.models.RequestBase;

public class RewardFinalAudioSaveReq extends RequestBase {

    private RewardFinalAudioSaveReq2 data;

    public RewardFinalAudioSaveReq2 getData() {
        return data;
    }

    public void setData(RewardFinalAudioSaveReq2 data) {
        this.data = data;
    }



}
