package com.seekex.hrmodule.markrewarks;

public class RewardFinalAudioSaveReq2 {
        private String file;

        private String meeting_id;

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(String meeting_id) {
        this.meeting_id = meeting_id;
    }
}