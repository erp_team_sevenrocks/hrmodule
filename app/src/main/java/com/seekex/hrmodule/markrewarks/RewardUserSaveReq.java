package com.seekex.hrmodule.markrewarks;

import com.seekx.webService.models.RequestBase;

public class RewardUserSaveReq extends RequestBase {


    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
