package com.seekex.hrmodule.markrewarks;

import com.seekx.webService.models.RequestBase;

public class RewardUploadmageReq extends RequestBase {

    private String  file;
    private String  party_id;

    public String getParty_id() {
        return party_id;
    }

    public void setParty_id(String party_id) {
        this.party_id = party_id;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
