package com.seekex.hrmodule.markrewarks;


import com.seekx.webService.models.RequestBase;

public class SaveRewardReq extends RequestBase {

    private SaveRewardReq2  data;

    public SaveRewardReq2 getData() {
        return data;
    }

    public void setData(SaveRewardReq2 data) {
        this.data = data;
    }
}
