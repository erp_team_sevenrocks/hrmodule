package com.seekex.hrmodule.markrewarks;

import com.seekx.webService.models.RequestBase;

public class GetListReq extends RequestBase {

    private int page  ;
    private String user_id  ;
    private String assignee_id  ;

    public String getAssignee_id() {
        return assignee_id;
    }

    public void setAssignee_id(String assignee_id) {
        this.assignee_id = assignee_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
