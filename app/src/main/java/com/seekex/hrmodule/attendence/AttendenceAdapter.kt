package com.sevenrocks.taskapp.appModules.markrewarks


import android.R
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.net.Uri
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.SeekBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.attendence.AttendenceActivity
import com.seekex.hrmodule.attendence.DatesDTO
import com.seekex.hrmodule.databinding.AttenAdapterBinding
import com.seekex.hrmodule.databinding.GreivanceAdapterBinding
import com.seekex.hrmodule.grievance.GreivanceListDTO
import com.seekex.hrmodule.grievance.GreivanceListMarkerAdapter
import com.seekex.hrmodule.grievance.GrievanceActivity
import com.seekex.hrmodule.grievance.MarkerSaveReq2
import com.seekx.webService.ApiUtils
import kotlinx.android.synthetic.main.play_audio.*
import java.io.File
import java.util.*

class AttendenceAdapter(
    private val context: Context?,
    private val adapterListener: AdapterListener
) :
    RecyclerView.Adapter<AttendenceAdapter.ViewHolder>() {
    private val rewardsActivity: AttendenceActivity?
    var count: Long = 0L
    var player: MediaPlayer? = null
    private var items: ArrayList<DatesDTO> = ArrayList()

    fun setDataValues(items: ArrayList<DatesDTO>?) {
        this.items = items!!

        notifyDataSetChanged()
    }

    init {
        rewardsActivity = context as AttendenceActivity?
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = AttenAdapterBinding.inflate(inflater)


        binding.userCardView.setOnClickListener {
            adapterListener.onSelection(binding.model, null, 1)
        }

        return ViewHolder(binding)
    }


    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(items[position])

    inner class ViewHolder(private val binding: AttenAdapterBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: DatesDTO) {
            binding.model = item
            binding.executePendingBindings()

        }

    }

}