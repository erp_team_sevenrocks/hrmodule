
package com.seekex.hrmodule.attendence;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AttenceDTO {

    @SerializedName("dates")
    private ArrayList<DatesDTO> dates;
    @SerializedName("present_count")
    private String present_count;
    @SerializedName("absent_count")
    private String absent_count;
    @SerializedName("holiday_count")
    private String holiday_count;
    @SerializedName("leave_count")
    private String leave_count;
    @SerializedName("working_hours")
    private String working_hours;
    @SerializedName("ignore_hours")
    private String ignore_hours;
    @SerializedName("net_hours")
    private String net_hours;
    @SerializedName("shift_hours")
    private String shift_hours;

    @SerializedName("shift_timings")
    private String shift_timings;

    public String getShift_timings() {
        return shift_timings;
    }

    public void setShift_timings(String shift_timings) {
        this.shift_timings = shift_timings;
    }

    public String getPresent_count() {
        return present_count;
    }

    public void setPresent_count(String present_count) {
        this.present_count = present_count;
    }

    public String getAbsent_count() {
        return absent_count;
    }

    public void setAbsent_count(String absent_count) {
        this.absent_count = absent_count;
    }

    public String getHoliday_count() {
        return holiday_count;
    }

    public void setHoliday_count(String holiday_count) {
        this.holiday_count = holiday_count;
    }

    public String getLeave_count() {
        return leave_count;
    }

    public void setLeave_count(String leave_count) {
        this.leave_count = leave_count;
    }

    public String getWorking_hours() {
        return working_hours;
    }

    public void setWorking_hours(String working_hours) {
        this.working_hours = working_hours;
    }

    public String getIgnore_hours() {
        return ignore_hours;
    }

    public void setIgnore_hours(String ignore_hours) {
        this.ignore_hours = ignore_hours;
    }

    public String getNet_hours() {
        return net_hours;
    }

    public void setNet_hours(String net_hours) {
        this.net_hours = net_hours;
    }

    public String getShift_hours() {
        return shift_hours;
    }

    public void setShift_hours(String shift_hours) {
        this.shift_hours = shift_hours;
    }

    public ArrayList<DatesDTO> getDates() {
        return dates;
    }

    public void setDates(ArrayList<DatesDTO> dates) {
        this.dates = dates;
    }
}
