
package com.seekex.hrmodule.attendence;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DetailDTO {



    @SerializedName("in_time")
    private String in_time;
    @SerializedName("out_time")
    private String out_time;
    @SerializedName("hours")
    private String hours;

    public String getIn_time() {
        return in_time;
    }

    public void setIn_time(String in_time) {
        this.in_time = in_time;
    }

    public String getOut_time() {
        return out_time;
    }

    public void setOut_time(String out_time) {
        this.out_time = out_time;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }
}
