
package com.seekex.hrmodule.attendence;

import com.google.gson.annotations.SerializedName;
import com.seekex.hrmodule.mysalary.SalaryListDTO;
import com.seekx.webService.models.BaseResponse;

public class AttendenceResponse extends BaseResponse {

    @SerializedName("data")
    private AttenceDTO data;

    public AttenceDTO getData() {
        return data;
    }

    public void setData(AttenceDTO data) {
        this.data = data;
    }
}

