
package com.seekex.hrmodule.attendence;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class DatesDTO {

    @SerializedName("attendance_detail")
    private ArrayList<DetailDTO> attendance_detail;

    @SerializedName("date")
    private String date;
    @SerializedName("title")
    private String title;
    @SerializedName("day_name")
    private String day_name;
    @SerializedName("working_hours")
    private String working_hours;
    @SerializedName("ignore_hours")
    private String ignore_hours;
    @SerializedName("net_hours")
    private String net_hours;


    public Boolean checkIf() {

        if (attendance_detail.size()>0){
            return true;
        }
        return false;
    }

    public String getNewHours()
    {
        if (checkIf()){
            return "Hours: "+attendance_detail.get(0).getHours();
        }
        return "0";
    }

    public String getInhours()
    {
        if (checkIf()){
            return "In Time: "+ attendance_detail.get(0).getIn_time();
        }
        return "0";
    }

    public String getOutHours()
    {
        if (checkIf()){
            return "Out Time: "+attendance_detail.get(0).getOut_time();
        }
        return "0";
    }
    public String getTextWorking_hours() {
        return "Working hours: "+working_hours;
    }
    public String getWorking_hours() {
        return working_hours;
    }

    public void setWorking_hours(String working_hours) {
        this.working_hours = working_hours;
    }

    public String getTextIgnore_hours() {
        return "Ignore Hours"+ignore_hours;
    }

    public String getIgnore_hours() {
        return ignore_hours;
    }

    public void setIgnore_hours(String ignore_hours) {
        this.ignore_hours = ignore_hours;
    }

    public String getTextNet_hours() {
        return "Net Hours: "+net_hours;
    }

    public String getNet_hours() {
        return net_hours;
    }

    public void setNet_hours(String net_hours) {
        this.net_hours = net_hours;
    }

    public ArrayList<DetailDTO> getAttendance_detail() {
        return attendance_detail;
    }

    public void setAttendance_detail(ArrayList<DetailDTO> attendance_detail) {
        this.attendance_detail = attendance_detail;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDay_name() {
        return day_name;
    }

    public void setDay_name(String day_name) {
        this.day_name = day_name;
    }
}
