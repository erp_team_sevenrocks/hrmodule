package com.sevenrocks.taskapp.appModules.vendorreg

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.downloader.OnDownloadListener
import com.downloader.PRDownloader
import com.downloader.PRDownloaderConfig
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.R
import com.seekex.hrmodule.attendence.AttendenceActivity
import com.seekex.hrmodule.attendence.AttendenceResponse
import com.seekex.hrmodule.attendence.DatesDTO
import com.seekex.hrmodule.grievance.GreivanceListDTO
import com.seekex.hrmodule.grievance.GreivanceListResponse
import com.seekex.hrmodule.leave_manager.AcceptReq
import com.seekex.hrmodule.leave_manager.AcceptReq2
import com.seekex.hrmodule.leave_manager.PdfResponse
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.mysalary.SalaryActivity
import com.seekex.hrmodule.mysalary.SalaryListResponse
import com.seekx.webService.ApiUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.sevenrocks.taskapp.appModules.markrewarks.AdapterListener
import com.sevenrocks.taskapp.appModules.markrewarks.AttendenceAdapter
import com.sevenrocks.taskapp.appModules.markrewarks.GListAdapter
import kotlinx.android.synthetic.main.attendence_data.view.*
import kotlinx.android.synthetic.main.rewardlist.view.*
import kotlinx.android.synthetic.main.rewardlist.view.txtheaderre
import kotlinx.android.synthetic.main.salary_list.view.*
import kotlinx.android.synthetic.main.salary_list.view.spin_months
import kotlinx.android.synthetic.main.salary_list.view.spin_year
import java.io.File


/**
 */
class MyAttendenceData : Fragment() {

    private lateinit var myAdapter: AttendenceAdapter
    private var pdfUrl: String? = ""
    public var leaveActivity: AttendenceActivity? = null
    private var rootView: View? = null
    var check = 0
    var monthpos: String = "1"
    var monthtext: String = "January"
    var yearpos: String = "2020"
    var mydataList: ArrayList<DatesDTO> = java.util.ArrayList()

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        BasicActi: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {


        // Inflate the layout for getActivity() fragment
        rootView = inflater.inflate(R.layout.attendence_data, BasicActi, false)
        leaveActivity = activity as AttendenceActivity?


        setAdapter(rootView!!)

        rootView!!.spin_months.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(parent: AdapterView<*>?) {

                }

                override fun onItemSelected(
                    parent: AdapterView<*>?,
                    view: View?,
                    position: Int,
                    id: Long
                ) {
                    if (++check > 1) {
                        try {
                            var search_type = parent!!.getItemAtPosition(position).toString()
                            if (!search_type.equals(monthtext)) {
                                monthpos = (position + 1).toString()
                                monthtext = search_type
                                getList()
                            }


                        } catch (e: Exception) {
                        }

                    }

                }

            }
        rootView!!.spin_year.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if (++check > 1) {
                    try {
                        var search_type = parent!!.getItemAtPosition(position).toString()
                        if (!yearpos.equals(search_type)) {
                            yearpos = search_type

                            getList()
                        }

                    } catch (e: Exception) {
                    }
                }


            }

        }
        val adapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.months_list,
            android.R.layout.simple_spinner_item
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        rootView?.spin_months?.setAdapter(adapter)
//

        val countries: ArrayList<String> = ArrayList()
//        countries.add("2015")
//        countries.add("2016")
//        countries.add("2017")
//        countries.add("2018")
//        countries.add("2019")
        countries.add("2020")
        countries.add("2021")
        countries.add("2022")

        val yadapter = ArrayAdapter.createFromResource(
            requireContext(),
            R.array.years_list,
            android.R.layout.simple_spinner_item
        )
        yadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        val aa = ArrayAdapter(activity!!, android.R.layout.simple_spinner_item, countries)

        rootView?.spin_year?.setAdapter(aa)

        rootView?.txtheaderre?.text = "My Salary"

        getList()
        return rootView
    }


    override fun onResume() {
        super.onResume()
    }

    private fun setAdapter(rootView: View) {
        myAdapter = AttendenceAdapter(activity, object : AdapterListener {
            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onSelection(any1: Any?, any2: Any?, i: Int) {

                when (i) {


                }
            }

        })
        rootView.rec_attendence!!.setLayoutManager(LinearLayoutManager(activity))
        rootView.rec_attendence!!.adapter = myAdapter




        myAdapter!!.notifyDataSetChanged()
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun getList() {
        clearData()
        mydataList.clear()
        val reqData = GetSalaryReq()
        val reqData2 = GetSalaryReq2()

        reqData.service_name = ApiUtils.my_attendance
        reqData.token = leaveActivity?.pref?.get(AppConstants.token)

        reqData2.month = monthpos
        reqData2.year = yearpos
        reqData.conditions = reqData2

        leaveActivity?.apiImp?.getattendencedata(
            reqData, object : ApiCallBack {
                override fun onSuccess(status: Boolean, any: Any) {
                    if (status) {
                        var data = any as AttendenceResponse

                        rootView!!.txt_presentcount.setText(data.data.present_count)
                        rootView!!.txt_absentcount.setText(data.data.absent_count)
                        rootView!!.txt_holidaycount.setText(data.data.holiday_count)
                        rootView!!.txt_leavecount.setText(data.data.leave_count)
                        rootView!!.txt_workingcount.setText(data.data.working_hours)
                        rootView!!.txt_ignorecount.setText(data.data.ignore_hours)
                        rootView!!.txt_netcount.setText(data.data.net_hours)
                        rootView!!.txt_shiftcount.setText(data.data.shift_hours)
                        rootView!!.txt_shifttimings.setText(data.data.shift_timings)


                        val datalist = data.data.dates

                        mydataList.addAll(datalist)
                        myAdapter!!.setDataValues(mydataList)
                        myAdapter!!.notifyDataSetChanged()

                    }
                }

            }
        )


    }

    private fun clearData() {

        rootView!!.txt_presentcount.setText("")
        rootView!!.txt_absentcount.setText("")
        rootView!!.txt_holidaycount.setText("")
        rootView!!.txt_leavecount.setText("")
        rootView!!.txt_workingcount.setText("")
        rootView!!.txt_ignorecount.setText("")
        rootView!!.txt_netcount.setText("")
        rootView!!.txt_shiftcount.setText("")
        rootView!!.txt_shifttimings.setText("")
    }


    companion object {


        @JvmStatic
        val instance: androidx.fragment.app.Fragment
            get() = MyAttendenceData()
    }


}