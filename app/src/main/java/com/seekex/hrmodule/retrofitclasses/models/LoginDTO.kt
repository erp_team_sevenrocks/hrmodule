package com.seekex.hrmodule.retrofitclasses.models



class LoginDTO(
    var deviceId: String,
    val uid: String,
    val timeStamp: String,
    var geoLocation: String,
    var fcmToken: String){

    constructor():this("","","","","")
}

