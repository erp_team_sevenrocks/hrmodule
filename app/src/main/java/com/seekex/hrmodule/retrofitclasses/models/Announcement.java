package com.seekex.hrmodule.retrofitclasses.models;

public class Announcement {
        private int id;
        private String name;
        private String detail;
        private String created;
        private String created_by;
        private int priority;
        private boolean is_seen;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }

        public String getCreated_by() {
            return "Created by " + created_by;
        }

        public void setCreated_by(String created_by) {
            this.created_by = created_by;
        }

        public int getPriority() {
            return priority;
        }

        public void setPriority(int priority) {
            this.priority = priority;
        }

        public boolean isIs_seen() {
            return is_seen;
        }

        public void setIs_seen(boolean is_seen) {
            this.is_seen = is_seen;
        }
    }