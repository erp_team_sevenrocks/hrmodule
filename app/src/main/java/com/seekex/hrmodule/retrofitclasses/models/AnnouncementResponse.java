package com.seekex.hrmodule.retrofitclasses.models;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.seekx.webService.models.BaseResponse;


import java.util.List;

public class AnnouncementResponse extends BaseResponse {


    @SerializedName("data")
    @Expose
    private AnnouncementData data;

    public AnnouncementData getData() {
        return data;
    }

    public void setData(AnnouncementData data) {
        this.data = data;
    }





}
