package com.seekex.hrmodule.retrofitclasses.models;

public class DashboardModel   {

    private  String label;
    private String detail;
    private int drawableId;
    private int id;

    public DashboardModel(String label) {
        this.label = label;
    }

    public DashboardModel(String label, String detail, int drawableId, int id) {
        this.label = label;
        this.detail = detail;
        this.drawableId = drawableId;
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public int getDrawableId() {
        return drawableId;
    }

    public void setDrawableId(int drawableId) {
        this.drawableId = drawableId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
