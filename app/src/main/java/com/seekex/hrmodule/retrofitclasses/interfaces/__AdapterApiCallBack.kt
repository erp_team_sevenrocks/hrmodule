package com.seekx.webService.interfaces

import java.util.*

interface __AdapterApiCallBack {

    fun onSuccess(serviceName:String ,objects: Objects)
    fun onFailed(error:String)

}