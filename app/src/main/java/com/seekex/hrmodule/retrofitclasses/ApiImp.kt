package com.seekx.webService

import android.app.Activity
import android.content.Context
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import androidx.annotation.RequiresApi
import com.google.gson.Gson
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekex.hrmodule.R
import com.seekex.hrmodule.attendence.AttendenceResponse
import com.seekex.hrmodule.criticalevent.EventSaveReq
import com.seekex.hrmodule.criticalevent.ParamlistResponse
import com.seekex.hrmodule.custombinding.CustomLoader
import com.seekex.hrmodule.feedback.SaveFeedbackReq
import com.seekex.hrmodule.grievance.*
import com.seekex.hrmodule.jobposting.ApplyPostReq
import com.seekex.hrmodule.jobposting.FilterListResponse
import com.seekex.hrmodule.jobposting.PostingListReq
import com.seekex.hrmodule.jobposting.PostingListResponse
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.leave_manager.*
import com.seekex.hrmodule.locationfeedback.*
import com.seekex.hrmodule.login.LoginResponse
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.GetSalaryReq2
import com.seekex.hrmodule.mysalary.SalaryListResponse
import com.seekex.hrmodule.remiender.GetRecordReq
import com.seekex.hrmodule.remiender.SaveDataReq
import com.seekex.hrmodule.retrofitclasses.models.AnnouncementResponse
import com.seekex.hrmodule.retrofitclasses.models.ImageDTO
import com.seekex.hrmodule.sop.*
import com.seekex.hrmodule.training.CodeGetResponse
import com.seekex.hrmodule.training.CodeSaveReq
import com.seekex.hrmodule.training.FeedbackSaveReq
import com.seekex.hrmodule.trainingtest.AnswerReq
import com.seekex.hrmodule.trainingtest.DeleteMediaReq
import com.seekex.hrmodule.trainingtest.FileDTOTest
import com.seekex.hrmodule.trainingtest.PasscodeReq
import com.seekex.trainingtestapp.login.PasscodeResponse
import com.seekex.trainingtestapp.login.QuestionRes

import com.seekx.utils.DialogUtils
import com.seekx.utils.ExtraUtils
import com.seekx.utils.ImageUtils
import com.seekx.webService.interfaces.ApiCallBack
import com.seekx.webService.models.RequestBase
import com.seekx.webService.models.BaseResponse
import com.seekx.webService.models.apiRequest.*
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import rx.Observer
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import java.io.File
import java.util.ArrayList


open class ApiImp(val context: Context) {

    var pref: Preferences = Preferences(context)
    var dialogUtil: DialogUtils = DialogUtils(context)


    companion object {
        const val TAG = "ApiImpResp"
    }


    private val webApi = ApiUtils.getWebApi()

    var customLoader: CustomLoader = CustomLoader(context)
    private val uploadFileApi = ApiUtils.getUploadApi(context)

    fun showProgressBar() {
        customLoader.show()
    }

    fun cancelProgressBar() {
        customLoader.cancel()
    }


//    fun login(loginReq: LoginReq) {
//
//        if (!onStartApi())
//            return
//        Log.e("TAG", "loginrequest: " + Gson().toJson(loginReq))
//        webApi.login(loginReq).subscribeOn(Schedulers.io())
//            .observeOn(AndroidSchedulers.mainThread())
//            .subscribe(object : Observer<LoginResponse> {
//                override fun onCompleted() {}
//
//                override fun onError(e: Throwable) {
//                    Log.e(TAG, "onError login: " + e.toString())
//                    onResponseApi(e.toString(), null)
//                }
//
//                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
//                @SuppressLint("LongLogTag")
//                override fun onNext(addUserRes: LoginResponse) {
//                    Log.e(TAG, "onlogin: " + addUserRes.toString())
//                    Log.e(ApiUtils.LOGIN, addUserRes.toString())
//                    if (addUserRes.getStatus() == 0) apiCallBacknew!!.onFailed(addUserRes.getMsg()) else {
//                        apiCallBacknew!!.onSuccess(addUserRes.getService_name(), addUserRes)
//                    }
//                    onResponseApi(addUserRes.getStatus() == 0, addUserRes.getMsg())
//
//                }
//
//            })
//    }
fun getQuestion(loginReq: GetQuestionReq, param: ApiCallBack) {

    if (!onStartApi())
        return
    Log.e("TAG", "requestData: " + Gson().toJson(loginReq))
    ApiUtils.getWebApiNEW(context).getQue(loginReq).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(object : Observer<QuestionRes> {
            override fun onCompleted() {}

            override fun onError(e: Throwable) {
                Log.e(TAG, "onError login: " + e.toString())
                onResponseApi(e.toString(), null)
            }

            @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
            override fun onNext(response: QuestionRes) {
                Log.e(TAG, "onlogin: " + response.toString())
                onResponseApi(response, param)

            }

        })
}
    fun saveAnswer(loginReq: AnswerReq, param: ApiCallBack) {

        if (!onStartApi())
            return
        Log.e("TAG", "requestData: " + Gson().toJson(loginReq))
        ApiUtils.getWebApiNEW(context).saveAns(loginReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError login: " + e.toString())
                    onResponseApi(e.toString(), null)
                }

                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                override fun onNext(response: BaseResponse) {
                    Log.e(TAG, "onlogin: " + response.toString())
                    onResponseApi(response, param)

                }

            })
    }
    fun deleteMedia(loginReq: DeleteMediaReq, param: ApiCallBack) {

        if (!onStartApi())
            return
        Log.e("TAG", "requestData: " + Gson().toJson(loginReq))
        ApiUtils.getWebApiNext(context).deleteMedia(loginReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError login: " + e.toString())
                    onResponseApi(e.toString(), null)
                }

                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                override fun onNext(response: BaseResponse) {
                    Log.e(TAG, "onlogin: " + response.toString())
                    onResponseApi(response, param)

                }

            })
    }
    private fun checkInternet(): Boolean {
        if (!ExtraUtils.isNetworkConnectedMainThread(context)) {
            dialogUtil.showAlert(context.getString(R.string.internet_error))
            return false
        }
        return true
    }

    private fun onStartApi(): Boolean {
        if (!checkInternet()) {
            dialogUtil.showAlert(context.getString(R.string.internet_error))
            return false
        }
//        customLoader.show()
        return true
    }


    private fun checkResMsg(msg: String): String {
        if (msg.contains("java.net") || msg.contains("retrofit2.adapter.rxjava")) {
//            toast(Preferences(context).get(AppConstants.token))
            return context.getString(R.string.connection_failed)
        }

        return msg
    }

    private fun onResponseApi(response: BaseResponse, Obj: Any?, apiCallBack: ApiCallBack) {
        customLoader.cancel()
        if (response.status == 0) {

            dialogUtil.showAlert(checkResMsg(response.msg!!))
            apiCallBack.onSuccess(false, response.msg!!)
        } else {
            apiCallBack.onSuccess(true, Obj!!)
        }
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onResponseApi(status: Boolean, msg: String?) {
        if (status) {
            dialogUtil.showAlert(msg!!)
        }
        if (!(context as Activity?)!!.isDestroyed) customLoader.cancel()
    }

    private fun onResponseApi(response: BaseResponse, apiCallBack: ApiCallBack?) {
        customLoader.cancel()
        if (response.status == 0) {

//            if (onLogoutCheck(response))
//                return

            dialogUtil.showAlert(checkResMsg(response.msg!!))
        }
        if (response.status == 0) {
            apiCallBack?.onSuccess(false, response)

        } else {
            apiCallBack?.onSuccess(true, response)

        }

    }

    private fun onResponseApi(msg: String, apiCallBack: ApiCallBack?) {
        customLoader.cancel()
        Log.e("upload 44", "onResponseApi:msg= " + msg)

        dialogUtil.showAlert(checkResMsg(msg))

        apiCallBack?.onSuccess(false, msg)

    }

    open fun isNetworkConnectedMainThred(ctx: Context): Boolean {
        val cm = ctx
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val ni = cm.activeNetworkInfo
        return if (ni == null) {
//            DialogUtil.showInternetError(ctx)
            false
        } else true
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    private fun onStartApi(`object`: RequestBase): Boolean {
        if (!isNetworkConnectedMainThred(context)) {
            dialogUtil.showAlert("No Internet Connection")
            return false
        }
        `object`.username=(pref.get(AppConstants.user_name))
        `object`.password=(pref.get(AppConstants.password))
        `object`.app_version = context!!.getString(R.string.version)
        `object`.secret_key = ImageUtils.getSecretKey()
//        `object`.token = pref.get(AppConstants.token)
        if (!(context as Activity?)!!.isDestroyed) customLoader.show()
        return true
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun onStartApinew(`object`: RequestBase): Boolean {
        if (!isNetworkConnectedMainThred(context)) {
            dialogUtil.showAlert("No Internet Connection")
            return false
        }
//        `object`.setUsername(pref.get(AppConstants.user_name))
//        `object`.setPassword(pref.get(AppConstants.password))

        if (!(context as Activity?)!!.isDestroyed) customLoader.show()
        return true
    }


    fun login(loginReq: LoginReq, param: ApiCallBack) {

        if (!onStartApi())
            return
        Log.e("TAG", "requestData: " + Gson().toJson(loginReq))
        ApiUtils.getWebApiNEW(context).login(loginReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LoginResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError login: " + e.toString())
                    onResponseApi(e.toString(), null)
                }

                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                override fun onNext(response: LoginResponse) {
                    Log.e(TAG, "onlogin: " + response.toString())
                    onResponseApi(response, param)

                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getKrasList(`object`: GetKrasReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getKrasList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveglsit(`object`: GreivanceSaveReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveglist(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<SaveGreivanceResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: SaveGreivanceResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveeventdata(`object`: EventSaveReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveevent(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }


    fun passcodeverification(loginReq: PasscodeReq, param: ApiCallBack) {

        if (!onStartApi())
            return
        Log.e("TAG", "requestData: " + Gson().toJson(loginReq))
        ApiUtils.getWebApiNEW(context).passcode(loginReq).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<PasscodeResponse> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e(TAG, "onError login: " + e.toString())
                    onResponseApi(e.toString(), null)
                }

                @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
                override fun onNext(response: PasscodeResponse) {
                    Log.e(TAG, "onlogin: " + response.toString())
                    onResponseApi(response, param)

                }

            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveCodeData(`object`: CodeSaveReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveCode(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<CodeGetResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: CodeGetResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveFeedback(`object`: FeedbackSaveReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveFeedback(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getGlist(`object`: RequestBase, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getGlist(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getWorkInst(`object`: WorkListReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getWorkInst(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<WorkListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: WorkListResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveAuditData(`object`: SaveAuditdataReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveauditdata(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun savePrimaryData(`object`: SavepredatReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).savework(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<SavePrimaryResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: SavePrimaryResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getDropdownList(`object`: RequestBase, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getRewardUsers(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getparamlist(`object`: RequestBase, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getParamlist(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<ParamlistResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: ParamlistResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getkrausers(`object`: GetSalaryReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getkraUsersList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getUsersList(`object`: SOpUsersReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getUsersList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getMasterList(`object`: MasterListReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getMasterList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getSubSection(`object`: SubSectionListReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getSubsection(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveReward(`object`: SaveRewardReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveReward(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardSaveResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardSaveResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun markAction(`object`: MarkActionReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).markAction(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun sendformark(`object`: DeleteRewardReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).sendformarked(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun deleteRecord(`object`: DeleteRewardReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).deletereward(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun applyPost(`object`: ApplyPostReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).applyPost(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun manageRecord(`object`: ManageGreivanceReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).manageRecord(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getRewardReasonList(`object`: RequestBase, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getRewardReasons(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardReasonResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardReasonResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getGreivancelist(`object`: GetListReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getgreivancelist(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<GreivanceListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: GreivanceListResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getGtypelist(`object`: RequestBase, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getgreivancetypelist(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getleavelist(`object`: GetLEaveReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNext(context).getmyleave(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LeaveListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: LeaveListResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getsalarypdf(`object`: GetSalaryReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getpdf(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<PdfResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: PdfResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun acceptreject(`object`: AcceptReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).acceptreject(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getsalarydata(`object`: GetSalaryReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
//        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNext(context).getmysalary(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<SalaryListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: SalaryListResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getattendencedata(`object`: GetSalaryReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
//        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNext(context).getatten(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<AttendenceResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: AttendenceResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getannpuncement(`object`: RequestBase, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
//        if (!onStartApi(`object`)) return
        ApiUtils.getAnnouceApi(context).getaccouncemnt(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<AnnouncementResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    apiCallBack?.onSuccess(false, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: AnnouncementResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun seenAccouncement(`object`: RequestBase, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
//        if (!onStartApi(`object`)) return
        ApiUtils.getAnnouceApi(context).seenaccouncemnt(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getrewardlist(`object`: GetListReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getAllRewards(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardListResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getPostingList(`object`: PostingListReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getJobPosting(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<PostingListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: PostingListResponse) {
                    Log.e("Response", "response ${response.toString()}")
                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getFilterData(`object`: PostingListReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getFilterData(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<FilterListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: FilterListResponse) {

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getAllKraList(`object`: GetListReq, apiCallBack: ApiCallBack?) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getAllKraList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<AllKraListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: AllKraListResponse) {
                    Log.e("Response", "onNextsqsqsqsqs$response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }

    fun getRealPathFromURIPath(contentURI: Uri, activity: Context): String? {
        var filePath = contentURI.path

        return try {
//                val proj = arrayOf(MediaStore.Images.Media.DATA)
            val cursor = activity.contentResolver.query(contentURI, null, null, null, null)
            if (cursor == null) {
                filePath
            } else {
                cursor.moveToFirst()
                val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
                cursor.getString(idx)
            }
        } catch (e: Exception) {
            filePath = filePath?.replace("/external_files", "")
            Environment.getExternalStorageDirectory().absolutePath + filePath
        }

    }
    fun savemarkers(`object`: MarkerSaveReq, apiCallBack: ApiCallBack) {
        Log.e("save meeting data", "savemeetings: " + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return

        ApiUtils.getWebApiNEW(context).saveMarkers(`object`)!!.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {

                    // Logs.v("Response","completee");
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("TAG", "onNext: $response")

                    onResponseApi(response, apiCallBack)
                }
            })
    }
    fun getAudioFileDetails(`object`: GetAudioDetailReq, apiCallBack: ApiCallBack) {
        Log.e("save meeting data", "savemeetings: " + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return

        ApiUtils.getWebApiNEW(context).getAudioDetails(`object`)!!.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<AudioDetailResponse> {
                override fun onCompleted() {

                    // Logs.v("Response","completee");
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                }

                override fun onNext(getMReponse: AudioDetailResponse) {
                    Log.e("TAG", "onNext: $getMReponse")

                    onResponseApi(getMReponse , apiCallBack)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun hitApivideo(counter: Int, imagelist: ArrayList<VideoDTO>, apiCallBack: ApiCallBack) {
        if (!onStartApi())
            return

        val filePath =imagelist.get(counter).path// getRealPathFromURIPath(imagelist.get(counter).getUri(), context)

        val file = File(filePath!!)

        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    Log.e("upload error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
//                    Log.e("upload res", "file: " + respnse.data.file)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun hitApileave(counter: Int, imagelist: ArrayList<com.seekex.hrmodule.trainingtest.ImageDTO>, apiCallBack: ApiCallBack) {
        if (!onStartApi())
            return

        val filePath = getRealPathFromURIPath(imagelist.get(counter).getUri(), context)

        val file = File(filePath!!)

        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    Log.e("upload error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
//                    Log.e("upload res", "file: " + respnse.data.file)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun hitApi(counter: Int, imagelist: ArrayList<ImageDTO>, apiCallBack: ApiCallBack) {
        if (!onStartApi())
            return

        val filePath = getRealPathFromURIPath(imagelist.get(counter).getUri(), context)

        val file = File(filePath!!)

        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    Log.e("upload error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
//                    Log.e("upload res", "file: " + respnse.data.file)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun hitApiTEst(counter: Int, imagelist: ArrayList<com.seekex.hrmodule.trainingtest.ImageDTO>, apiCallBack: ApiCallBack) {
        if (!onStartApi())
            return

        val filePath = getRealPathFromURIPath(imagelist.get(counter).getUri(), context)

        val file = File(filePath!!)

        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("multipart/form-data"), file)
        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    Log.e("upload error ", "file: " + e.toString())
                    onResponseApi(e.toString(), null)

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
//                    Log.e("upload res", "file: " + respnse.data.file)
                    onResponseApi(respnse, apiCallBack)

                }
            })
    }


    fun saveExcel(counter: Int, imagelist: ArrayList<FileDTO>, param: ApiCallBack) {

        val file = File(imagelist.get(counter).path)
        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("application/pdf"), file)

        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    onResponseApi(true, e.toString())

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, param)

                }
            })
    }

    fun saveExcelTest(counter: Int, imagelist: ArrayList<FileDTOTest>, param: ApiCallBack) {

        val file = File(imagelist.get(counter).path)
        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("application/pdf"), file)

        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    onResponseApi(true, e.toString())

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, param)

                }
            })
    }
    fun hitDataApi(
        partyid: String,
        list: ArrayList<ImageDTO>,
        filepath: String,
        apiCallBack: ApiCallBack
    ) {

        if (!onStartApi())
            return
        val reqData =
            RewardUploadmageReq()

        reqData.service_name = ApiUtils.SAVEIMAGEFILE
        reqData.token = pref?.get(AppConstants.token)
        reqData.file = filepath
        reqData.party_id = partyid


        Log.e("hitDataApi 11 ", "reportReq:i== " + Gson().toJson(reqData))

        ApiUtils.getWebApi().saveImagesdata(reqData)?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {}
                override fun onError(e: Throwable) {
                    Log.e("inbox api", "err: " + e.toString())

                    onResponseApi(e.toString(), apiCallBack)
                }

                override fun onNext(respnse: BaseResponse) {
                    Log.e("inbox api", "onNext: " + respnse.toString())
                    onResponseApi(respnse, apiCallBack)
                }
            })
    }

    fun saveAudio(filename: String, param: ApiCallBack) {

        val file = File(filename)
        val mFile: RequestBody
        mFile = RequestBody.create(MediaType.parse("application/pdf"), file)

        val fileToUpload = MultipartBody.Part.createFormData("file", file.name, mFile)
        ApiUtils.getUploadApi(context).uploadSingleFile(fileToUpload)
            ?.subscribeOn(Schedulers.io())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.subscribe(object : Observer<uploadaudiores> {
                override fun onCompleted() {}

                override fun onError(e: Throwable) {
                    Log.e("upload error ", "file: " + e.printStackTrace())
                    onResponseApi(true, e.toString())

                }

                override fun onNext(respnse: uploadaudiores) {
                    Log.e("upload res", "file: " + respnse)
                    onResponseApi(respnse, param)

                }
            })
    }


    fun saveRewardRec(`object`: RewardFinalAudioSaveReq, apiCallBack: ApiCallBack?) {
        Log.e("save meeting data", "savemeetings: " + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApi().saverewardaudiodata(`object`)!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {

                    // Logs.v("Response","completee");
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                }

                override fun onNext(getMReponse: BaseResponse) {
                    Log.e("TAG", "onNext: $getMReponse")

                    onResponseApi(getMReponse, apiCallBack)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getDesignatinlist(`object`: GetDesignationReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getdesignationList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getSkillsList(`object`: RequestBase, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getSkillsList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getKrasData(`object`: GetKrasReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getKrasData(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<KraListResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: KraListResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getReasonList(`object`: RequestBase, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getReasonList(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getdataontype(`object`: DataonTypeReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getdataontype(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LocationlistResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: LocationlistResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getlocationdetails(`object`: LocationdetailReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getlocationdetails(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<LocationResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: LocationResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }
    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getFeedbackType(`object`: RequestBase, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getFeedbackTypelist(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun savereminder(`object`: SaveDataReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).savedata(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveLeave(`object`: SaveLeaveDataReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveleave(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<SaveLEaveResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: SaveLEaveResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveMedia(`object`: UploadmedisReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveMedia(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getrecord(`object`: GetRecordReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getrecord(`object`)!!!!
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardUsersResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardUsersResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getSkilsData(`object`: GetSkillsDataReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getSkillsData(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<SkillDataResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: SkillDataResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun getRewardText(`object`: GetSkillsDataReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).getRewardData(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<RewardnamesResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: RewardnamesResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }


    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun sendRecoomedataion(`object`: SendRecommdationReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).sendReccomdation(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext$response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveeedata(`object`: SaveKraDetailReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveData(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext $response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun saveNewFeedback(`object`: SaveFeedbackReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).saveFeedbacknew(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext $response")

                    onResponseApi(response, param)
                }
            })
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun savelocFeedback(`object`: SaveLocFeedbackReq, param: ApiCallBack) {
        Log.e("setPickListDetails", "setPickListDetails" + Gson().toJson(`object`))
        if (!onStartApi(`object`)) return
        ApiUtils.getWebApiNEW(context).savelocfeedback(`object`)!!!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : Observer<BaseResponse> {
                override fun onCompleted() {
                    Log.v("Response", "completee")
                }

                override fun onError(e: Throwable) {
                    onResponseApi(true, e.toString())
                    Log.e("Response", "error $e")
                }

                override fun onNext(response: BaseResponse) {
                    Log.e("Response", "onNext $response")

                    onResponseApi(response, param)
                }
            })
    }
}