package com.seekx.webService.models


open class BaseResponse {
    var msg: String? = null
    var status = 0
    var service_name: String? = null

    override fun toString(): String {
        return "ResponseBase{" +
                "msg='" + msg + '\'' +
                ", status=" + status +
                ", service_name='" + service_name + '\'' +
                '}'
    }
}
