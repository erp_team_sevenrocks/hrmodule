package com.seekex.hrmodule.retrofitclasses.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AnnouncementData {

        @SerializedName("Announcement")
        @Expose
        private Announcement announcement;

        @SerializedName("AnnouncementImage")
        @Expose
        private List<AnnouncementImage> announcementImage;

        public Announcement getAnnouncement() {
            return announcement;
        }

        public void setAnnouncement(Announcement announcement) {
            this.announcement = announcement;
        }

        public List<AnnouncementImage> getAnnouncementImage() {
            return announcementImage;
        }

        public void setAnnouncementImage(List<AnnouncementImage> announcementImage) {
            this.announcementImage = announcementImage;
        }
    }
