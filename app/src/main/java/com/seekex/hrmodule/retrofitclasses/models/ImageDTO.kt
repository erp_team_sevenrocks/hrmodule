package com.seekex.hrmodule.retrofitclasses.models

import android.net.Uri

class ImageDTO(
    var image: String){

    fun getUri(): Uri {
        return  Uri.parse(image)
    }
}