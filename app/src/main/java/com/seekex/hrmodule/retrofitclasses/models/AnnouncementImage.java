package com.seekex.hrmodule.retrofitclasses.models;

import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.bumptech.glide.Glide;

public class AnnouncementImage {

        private String image;
        private String thumbnail;

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @BindingAdapter("bindthumbnail")
        public void loadImage(ImageView view, String thumbnail) {
            try {

                Glide.with(view.getContext()).load(thumbnail)
                        .into(view);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }