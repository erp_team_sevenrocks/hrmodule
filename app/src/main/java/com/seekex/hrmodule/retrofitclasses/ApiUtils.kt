package com.seekx.webService

import android.content.Context
import android.util.Log
import com.seekex.hrmodule.AppConstants
import com.seekex.hrmodule.Preferences
import com.seekx.webService.interfaces.ApiServices

class ApiUtils {

    companion object {
        const val APIPREFIX = "https://master.sevenrocks.in/dashboard-web-api/"
        const val API_HEADER = "Content-Type: application/json;charset=UTF-8"
        const val DOMAIN = "https://master.sevenrocks.in/"

        const val UPLOAD_REC_FILE = "https://master.sevenrocks.in/CronJobs/ajaxUploadTempFile"
        const val SAVEIMAGEFILE = "job_worker_party_photo_save"
        const val SAVE_REWARD_RECORDING = "meeting_recording_save"
        const val GETRECORDINGDETAILS = "get_grievance_recording"
        const val SAVEMARKERS = "save_grievance_recording_marker"
        const val LOGIN = "login"
        const val GETREWARDREASONLIST = "reward_point_type_get"
        const val GETSTRIKEPOINTREASONLIST = "strike_point_type_get"
        const val GETUSERSLIST = "get_users_for_reward_or_strike"
        const val GTYPE = "get_grievance_type"
        const val get_location = "get_grievance_location"
        const val GSAVE = "save_grievance"
        const val GGET = "get_grievance"
        const val GGETMYASSIGNED = "get_assignee_grievance"
        const val GSTATUS = "get_grievance_status"
        const val GDELETE = "delete_grievance"
        const val SAVEACTION = "save_grievance_action"
        const val SENDFORMARK = "send_for_marked"
        const val GACCEPT = "send_for_approval"
        const val GREJECT = "send_for_reject"
        const val GUSERS = "get_user_list"
        const val SAVEREWARD = "reward_point_save"
        const val SAVESTRIKE = "strike_point_save"
        const val GETKRASLIST = "get_user_kras"

        const val GETREWARDS = "reward_point_get"
        const val GETUSERKRAASSESSMENT = "user_kra_assessment_list"
        const val GETSTRIKE = "strike_point_get"
        const val DELETEREWARD = "reward_point_delete"
        const val DELETESTRIKE = "strike_point_delete"


        const val DEPARTMENTLIST = "strike_point_delete"
        const val TRAININGTYPELIST = "get_training_type"
        const val DESIGNATIONLIST = "strike_point_delete"
        const val SKILLSLIST = "strike_point_delete"
        const val SAVECODEDATA = "startnstop_training"


        //training
        const val SAVEFEEDBACK = "training_feedback_save"
        const val GETKRADATA = "user_kra_info"
        const val GETREASONLIST = "user_assessment_reason"
        const val GETSKILLSDATA = "user_get_kra_skills"
        const val GETREWARDNAME = "get_user_kra_reward_names"
        const val GETSTRIKENAME = "get_user_kra_strike_names"
        const val SENDRECOOMENDATION = "send_recommendation"
        const val SAVEKRA = "user_save_kra_assessment"

        //feedback
        const val FEEDBACKTYPE = "get_public_feedback_type"
        const val FEEDBACKVALUE = "get_public_feedback_value"
        const val SAVEFEEDBACKk = "save_public_feedback"

        const val PASSCODE = "training_test_passcode_auth"
        const val GETQUESTION = "training_get_question"
        const val COLLECTANSWER = "training_collect_answer"
        const val DELETEMEDIA = "delete_media_answer"

        const val GETJOBPOSTING = "get_internal_job"
        const val APPLYJOB = "apply_internal_job"
        const val FILTERDATA = "get_requisition_filter_data"


        const val GETTEAM = "get_user_team"
        const val GETDEPARTMENTSOP = "get_sop_department"
        const val SOPMASTER = "get_sop_master"
        const val SOPSUBSECTION = "get_sop_subsection"
        const val SOPWORK = "get_work_instructions"
        const val SAVESOPWORK = "save_sop_audit"
        const val SAVEAUDIT = "save_sop_work_instruction_audit"
        const val SOPLOCATION = "get_sop_location"
        const val SOPUSERS = "get_sop_users"


        const val EVENTPARAMLIST = "get_parameter_list"
        const val save_critical_event = "save_critical_event"

        const val team_event_feedback_save = "team_event_feedback_save"


        const val suggestion_type_list = "suggestion_type_list"
        const val get_location_info = "get_location_info"
        const val get_data_on_type = "get_data_on_type"
        const val save_suggestion = "save_suggestion"

        const val reminder_type_list = "reminder_type_list"
        const val get_list_on_type = "get_list_on_type"
        const val reminder_duration_list = "reminder_duration_list"
        const val save_reminder = "save_reminder"

        const val leave_apply = "leave_apply"
        const val get_my_leave_requests = "get_my_leave_requests"
        const val my_leave_delete = "my_leave_delete"
        const val leave_request_file_save = "leave_request_file_save"
        const val leave_request_leave_delete = "leave_request_leave_delete"
        const val my_salary = "my_salary"
        const val my_salary_pdf = "my_salary_pdf"
        const val my_salary_csv = "my_salary_csv"
        const val my_salary_accept_or_reject = "my_salary_accept_or_reject"
        const val my_attendance = "my_attendance"

        const val localUrl = "https://seekex.in/"


        fun getWebApi(): ApiServices {
            return RetrofitClient.getClient(APIPREFIX)!!.create(ApiServices::class.java)
        }

        fun getUploadApi(context: Context): ApiServices {
            var pref: Preferences
            pref = Preferences(context!!)
            Log.e("TAG", "getWebApi: " + pref.get(AppConstants.BASEURL)+"hr-web-api/")
            return RetrofitClient.getUploadFileClient(pref.get(AppConstants.BASEURL)+"hr-web-api/")!!
                .create(ApiServices::class.java)
        }
        @JvmStatic
        fun getWebApiNext(context: Context?): ApiServices {

            var pref: Preferences

            pref = Preferences(context!!)
            Log.e("TAG", "getWebApiNext: " + pref.get(AppConstants.BASEURL)+"hr-web-api/")

            return RetrofitClient.getClient(pref.get(AppConstants.BASEURL)+"hr-web-api/")!!
                .create(ApiServices::class.java)
        }

        @JvmStatic
        fun getWebApiNEW(context: Context?): ApiServices {

            var pref: Preferences

            pref = Preferences(context!!)
            Log.e("TAG", "getWebApi: " + pref.get(AppConstants.BASEURL)+"hr-web-api/")

            return RetrofitClient.getClient(pref.get(AppConstants.BASEURL)+"hr-web-api/")!!
                .create(ApiServices::class.java)
        }
        @JvmStatic
        fun getAnnouceApi(context: Context?): ApiServices {
            Log.e("TAG", "getapi: " + "https://stagging.sevenrocks.in/web-api/")

            return RetrofitClient.getClient("https://stagging.sevenrocks.in/web-api/")!!
                .create(ApiServices::class.java)
        }
//        fun getUploadApiNew(): ApiServices {
//
//
//            return RetrofitClient.getUploadFileClient(NEW_URL)!!.create(ApiServices::class.java)
//        }

    }

}