package com.seekx.webService.models


open class RequestBase {
    var username: String? = null
    var password: String? = null
    var service_name: String? = null
    var announcement_id: String? = null
    var secret_key: String? = null
    var app_version: String? = null
    var token: String? = null
}
