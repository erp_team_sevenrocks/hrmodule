package com.seekx.webService.interfaces


import com.seekex.hrmodule.attendence.AttendenceResponse
import com.seekex.hrmodule.criticalevent.EventSaveReq
import com.seekex.hrmodule.criticalevent.ParamlistResponse
import com.seekex.hrmodule.feedback.SaveFeedbackReq
import com.seekex.hrmodule.grievance.*
import com.seekex.hrmodule.jobposting.ApplyPostReq
import com.seekex.hrmodule.jobposting.FilterListResponse
import com.seekex.hrmodule.jobposting.PostingListReq
import com.seekex.hrmodule.jobposting.PostingListResponse
import com.seekex.hrmodule.kra.*
import com.seekex.hrmodule.leave_manager.*
import com.seekex.hrmodule.locationfeedback.*
import com.seekex.hrmodule.login.LoginResponse
import com.seekex.hrmodule.markrewarks.*
import com.seekex.hrmodule.mysalary.GetSalaryReq
import com.seekex.hrmodule.mysalary.SalaryListResponse
import com.seekex.hrmodule.remiender.GetRecordReq
import com.seekex.hrmodule.remiender.SaveDataReq
import com.seekex.hrmodule.retrofitclasses.models.AnnouncementResponse
import com.seekex.hrmodule.sop.*
import com.seekex.hrmodule.training.CodeGetResponse
import com.seekex.hrmodule.training.CodeSaveReq
import com.seekex.hrmodule.training.FeedbackSaveReq
import com.seekex.hrmodule.trainingtest.AnswerReq
import com.seekex.hrmodule.trainingtest.DeleteMediaReq
import com.seekex.hrmodule.trainingtest.PasscodeReq
import com.seekex.trainingtestapp.login.PasscodeResponse
import com.seekex.trainingtestapp.login.QuestionRes
import com.seekx.webService.ApiUtils
import com.seekx.webService.models.BaseResponse
import com.seekx.webService.models.RequestBase
import com.seekx.webService.models.apiRequest.*
import okhttp3.MultipartBody
import retrofit2.http.*
import rx.Observable


interface ApiServices {


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getQue(@Body loginReq: GetQuestionReq): Observable<QuestionRes>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveAns(@Body loginReq: AnswerReq): Observable<BaseResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun deleteMedia(@Body loginReq: DeleteMediaReq): Observable<BaseResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun login(@Body loginReq: LoginReq): Observable<LoginResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getRewardReasons(@Body `object`: RequestBase?): Observable<RewardReasonResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getParamlist(@Body `object`: RequestBase?): Observable<ParamlistResponse>?


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getRewardUsers(@Body `object`: RequestBase?): Observable<RewardUsersResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getUsersList(@Body `object`: SOpUsersReq?): Observable<RewardUsersResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getkraUsersList(@Body `object`: GetSalaryReq?): Observable<RewardUsersResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getWorkInst(@Body `object`: WorkListReq?): Observable<WorkListResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun savework(@Body `object`: SavepredatReq?): Observable<SavePrimaryResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveauditdata(@Body `object`: SaveAuditdataReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getMasterList(@Body `object`: MasterListReq?): Observable<RewardUsersResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getSubsection(@Body `object`: SubSectionListReq?): Observable<RewardUsersResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getAllRewards(@Body `object`: GetListReq?): Observable<RewardListResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getmyleave(@Body `object`: GetLEaveReq?): Observable<LeaveListResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getpdf(@Body `object`: GetSalaryReq?): Observable<PdfResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun acceptreject(@Body `object`: AcceptReq?): Observable<BaseResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getmysalary(@Body `object`: GetSalaryReq?): Observable<SalaryListResponse>?
 @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getatten(@Body `object`: GetSalaryReq?): Observable<AttendenceResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getaccouncemnt(@Body `object`: RequestBase?): Observable<AnnouncementResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun seenaccouncemnt(@Body `object`: RequestBase?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getJobPosting(@Body `object`: PostingListReq?): Observable<PostingListResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getFilterData(@Body `object`: PostingListReq?): Observable<FilterListResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getgreivancelist(@Body `object`: GetListReq?): Observable<GreivanceListResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getgreivancetypelist(@Body `object`: RequestBase?): Observable<RewardUsersResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun deletereward(@Body `object`: DeleteRewardReq?): Observable<BaseResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun manageRecord(@Body `object`: ManageGreivanceReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun applyPost(@Body `object`: ApplyPostReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveReward(@Body `object`: SaveRewardReq?): Observable<RewardSaveResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getKrasList(@Body `object`: GetKrasReq?): Observable<RewardUsersResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getdesignationList(@Body `object`: GetDesignationReq?): Observable<RewardUsersResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getSkillsList(@Body `object`: RequestBase?): Observable<RewardUsersResponse>?
    @Multipart
    @POST(ApiUtils.UPLOAD_REC_FILE)
    fun uploadSingleFile(@Part file: MultipartBody.Part?): Observable<uploadaudiores>

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveImagesdata(@Body `object`: RewardUploadmageReq?): Observable<BaseResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saverewardaudiodata(@Body `object`: RewardFinalAudioSaveReq?): Observable<BaseResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getGlist(@Body `object`: RequestBase?): Observable<RewardUsersResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveglist(@Body `object`: GreivanceSaveReq?): Observable<SaveGreivanceResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveevent(@Body `object`: EventSaveReq?): Observable<BaseResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveCode(@Body `object`: CodeSaveReq?): Observable<CodeGetResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun passcode(@Body loginReq: PasscodeReq): Observable<PasscodeResponse>
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveFeedback(@Body `object`: FeedbackSaveReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getKrasData(@Body `object`: GetKrasReq?): Observable<KraListResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getReasonList(@Body `object`: RequestBase?): Observable<RewardUsersResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getSkillsData(@Body `object`: GetSkillsDataReq?): Observable<SkillDataResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getRewardData(@Body `object`: GetSkillsDataReq?): Observable<RewardnamesResponse>?


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun sendReccomdation(@Body `object`: SendRecommdationReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveData(@Body `object`: SaveKraDetailReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getFeedbackTypelist(@Body `object`: RequestBase?): Observable<RewardUsersResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun savedata(@Body `object`: SaveDataReq?): Observable<BaseResponse>?


    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveleave(@Body `object`: SaveLeaveDataReq?): Observable<SaveLEaveResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveMedia(@Body `object`: UploadmedisReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getrecord(@Body `object`: GetRecordReq?): Observable<RewardUsersResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getlocationdetails(@Body `object`: LocationdetailReq?): Observable<LocationResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getdataontype(@Body `object`: DataonTypeReq?): Observable<LocationlistResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveFeedbacknew(@Body `object`: SaveFeedbackReq?): Observable<BaseResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun savelocfeedback(@Body `object`: SaveLocFeedbackReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun markAction(@Body `object`: MarkActionReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun sendformarked(@Body `object`: DeleteRewardReq?): Observable<BaseResponse>?

    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getAllKraList(@Body `object`: GetListReq?): Observable<AllKraListResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun saveMarkers(@Body `object`: MarkerSaveReq?): Observable<BaseResponse>?
    @POST("./")
    @Headers("Content-Type: application/json;charset=UTF-8")
    fun getAudioDetails(@Body `object`: GetAudioDetailReq?): Observable<AudioDetailResponse>?

}