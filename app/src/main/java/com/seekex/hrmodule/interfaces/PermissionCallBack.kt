package com.seekx.interfaces

interface PermissionCallBack {
    fun onPermissionAccessed()
}