package com.seekx.interfaces

interface AdapterCallBack {
    fun onSelection(any: Any?)
}